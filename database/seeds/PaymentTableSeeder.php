<?php

use Illuminate\Database\Seeder;

class PaymentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'payment_method' => 'cash', 
                'payment_description'=>'Efectivo',
                'payment_image'=>'pf-cash', 
                'payment_charge'=>0, 
                'payment_discount'=>0,
                'enable'=>true, 
                'deleted'=>false, 
                'created_at'=> now(), 
                'updated_at'=>now()
            ], 
            [
                'payment_method' => 'credit', 
                'payment_description'=>'Credito',
                'payment_image'=>'pf-credit-card', 
                'payment_charge'=>0, 
                'payment_discount'=>0,
                'enable'=>true, 
                'deleted'=>false, 
                'created_at'=> now(), 
                'updated_at'=>now()
            ],
            [
                'payment_method' => 'mercadopago', 
                'payment_description'=>'MercadoPago',
                'payment_image'=>'pf-mercado-pago', 
                'payment_charge'=>0, 
                'payment_discount'=>0,
                'enable'=>true, 
                'deleted'=>false, 
                'created_at'=> now(), 
                'updated_at'=>now()
            ],
            [
                'payment_method' => 'paypal', 
                'payment_description'=>'PayPal',
                'payment_image'=>'pf-paypal', 
                'payment_charge'=>0, 
                'payment_discount'=>0,
                'enable'=>true, 
                'deleted'=>false, 
                'created_at'=> now(), 
                'updated_at'=>now()
            ],
            [
                'payment_method' => 'cryptocurrency', 
                'payment_description'=>'Cripto Moneda',
                'payment_image'=>'pf-bitcoin-sign', 
                'payment_charge'=>0, 
                'payment_discount'=>0,
                'enable'=>true, 
                'deleted'=>false, 
                'created_at'=> now(), 
                'updated_at'=>now()
            ],
            [
                'payment_method' => 'bank_transfer', 
                'payment_description'=>'Transferencia Bancaria',
                'payment_image'=>'pf-bank-transfer', 
                'payment_charge'=>0, 
                'payment_discount'=>0,
                'enable'=>true, 
                'deleted'=>false, 
                'created_at'=> now(), 
                'updated_at'=>now()
            ],
        ];
        DB::table('payment')->insert($data);
    }
}
