<?php

use Illuminate\Database\Seeder;

class OrderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders_status')->insert([
            'status_name' => 'Pending_Payment',
            'status_description' => 'Pendiente de pago',
            "created_at"=> now(), "updated_at"=> now()
        ]);

        DB::table('orders_status')->insert([
            'status_name' => 'Confirmed',
            'status_description' => 'Pago confirmado',
            "created_at"=> now(), "updated_at"=> now()
        ]);

        DB::table('orders_status')->insert([
            'status_name' => 'Processing',
            'status_description' => 'Procesando la orden',
            "created_at"=> now(), "updated_at"=> now()
        ]);

        DB::table('orders_status')->insert([
            'status_name' => 'Out_For_Delivery',
            'status_description' => 'Procesando la entrega',
            "created_at"=> now(), "updated_at"=> now()
        ]);

        DB::table('orders_status')->insert([
            'status_name' => 'Delivered',
            'status_description' => 'Entregado al cliente',
            "created_at"=> now(), "updated_at"=> now()
        ]);

        DB::table('orders_status')->insert([
            'status_name' => 'Attempted_Delivery',
            'status_description' => 'No se encontro el cliente en la entrega',
            "created_at"=> now(), "updated_at"=> now()
        ]);

        DB::table('orders_status')->insert([
            'status_name' => 'Canceled',
            'status_description' => 'Cancelado',
            "created_at"=> now(), "updated_at"=> now()
        ]);

        DB::table('orders_status')->insert([
            'status_name' => 'Payment_error',
            'status_description' => 'Error en el pago',
            "created_at"=> now(), "updated_at"=> now()
        ]);

        DB::table('orders_status')->insert([
            'status_name' => 'Payment_rejected',
            'status_description' => 'Pago rechazado',
            "created_at"=> now(), "updated_at"=> now()
        ]);
    }
}
