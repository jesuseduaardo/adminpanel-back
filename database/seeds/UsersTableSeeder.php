<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = DB::table('users')->insertGetId([
            'name'      =>  'test',
            'email'     =>  'test@mail.com',
            'telephone' =>  '+54-12344321',
            'password'  =>  bcrypt('123'),
            'email_verified_at'=> now(),
            'active'    =>  1,
            'admin'     =>  1,
            'created_at'=>  now(),
            'updated_at'=>  now()
        ]);
        DB::table('user_info')->insert([
            'user_id'       =>  $user,
            'first_name'    =>  'Jesus Eduardo',
            'last_name'     =>  'Castillo Aguilar',
            'birthday'      =>  '1986-07-23',
            'gender'        =>  'm',
            'created_at'=>  now(),
            'updated_at'=>  now()
        ]);
        DB::table('user_info_address')->insert([
            'user_id'       =>  $user,
            'country'       =>  'Argentina',
            'state'         =>  'Buenos Aires',
            'city'          =>  'CABA',
            'zip_code'      =>  '1093',
            'created_at'    =>  now(),
            'updated_at'    =>  now()
        ]);
        $user = DB::table('users')->insertGetId([
            'name'      =>  'Prueba',
            'email'     =>  'prueba@mail.com',
            'telephone' =>  '+54-12344321',
            'password'  =>  bcrypt('123'),
            'email_verified_at'=> now(),
            'active'    =>  1,
            'admin'     =>  1,
            'created_at'=>  now(),
            'updated_at'=>  now()
        ]);
        DB::table('user_info')->insert([
            'user_id'       =>  $user,
            'first_name'    =>  'Prueba',
            'last_name'     =>  'Test',
            'birthday'      =>  '1986-07-23',
            'gender'        =>  'm',
            'created_at'=>  now(),
            'updated_at'=>  now()
        ]);
        DB::table('user_info_address')->insert([
            'user_id'       =>  $user,
            'country'       =>  'Argentina',
            'state'         =>  'Buenos Aires',
            'city'          =>  'CABA',
            'zip_code'      =>  '1093',
            'created_at'    =>  now(),
            'updated_at'    =>  now()
        ]);
    }
}
