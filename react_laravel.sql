-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-12-2019 a las 12:54:28
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `react_laravel`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `category`
--

CREATE TABLE `category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `enable` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `category`
--

INSERT INTO `category` (`id`, `name`, `category_id`, `enable`, `created_at`, `updated_at`) VALUES
(1, 'Computacion', 0, 1, '2019-09-12 04:59:10', '2019-09-12 04:59:10'),
(5, 'Laptops', 1, 1, '2019-09-22 06:09:03', '2019-09-22 06:09:03'),
(7, 'Ferreteria', 0, 1, '2019-09-22 06:24:07', '2019-09-22 23:56:51'),
(8, 'Tintes', 7, 1, '2019-09-22 06:24:33', '2019-09-22 06:24:33'),
(9, 'Prueba', 0, 1, '2019-09-22 06:24:48', '2019-09-22 06:24:48'),
(11, 'Coso', 18, 0, '2019-09-22 06:37:51', '2019-09-23 00:27:01'),
(18, 'qwes', 1, 1, '2019-09-22 18:37:41', '2019-09-22 18:37:41'),
(19, 'Pinturas', 0, 1, '2019-09-23 00:28:13', '2019-09-23 00:28:13'),
(20, 'Pinceles', 19, 1, '2019-09-23 00:28:28', '2019-09-23 00:28:28'),
(21, 'Lijas', 19, 1, '2019-09-23 00:30:00', '2019-09-23 00:30:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `frase_diaria`
--

CREATE TABLE `frase_diaria` (
  `id` int(11) NOT NULL,
  `imagen` varchar(50) NOT NULL,
  `frase` text NOT NULL,
  `autor` varchar(25) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `mes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `frase_diaria`
--

INSERT INTO `frase_diaria` (`id`, `imagen`, `frase`, `autor`, `titulo`, `mes`) VALUES
(1, 'steveJobs.jpg', 'Estoy convencido de que por lo menos la mitad de lo que separa a los emprendedores exitosos de los que no lo son es mera perseverancia', 'Steve Jobs', 'Cofundador y presidente ejecutivo de Apple Inc.', 1),
(2, 'CharlesCalebColton.jpg', 'La riqueza, después de todo, es algo relativo, ya que el que tiene poco y quiere menos es más rico que el que tiene más y quiere aún más', 'Charles Caleb Colton', 'Clérigo, Escritor y Coleccionista inglés', 1),
(3, 'CoronelSanders.jpg', 'El modo de dar una vez en el clavo, es dar cien veces en la herradura', 'Harland David Sanders', 'Fundador de KFC', 1),
(5, 'EleanorRoosevelt.jpg', 'El que pierde dinero, pierde mucho; el que pierde un amigo; pierde aún más; el que pierde fe, lo pierde todo', 'Eleanor Roosevelt', 'Escritora y Política Estadounidense', 1),
(6, 'DonaldTrump.jpg', 'Mantén una visión global mientras atiendes los detalles cotidianos', 'Donald Trump', 'Empresario y político estadounidense', 1),
(7, 'TheodoreRoosevelt.jpg', 'Haz lo que puedas, con lo que tengas, donde estés', 'Theodore Roosevelt', 'Estadista, político, conservacionista, naturalista y escritor estadounidense', 1),
(8, 'MarkCuban.jpg', 'No importa cuántas veces falles, sólo debes de estar en lo correcto una vez. Entonces todos te llamarán un éxito de la noche a la mañana y te dirán lo afortunado que eres', 'Mark Cuban', 'Empresario estadounidense e inversionista.', 1),
(9, 'AlexanderGrahamBell.jpg', 'Nunca vayas por el camino trazado, porque conduce hacia donde otros ya han estado', 'Alexander Graham Bell', 'Científico e inventor del teléfono', 1),
(10, 'RayKroc.jpg', 'Si no te gusta tomar riesgos, debes salir corriendo del negocio', 'Ray Kroc', 'Fundador de McDonald\'s', 1),
(11, 'ThomasEdison.jpg', 'El éxito es 1% inspiración y 99% transpiración', 'Thomas Edison', 'Inventor y Empresario Estadounidense', 1),
(12, 'steveJobs.jpg', 'Tu trabajo va a llenar gran parte de tu vida, la única manera de estar realmente satisfecho es hacer lo que creas es un gran trabajo y la única manera de hacerlo es amar lo que haces. \r\nSi no lo has encontrado aún, sigue buscando. Como con todo lo que tiene que ver con el corazón, sabrás cuando lo hayas encontrado', 'Steve Jobs', 'Cofundador y presidente ejecutivo de Apple Inc.', 1),
(13, 'GraceCoddington.jpg', 'Mantén siempre tus ojos abiertos, Siempre observando. Porque cualquier cosa que veas puede inspirarte', 'Grace Coddington', 'Modelo y directora creativa de la revista Vogue', 1),
(14, 'ChrisGrosser.jpg', 'Las oportunidades no ocurren, se crean', 'Chris Grosser', 'Fotógrafo ', 1),
(15, 'WarrenBuffett.jpg', 'Compra solo algo con lo que seas feliz si el mercado cierra 10 años', 'Warren Buffett', 'Inversor y Empresario Estadounidense', 1),
(16, 'MarcosGalperín.jpg', 'No dejes que los éxitos se te suban a la cabeza y los fracasos te invadan el corazón', 'Marcos Galperín', 'Fundador y CEO de MercadoLibre', 1),
(17, 'AbrahamLincoln.jpg', 'No puedes escapar de la responsabilidad de mañana evadiéndola hoy', 'Abraham Lincoln', 'Político y Abogado Estadounidense', 1),
(18, 'AlfredoCoto.jpg', 'Mi empresa tiene que ser excelencia en todo', 'Alfredo Coto', 'Dueño de la cadena de supermercados COTO', 1),
(19, 'SeanOCasey.jpg', 'El dinero no te hace feliz, pero relaja los nervios', 'Sean O’Casey', 'Dramaturgo Irlandés', 1),
(20, 'JeffBezos.jpg', 'Si no eres terco, te rendirás de tus propios experimentos antes de tiempo. Y si no eres flexible, no verás una solución distinta al problema que intentas resolver', 'Jeff Bezos', 'Fundador y Director ejecutivo de Amazon', 1),
(21, 'PhilLibin.jpg', 'Hay muchas malas razones para empezar una empresa. Pero sólo hay una buena razón y creo que sabes cuál es: para cambiar el mundo', 'Phil Libin', 'CEO de Evernote', 1),
(22, 'LarryPage.jpg', 'Siempre da más de que lo esperan de ti', 'Larry Page', 'Empresario estadounidense, creador de Google', 1),
(23, 'MarkZuckerberg.jpg', 'Muévete rápido y rompe objetos. Si no estás rompiendo cosas, no te estás moviendo lo suficientemente rápido', 'Mark Zuckerberg', 'Programador y Empresario Estadounidense, Creador de Facebook', 1),
(24, 'EleanorRoosevelt.jpg', 'El futuro pertenece a quienes creen en la belleza de sus sueños', 'Eleanor Roosevelt', 'Escritora y Política Estadounidense', 1),
(25, 'MartinLutherKing.jpg', 'Da tu primer paso con fe, no es necesario que veas toda la escalera completa, sólo da tu primer paso', 'Martin Luther King', 'Ministro Bautista y Activista por los Derechos Civiles', 1),
(26, 'WinstonChurchill.jpg', 'Un optimista ve una oportunidad en toda calamidad, un pesimista ve una calamidad en toda oportunidad', 'Winston Churchill', 'Político, Estadista, Historiador y Escritor británico', 1),
(27, 'Confucio.jpg', 'Nuestra gloria más grande no consiste en no haberse caído nunca, sino en haberse levantado después de cada caída', 'Confucio', 'Filósofo y Político Chino', 1),
(28, 'WarrenBuffett.jpg', 'La cosa más importante que debes hacer si estás dentro de un hoyo es dejar de cavar', 'Warren Buffett', 'Inversor y Empresario Estadounidens', 1),
(29, 'GaryVaynerchuk.jpg', 'Odio cómo piensa la gente con el “vaso medio vacío” cuando en realidad su vaso está casi lleno. Estoy agradecido cuando tengo una gota más en el vaso porque sé exactamente qué hacer con ella', 'Gary Vaynerchuk', 'Empresario Estadounidense', 1),
(30, 'TheodoreRoosevelt.jpg', 'Es duro fracasar, pero es todavía peor no haber intentado nunca triunfar', 'Theodore Roosevelt', 'Estadista, político, conservacionista, naturalista y escritor estadounidense', 1),
(31, 'MarkZuckerberg.jpg', 'La gente puede ser muy inteligente o tener habilidades que son aplicables, pero si no creen en ello, entonces no van a trabajar realmente duro', 'Mark Zuckerberg', 'Programador y Empresario Estadounidense, Creador de Facebook', 1),
(32, 'ThomasEdison.jpg', 'Nuestra mayor debilidad es rendirse, la única manera de tener éxito es intentarlo siempre una vez más', 'Thomas Edison', 'Inventor y Empresario Estadounidense', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `entity_id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `extension` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `images`
--

INSERT INTO `images` (`id`, `entity_id`, `name`, `extension`, `size`, `created_at`) VALUES
(32, 1, 'ProPrue2_rollingstones', 'jpg', 4848, '2019-09-05 03:11:23'),
(33, 1, 'ProPrue2_shellIsland', 'jpg', 7646, '2019-09-05 03:11:23'),
(35, 1, 'ProPrue2_blown-unique-tee-shirts-hombre-hot-selling', 'jpg', 37414, '2019-09-05 03:26:52'),
(36, 1, 'ProPrue2_59839518_l', 'jpg', 38750, '2019-09-05 03:26:52'),
(37, 2, 'prueba01_59839518_l', 'jpg', 38750, '2019-09-23 02:26:28'),
(38, 2, 'prueba01_army', 'jpg', 3942, '2019-09-23 02:26:29'),
(39, 2, 'prueba01_rollingstones', 'jpg', 4848, '2019-09-23 02:26:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(11, '2014_10_12_000000_create_users_table', 1),
(12, '2014_10_12_100000_create_password_resets_table', 1),
(13, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(14, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(15, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(16, '2016_06_01_000004_create_oauth_clients_table', 1),
(17, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(18, '2019_04_19_013039_alter_create_password_resets_table', 1),
(19, '2019_04_19_032459_create_entity_table', 1),
(20, '2019_04_19_032644_create_entity_attribute_table', 1),
(21, '2019_04_19_040616_create_categories_values_table', 1),
(22, '2019_04_19_040655_create_categories_entity_table', 1),
(23, '2019_04_19_040725_create_categories_attributes_table', 1),
(24, '2019_04_21_044248_create_entity_value_table', 1),
(25, '2019_09_12_012426_create_category_table', 2),
(26, '2019_09_29_150304_create_ref_payment_methods_table', 3),
(30, '2019_09_29_191118_create_orders_items_table', 4),
(32, '2019_09_29_211904_create_orders_number_table', 6),
(34, '2019_09_29_200037_create_orders_status_table', 8),
(39, '2019_10_06_171846_create_user_info_table', 9),
(40, '2019_10_06_172021_create_user_info_address_table', 9),
(42, '2019_10_06_225105_create_order_status_history_table', 10),
(43, '2019_09_29_185623_create_orders_table', 11),
(48, '2019_10_12_021720_create_shipping_table', 13),
(49, '2019_10_12_021845_create_payment_table', 13),
(52, '2019_10_12_024005_create_orders_payment_table', 15),
(53, '2019_10_12_023944_create_orders_shipping_table', 16);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('000ecf540f9c66b9588f639ddfafad26abd3d10e2cc6d032e75d82f07d4b26c97d3fdd99b610eb58', 1, 1, 'MyApp', '[]', 0, '2019-09-14 01:09:29', '2019-09-14 01:09:29', '2020-09-13 22:09:29'),
('00c49dd5f25e74fe79d1db8a56e7b654286c971004a09717d4c1efdf7e1ef4c64d56e9a378182350', 1, 1, 'MyApp', '[]', 0, '2019-08-31 05:17:47', '2019-08-31 05:17:47', '2020-08-31 02:17:47'),
('00e94af2e2e26de40893e82977fc9c57ffc5617a91cdd0ba519fcc9c77a310ad48e458f80d26dde0', 1, 1, 'MyApp', '[]', 1, '2019-12-24 02:54:44', '2019-12-24 02:54:44', '2020-12-23 23:54:44'),
('0478893e74195b623983ad5ae7edf30dcbcf60532cc31dbe052e9c25b049d5f498d8cb17c5ed080d', 1, 1, 'MyApp', '[]', 0, '2019-08-03 04:08:13', '2019-08-03 04:08:13', '2020-08-03 01:08:13'),
('06688260e7a23d339ed9b80216e26f37cc93040452628f13c9c0ff40eff953ea46de959afd79e603', 1, 1, 'MyApp', '[]', 0, '2019-07-18 06:14:36', '2019-07-18 06:14:36', '2020-07-18 03:14:36'),
('0a633e64f8452036843df8ec506249c38e32399af2d8233d942e9834682b2cfea0c90ede64182743', 1, 1, 'MyApp', '[]', 0, '2019-07-18 06:23:24', '2019-07-18 06:23:24', '2020-07-18 03:23:24'),
('0af8375c3622155c6bcb9f0bd08a450dc02bed6ef497fb1c5ff4705969937ca40d6e9f9943dfbf4c', 1, 1, 'MyApp', '[]', 0, '2019-07-21 17:15:51', '2019-07-21 17:15:51', '2020-07-21 14:15:51'),
('0b44b6b5401473701eb03a0b56d9c4315c5b4475b5420687d59c3f8ebef33904b24b0c0e7f5a6c58', 1, 1, 'MyApp', '[]', 0, '2019-10-17 02:48:26', '2019-10-17 02:48:26', '2020-10-16 23:48:26'),
('0b5e1247d143aa5ad972c5f6fcc05ed90e0f04059dc3b110163d311f541e657e2f6c54913c468e40', 1, 1, 'MyApp', '[]', 0, '2019-11-23 01:55:37', '2019-11-23 01:55:37', '2020-11-22 22:55:37'),
('0c3fbb96c1549cdd8b1f58fbc4fcacf77efeea47a200ca9e02a2edc3bba814ae1df7480e790b94cb', 1, 1, 'MyApp', '[]', 1, '2019-11-23 01:14:03', '2019-11-23 01:14:03', '2020-11-22 22:14:03'),
('0cc3c5bfbfe75ba32cb9b80aa05e55fd57f68c33d39d9267314bb225c6f01620fa90cbb4507b0011', 1, 1, 'MyApp', '[]', 0, '2019-07-19 04:04:06', '2019-07-19 04:04:06', '2020-07-19 01:04:06'),
('0d76a9b310ddc23a4c44c7c622867af3599801d66a47048e257b588948fd7663486b8ed22c9781d5', 1, 1, 'MyApp', '[]', 0, '2019-07-19 04:13:25', '2019-07-19 04:13:25', '2020-07-19 01:13:25'),
('0e1f7f68d324ed624b4f6f29a90553f2d95f7378d145799cdd3ebeb970fc4865ec430ca9a772267c', 1, 1, 'MyApp', '[]', 0, '2019-07-20 02:54:21', '2019-07-20 02:54:21', '2020-07-19 23:54:21'),
('0f025d042dd4b2864961930c158a438ce78a475bf15d0c3a82b29037aa67432234fd06aca3452571', 1, 1, 'MyApp', '[]', 1, '2019-08-22 04:00:28', '2019-08-22 04:00:28', '2020-08-22 01:00:28'),
('0fbde2dbbf9335cd06b0012df3d0d20afe3c36e60bb30ee55fed64d8c733b76e4c573c2198d70995', 1, 1, 'MyApp', '[]', 0, '2019-09-22 05:20:55', '2019-09-22 05:20:55', '2020-09-22 02:20:55'),
('1117fc4ff0368ae6c58d710464039b03a8b33dd6407b8199ce985f4b5b4a1ead39b212ddae494e9b', 1, 1, 'MyApp', '[]', 1, '2019-07-26 03:49:12', '2019-07-26 03:49:12', '2020-07-26 00:49:12'),
('121c754cc0fdacae4c01227e395240935ea0f0b92637db0502d62836130392671c40dae4294367ca', 1, 1, 'MyApp', '[]', 0, '2019-07-20 05:03:24', '2019-07-20 05:03:24', '2020-07-20 02:03:24'),
('145db8a8f938eb0b142e0be79ecf6044312d1660c8d589387849c9297ff78e935920f2786abcea90', 1, 1, 'MyApp', '[]', 0, '2019-07-30 03:35:11', '2019-07-30 03:35:11', '2020-07-30 00:35:11'),
('166f6a593f1095f82d0c9a017aab1f6826d2cab050e889395f449260ff8eb0bab9b46f33bbcb5fb0', 1, 1, 'MyApp', '[]', 1, '2019-07-26 03:44:09', '2019-07-26 03:44:09', '2020-07-26 00:44:09'),
('16d9938dcb6d3e258d67dd4a451bba573c0a24006f961ac8a145756d5c2d092b80cd0c07fb0cfd61', 1, 1, 'MyApp', '[]', 0, '2019-10-22 03:26:10', '2019-10-22 03:26:10', '2020-10-22 00:26:10'),
('18112d5a597e522ca62200d72b0b31e8b61fb9175f630267b7533c50a7de4e111d14f7d390e4480a', 1, 1, 'MyApp', '[]', 0, '2019-10-02 04:39:11', '2019-10-02 04:39:11', '2020-10-02 01:39:11'),
('18a6ed9422a19185d71e78d1941437b58bc233b13ce859a923b83b8c7d520c3e53d7621dd09b6346', 1, 1, 'MyApp', '[]', 0, '2019-07-21 17:27:29', '2019-07-21 17:27:29', '2020-07-21 14:27:29'),
('1935947cb8927f9fdbc2c2ed79888f15a6d0bce0099ecd3eba34b0babc95226c7af59a59b7df9b33', 1, 1, 'MyApp', '[]', 1, '2019-07-25 03:42:26', '2019-07-25 03:42:26', '2020-07-25 00:42:26'),
('1c8092d6fc170396203fb2d4222c215584792a2e4a7cfee174720ff0a31a09bb5ef8c0cba0222a95', 1, 1, 'MyApp', '[]', 1, '2019-11-15 03:30:31', '2019-11-15 03:30:31', '2020-11-15 00:30:31'),
('1d0c8fa51e9c5e895b6b8b960d9e1b62586555c74abef60ad91e453228cbae4b3badb84ab67f03b4', 1, 1, 'MyApp', '[]', 0, '2019-10-05 03:49:17', '2019-10-05 03:49:17', '2020-10-05 00:49:17'),
('1e347db0328cd024c90c47440d188339d955d1a10c11c5e7331a76695fa388f13beb094ece762efd', 1, 1, 'MyApp', '[]', 0, '2019-11-29 02:29:27', '2019-11-29 02:29:27', '2020-11-28 23:29:27'),
('205877d2a1acac0cfed084d7b0dd90ee678a5cebb4430406a3f1ff5f45adb9c81d206c9f8156581d', 1, 1, 'MyApp', '[]', 0, '2019-08-28 03:43:34', '2019-08-28 03:43:34', '2020-08-28 00:43:34'),
('20b124437f5459dab427666464b3443be27d459ac1a8d4336e9fecc5580f40d2f8781ea74225f58e', 1, 1, 'MyApp', '[]', 0, '2019-10-09 03:40:22', '2019-10-09 03:40:22', '2020-10-09 00:40:22'),
('24b3f22188b22feb44fe3f2fd42644aebb73f63db9dd24f8dd24ef38a885f8d490ada639c69a6d72', 1, 1, 'MyApp', '[]', 1, '2019-07-25 03:11:06', '2019-07-25 03:11:06', '2020-07-25 00:11:06'),
('24f4eb1fee2768e7063c3365d5609a704cc48a4ff455558b65b94a4f1fd849cfdec0e429c37d8143', 1, 1, 'MyApp', '[]', 1, '2019-07-21 17:51:36', '2019-07-21 17:51:36', '2020-07-21 14:51:36'),
('25eb3cd0752a52ce903462c64a2bed662d42c736dd7cd63010d31f24adac0980ed504c0570f2f9eb', 1, 1, 'MyApp', '[]', 0, '2019-11-09 15:26:02', '2019-11-09 15:26:02', '2020-11-09 12:26:02'),
('266d5ccfc69b87cadbafae6d87d295344710d4cd2b61692a79cffe11db5f613b8f4cdb5537449d8a', 1, 1, 'MyApp', '[]', 1, '2019-07-25 04:15:46', '2019-07-25 04:15:46', '2020-07-25 01:15:46'),
('271439f45546532daafa5a269509a006a595e10809bda01ff8430e5292fe8ef952d8be2ace64e2fb', 1, 1, 'MyApp', '[]', 0, '2019-07-24 14:08:43', '2019-07-24 14:08:43', '2020-07-24 11:08:43'),
('275ec4b849b2c99f705534a5d5d3a0e2c3aea6217f3af16562c3f6df1f4018c108bab78e0787954a', 1, 1, 'MyApp', '[]', 0, '2019-09-12 21:37:42', '2019-09-12 21:37:42', '2020-09-12 18:37:42'),
('2815f4d03b4a94895fa427ddd5439b876c9302311a321495d16840eb5a8b09fb9f657b4181380d2d', 1, 1, 'MyApp', '[]', 0, '2019-10-06 17:09:46', '2019-10-06 17:09:46', '2020-10-06 14:09:46'),
('2872bb7e2ab634ea994c078f88bd5ccf7c5ef0501932079e7d8878157f99f4e6de460a375749a75b', 1, 1, 'MyApp', '[]', 0, '2019-07-21 17:27:26', '2019-07-21 17:27:26', '2020-07-21 14:27:26'),
('287af48ba484417d8282452e2a84faababd3892a611840d52358d0fe22024eef8ee68d0b5bcc09b5', 1, 1, 'MyApp', '[]', 0, '2019-09-21 21:51:59', '2019-09-21 21:51:59', '2020-09-21 18:51:59'),
('2a107a00c505168928319170d345c97bee3fa15d3efbabb39c4de5dcde78d04f50158fe9a221af01', 1, 1, 'MyApp', '[]', 1, '2019-07-24 03:21:36', '2019-07-24 03:21:36', '2020-07-24 00:21:36'),
('2b88b26a44417925ca56104c8600686ad37182b21e0ea0d4a31a41d022002b2b9932b778725b2f54', 1, 1, 'MyApp', '[]', 0, '2019-07-18 06:01:46', '2019-07-18 06:01:46', '2020-07-18 03:01:46'),
('2e436d6ef5274383cf55d7ddc19fd1745b3a25b9146e18c274ac613de078e796ceada9f335c466a5', 1, 1, 'MyApp', '[]', 0, '2019-07-31 02:44:19', '2019-07-31 02:44:19', '2020-07-30 23:44:19'),
('317a08e6578814b9758b02e913b64a08a92acad8fa4bef666d4ae3b55ea8ed3f0a50f0a6478fe381', 1, 1, 'MyApp', '[]', 0, '2019-07-18 06:21:13', '2019-07-18 06:21:13', '2020-07-18 03:21:13'),
('32d8e14f4e87db5c98cbb77403bde7e0b38907cfe6238a6f1fd484f1e7eab935269220c6a829d55c', 1, 1, 'MyApp', '[]', 0, '2019-10-25 23:25:02', '2019-10-25 23:25:02', '2020-10-25 20:25:02'),
('34c9111da5d90a41152a7481afa818d09533f68eff821b7cffd43031a8cb85d066ecfb3edd89db74', 1, 1, 'MyApp', '[]', 1, '2019-07-31 03:23:32', '2019-07-31 03:23:32', '2020-07-31 00:23:32'),
('3770f43d9e7b572a749859b119d7683e589a0012debb003233e400d65ff48ed00d52fb10c341698a', 1, 1, 'MyApp', '[]', 0, '2019-11-16 22:09:39', '2019-11-16 22:09:39', '2020-11-16 19:09:39'),
('386b61d0ccb79097f164e51ec2ea1e5ea22ae6e3c0b1452b9c11d95e3794abbea21b6e66c24bc87f', 1, 1, 'MyApp', '[]', 0, '2019-11-08 03:37:19', '2019-11-08 03:37:19', '2020-11-08 00:37:19'),
('3bacd4aa3f17781decd17c6370804beee704d080c80165f36c0f20c8d558cd05e350e14efb392b35', 1, 1, 'MyApp', '[]', 0, '2019-09-12 05:20:33', '2019-09-12 05:20:33', '2020-09-12 02:20:33'),
('3e1d59e7943f55cd06f1271d8b50b6b1add03dd4f9cbf6e6210338e1f2850156e5cf92fecced7734', 1, 1, 'MyApp', '[]', 0, '2019-09-15 20:12:25', '2019-09-15 20:12:25', '2020-09-15 17:12:25'),
('3fc46733ba373666bb131e449e20d80e6581e3f1b9abc14197a6d30e7538ba524c240e0c96fdd531', 1, 1, 'MyApp', '[]', 0, '2019-10-01 04:26:56', '2019-10-01 04:26:56', '2020-10-01 01:26:56'),
('406f90f0383ce48419f12edc0732d20244414dade9a8d7d41c37e1a7f8e5761cfc56201a051ae0e9', 1, 1, 'MyApp', '[]', 0, '2019-07-19 04:31:36', '2019-07-19 04:31:36', '2020-07-19 01:31:36'),
('40ca88e3f6b8054201b6a62d690c50562e7db33cc3ce055f7051d2c3e4bf34de0c1e5ade660467eb', 1, 1, 'MyApp', '[]', 1, '2019-08-23 03:11:47', '2019-08-23 03:11:47', '2020-08-23 00:11:47'),
('420653576fd7ca9ed31a79128e7b57da2469ac88f0c39680a401341d71632673bec28b366a5d58b4', 1, 1, 'MyApp', '[]', 1, '2019-07-24 13:32:53', '2019-07-24 13:32:53', '2020-07-24 10:32:53'),
('4241390621b17f4bb5ac919059f31c43068571cd5886739dcb17c5967f09e41405bf95443a78fc10', 1, 1, 'MyApp', '[]', 1, '2019-09-12 03:36:48', '2019-09-12 03:36:48', '2020-09-12 00:36:48'),
('42d2bfebd5b629436462fd75d98e3498d0f6fa5036cc2b6844e6b2ef94389e207e56488fc889e8a3', 1, 1, 'MyApp', '[]', 0, '2019-07-27 03:03:07', '2019-07-27 03:03:07', '2020-07-27 00:03:07'),
('4497eb641e9a59703b2bef1e0f7927bd55c5314296391611cb211b9bd6221167c955fd775361ee27', 1, 1, 'MyApp', '[]', 1, '2019-07-21 18:24:26', '2019-07-21 18:24:26', '2020-07-21 15:24:26'),
('453da82ce746b277d21a4184ad435a7664d197689473047de4dfb0ff4b1987ef7fae873f0b17cfa0', 1, 1, 'MyApp', '[]', 0, '2019-11-01 04:44:01', '2019-11-01 04:44:01', '2020-11-01 01:44:01'),
('45a23da7d1d91ef8eed8c3dd79ad4ae03c015e74a6b4c328038a40a4100dea15ed28686802708206', 1, 1, 'MyApp', '[]', 0, '2019-10-26 22:34:02', '2019-10-26 22:34:02', '2020-10-26 19:34:02'),
('45c292c9b5014a55fc804332d542323453c48aa1f013eb251b153fb941294c6b0a5234ed5d765816', 1, 1, 'MyApp', '[]', 0, '2019-09-14 16:37:10', '2019-09-14 16:37:10', '2020-09-14 13:37:10'),
('469f1b08507e02cd1c27b559233d0a60f6277cdef2b14a81817857dc579097c630cd668ee34cbbad', 1, 1, 'MyApp', '[]', 0, '2019-07-18 06:08:38', '2019-07-18 06:08:38', '2020-07-18 03:08:38'),
('49b1c77543bbfb79487634e0e9c3134d61590b3e5c5b3a49bacfba33c71fcabfa23da53d4744f77b', 1, 1, 'MyApp', '[]', 0, '2019-09-20 01:44:02', '2019-09-20 01:44:02', '2020-09-19 22:44:02'),
('4a84427433ffa23fafa00f36da4be8e35bb64de16d14d56bacbb61c3b1669a8f195207933625d54d', 1, 1, 'MyApp', '[]', 1, '2019-08-23 03:27:45', '2019-08-23 03:27:45', '2020-08-23 00:27:45'),
('4c0b1ce12d9188eaaa93f6f44568218eae5f110dfca645ffe05217ea94b6eb684ad98ff3b144f527', 1, 1, 'MyApp', '[]', 1, '2019-10-24 03:54:41', '2019-10-24 03:54:41', '2020-10-24 00:54:41'),
('4d8c080e3fb948a666c2279cd2e661f271dc8581c74530aac805066338e675bdfd189e7398b34373', 1, 1, 'MyApp', '[]', 0, '2019-09-08 14:50:53', '2019-09-08 14:50:53', '2020-09-08 11:50:53'),
('4e5241c2ce077597c0807dc31f449eb8d544ba155c62c4f399ab9c304ef883bc0ef02a8bc2a6915a', 1, 1, 'MyApp', '[]', 0, '2019-09-30 00:09:02', '2019-09-30 00:09:02', '2020-09-29 21:09:02'),
('4eb1bea4236c2cf8d26946d2763326d05c40bb8ea8c1981314ab802ede753169ec83b5fc8e661bf8', 1, 1, 'MyApp', '[]', 0, '2019-07-18 06:21:42', '2019-07-18 06:21:42', '2020-07-18 03:21:42'),
('4eeeddc2adde15193088200ba1f2f56464c4e9f1d5f4171f8520ccce818e540f7f1f6515f8e40a9c', 1, 1, 'MyApp', '[]', 1, '2019-07-21 19:34:56', '2019-07-21 19:34:56', '2020-07-21 16:34:56'),
('4f0940dd99ba2b2e0f2818962e72f2499b1ee79e43762a4f00814f47356692009b7487cf3a15373c', 1, 1, 'MyApp', '[]', 0, '2019-08-19 05:05:11', '2019-08-19 05:05:11', '2020-08-19 02:05:11'),
('4fcf098f3a8f77366645a489bb9c884f0a89dbc5815f03bab08cbb4a9165f6c74992decbae88cb5e', 1, 1, 'MyApp', '[]', 1, '2019-08-23 02:37:52', '2019-08-23 02:37:52', '2020-08-22 23:37:52'),
('50080ff57b93743979a0326c12659a71e78b11692117dcf9e5e5d28028f2c9c2340657961b86e72f', 1, 1, 'MyApp', '[]', 1, '2019-07-25 02:51:26', '2019-07-25 02:51:26', '2020-07-24 23:51:26'),
('508890e551082cd65055d65ed3c040312fb25726660e51073e284779bf0b2bb134abaaa0dff7f77f', 1, 1, 'MyApp', '[]', 0, '2019-11-09 22:06:43', '2019-11-09 22:06:43', '2020-11-09 19:06:43'),
('50c618288dc56fa7f6cafd1276fa66010303d17e23b0ef0ab68d3c9c44bb52f8a22c6e2f9a337b51', 1, 1, 'MyApp', '[]', 1, '2019-07-21 02:49:25', '2019-07-21 02:49:25', '2020-07-20 23:49:25'),
('52b704d448cc3c93cdb981b226cc2e9fd54015ae95dc6336ee661b88030575527a6ac046dc2ca5aa', 1, 1, 'MyApp', '[]', 0, '2019-09-21 19:07:41', '2019-09-21 19:07:41', '2020-09-21 16:07:41'),
('52e494220b25a5bff05d2fd3114edc0aa3f61f9b26ae9d1375e3d19ad3e6fd04873b29259b69c2ca', 1, 1, 'MyApp', '[]', 0, '2019-07-24 13:39:22', '2019-07-24 13:39:22', '2020-07-24 10:39:22'),
('5344251aaa7c82c9210a5df3a378d259ed481940c4cf9b2c97b77063627a91a6f891e1d6f2c1a8fe', 1, 1, 'MyApp', '[]', 1, '2019-08-23 02:58:21', '2019-08-23 02:58:21', '2020-08-22 23:58:21'),
('5378b0fb916bd0f5dafd73585e8ab57fe3c317dc5267cb9d6294fed9e1a504894815690b71768ffc', 1, 1, 'MyApp', '[]', 0, '2019-11-13 01:32:16', '2019-11-13 01:32:16', '2020-11-12 22:32:16'),
('54e7efc851bbb57015811faad70aa2a08a722e037079b82bf52fcea0deaa52d22bf1ec43ee7f0902', 1, 1, 'MyApp', '[]', 0, '2019-09-06 03:29:31', '2019-09-06 03:29:31', '2020-09-06 00:29:31'),
('5ad2331f7e4d0a99d40d1d0efc65cb10fda96bce4b10fb5e5700fba478d405fc11a694b209b34de0', 1, 1, 'MyApp', '[]', 0, '2019-07-20 05:04:58', '2019-07-20 05:04:58', '2020-07-20 02:04:58'),
('5b5c3a85ba1fce861b971dc649ffc8c0cdc11ec26ccf8f4179b4f79a95b781c6b6d844485cfb0bcb', 1, 1, 'MyApp', '[]', 0, '2019-10-03 03:25:15', '2019-10-03 03:25:15', '2020-10-03 00:25:15'),
('5bd02247aef3922bcd6ecd6d89795cf50770a31bccea086cdfd7187b661f144dc090935fc78b754b', 1, 1, 'MyApp', '[]', 1, '2019-07-21 20:04:29', '2019-07-21 20:04:29', '2020-07-21 17:04:29'),
('5ea812ecfb060a4e8bccdd9596b2eaa01670928d7a11c9085bbe9c5eedfa4e405df36080dbd1ef1f', 1, 1, 'MyApp', '[]', 0, '2019-11-01 02:40:14', '2019-11-01 02:40:14', '2020-10-31 23:40:14'),
('5ed322f51d6b76dd328e8234a66a8578ed3fe59640340ef0437fc86c91c4a48a5e1069dbdf0b5956', 1, 1, 'MyApp', '[]', 0, '2019-07-24 14:13:50', '2019-07-24 14:13:50', '2020-07-24 11:13:50'),
('60463318a83d93db275e4861f37f7bc5e65fc753e5ef19302e814a52f30db3e25eae030a33a1c6f2', 1, 1, 'MyApp', '[]', 0, '2019-08-11 02:29:35', '2019-08-11 02:29:35', '2020-08-10 23:29:35'),
('606519733443319d95d6b5869a3d6def127303b9294b4503040d0b488041b4b620a9ea7ad6bbcd75', 1, 1, 'MyApp', '[]', 0, '2019-07-18 06:05:57', '2019-07-18 06:05:57', '2020-07-18 03:05:57'),
('6174a927a1e4c8e1b2c23920b09fa514248663763634b58b77f016a0eee038944ac3bd00efacad3f', 1, 1, 'MyApp', '[]', 0, '2019-10-13 15:57:48', '2019-10-13 15:57:48', '2020-10-13 12:57:48'),
('622d11a88c6eb28d337cc5c83b964f0b57ec8e7a1ce568d7e1b5b389266b7a323b46f81213fe4afa', 1, 1, 'MyApp', '[]', 0, '2019-07-19 04:23:58', '2019-07-19 04:23:58', '2020-07-19 01:23:58'),
('665fe6f463d34807fe54ca686562bd8942d9578edb6853498cc2ec80149362e501540435fc7f275f', 1, 1, 'MyApp', '[]', 1, '2019-08-22 03:45:29', '2019-08-22 03:45:29', '2020-08-22 00:45:29'),
('6765ca7bea6eaddc8c96acd0b523b7dbb37338878f5bed012bc751e171a7e188d1c00b8933de6155', 1, 1, 'MyApp', '[]', 0, '2019-07-25 04:18:00', '2019-07-25 04:18:00', '2020-07-25 01:18:00'),
('69116bd092b241e08d6155caad449a8ebaeec9e199f660a5d2c47909c78dfd83463f78796b3344be', 1, 1, 'MyApp', '[]', 1, '2019-07-25 03:57:14', '2019-07-25 03:57:14', '2020-07-25 00:57:14'),
('696abaa7ccc7b0a047e23c180435b79a63349faaf0f40de97d99bc13c8f06ac6ac02919d60b46edb', 1, 1, 'MyApp', '[]', 0, '2019-11-22 12:01:19', '2019-11-22 12:01:19', '2020-11-22 09:01:19'),
('6a3074a6f91184cff348077c977a66777eba7deaac71e5f14a2c6c215332c617019be76895050c58', 1, 1, 'MyApp', '[]', 0, '2019-07-18 06:24:15', '2019-07-18 06:24:15', '2020-07-18 03:24:15'),
('6a6562e997000be0993cadae8b6fd584963d3495770110433229ee7fccdf55289b185386031da4a6', 1, 1, 'MyApp', '[]', 0, '2019-08-09 03:23:35', '2019-08-09 03:23:35', '2020-08-09 00:23:35'),
('6b70da9af6110a277c1e5393eee91cee76d85899fcf0fc447f635a4e33031175970c5d91738583be', 1, 1, 'MyApp', '[]', 0, '2019-11-09 22:01:29', '2019-11-09 22:01:29', '2020-11-09 19:01:29'),
('6c798c1fee83795b3ca8c07cfacc49b4c5bf38f1bf01801ce7a49c992588556f8aa981956191a55c', 1, 1, 'MyApp', '[]', 0, '2019-07-28 17:41:08', '2019-07-28 17:41:08', '2020-07-28 14:41:08'),
('6ec6cf546ca18dc13fe07fc135916a2e973a8e3b2fbf13f5da4d91adbed90fa792f0ef1b5b31fb75', 1, 1, 'MyApp', '[]', 0, '2019-07-24 13:28:43', '2019-07-24 13:28:43', '2020-07-24 10:28:43'),
('6fcd093760b52616bb5861277ef92e824380ced2ac1ccb06c1bac9570ed0b3555b3c1400fbf2235f', 1, 1, 'MyApp', '[]', 0, '2019-10-12 04:56:17', '2019-10-12 04:56:17', '2020-10-12 01:56:17'),
('71a63b0e76d9e5672bad04fbbef008bddcb6c615ce99e1c3b5888f87bff510ef488462861317c39e', 1, 1, 'MyApp', '[]', 0, '2019-11-18 19:51:01', '2019-11-18 19:51:01', '2020-11-18 16:51:01'),
('7208c6a5eaafe87bcfb7aab73833c94e7383abc8886eba8dd95f388bcfcf5ffeeb40d89269b4a1f2', 1, 1, 'MyApp', '[]', 1, '2019-07-25 02:55:08', '2019-07-25 02:55:08', '2020-07-24 23:55:08'),
('725d50e8c50ace479035490387d067ed330dd99bdfc6577d9a0fb35cacc321fa8eb080c7a79d8872', 1, 1, 'MyApp', '[]', 1, '2019-08-22 03:52:52', '2019-08-22 03:52:52', '2020-08-22 00:52:52'),
('74a3ea1a1f333ed49bfe239af1390e209330b3b9bb6ac4a44fc2d003f592c47b313c834db96e76df', 1, 1, 'MyApp', '[]', 1, '2019-07-25 03:51:37', '2019-07-25 03:51:37', '2020-07-25 00:51:37'),
('7599a19fc6bfaf6d8213221eab53325d1d4348d5c8e13ad3c5c525e871518e3ce65d8e492df059a0', 1, 1, 'MyApp', '[]', 0, '2019-07-31 03:21:21', '2019-07-31 03:21:21', '2020-07-31 00:21:21'),
('75f84026768ffb78b507d8456b7548b989341c48e9f7d751cd2022f85bad62387a78c7633608e47c', 1, 1, 'MyApp', '[]', 0, '2019-07-18 05:29:42', '2019-07-18 05:29:42', '2020-07-18 02:29:42'),
('7668e7101e11cf61a671cd59ed3379c58c99f526597866ab56aa5d98a127686f9e712f27249b555d', 1, 1, 'MyApp', '[]', 1, '2019-07-25 03:04:08', '2019-07-25 03:04:08', '2020-07-25 00:04:08'),
('76ae8e05114931cb1ceed3ae81e8c623327a0d48da8da76e905932c79589c3fb81201b013b183e3c', 1, 1, 'MyApp', '[]', 0, '2019-11-12 02:58:54', '2019-11-12 02:58:54', '2020-11-11 23:58:54'),
('7732d8f0dcab1bc153315b10441027d4fa8607e1828f6809c85e02f3826a2635b5bcfcb81bc43f1d', 1, 1, 'MyApp', '[]', 0, '2019-11-16 01:59:54', '2019-11-16 01:59:54', '2020-11-15 22:59:54'),
('778028e19599a37b44c538b23df109972ee1926a57fde86131ad7377877077e09666a3a1d291d1ec', 1, 1, 'MyApp', '[]', 1, '2019-08-23 02:59:55', '2019-08-23 02:59:55', '2020-08-22 23:59:55'),
('77ee3f213fc7386f4b031e2b6d3bd092d89520ae7899cafd6f79fb34569b236fca41ae40d6dd6b73', 1, 1, 'MyApp', '[]', 0, '2019-10-04 01:56:35', '2019-10-04 01:56:35', '2020-10-03 22:56:35'),
('7962d11d37ed55a314f59de355c8dd5c652e664d863a65b1eed955d2bd53aa607b7d3c6fb1f52e25', 1, 1, 'MyApp', '[]', 0, '2019-08-23 03:16:27', '2019-08-23 03:16:27', '2020-08-23 00:16:27'),
('7986aa43cd4c1bb8dc66ada758ac763c216c1c023ba5cf016013594e78418563daba9abd4c181add', 1, 1, 'MyApp', '[]', 0, '2019-11-10 20:09:03', '2019-11-10 20:09:03', '2020-11-10 17:09:03'),
('7a920bca18b9aa2517e96475e66fe43c0e216a9d890b4876e93d850e18f9e0d806d8bc423596ec64', 1, 1, 'MyApp', '[]', 1, '2019-08-28 03:42:37', '2019-08-28 03:42:37', '2020-08-28 00:42:37'),
('7acf26aeb90e3defe15f4fa95f21bd036df326235c410aab351fabd18579fdd4da4d9c9b69b187b5', 1, 1, 'MyApp', '[]', 0, '2019-07-21 02:08:19', '2019-07-21 02:08:19', '2020-07-20 23:08:19'),
('7b4dbe86655e445beedca2e01ed3c925f59c28ac04fc7b32ea7808a240af94e92d56289d36533471', 1, 1, 'MyApp', '[]', 0, '2019-11-28 02:13:24', '2019-11-28 02:13:24', '2020-11-27 23:13:24'),
('7c424831187001e0e45531da10629724a83cf4d1bc951c0c51fbd5d928fc773723a45814e7547f68', 1, 1, 'MyApp', '[]', 0, '2019-11-12 03:53:00', '2019-11-12 03:53:00', '2020-11-12 00:53:00'),
('7dd391e41ffd078dd89d7628dc1fb8e9de79bb263d1018f8387550945becf1c72d11821b432b9c53', 1, 1, 'MyApp', '[]', 1, '2019-08-22 04:11:02', '2019-08-22 04:11:02', '2020-08-22 01:11:02'),
('837fead4ef728a32ac64b24bf07083b5963c3f1e5b5ae9f4a6a688ad75ededd94404e57a226e27f7', 1, 1, 'MyApp', '[]', 0, '2019-11-09 22:08:23', '2019-11-09 22:08:23', '2020-11-09 19:08:23'),
('8411c49a1e631365752403c6645d79897fd3989f2b260438a691832a27514d66625f1311d582ddab', 1, 1, 'MyApp', '[]', 0, '2019-09-07 04:10:36', '2019-09-07 04:10:36', '2020-09-07 01:10:36'),
('84ae2cea72e649e84a8c46a84c08c8bdc52aec0756753c403427f3feeed92fb60bf68405c9edca91', 1, 1, 'MyApp', '[]', 1, '2019-07-24 02:49:22', '2019-07-24 02:49:22', '2020-07-23 23:49:22'),
('8658225510261eb1c80dde10ccf3bfa8424681a7e1c28f635fbc735475bf1f4e6c1dfae4151ccd3c', 1, 1, 'MyApp', '[]', 0, '2019-11-09 19:59:11', '2019-11-09 19:59:11', '2020-11-09 16:59:11'),
('8659c113e1bb2111234ddb9f45c6918c6f0e94edda30ad2f0f0e234929b2ceb9c17255bf61c054f7', 1, 1, NULL, '[]', 0, '2019-09-15 07:41:39', '2019-09-15 07:41:39', '2020-09-15 04:41:39'),
('876c762e93171b6ecebb675d29c4c9ed63609f9b1fd6e6841c41d308bfa925232f82d6365a5aa4cc', 1, 1, 'MyApp', '[]', 0, '2019-07-14 18:15:48', '2019-07-14 18:15:48', '2020-07-14 15:15:48'),
('8b266c7d844c902317cba43746d238c646291bad4db0e3df7a750cafd6bd71a0be0f278ccc55bcfe', 1, 1, 'MyApp', '[]', 0, '2019-07-24 14:12:43', '2019-07-24 14:12:43', '2020-07-24 11:12:43'),
('8ba5033cf7f7fcbac71446b89cbe7d987f3e24387ec9dd8c7a88896d31cdd82dde1f7e78efc3d0d5', 1, 1, 'MyApp', '[]', 1, '2019-07-24 03:17:53', '2019-07-24 03:17:53', '2020-07-24 00:17:53'),
('8bbcd565f7cc1ef350ea6687c0ebaa5fd8c6d88a57e755daa82a2cbe61a1c2e78153a56cf059eed0', 1, 1, 'MyApp', '[]', 0, '2019-08-22 04:18:08', '2019-08-22 04:18:08', '2020-08-22 01:18:08'),
('8c4a68a8b6f54fb30ed51235c13b2bb48d64b030a45a4bf78f93f4cdf5de136662e6063825aa99a2', 1, 1, 'MyApp', '[]', 1, '2019-07-24 13:47:37', '2019-07-24 13:47:37', '2020-07-24 10:47:37'),
('8d6b6b63149985ace643cad6cbc0a35298fe36f10ea08a9924e61cc8ca5e8652ac6689b0533bd8cd', 1, 1, 'MyApp', '[]', 0, '2019-09-12 04:39:41', '2019-09-12 04:39:41', '2020-09-12 01:39:41'),
('8e71dc2aae1e4c7d84b0c72df7e4ae40f2301babe79cd3cc50acc313eb3d3484163087de4ff112cc', 1, 1, 'MyApp', '[]', 0, '2019-07-21 19:54:40', '2019-07-21 19:54:40', '2020-07-21 16:54:40'),
('8e9915e71d223a060775e0414bc20c12b2bb62ce21840df56bfe79a0545adf4a33e334c9970bbbb2', 1, 1, 'MyApp', '[]', 1, '2019-08-23 03:33:08', '2019-08-23 03:33:08', '2020-08-23 00:33:08'),
('8f86c303d4b5faf71ceb9a92b98274eec8377846d7b3530596e8963c94d0ebc3afa3d0dedf3c0cce', 1, 1, 'MyApp', '[]', 1, '2019-07-21 19:54:44', '2019-07-21 19:54:44', '2020-07-21 16:54:44'),
('9062c5f1858e936d1df4b015435fd3b14aef1eef3588dddf5abec5545858e4cd59eb4ab985060222', 1, 1, 'MyApp', '[]', 0, '2019-08-17 16:14:35', '2019-08-17 16:14:35', '2020-08-17 13:14:35'),
('919921c030cfd9eab3ea919293e332b20d720f54aa695936b4d144c1e224f4602cd9dba9f0ddef35', 1, 1, 'MyApp', '[]', 0, '2019-11-09 22:13:24', '2019-11-09 22:13:24', '2020-11-09 19:13:24'),
('91a380374c3ac1ae36e9aaf088f88e260c856096fa950591368583f535e16516d51602d01aac1a5a', 1, 1, 'MyApp', '[]', 0, '2019-07-18 05:27:58', '2019-07-18 05:27:58', '2020-07-18 02:27:58'),
('91cec1ebff5bdc0426fc30694caf56bfac03062f4dff3244654b0875dd11beb4b8248e54bce5dab8', 1, 1, 'MyApp', '[]', 0, '2019-07-21 20:09:44', '2019-07-21 20:09:44', '2020-07-21 17:09:44'),
('920d5be63a650dc37d23fcb978ac711f671f0aa064bad9154a8e3d5bde8de9381bf5c725b33fea5d', 1, 1, 'MyApp', '[]', 1, '2019-07-21 17:18:22', '2019-07-21 17:18:22', '2020-07-21 14:18:22'),
('936b6d536eeac2226864ee372bb9d06e587231bd52588f1ef2e480bf91d3ee5f0724d2d6a06dc864', 1, 1, 'MyApp', '[]', 0, '2019-07-18 06:01:10', '2019-07-18 06:01:10', '2020-07-18 03:01:10'),
('9378bc3925b8bebc42fb61d3cdac6814ff157c002849ea5f0e8a7df87dd47c8d6fe18af1ea85a6b3', 1, 1, 'MyApp', '[]', 0, '2019-07-18 06:10:07', '2019-07-18 06:10:07', '2020-07-18 03:10:07'),
('93828183206c03636cd2385ea544bc974558ddf5467b9df389f053b165bf0196ee1852a2aea2ca17', 1, 1, 'MyApp', '[]', 0, '2019-11-09 22:00:13', '2019-11-09 22:00:13', '2020-11-09 19:00:13'),
('950fd8112c78891730a173b97311a966e7768f40e3a4d5de27d274aa16eaf4ffce72db5cd5f89c35', 1, 1, 'MyApp', '[]', 0, '2019-07-27 05:15:05', '2019-07-27 05:15:05', '2020-07-27 02:15:05'),
('967790f93e0b2a986535f50f22209e06934adc538b33e640ea9123cda3630d55d7e20ed04ce9f9f8', 1, 1, 'MyApp', '[]', 1, '2019-07-25 03:50:53', '2019-07-25 03:50:53', '2020-07-25 00:50:53'),
('9707ec6fbe32f143ab05429340470b6c530ada7eec1c5b2055a3ab799d573377cb1e199bba276f7f', 1, 1, 'MyApp', '[]', 1, '2019-08-23 03:13:40', '2019-08-23 03:13:40', '2020-08-23 00:13:40'),
('975b3fbafdc81c2e5fc32497b9635f582f3851cf771b4eddf342b99dd6c845f065a0983666e939e5', 1, 1, 'MyApp', '[]', 0, '2019-10-18 03:52:23', '2019-10-18 03:52:23', '2020-10-18 00:52:23'),
('98f7f5a024f0c122594dfa3577191dc1ec1056d518fbb6bba325486f3e02d1a7e0f10355075ef02c', 1, 1, 'MyApp', '[]', 0, '2019-07-18 06:03:54', '2019-07-18 06:03:54', '2020-07-18 03:03:54'),
('9a1429114f07ef66fa05639e5133c48bbeb357b74c2c5e1eb9f5d03ac39ab5faa04a4e97602a3f89', 1, 1, 'MyApp', '[]', 0, '2019-11-13 01:31:05', '2019-11-13 01:31:05', '2020-11-12 22:31:05'),
('9a948c32b5c3be29d51a783dce259d315a41ce39fd56e3c2ba591532c08a963956acf02929e9cced', 1, 1, 'MyApp', '[]', 0, '2019-07-18 06:35:07', '2019-07-18 06:35:07', '2020-07-18 03:35:07'),
('9c779d1ca0a4c0b5620d57fb81fe3caad1e1e23c4276df66c421feb0ed7d04b84b5353c7eaeaea76', 1, 1, 'MyApp', '[]', 0, '2019-09-15 07:42:14', '2019-09-15 07:42:14', '2020-09-15 04:42:14'),
('9c850cbd422d3eceed9ea06614723ddf96a88b6aaab0c31bdf18646990f9ba12d05993c3a27031cc', 1, 1, 'MyApp', '[]', 0, '2019-11-22 02:50:45', '2019-11-22 02:50:45', '2020-11-21 23:50:45'),
('9e3b2ef741042190d473a9b682e5a1d221c9f48fb69794130e67223a0fdab67eb1ad254074c04602', 1, 1, 'MyApp', '[]', 1, '2019-08-22 04:14:58', '2019-08-22 04:14:58', '2020-08-22 01:14:58'),
('9fbf47d47b634d178c8cb248d87e10369276a080a572aab03d0bfc22903b44e31dd7fe653ec6eb73', 1, 1, 'MyApp', '[]', 1, '2019-11-23 01:09:07', '2019-11-23 01:09:07', '2020-11-22 22:09:07'),
('a010758a61006c5f1efbc6ca43713bd6435c596d22b89c1b7c26922f3370a7934756731637329ef6', 1, 1, 'MyApp', '[]', 0, '2019-07-19 04:31:13', '2019-07-19 04:31:13', '2020-07-19 01:31:13'),
('a02ad32bc9ac46a30931ad1cac7b12b90c1e657cd05064bc453843912717aa3134f546eabc63aa37', 1, 1, 'MyApp', '[]', 0, '2019-07-21 17:45:38', '2019-07-21 17:45:38', '2020-07-21 14:45:38'),
('a0efd010d167e9872354fc97e9c33f12c28f324de31f625d4ba5c53282aaeab00ff14ea096939d1c', 1, 1, 'MyApp', '[]', 0, '2019-07-31 03:57:32', '2019-07-31 03:57:32', '2020-07-31 00:57:32'),
('a17426a9e040d0e969af2e348b7b37aa257bea4d6e25960d9aec28079b17acccb38cf2cd0dcd624d', 1, 1, 'MyApp', '[]', 0, '2019-11-16 16:46:41', '2019-11-16 16:46:41', '2020-11-16 13:46:41'),
('a1b07554391a5dc3da2c99121d86d40c9d4683f87b1e3a6ae89521a066b5c28d55f2965b85fa980d', 1, 1, 'MyApp', '[]', 0, '2019-09-22 18:01:26', '2019-09-22 18:01:26', '2020-09-22 15:01:26'),
('a3deb9a136eef8463155aa0dcb565d2dd92b17ed47768df861f1e8fef65f34f95cd6e53078f2c13d', 1, 1, 'MyApp', '[]', 0, '2019-07-18 06:19:18', '2019-07-18 06:19:18', '2020-07-18 03:19:18'),
('a3e6e945eb94fbd1b3837f6d2a723a9954ff6298b296fe1cf2a55446db73120b2ed56337e4bff618', 1, 1, 'MyApp', '[]', 0, '2019-10-31 05:38:16', '2019-10-31 05:38:16', '2020-10-31 02:38:16'),
('a45149e4f57e03efa51ae48c1b5add77647927fd3a59ece0b9cc3144b622dbe11727217062fa3d25', 1, 1, 'MyApp', '[]', 0, '2019-09-04 02:08:26', '2019-09-04 02:08:26', '2020-09-03 23:08:26'),
('a4b402f1a6da123baad5dc16973be9720b8dcf008884da19f391cc6b4811f7e46ae44691d8f408cd', 1, 1, 'MyApp', '[]', 0, '2019-09-12 05:21:50', '2019-09-12 05:21:50', '2020-09-12 02:21:50'),
('a59a172838885e0662aeadb6e5a5eb7dfd1b57a94ba607936a92c46f89391c7c2efdfd1d4f7e8dfc', 1, 1, 'MyApp', '[]', 0, '2019-11-15 03:32:17', '2019-11-15 03:32:17', '2020-11-15 00:32:17'),
('a5adb241a1c2973fd447c48d44c39f110128417eed24dc26d0375c2ec6217b44ced311841a6b855a', 1, 1, 'MyApp', '[]', 0, '2019-08-28 03:35:35', '2019-08-28 03:35:35', '2020-08-28 00:35:35'),
('a60d6787b3a901204f134936350a75b411c42669c879c4e7cdb8dbd600acdcfdf1acd9852f01ae86', 1, 1, 'MyApp', '[]', 0, '2019-07-19 04:15:29', '2019-07-19 04:15:29', '2020-07-19 01:15:29'),
('a875bba32befadcdd3260f8d71d7e5fb4f88812a654e6fdcf3eebc6fffacfa52d52bfbef703a306b', 1, 1, 'MyApp', '[]', 0, '2019-08-18 20:26:27', '2019-08-18 20:26:27', '2020-08-18 17:26:27'),
('a87a48516322427f978ffa123281fe0fa45f1d3dc1dca38bfd4d70d8f8fcf2afbea150d41b1dba88', 1, 1, 'MyApp', '[]', 1, '2019-08-22 04:07:59', '2019-08-22 04:07:59', '2020-08-22 01:07:59'),
('a8f06218826ae9994bc80a266c9e8414e092a9d3aecdff549dff1be8ce46aee2f3d7566a80261ffb', 1, 1, 'MyApp', '[]', 0, '2019-07-25 02:48:14', '2019-07-25 02:48:14', '2020-07-24 23:48:14'),
('a9237500be2775656d8d37993a14be9627d938c6b34ad80c02e793b6896f513744e8388a3cb61a77', 1, 1, 'MyApp', '[]', 0, '2019-11-09 21:25:50', '2019-11-09 21:25:50', '2020-11-09 18:25:50'),
('a9a5a0b119739ccbed496ebce2038a38d8609138445e2bbdf4737886cb2672ff12755c68ac9d937d', 1, 1, 'MyApp', '[]', 0, '2019-08-18 05:05:35', '2019-08-18 05:05:35', '2020-08-18 02:05:35'),
('aadd8cec73d2f6e0aac09c504588722221bb76dd9b7adc2e1a5f8941d287f0bc716aa30be2950a79', 1, 1, 'MyApp', '[]', 0, '2019-09-19 03:07:06', '2019-09-19 03:07:06', '2020-09-19 00:07:06'),
('ab149240d30fd048c6faf050cf21feddc18743a97b322c99c92e8c94cbe71ed006941f98fda36d6f', 1, 1, 'MyApp', '[]', 0, '2019-11-12 03:28:21', '2019-11-12 03:28:21', '2020-11-12 00:28:21'),
('ab2a67e6afe3d51694c2937b8d504735686c512e5c573daab40894872ad5a07bbf876b8ed3853152', 1, 1, 'MyApp', '[]', 1, '2019-07-21 17:46:05', '2019-07-21 17:46:05', '2020-07-21 14:46:05'),
('ad4514f4fdd8d29ed493bc1fe560f7be4c5ddd4da8aaa9bab7ab6fdd8765384b299a6211ee75a2ff', 1, 1, 'MyApp', '[]', 0, '2019-07-31 02:40:38', '2019-07-31 02:40:38', '2020-07-30 23:40:38'),
('ae41b83be9886dd881d812f8afcf35118772e7ab6cb9e2f3653cd25379572732195caf66e164816c', 1, 1, 'MyApp', '[]', 0, '2019-11-08 23:50:42', '2019-11-08 23:50:42', '2020-11-08 20:50:42'),
('ae433d5b2d6b725e458356cd5b1470bd2607b051520290b1bb3bc126829f1c4f484535c09a0844d0', 1, 1, 'MyApp', '[]', 1, '2019-07-25 04:16:24', '2019-07-25 04:16:24', '2020-07-25 01:16:24'),
('aee6cb6cd40b1412182957c2583d2944da9b08ffef1d4f95a7fd6cbd3e108acc9f3e8181953d2104', 1, 1, 'MyApp', '[]', 0, '2019-11-17 21:39:37', '2019-11-17 21:39:37', '2020-11-17 18:39:37'),
('af103c3a0ec441b0c008fae81b036cc66ef990c701e7a79a286d02a0cbc4171d080cedacd49b9601', 1, 1, 'MyApp', '[]', 0, '2019-10-31 03:01:58', '2019-10-31 03:01:58', '2020-10-31 00:01:58'),
('b09e58b254cf527bf2c86c5349dddb5210ce024d25917082ea292f067a67c8880c537124d88735c9', 1, 1, 'MyApp', '[]', 0, '2019-08-17 04:31:50', '2019-08-17 04:31:50', '2020-08-17 01:31:50'),
('b1c551f3947306d60819cf2eead3576c1475c52ea3dc8b76e4c48fa8f02eb45481cb5849f27f910a', 1, 1, 'MyApp', '[]', 0, '2019-07-18 04:23:24', '2019-07-18 04:23:24', '2020-07-18 01:23:24'),
('b25842fb658d304fca93b91fffef9bdbb52258feef7594ba1171dfaeb015ccaa74cbdd35351800c8', 1, 1, 'MyApp', '[]', 0, '2019-07-18 05:54:35', '2019-07-18 05:54:35', '2020-07-18 02:54:35'),
('b25886473c9280d230e798e808021bb5dd7f65a05684267d1a068512dd18ab242e268c25c6318b61', 1, 1, 'MyApp', '[]', 1, '2019-07-25 03:05:24', '2019-07-25 03:05:24', '2020-07-25 00:05:24'),
('b26fb781b514cd47c0e9140340510654746ea8d5437c4a87fa23e568bfff93d06583bae9d34d63a4', 1, 1, 'MyApp', '[]', 1, '2019-07-21 01:24:51', '2019-07-21 01:24:51', '2020-07-20 22:24:51'),
('b2d9a0cfc7d1689ba99c4f5223be2958f5e69a2cd63bc3a6c76126d375d17145ed894d415597a8df', 1, 1, 'MyApp', '[]', 0, '2019-10-14 08:01:16', '2019-10-14 08:01:16', '2020-10-14 05:01:16'),
('b381b8bdbf6df3fd48bddb1d3ba7390f878a16498acbb831810200c8aec2f7c92e8459e6fc8faf16', 1, 1, 'MyApp', '[]', 1, '2019-07-25 04:17:45', '2019-07-25 04:17:45', '2020-07-25 01:17:45'),
('b3e32718e463983561950a48e42835f9a8429c45bebec971db066d01c8be44d79ba9053052461178', 1, 1, 'MyApp', '[]', 0, '2019-11-10 19:02:52', '2019-11-10 19:02:52', '2020-11-10 16:02:52'),
('b456797d1e05dff40fd4fafe2c40cdb7f5ffbbfd7f7961457e82bd06516dbbf323a1d1913b80dd1b', 1, 1, 'MyApp', '[]', 0, '2019-07-19 04:32:22', '2019-07-19 04:32:22', '2020-07-19 01:32:22'),
('b4cd2b2c69fb530db185e4afec8c5897f0063d45e5e8a508fcd021f457f71d2dd0ea5eb53ce7efdf', 1, 1, 'MyApp', '[]', 1, '2019-07-24 12:49:30', '2019-07-24 12:49:30', '2020-07-24 09:49:30'),
('b5fe0c646707c92231baabca7af5975f0979328bddd7af67be3dcec7b40f7ae4dbcecff79bb3d726', 1, 1, 'MyApp', '[]', 0, '2019-10-19 04:15:08', '2019-10-19 04:15:08', '2020-10-19 01:15:08'),
('b62c09845dacc353b3e57c30f2b18432f04c076bf1b9f3e2960f4b887578694556f5bf85a7493a2c', 1, 1, 'MyApp', '[]', 0, '2019-07-20 04:13:41', '2019-07-20 04:13:41', '2020-07-20 01:13:41'),
('b664caf06dce66dcdc793844339482d320696cefa7f595eb77a7776f50a1e105ff19070f2c9b5bcf', 1, 1, 'MyApp', '[]', 0, '2019-08-08 04:28:13', '2019-08-08 04:28:13', '2020-08-08 01:28:13'),
('b6a84b7d20ef2e3073f25e955e94d1bfded35e85611739411b9037227d5b58373de46bdcf49b3f27', 1, 1, 'MyApp', '[]', 0, '2019-07-24 14:18:50', '2019-07-24 14:18:50', '2020-07-24 11:18:50'),
('b7d163b9a56932124b69afb8637bb0dc3dca2b79250fb60d4cf0801140ed3bbb3fcf2cb5590dc8f7', 1, 1, 'MyApp', '[]', 0, '2019-11-09 20:47:41', '2019-11-09 20:47:41', '2020-11-09 17:47:41'),
('b98878c4f20a0c85f0823e0f916d0964f8eeea94b1d117754f0a4d71de9fb4f0bb7cb6b11b65cd96', 1, 1, 'MyApp', '[]', 1, '2019-07-24 03:29:18', '2019-07-24 03:29:18', '2020-07-24 00:29:18'),
('bb40b6de6cae917ffa8d3c06f19c636ce494411ede645bf90b165b55324becb93d0ba9433f6ae5f9', 1, 1, 'MyApp', '[]', 1, '2019-07-25 03:00:57', '2019-07-25 03:00:57', '2020-07-25 00:00:57'),
('bcb30ce7ab2983b47bb021450a3baf36a9b647fadfee40a5d9a669b706a7d79e89a4b7035420d9af', 1, 1, 'MyApp', '[]', 1, '2019-07-24 03:46:27', '2019-07-24 03:46:27', '2020-07-24 00:46:27'),
('bcbb78e4e945e4ce28b2e8e740c4a362b14001bc4b5212fa0639f03b388c382cb9cf5855b7e545d6', 1, 1, 'MyApp', '[]', 0, '2019-07-24 14:11:26', '2019-07-24 14:11:26', '2020-07-24 11:11:26'),
('bdd6a840115f6fa0cb448bbcb78fbb255f70251d3f3a5811d51ebf3477aa32612f6f2e0f5f5c4eec', 1, 1, 'MyApp', '[]', 1, '2019-07-31 03:29:18', '2019-07-31 03:29:18', '2020-07-31 00:29:18'),
('bdd90410973a700e82bc5f2763c8b8a277633d88dce0261de6518ab4f5cb39d6a36b816787e7d3ec', 1, 1, 'MyApp', '[]', 0, '2019-07-31 03:18:54', '2019-07-31 03:18:54', '2020-07-31 00:18:54'),
('be79775b4f98188c016e64516cf006e626cc1abd62caae5f2acc49f2e0a09f07b6a6fdeafe026759', 1, 1, 'MyApp', '[]', 0, '2019-11-17 23:37:27', '2019-11-17 23:37:27', '2020-11-17 20:37:27'),
('bf50ee0e7f113d652ec0fa2f5ad8a8d53d571431a36709c15569065b8cfe4641e716e0f6e258bd63', 1, 1, 'MyApp', '[]', 0, '2019-07-18 06:22:50', '2019-07-18 06:22:50', '2020-07-18 03:22:50'),
('c021095b12a29c89dda17435fd69c1e752bc69aee8d87fb672a65cc86fca99e4fa606dd76d36c3b4', 1, 1, 'MyApp', '[]', 0, '2019-07-18 06:36:07', '2019-07-18 06:36:07', '2020-07-18 03:36:07'),
('c07fb0c5168565c68b696f9ec0830d387a78fbb47da76f7f2bd813a33878bd5d3076e2d787af7a43', 1, 1, 'MyApp', '[]', 0, '2019-07-20 16:07:37', '2019-07-20 16:07:37', '2020-07-20 13:07:37'),
('c099f264742cfd4f1658b9e0534138ea03c8c425491622c6cec11d9f7e7f9dd5b9fd0452bf15e7cf', 1, 1, 'MyApp', '[]', 0, '2019-11-20 18:29:54', '2019-11-20 18:29:54', '2020-11-20 15:29:54'),
('c3688670fc13e8c78c748d82537fc228f7c5b634dd9236348fc37e6c85d1731f3c030becef6726b5', 1, 1, 'MyApp', '[]', 1, '2019-07-24 03:37:23', '2019-07-24 03:37:23', '2020-07-24 00:37:23'),
('c444c283c68fdb5d64e149cfb55eb22d8c42d8bffddc9e00c9c85a76f62c358e9e304e129149364f', 1, 1, 'MyApp', '[]', 0, '2019-07-27 05:46:52', '2019-07-27 05:46:52', '2020-07-27 02:46:52'),
('c4b9dc8ce8dc241a07127dba0156794a1b8b8be298ae38392a945563927d12c76daf12a6db555672', 1, 1, 'MyApp', '[]', 0, '2019-09-01 22:19:05', '2019-09-01 22:19:05', '2020-09-01 19:19:05'),
('c5a816c0ce9c6effac26a2f4a6ebfa7fdbaed523f9d2d513494c67bf1d7d8b7c7425bc0ae527f194', 1, 1, 'MyApp', '[]', 0, '2019-07-19 04:30:42', '2019-07-19 04:30:42', '2020-07-19 01:30:42'),
('c5af6b5874d43efd6721acbac88b8b7426bf2e0c3bb5adbf4f6accf8960795d95444a256649d55b3', 1, 1, 'MyApp', '[]', 0, '2019-11-14 03:50:06', '2019-11-14 03:50:06', '2020-11-14 00:50:06'),
('c6ca1c1fcacf83f5bab9989a3ad41ade5156b944e2aa3b5ca1c4e57caabba2f6f72eb66372a62d53', 1, 1, 'MyApp', '[]', 0, '2019-09-05 02:40:33', '2019-09-05 02:40:33', '2020-09-04 23:40:33'),
('c764a44cc9c35656b2e90392fec17379e0a0a28e864b9ce2991bd32657e8426d13b95d04d7bc5ef0', 1, 1, 'MyApp', '[]', 0, '2019-08-23 03:34:01', '2019-08-23 03:34:01', '2020-08-23 00:34:01'),
('c945306d2d2b8c2cc13d05e99314dc20de3bababa0ba175d21465927f740ac6760face8c86bb66a7', 1, 1, 'MyApp', '[]', 0, '2019-11-20 02:33:05', '2019-11-20 02:33:05', '2020-11-19 23:33:05'),
('c98896e7ffd5143c1566d05daddd5d0ffa040762e6e7c943f20dafe92a0d73b200f4b4b811bdbca5', 1, 1, 'MyApp', '[]', 0, '2019-10-30 02:17:47', '2019-10-30 02:17:47', '2020-10-29 23:17:47'),
('c9dcdd47d0d9787b6677ff6766b37ff8430d1ffb1bb7161f3d9734f82f2c5e17a14e77c66fcdd7b3', 1, 1, 'MyApp', '[]', 0, '2019-07-26 04:27:46', '2019-07-26 04:27:46', '2020-07-26 01:27:46'),
('cadda101abdc8136eeeb4fad60a7c20cef51bf7bbdea6a7622687ceca4afac735a56050cfda7db69', 1, 1, 'MyApp', '[]', 0, '2019-07-14 18:41:24', '2019-07-14 18:41:24', '2020-07-14 15:41:24'),
('cafa76423bce7d7440a36cf794416d2b532c19ba194d7a48f1510b72275c9b4857936c2f9dd88609', 1, 1, 'MyApp', '[]', 0, '2019-07-16 02:59:29', '2019-07-16 02:59:29', '2020-07-15 23:59:29'),
('cbde06c7c07775da58b8d15d9d190ec298c8a8c8063a8ff1f2a6ee8fcd92fdbb8059f94664693bf5', 1, 1, 'MyApp', '[]', 1, '2019-07-24 02:45:11', '2019-07-24 02:45:11', '2020-07-23 23:45:11'),
('cccf87b5b44c20bf8ac28aba9350e0bd5092cef82c2225601e6abbe16f6e7b840587a4ac013c5e94', 1, 1, 'MyApp', '[]', 0, '2019-07-27 22:02:05', '2019-07-27 22:02:05', '2020-07-27 19:02:05'),
('cd968735d0152d3ef933689dc876cc63602c87c1c7ade0f51d96c1d5c8007da161912ba459772001', 1, 1, 'MyApp', '[]', 0, '2019-11-13 01:03:51', '2019-11-13 01:03:51', '2020-11-12 22:03:51'),
('cea737c8fa8435a8bda2e255853f84c3b8bec89052cd9b57b6e817763f9f51edba78e0279d3fff43', 1, 1, 'MyApp', '[]', 0, '2019-07-18 06:16:52', '2019-07-18 06:16:52', '2020-07-18 03:16:52'),
('cf3637b2f3f365ed374a89732d8fd3fcd7c3ace0e03947126c4b88a6d00c8dc1af62ccb63ea4f7f0', 1, 1, 'MyApp', '[]', 0, '2019-11-09 20:39:00', '2019-11-09 20:39:00', '2020-11-09 17:39:00'),
('cf7d30f9152be1cb6eaa243fa51302d151d435fd839411820d5425accd172c85e7a7a57a293aca6c', 1, 1, NULL, '[]', 0, '2019-11-23 01:07:12', '2019-11-23 01:07:12', '2020-11-22 22:07:12'),
('cfb8b161fe11f8a75bb213b76ec493b90988efb0e92945961dbafd90735a4aa799545539c90f8597', 1, 1, 'MyApp', '[]', 1, '2019-07-25 03:51:20', '2019-07-25 03:51:20', '2020-07-25 00:51:20'),
('d284984a298197e0da334c23507936612ed165140d74bd30637cdf4077dd640aa37276fe3ddd1736', 1, 1, 'MyApp', '[]', 1, '2019-07-24 13:29:59', '2019-07-24 13:29:59', '2020-07-24 10:29:59'),
('d62515c4a04fa0c0910fde232454af1e7f933d9d67eab4b1b31bd89d6731a3c9ef1009f5a877ad95', 1, 1, 'MyApp', '[]', 0, '2019-08-25 06:45:10', '2019-08-25 06:45:10', '2020-08-25 03:45:10'),
('d6715c08b5a65359283e91b409b48e376db4631796ea6f6451249fc96803c7390f3b73261139a1c2', 1, 1, 'MyApp', '[]', 0, '2019-11-09 22:58:27', '2019-11-09 22:58:27', '2020-11-09 19:58:27'),
('d67d66be6fd6fcd4e772c4df99264eefcaaf3b4a7b7b9b4eb9a8a48320dd3bb2f44cf83d2a22053a', 1, 1, 'MyApp', '[]', 1, '2019-08-22 03:37:13', '2019-08-22 03:37:13', '2020-08-22 00:37:13'),
('d7e33dc50727ea45a3e1d2fac1877a8bd6aafc1dc979c725a232f08614766ebcb3505bd38e80ab43', 1, 1, 'MyApp', '[]', 0, '2019-10-08 01:49:10', '2019-10-08 01:49:10', '2020-10-07 22:49:10'),
('d82b3de3024194a12c353682c7b819e0d469a06220362c8f0a6cd3fbd34a2c8e0318aa72a7a438c7', 1, 1, 'MyApp', '[]', 1, '2019-08-22 03:38:31', '2019-08-22 03:38:31', '2020-08-22 00:38:31'),
('d866514d51589472d4ad0d0342afb4a8f596339556330d076ae357e7c79356368a753abcb7f2ab81', 1, 1, 'MyApp', '[]', 0, '2019-11-18 06:26:06', '2019-11-18 06:26:06', '2020-11-18 03:26:06'),
('d93d05d47f0f3647c68508e78f4bc38118e08cf81af77fe90206e16986d8cd234b4ce765a5f80d19', 1, 1, 'MyApp', '[]', 0, '2019-11-12 03:59:14', '2019-11-12 03:59:14', '2020-11-12 00:59:14'),
('d9aa3ca02e0b3eea6826694e8f8a49261b0cc74dc157b894cd0a2e8198cba9366812335f50a04189', 1, 1, 'MyApp', '[]', 1, '2019-07-24 13:06:15', '2019-07-24 13:06:15', '2020-07-24 10:06:15'),
('da1c6ce8339ef6503621060b7c2a2c13018f0b997d65fa47165e3fbc8c5a006afb36a480e5a3f218', 1, 1, 'MyApp', '[]', 0, '2019-07-18 06:23:19', '2019-07-18 06:23:19', '2020-07-18 03:23:19'),
('da5475d1c9f3f3cc926b43c6af82f2b989d73c720dca841badcfa645ba5b05c78fdafc6497c040ae', 1, 1, 'MyApp', '[]', 0, '2019-10-16 03:21:52', '2019-10-16 03:21:52', '2020-10-16 00:21:52'),
('da8e8d934f1adad0b5bce97e26a5bb21bbb67b6b57fc818d08e39e3e6844528358b4b5235f3bebd7', 1, 1, 'MyApp', '[]', 0, '2019-07-18 06:24:00', '2019-07-18 06:24:00', '2020-07-18 03:24:00'),
('db072550de5f86d7561540d9b9fa25ffa378bfe1c82f33cf3f3102569762c1af04f3651e3347e631', 1, 1, 'MyApp', '[]', 0, '2019-12-03 04:32:58', '2019-12-03 04:32:58', '2020-12-03 01:32:58'),
('dbbdba0aff8f7bd8d212913a17ef2bbfd4214d09219c5e6e1b55c3437f78e0b09576ac845ec38d2c', 1, 1, 'MyApp', '[]', 0, '2019-07-24 14:09:47', '2019-07-24 14:09:47', '2020-07-24 11:09:47'),
('dd24f066d7af4bffa897eea661572ff0a986b1858027e24025359c4b391565e16355f7e6d94867c8', 1, 1, 'MyApp', '[]', 0, '2019-09-21 19:03:52', '2019-09-21 19:03:52', '2020-09-21 16:03:52'),
('ddeae64524f7a8801e7e62fccad9857a021dce7cce1cd7b984c1061a48ba667d3f034e04dce47de2', 1, 1, 'MyApp', '[]', 0, '2019-07-18 06:12:13', '2019-07-18 06:12:13', '2020-07-18 03:12:13'),
('dedbaf34815bdf4071010fa234a2427a085f6865e622e48eacd6b0fa521575d9198212188f989e39', 1, 1, 'MyApp', '[]', 1, '2019-07-25 03:59:58', '2019-07-25 03:59:58', '2020-07-25 00:59:58'),
('e1e8378bbdaecdd521c58c5dbe7e3f5ddf9ae3d40be2cd78165e936df25d0d48f7ccf9dab39b6c7c', 1, 1, 'MyApp', '[]', 1, '2019-07-31 02:34:58', '2019-07-31 02:34:58', '2020-07-30 23:34:58'),
('e2c47cc6c5fa710884381675a2347d75c27ce5e745c75bedab5fd584c43695762c64524ee33047f3', 1, 1, 'MyApp', '[]', 0, '2019-07-28 21:37:37', '2019-07-28 21:37:37', '2020-07-28 18:37:37'),
('e6d67533a58aa3cb8e108648571afee320e32a333dec6741b3173bc05a90b247519ebba1c093ba0f', 1, 1, 'MyApp', '[]', 0, '2019-07-14 21:27:28', '2019-07-14 21:27:28', '2020-07-14 18:27:28'),
('e7bb28efddee461c56c55081be12b2aaad48a15ba634f13dede7a5bb020c1330814977813b233dd0', 1, 1, 'MyApp', '[]', 0, '2019-11-12 03:24:27', '2019-11-12 03:24:27', '2020-11-12 00:24:27'),
('e7da11d4f510efc2e905b39ffa102d2898f93357df1e848489e1a4f4b2f7f0af2be4c9d576e32567', 1, 1, 'MyApp', '[]', 0, '2019-08-25 01:58:58', '2019-08-25 01:58:58', '2020-08-24 22:58:58'),
('e9116f69528e6115665e9111310f7fd947c746712be57b52d7cd3de2fa118048b4a60b6adfbb99f0', 1, 1, 'MyApp', '[]', 0, '2019-08-18 08:54:54', '2019-08-18 08:54:54', '2020-08-18 05:54:54'),
('e990c2563ac969d797d18f90caa1821fd95d1dbe1a0e31ab060a0b4f3598a85feb287dd5e3786a4f', 1, 1, 'MyApp', '[]', 0, '2019-11-10 14:19:20', '2019-11-10 14:19:20', '2020-11-10 11:19:20'),
('e9da4d5912b6a3a8dbdcfc6f00f6e349f8ad96e656e81f32af1afadfea420abc282886f60464b85f', 1, 1, 'MyApp', '[]', 1, '2019-07-25 02:48:30', '2019-07-25 02:48:30', '2020-07-24 23:48:30'),
('ea92282d10cf50602e6cee1235e6fd27f93b58bba153ebcf1b2676a2a89b79e491d34305d4d71571', 1, 1, 'MyApp', '[]', 0, '2019-08-12 00:01:16', '2019-08-12 00:01:16', '2020-08-11 21:01:16'),
('eae948a555612977dbb900a228ec50d774119e020b0aeaa07619751aefe171c33e984ab3767de93a', 1, 1, 'MyApp', '[]', 0, '2019-09-21 21:50:06', '2019-09-21 21:50:06', '2020-09-21 18:50:06'),
('eb84463c3aa062ff81978d6845ee42edfc6ace8c57816bdfdaeb8a0cce580c6423b7dc2afe35fc14', 1, 1, 'MyApp', '[]', 0, '2019-07-18 05:59:02', '2019-07-18 05:59:02', '2020-07-18 02:59:02'),
('ed35dcee4562ba32ba24c2c53e6bc1a54b329ec3c2dccbf682a990e48ae64502f1866abe7826f2ef', 1, 1, 'MyApp', '[]', 0, '2019-10-11 03:16:13', '2019-10-11 03:16:13', '2020-10-11 00:16:13'),
('ed8eec10f7116433b9b2068593acae9bad35e55aebe857644c31b4231ab3478472e0254b3aeaa755', 1, 1, 'MyApp', '[]', 0, '2019-04-21 11:17:19', '2019-04-21 11:17:19', '2020-04-21 08:17:19'),
('edb291a6214ace16171ff6b9c8feb6226453ca1c67cdf1e4770b50a891108cf4d51ce9dbdbc17a4a', 1, 1, 'MyApp', '[]', 0, '2019-04-21 11:15:12', '2019-04-21 11:15:12', '2020-04-21 08:15:12'),
('edbc80e840ac8147ef090a89ac0e6f0621686bf8920b3db8c8d886b2a60c7a7047747d512fe39306', 1, 1, 'MyApp', '[]', 1, '2019-08-23 02:57:16', '2019-08-23 02:57:16', '2020-08-22 23:57:16'),
('f055988970d86f4be57c07d6f0adc041008e6c0cef10c645100fab8620fe2e36b07c6957c903c164', 1, 1, 'MyApp', '[]', 0, '2019-11-26 22:25:06', '2019-11-26 22:25:06', '2020-11-26 19:25:06'),
('f0e992bb990333a3af540ad0389c4f7a3f644a15843fd74db79bd9508321b903e625934d3e21e434', 1, 1, 'MyApp', '[]', 0, '2019-07-15 05:30:57', '2019-07-15 05:30:57', '2020-07-15 02:30:57'),
('f14cb90673ddda2de96abfe4ac986829a47e2466529defd43c1fd587c1eabf4273a2f8172fb1c7e5', 1, 1, 'MyApp', '[]', 0, '2019-09-18 03:43:31', '2019-09-18 03:43:31', '2020-09-18 00:43:31'),
('f1b25a71c21974f7fe7db6f96dff999233596e0b35425934213b20d270fe108e5f305ecd39f706a6', 1, 1, 'MyApp', '[]', 0, '2019-11-09 05:15:55', '2019-11-09 05:15:55', '2020-11-09 02:15:55'),
('f4262a0474714cf06806a83190e55aa7d5a1d97e5c701c6cc7457206034224382695302ce42c0069', 1, 1, 'MyApp', '[]', 0, '2019-07-18 05:28:03', '2019-07-18 05:28:03', '2020-07-18 02:28:03'),
('f53b6719df2d135060f8963ba4d1566717ad6ccb25d06f2651ac041ece558c5a61e66620bf9de32c', 1, 1, 'MyApp', '[]', 1, '2019-07-25 03:48:29', '2019-07-25 03:48:29', '2020-07-25 00:48:29'),
('f5e0101ef91a27d7ac42d3f1e5cfa4b156ccad6988e068eb5404054407d5c5586bff21b3622ea425', 1, 1, 'MyApp', '[]', 0, '2019-07-17 02:59:01', '2019-07-17 02:59:01', '2020-07-16 23:59:01'),
('f6ef335c5cc5568ab72e94ec6657c67a1b178ea1d16548c42fefdaf33b6d7a5665021acbe72fc6bf', 1, 1, 'MyApp', '[]', 0, '2019-08-13 02:24:31', '2019-08-13 02:24:31', '2020-08-12 23:24:31'),
('f7b6d4d7c2a193081042811ee2afb9a6a77bffba026cda547cac34497b4f5eea0fe96e105f86540f', 1, 1, 'MyApp', '[]', 1, '2019-08-23 03:23:49', '2019-08-23 03:23:49', '2020-08-23 00:23:49'),
('f7d7ce27f7c98ea7c2870bd3f20ab57e1d7b67c5907bf599df807c48bc95c2755faef5fd6556e4ed', 1, 1, 'MyApp', '[]', 1, '2019-07-24 13:21:45', '2019-07-24 13:21:45', '2020-07-24 10:21:45'),
('f8c080f198b45bbbcc7e73a0f7108a54dbb61e99d53521510362f1f30873afe6de6c9367f6afe1ba', 1, 1, 'MyApp', '[]', 0, '2019-11-12 03:52:10', '2019-11-12 03:52:10', '2020-11-12 00:52:10'),
('f91d0eed458414be721f7ac893229dd3a85772c260eb5c1322f68d4181f55754b1edd28df4d596a1', 1, 1, 'MyApp', '[]', 0, '2019-07-18 05:58:27', '2019-07-18 05:58:27', '2020-07-18 02:58:27'),
('f94510ba80cd7755ed79bd70c92db8649d1020924d2ce0f1d4f69fe47b49f989cfdb327a9ef5909f', 1, 1, 'MyApp', '[]', 0, '2019-08-19 17:59:17', '2019-08-19 17:59:17', '2020-08-19 14:59:17'),
('f99db2678949cbe4a1bb9d27eb0abf5fed0f7741c45c116d5dd1876536f4f60bfe74569fafbae67f', 1, 1, 'MyApp', '[]', 0, '2019-08-13 02:39:46', '2019-08-13 02:39:46', '2020-08-12 23:39:46'),
('fa8713851054e51a8ca419faab522fbf9a23d2e8d17af3e2147126f5974e21172360756d8a0b08ad', 1, 1, 'MyApp', '[]', 0, '2019-07-27 21:04:58', '2019-07-27 21:04:58', '2020-07-27 18:04:58'),
('fb71351158ce4e45242e95028af7a608d93da07ef73e0a9735c1ecd2d6dc315406f8ac04c346e058', 1, 1, 'MyApp', '[]', 1, '2019-11-15 22:53:55', '2019-11-15 22:53:55', '2020-11-15 19:53:55'),
('fb7f3eef163d2628142d97c54e61dc99a60464df5f4ed45abe9503ff2eaea2da5c3289e86e1e4362', 1, 1, 'MyApp', '[]', 1, '2019-07-24 03:36:58', '2019-07-24 03:36:58', '2020-07-24 00:36:58'),
('fc8bddbf6753b5ed18a5e9373166b30f7f87c63cf86412db22be624324634e19bd76b38220cddf01', 1, 1, 'MyApp', '[]', 1, '2019-07-24 03:18:57', '2019-07-24 03:18:57', '2020-07-24 00:18:57'),
('fc967ba19b5e30a03273c4e8b53f01bc03cef2b3fea3f2cb7adefbcca6c53785908753928a59c4c0', 1, 1, 'MyApp', '[]', 1, '2019-07-24 12:40:18', '2019-07-24 12:40:18', '2020-07-24 09:40:18'),
('fd3c71f59ea3937fac9a8c7799b80e7ddd67f3252a3878513e4fb4a4d20b3b439f3614f50d9e2f35', 1, 1, 'MyApp', '[]', 1, '2019-07-21 19:50:08', '2019-07-21 19:50:08', '2020-07-21 16:50:08'),
('fd99a39c2d8c814ad23b655efce2198de0f74550bb3f6099330fdafe4c42ef4e550cd3775a94ea90', 1, 1, 'MyApp', '[]', 0, '2019-09-21 16:37:31', '2019-09-21 16:37:31', '2020-09-21 13:37:31'),
('fde9f52769b3f655b5d74059c3184c3145b9003d204bea550c5ef8ba21fa90fd9419f2ae03a66bf2', 1, 1, 'MyApp', '[]', 1, '2019-08-22 03:38:51', '2019-08-22 03:38:51', '2020-08-22 00:38:51'),
('ff9b6e5ac93af7c3d492a098717658b3a86df928551e11fc50c369c23a11fe5ca5d249f7ba8333e2', 1, 1, 'MyApp', '[]', 0, '2019-08-18 06:54:45', '2019-08-18 06:54:45', '2020-08-18 03:54:45');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'sqa8n28OsqxHu0heerWuBoqwPoSYyGdRLlZiX4PG', 'http://localhost', 1, 0, 0, '2019-04-21 11:14:46', '2019-04-21 11:14:46'),
(2, NULL, 'Laravel Password Grant Client', 'PVLuvJx2uvuy6jg6wx24lmbDJmTFy1eadjZDOmMB', 'http://localhost', 0, 1, 0, '2019-04-21 11:14:46', '2019-04-21 11:14:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-04-21 11:14:46', '2019-04-21 11:14:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `users_id` bigint(20) NOT NULL,
  `orders_status_code` tinyint(4) NOT NULL,
  `purchasedFrom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IP` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_date` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `orders`
--

INSERT INTO `orders` (`id`, `order_id`, `users_id`, `orders_status_code`, `purchasedFrom`, `IP`, `order_date`, `created_at`, `updated_at`) VALUES
(1, 5, 7, 12, 'site', '127.0.0.1', '2019-10-06 23:16:53', '2019-10-07 02:16:53', '2019-11-29 03:10:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders_items`
--

CREATE TABLE `orders_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `product_qty` decimal(10,0) NOT NULL,
  `product_price` decimal(10,0) NOT NULL,
  `product_tax` decimal(10,0) DEFAULT NULL,
  `product_discount` decimal(10,0) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `orders_items`
--

INSERT INTO `orders_items` (`id`, `product_id`, `order_id`, `product_qty`, `product_price`, `product_tax`, `product_discount`, `created_at`, `updated_at`) VALUES
(2, 2, 5, '1', '500', '10', NULL, '2019-10-07 02:16:53', '2019-10-07 02:16:53'),
(3, 2, 5, '1', '500', '10', NULL, '2019-10-07 02:16:53', '2019-10-07 02:16:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders_number`
--

CREATE TABLE `orders_number` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `orders_number`
--

INSERT INTO `orders_number` (`id`, `created_at`, `updated_at`) VALUES
(4, '2019-10-07 02:12:53', '2019-10-07 02:12:53'),
(5, '2019-10-07 02:16:52', '2019-10-07 02:16:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders_payment`
--

CREATE TABLE `orders_payment` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `payment_method_id` bigint(20) NOT NULL,
  `payment_amount` double(10,2) NOT NULL,
  `payment_charge` double(10,2) NOT NULL DEFAULT '0.00',
  `payment_discount` double(10,2) NOT NULL DEFAULT '0.00',
  `verification_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `orders_payment`
--

INSERT INTO `orders_payment` (`id`, `order_id`, `payment_method_id`, `payment_amount`, `payment_charge`, `payment_discount`, `verification_time`, `created_at`, `updated_at`) VALUES
(1, 5, 2, 1020.00, 10.80, 0.00, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders_shipping`
--

CREATE TABLE `orders_shipping` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `shipping_id` bigint(20) NOT NULL,
  `ship_weight` decimal(5,2) NOT NULL,
  `ship_charge` double(6,2) NOT NULL,
  `ship_tracking_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `orders_shipping`
--

INSERT INTO `orders_shipping` (`id`, `order_id`, `shipping_id`, `ship_weight`, `ship_charge`, `ship_tracking_number`, `delivery_time`, `created_at`, `updated_at`) VALUES
(1, 5, 1, '25.80', 150.00, NULL, '30 mins', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders_status`
--

CREATE TABLE `orders_status` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `status_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `orders_status`
--

INSERT INTO `orders_status` (`id`, `status_name`, `status_description`, `created_at`, `updated_at`) VALUES
(8, 'Pending_Payment', 'Pendiente de pago', '2019-10-04 02:52:29', '2019-10-04 02:52:29'),
(9, 'Confirmed', 'Pago confirmado', '2019-10-04 02:52:29', '2019-10-04 02:52:29'),
(10, 'Processing', 'Procesando la orden', '2019-10-04 02:52:30', '2019-10-04 02:52:30'),
(11, 'Out_For_Delivery', 'Procesando la entrega', '2019-10-04 02:52:30', '2019-10-04 02:52:30'),
(12, 'Delivered', 'Entregado al cliente', '2019-10-04 02:52:30', '2019-10-04 02:52:30'),
(13, 'Attempted_Delivery', 'No se encontro el cliente en la entrega', '2019-10-04 02:52:30', '2019-10-04 02:52:30'),
(14, 'Canceled', 'Cancelado', '2019-10-04 02:52:30', '2019-10-04 02:52:30'),
(15, 'Payment_error', 'Error en el pago', '2019-10-04 02:52:30', '2019-10-04 02:52:30'),
(16, 'Payment_rejected', 'Pago rechazado', '2019-10-04 02:52:30', '2019-10-04 02:52:30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `order_status_history`
--

CREATE TABLE `order_status_history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `orders_status_code` bigint(20) NOT NULL,
  `comentarios` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `order_status_history`
--

INSERT INTO `order_status_history` (`id`, `order_id`, `user_id`, `orders_status_code`, `comentarios`, `created_at`, `updated_at`) VALUES
(1, 5, 1, 8, 'En espera de pago', '2019-10-25 03:00:00', '2019-10-25 03:00:00'),
(2, 5, 1, 11, 'Pago recibido a través del banco', '2019-10-31 03:54:45', '2019-10-31 03:54:45'),
(3, 5, 1, 12, 'Entregado a fulanito', '2019-10-31 05:23:45', '2019-10-31 05:23:45'),
(4, 5, 1, 14, 'Se cancela la operacion', '2019-10-31 05:31:18', '2019-10-31 05:31:18'),
(5, 5, 1, 11, 'test coment', '2019-10-31 05:40:20', '2019-10-31 05:40:20'),
(6, 5, 1, 10, 'test comment', '2019-10-31 05:41:29', '2019-10-31 05:41:29'),
(7, 5, 1, 11, 'testetest', '2019-10-31 05:47:11', '2019-10-31 05:47:11'),
(8, 5, 1, 11, 'testetest', '2019-10-31 05:49:58', '2019-10-31 05:49:58'),
(9, 5, 1, 8, 'ytuyrgcbcvb', '2019-10-31 05:58:54', '2019-10-31 05:58:54'),
(10, 5, 1, 9, 'gsdsdfsdhfdgdfg', '2019-10-31 06:05:16', '2019-10-31 06:05:16'),
(11, 5, 1, 10, 'csasdasdasd', '2019-11-01 03:34:55', '2019-11-01 03:34:55'),
(12, 5, 1, 11, 'rwerfwersdf', '2019-11-01 03:44:43', '2019-11-01 03:44:43'),
(13, 5, 1, 12, 'wdfsdfsdfsd', '2019-11-01 04:01:14', '2019-11-01 04:01:14'),
(14, 5, 1, 10, 'sdfsdfsdf', '2019-11-01 04:44:28', '2019-11-01 04:44:28'),
(15, 5, 1, 10, 'asdfasdf', '2019-11-01 05:02:15', '2019-11-01 05:02:15'),
(16, 5, 1, 10, 'testes', '2019-11-01 05:04:05', '2019-11-01 05:04:05'),
(17, 5, 1, 9, 'wwerwer', '2019-11-01 05:05:07', '2019-11-01 05:05:07'),
(18, 5, 1, 9, NULL, '2019-11-01 05:09:24', '2019-11-01 05:09:24'),
(19, 5, 1, 15, 'Cosas', '2019-11-28 03:16:37', '2019-11-28 03:16:37'),
(20, 5, 1, 10, 'hjhgjgjhghjg', '2019-11-28 04:46:02', '2019-11-28 04:46:02'),
(21, 5, 1, 10, 'sdfsfsdf', '2019-11-28 04:58:43', '2019-11-28 04:58:43'),
(22, 5, 1, 9, 'sdfsfdsdfsdfsdf', '2019-11-28 05:01:17', '2019-11-28 05:01:17'),
(23, 5, 1, 16, 'fwegrasdq2dasd', '2019-11-28 05:02:22', '2019-11-28 05:02:22'),
(24, 5, 1, 10, 'asdasdas', '2019-11-28 05:30:43', '2019-11-28 05:30:43'),
(25, 5, 1, 10, 'jlkjlk', '2019-11-29 02:46:11', '2019-11-29 02:46:11'),
(26, 5, 1, 12, NULL, '2019-11-29 03:10:42', '2019-11-29 03:10:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payment`
--

CREATE TABLE `payment` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `payment_method` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_charge` double(10,2) NOT NULL,
  `payment_discount` double(10,2) DEFAULT NULL,
  `verification_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enable` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `payment`
--

INSERT INTO `payment` (`id`, `payment_method`, `payment_description`, `payment_image`, `payment_charge`, `payment_discount`, `verification_time`, `enable`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'Efectivo', 'Efectivo', 'https://img.icons8.com/color/1600/cash-in-hand.png', 0.00, NULL, NULL, 1, 0, '2019-10-12 03:00:00', '2019-10-12 03:00:00'),
(2, 'Visa', 'Credito Visa', 'http://icons.iconarchive.com/icons/iconshock/credit-card/512/visa-icon.png', 10.00, NULL, NULL, 1, 0, '2019-10-12 03:00:00', '2019-10-12 03:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id` int(11) UNSIGNED NOT NULL,
  `sku` varchar(250) NOT NULL,
  `name` tinytext NOT NULL,
  `tax` double NOT NULL,
  `price` double NOT NULL,
  `stock` int(11) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `short_description` tinytext,
  `visibility` varchar(4) NOT NULL COMMENT 'm = Principal, c = Catalogo, s = busqueda, mc = Principal y Catalogo, ms = Busqueda y Principal, cs = Busqueda y Catalogo, mcs = Principal, Catalogo y Busqueda',
  `enable` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id`, `sku`, `name`, `tax`, `price`, `stock`, `category_id`, `short_description`, `visibility`, `enable`, `deleted`, `created_at`, `updated_at`) VALUES
(3, 'prueba0115', 'Producto de Prueba', 10, 500, 20, 5, 'Descripcion corta', 'm', 1, 0, '2019-11-13 04:25:10', '2019-11-23 01:15:28'),
(6, 'RemNeg01', 'Remera negra', 10, 8500, 100, 9, 'Remera negra hecha de hilos de oro', 'mcs', 1, 0, '2019-11-18 01:24:19', '2019-11-18 01:42:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_attributes`
--

CREATE TABLE `products_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `weight` double DEFAULT NULL,
  `height` double DEFAULT NULL,
  `width` double DEFAULT NULL,
  `depth` double DEFAULT NULL,
  `color0` varchar(20) DEFAULT NULL,
  `color1` varchar(20) DEFAULT NULL,
  `color2` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `products_attributes`
--

INSERT INTO `products_attributes` (`id`, `product_id`, `weight`, `height`, `width`, `depth`, `color0`, `color1`, `color2`) VALUES
(1, 3, 1200, 20, 20, 10, '#d0021b', '#417505', ''),
(3, 6, NULL, NULL, NULL, NULL, '#000000', '#7ed321', '#bd10e0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_description`
--

CREATE TABLE `products_description` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `long_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `products_description`
--

INSERT INTO `products_description` (`id`, `product_id`, `long_description`) VALUES
(1, 3, 'Descripcion Larga'),
(3, 6, 'Remera negra hecha de hilos de oro, muy costosa, seguramente nadie la compra.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_events`
--

CREATE TABLE `products_events` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `description` tinytext,
  `discount` double DEFAULT NULL,
  `date_from` datetime DEFAULT NULL,
  `date_to` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `products_events`
--

INSERT INTO `products_events` (`id`, `product_id`, `description`, `discount`, `date_from`, `date_to`, `created_at`, `updated_at`) VALUES
(1, 3, 'Nuevo', 0, '2019-01-01 00:00:00', '2019-01-01 00:00:00', '2019-11-13 04:25:22', '2019-11-23 01:15:29'),
(2, 6, 'Nuevo', 0, '2019-11-18 12:00:00', '2019-11-18 01:00:00', '2019-11-18 01:24:19', '2019-11-18 01:42:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_images`
--

CREATE TABLE `products_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `extension` varchar(5) DEFAULT NULL,
  `size` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `products_images`
--

INSERT INTO `products_images` (`id`, `product_id`, `name`, `extension`, `size`, `created_at`) VALUES
(24, 3, 'prueba0115_WJfdD', 'jpg', 2860, '2019-11-18 01:14:12'),
(27, 6, 'RemNeg01_vTd52', 'jpg', 2068, '2019-11-18 01:42:19'),
(28, 6, 'RemNeg01_FSooA', 'jpg', 4848, '2019-11-18 01:42:21'),
(29, 3, 'prueba0115_xtjcC', 'jpg', 3942, '2019-11-23 01:15:30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_seo`
--

CREATE TABLE `products_seo` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `url_key` varchar(200) DEFAULT NULL,
  `meta_keywords` varchar(200) DEFAULT NULL,
  `meta_title` varchar(200) DEFAULT NULL,
  `meta_description` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `products_seo`
--

INSERT INTO `products_seo` (`id`, `product_id`, `url_key`, `meta_keywords`, `meta_title`, `meta_description`) VALUES
(1, 3, 'Productos de prueba', 'Testing, preba, test', 'Productos de prueba', 'Cosas'),
(2, 6, 'Productos de prueba', 'Productos de prueba', 'Productos de prueba', 'Remera carisima que nadie comprara');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_stats`
--

CREATE TABLE `products_stats` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `counter` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `routes`
--

CREATE TABLE `routes` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `route` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `routes`
--

INSERT INTO `routes` (`id`, `name`, `route`) VALUES
(1, 'products_thumbs', 'img/thumbs/products/'),
(2, 'products', 'img/products/');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `shipping`
--

CREATE TABLE `shipping` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ship_method` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ship_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ship_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ship_charge` double(10,2) NOT NULL,
  `delivery_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enable` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `shipping`
--

INSERT INTO `shipping` (`id`, `ship_method`, `ship_description`, `ship_image`, `ship_charge`, `delivery_time`, `enable`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'Pedidos_Ya', 'Moto delivery', 'https://cdn6.aptoide.com/imgs/9/c/1/9c1e72ae6e0c6d5135f73a2f252ac484_icon.png', 150.00, '30 mins', 1, 0, '2019-10-11 03:00:00', '2019-10-12 03:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` datetime DEFAULT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `activation_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `active`, `activation_token`, `admin`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Prueba', 'prueba@mail.com', '2019-04-21 08:16:38', '$2y$10$ochklTYzysRX4/oqt.DPmuHYJBtmT2hBswAh2bMi2S7opBcaBNTeC', 1, '', '1', NULL, '2019-04-21 11:15:12', '2019-04-21 11:16:38', NULL),
(7, 'test', 'test@mail.com', '2019-10-06 18:00:26', '$2y$10$1h4fY2ymE5M08ThWXEb/0OoNYM2JUw0E3P39ebMmiTiyCbUiakRlu', 1, NULL, '1', NULL, '2019-10-06 21:00:26', '2019-10-06 21:00:26', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_info`
--

CREATE TABLE `user_info` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` date DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `user_info`
--

INSERT INTO `user_info` (`id`, `user_id`, `first_name`, `last_name`, `birthday`, `gender`, `created_at`, `updated_at`) VALUES
(1, 7, 'Jesus Eduardo', 'Castillo Aguilar', '1986-07-23', 'm', '2019-10-06 21:00:27', '2019-10-06 21:00:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_info_address`
--

CREATE TABLE `user_info_address` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `telephone1` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone2` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zip_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_address` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `user_info_address`
--

INSERT INTO `user_info_address` (`id`, `user_id`, `telephone1`, `telephone2`, `country`, `state`, `city`, `zip_code`, `user_address`, `billing_address`, `created_at`, `updated_at`) VALUES
(1, 7, '1127834634', '', 'Argentina', 'Buenos Aires', 'CABA', '1093', 'Av Belgrano 1265', 'Av Belgrano 1265', '2019-10-06 21:00:27', '2019-10-06 21:00:27');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `category_name_unique` (`name`);

--
-- Indices de la tabla `frase_diaria`
--
ALTER TABLE `frase_diaria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indices de la tabla `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indices de la tabla `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indices de la tabla `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indices de la tabla `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `orders_items`
--
ALTER TABLE `orders_items`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `orders_number`
--
ALTER TABLE `orders_number`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `orders_payment`
--
ALTER TABLE `orders_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `orders_shipping`
--
ALTER TABLE `orders_shipping`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `orders_status`
--
ALTER TABLE `orders_status`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `order_status_history`
--
ALTER TABLE `order_status_history`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `products_attributes`
--
ALTER TABLE `products_attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `products_description`
--
ALTER TABLE `products_description`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `products_events`
--
ALTER TABLE `products_events`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `products_images`
--
ALTER TABLE `products_images`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `products_seo`
--
ALTER TABLE `products_seo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `products_stats`
--
ALTER TABLE `products_stats`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `routes`
--
ALTER TABLE `routes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `shipping`
--
ALTER TABLE `shipping`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indices de la tabla `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user_info_address`
--
ALTER TABLE `user_info_address`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `category`
--
ALTER TABLE `category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `frase_diaria`
--
ALTER TABLE `frase_diaria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT de la tabla `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `orders_items`
--
ALTER TABLE `orders_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `orders_number`
--
ALTER TABLE `orders_number`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `orders_payment`
--
ALTER TABLE `orders_payment`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `orders_shipping`
--
ALTER TABLE `orders_shipping`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `orders_status`
--
ALTER TABLE `orders_status`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `order_status_history`
--
ALTER TABLE `order_status_history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `payment`
--
ALTER TABLE `payment`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `products_attributes`
--
ALTER TABLE `products_attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `products_description`
--
ALTER TABLE `products_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `products_events`
--
ALTER TABLE `products_events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `products_images`
--
ALTER TABLE `products_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de la tabla `products_seo`
--
ALTER TABLE `products_seo`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `products_stats`
--
ALTER TABLE `products_stats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `routes`
--
ALTER TABLE `routes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `shipping`
--
ALTER TABLE `shipping`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `user_info`
--
ALTER TABLE `user_info`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `user_info_address`
--
ALTER TABLE `user_info_address`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
