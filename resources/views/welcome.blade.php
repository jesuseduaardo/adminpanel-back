<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8" />
		<link rel="shortcut icon" href="{{ url('/favicon.ico') }}" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta name="theme-color" content="#000000" />
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<link rel="manifest" href="{{ url('/manifest.json') }}" />
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
		<title>React App</title>
    </head>
    <body>
        <noscript>You need to enable JavaScript to run this app.</noscript>
		<div id="root"></div>
		<script src="{{ url('/admin/js/app.js') }}"></script>
		<script>
		window.Laravel = { csrfToken: '{{csrf_token()}}', baseUrl: '{{url("/")}}' }
		</script>
    </body>
</html>