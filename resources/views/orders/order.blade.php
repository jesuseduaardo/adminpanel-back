<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <style>
	  body{
		font-size: 13.5px;
	  }
      .table {
        width: 100%;
        color: #525252;
        margin-bottom: 30px;
      }
      .table td {
        padding: 0px 3px;
        margin: 0px;
      }
      .table .table_header {
        padding: 6px 3px;
      }
      .table_header {
        background-color: #7e7e7e;
        color: white;
      }
      .totals {
        text-align: right;
        font-weight: bolder;
      }
      .order_header > td {
        font-weight: bolder;
        font-size: 1.1em;
        padding: 0px;
        margin: 0px;
      }
      .order_number {
        color: #c00000;
      }
	  .notas{
		  width:50% !important;
	  }
    </style>
  </head>
  <body>
    <table class="table">
      <tbody>
        <tr class="order_header">
          <td>Logo</td>
          <td>ORDEN NRO <span class="order_number">{{$order[0]->order_number}}</span></td>
        </tr>
        <tr class="order_header">
          <td>Direccion Empresa</td>
          <td>Fecha: {{$order[0]->order_date}}</td>
        </tr>
        <tr class="order_header">
          <td>Telefono</td>
          <td>Monto: {{$amounts['total']}}</td>
        </tr>
        <tr class="order_header">
          <td>Email</td>
          <td></td>
        </tr>
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr><td colspan="2">&nbsp;</td></tr>
      </tbody>
    </table>
    <table class="table">
      <tbody>
        <tr>
          <td class="table_header">Nombre Cliente</td>
          <td class="table_header">Nro Documento Identidad</td>
        </tr>
        <tr>
          <td>{{$order[0]->first_name}}  {{$order[0]->last_name}}</td>
          <td>{{$order[0]->document_number}}</td>
        </tr>
        <tr>
          <td></td>
          <td></td>
        </tr>
      </tbody>
    </table>
    <table class="table">
      <tbody>
        <tr>
          <td class="table_header">Direccion</td>
          <td class="table_header">Metodo de Pago</td>
        </tr>
        <tr>
          <td>{{$order[0]->direccion}}</td>
          <td>{{$order[0]->payment_method}}</td>
        </tr>
        <tr>
          <td></td>
          <td></td>
        </tr>
      </tbody>
    </table>
    <table class="table">
      <tbody>
        <tr>
          <td class="table_header">Producto</td>
          <td class="table_header">Cant.</td>
          <td class="table_header">Precio</td>
          <td class="table_header">Subtotal</td>
        </tr>
		@foreach($products as $product)
			<tr>
			<td>{{$product->sku}}-{{$product->name}}</td>
			<td>{{$product->product_qty}}</td>
			<td>
				{{
					number_format($product->product_price, 2)
				}}
			</td>
			<td style="text-align:right">
				{{
					number_format(
							($product->product_price) * $product->product_qty, 2
						)
				}}
			</td>
			</tr>
		@endforeach
        <tr>
          <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="3" class="totals">Subtotal</td>
          <td class="totals">{{$amounts['subtotal']}}</td>
        </tr>
        <tr>
          <td colspan="3" class="totals">Manejo y envio</td>
          <td class="totals">0</td>
        </tr>
        <tr>
          <td colspan="3" class="totals">Impuestos</td>
          <td class="totals">{{$amounts['impuesto']}}</td>
        </tr>
        <tr>
          <td colspan="3" class="totals">Descuento</td>
          <td class="totals">
           @if($amounts['descuento'] > 0 )
            - {{$amounts['descuento']}}
           @else
            {{$amounts['descuento']}}
           @endif
           </td>
        </tr>
        <tr>
          <td colspan="3" class="totals">Total</td>
          <td class="totals">{{$amounts['total']}}</td>
        </tr>
      </tbody>
    </table>
    <table class="table notas" style="margin-top:70px;">
      <tr>
        <td class="table_header">Notas</td>
      </tr>
      <tr>
        <td><b>Para efectuar un reclamo o solicitud es importante indicar el numero de orden</b></td>
      </tr>
    </table>
  </body>
</html>
