export function pad(n, z) {
   let width = 9;
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

export function decimalFormat(number) {
    if(typeof number !== 'undefined'){
        // return number.toString().split(/(?=(?:\d{3})+(?:\.|$))/g).join(".");
        const formatter = new Intl.NumberFormat("de-DE", { maximumFractionDigits: 2 });
        return formatter.format(number);
    }
};