import { CONFIRM } from './types';

export const confirmar = data => {
    return {
        type: CONFIRM,
        payload: data
    }
}