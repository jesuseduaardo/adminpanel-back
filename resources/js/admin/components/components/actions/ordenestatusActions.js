import { MOSTRAR_STATUS, ELIMINAR_STATUS, AGREGAR_STATUS, EDITAR_STATUS, REQUEST, SUCCESS, FAILURE, CONFIRM } from './types';
import { toast } from "react-toastify";
import axios from 'axios';

const server = "";

export const mostrarOrdenStatus = id => async dispatch => {
    const token = sessionStorage.getItem('token') !== 'undefined' ? sessionStorage.getItem('token') : null
    const config = {
        headers: {
            "authorization": token, //the token is a variable which holds the token
            'Content-Type': "application/json"
        },
    };
    dispatch({
        type: REQUEST,
        payload: 'inner',
    })
    try {
        const respuesta = await axios.get(`${server}/admin/ordenStatus/${id}`, config);
        dispatch({ type: SUCCESS })
        dispatch({
            type: MOSTRAR_STATUS,
            payload: respuesta.data
        })
    } catch (error) {
        dispatch({ type: FAILURE, payload: error.response.data.message })
    }
}

export const borrarOrden = id => async dispatch => {

    const token = sessionStorage.getItem('token') !== 'undefined' ? sessionStorage.getItem('token') : null
    const config = {
        headers: {
            "authorization": token, //the token is a variable which holds the token
            'Content-Type': "application/json"
        },
    };
    dispatch({
        type: REQUEST,
        payload: 'inner',
    })
    try {
        const respuesta = await axios.delete(`${server}/admin/categorias/${id}`, config);
        dispatch({ type: SUCCESS })
        dispatch({
            type: ELIMINAR_ORDEN,
            payload: respuesta.data
        });
        toast.info("Se ha borrado la categoria exitosamente!");
        return true;
    } catch (error) {
        dispatch({ type: FAILURE, payload: error.response.data.error });
        return false;
    }
}

export const agregarOrdenStatus = status => async dispatch => {

    const token = sessionStorage.getItem('token') !== 'undefined' ? sessionStorage.getItem('token') : null
    const config = {
        headers: {
            "authorization": token, //the token is a variable which holds the token
            'Content-Type': "application/json"
        },
    };
    dispatch({
        type: REQUEST,
        payload: 'inner',
    })
    try {
        const respuesta = await axios.post(`${server}/admin/ordenStatus/`, status, config);
        dispatch({ type: SUCCESS })
        dispatch({
            type: AGREGAR_STATUS,
            payload: respuesta.data
        })
        toast.info("Status actualizado exitosamente");
        return true;
    } catch (error) {
        dispatch({ type: FAILURE, payload: error.response.data.error });
        return false;
    }
}

export const editarOrden = categoria => async dispatch => {
    const token = sessionStorage.getItem('token') !== 'undefined' ? sessionStorage.getItem('token') : null
    const config = {
        headers: {
            "authorization": token, //the token is a variable which holds the token
            'Content-Type': "application/json"
        },
    };
    dispatch({
        type: REQUEST,
        payload: 'inner',
    })
    try {
        const respuesta = await axios.put(`${server}/admin/categorias/${categoria['category_id']}`, categoria, config);
        dispatch({ type: SUCCESS })
        dispatch({
            type: EDITAR_ORDEN,
            payload: respuesta.data
        })
        toast.info("categoria editado exitosamente");
        return true;
    } catch (error) {
        dispatch({ type: FAILURE, payload: error.response.data.message })
        return false;
    }
}