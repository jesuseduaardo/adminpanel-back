import { MOSTRAR_CATEGORIAS, MOSTRAR_CATEGORIA, ELIMINAR_CATEGORIA, AGREGAR_CATEGORIA, EDITAR_CATEGORIA, REQUEST, SUCCESS, FAILURE, CONFIRM } from './types';
import { toast } from "react-toastify";
import axios from 'axios';


const server = "";

export const mostrarCategorias = () => async dispatch => {
    const token = sessionStorage.getItem('token') !== 'undefined' ? sessionStorage.getItem('token') : null
    const config = {
        headers: {
            "authorization": token, //the token is a variable which holds the token
            'Content-Type': "application/json"
        },
    };
    dispatch({ 
        type: REQUEST,
        payload:'inner',
     })
    try {
        const respuesta = await axios.get(server+"/admin/categorias/", config);
        dispatch({ type: SUCCESS })
        dispatch({
            type:MOSTRAR_CATEGORIAS,
            payload: respuesta.data        
        })
    } catch (error) {
        dispatch({ type: FAILURE, payload: error.response.data.message })
        return false;
    }
}

export const mostrarCategoria = id => async dispatch => {
    const token = sessionStorage.getItem('token') !== 'undefined' ? sessionStorage.getItem('token') : null
    const config = {
        headers: {
            "authorization": token, //the token is a variable which holds the token
            'Content-Type': "application/json"
        },
    };
    dispatch({
        type: REQUEST,
        payload: 'inner',
    })
    try {
        const respuesta = await axios.get(`${server}/admin/categorias/${id}`, config);
        dispatch({ type: SUCCESS })
        dispatch({
            type: MOSTRAR_CATEGORIA,
            payload: respuesta.data
        })
    } catch (error) {
        dispatch({ type: FAILURE, payload: error.response.data.message })
    }
}

export const borrarCategoria = id => async dispatch =>{
 
    const token = sessionStorage.getItem('token') !== 'undefined' ? sessionStorage.getItem('token') : null
    const config = {
        headers: {
            "authorization": token, //the token is a variable which holds the token
            'Content-Type': "application/json"
        },
    };
    dispatch({
        type: REQUEST,
        payload: 'inner',
    })
    try {
        const respuesta =await axios.delete(`${server}/admin/categorias/${id}`, config);
        dispatch({ type: SUCCESS })
        dispatch({
            type: ELIMINAR_CATEGORIA,
            payload: respuesta.data
        });
        toast.info("Se ha borrado la categoria exitosamente!");
        return true;
    } catch (error) {
        dispatch({ type: FAILURE, payload: error.response.data.error });
        return false;
    }
}

export const agregarCategoria = categoria => async dispatch=>{
    const token = sessionStorage.getItem('token') !== 'undefined' ? sessionStorage.getItem('token') : null
    const config = {
        headers: {
            "authorization": token, //the token is a variable which holds the token
            'Content-Type': "application/json"
        },
    };
    dispatch({
        type: REQUEST,
        payload: 'inner',
    })
    try {
        const respuesta = await axios.post(`${server}/admin/categorias/`, categoria, config);
        dispatch({ type: SUCCESS })
        dispatch({
            type: AGREGAR_CATEGORIA,
            payload: respuesta.data
        })
        toast.info("Categoria creada exitosamente");
        return true;
    } catch (error) {
        dispatch({ type: FAILURE, payload: error.response.data.error });
        return false;
    }
}

export const editarCategoria = categoria => async dispatch=>{
    const token = sessionStorage.getItem('token') !== 'undefined' ? sessionStorage.getItem('token') : null
    const config = {
        headers: {
            "authorization": token, //the token is a variable which holds the token
            'Content-Type': "application/json"
        },
    };
    dispatch({
        type: REQUEST,
        payload: 'inner',
    })
    try {
        const respuesta = await axios.put(`${server}/admin/categorias/${categoria['category_id'] }`, categoria, config);
        dispatch({ type: SUCCESS })
        dispatch({
            type: EDITAR_CATEGORIA,
            payload: respuesta.data
        })
        toast.info("categoria editado exitosamente");
        return true;
    } catch (error) {
        dispatch({ type: FAILURE, payload: error.response.data.message })
        return false;
    }
}