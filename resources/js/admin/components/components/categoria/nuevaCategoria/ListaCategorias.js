import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Remove from '@material-ui/icons/Remove';

import '../categoria.css';

const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: '#fff',
    },
    nested: {
        paddingLeft: 8,
        marginLeft: 8,
    },
    paper: {
        padding: 5,
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    fixedHeight: {
        height: 125,
    },
});

class ListaCategorias extends Component {

    state = {
        'info':''
    }

    componentDidMount() {
        this.setState({
            'info':this.props.info
        }, ()=>this.makeStateList(this.state.info)
        );
    }

    makeStateList(categorias) {
        let listas = { ...this.state.listas }
        if (typeof categorias !== 'undefined') {
            if (Array.isArray(categorias) && categorias.length > 0) {
                Object.keys(categorias).map((val, i) => {
                    this.makeStateList(categorias[val])
                })
            } else {
                if (typeof categorias.subcategorias !== 'undefined') {
                    listas = { ...listas, [categorias.name]: false };
                    this.makeStateList(categorias.subcategorias)
                }
            }
        }
        this.setState({
            ...this.state,
            ...listas
        })
    }

    handleClick = name => event => {
        if (typeof this.state[name] !== 'undefined') {
            let val = this.state[name];
            this.setState({
                ...this.state,
                [name]: !val
            });
        }
    }

    handleChange = name => event =>{
        const name = event.target.name
        this.props.listItemClick(name);
    }

    imprimeSubLista(lista) {
        if (Object.keys(lista).length > 0) {
            return (
                <List component="div" className="anidado">
                    {
                        Object.keys(lista).map((categoria, index) => (
                            <React.Fragment>
                                <ListItem key={lista[index].id}
                                    button={typeof this.state[lista[index].name] !== 'undefined'}
                                    onClick={typeof this.state[lista[index].name] !== 'undefined' ? this.handleClick(lista[index].name) : ''}>
                                    <ListItemIcon>
                                        {
                                            typeof this.state[lista[index].name] !== 'undefined' ?
                                                this.state[lista[index].name] ? <ExpandLess /> : <ExpandMore />
                                                : <Remove />
                                        }
                                    </ListItemIcon>
                                    <ListItemText
                                        primary={lista[index].name}
                                        secondary="Hay 0 productos en esta categoria" />
                                    <ListItemSecondaryAction>
                                        <Checkbox
                                            checked={parseInt(this.props.categoriaPadre) === parseInt(lista[index].id)}
                                            onChange={this.handleChange()}
                                            name={lista[index].id}
                                            color="primary"
                                            inputProps={{
                                                'aria-label': 'secondary checkbox',
                                            }}
                                        />
                                    </ListItemSecondaryAction>

                                </ListItem>
                                {typeof lista[index].subcategorias !== 'undefined' ?
                                    <Collapse in={this.state[lista[index].name]} timeout="auto" unmountOnExit>
                                        {this.imprimeSubLista(lista[index].subcategorias)}
                                    </Collapse>
                                    : ''}
                            </React.Fragment>
                        ))
                    }
                </List>
            );
        }
    }

    render() {
        const { info, classes } = this.props;
        return (
            <React.Fragment>
                <ListItem key={info.id}
                    button={typeof this.state[info.name] !== 'undefined'}
                    onClick={typeof this.state[info.name] !== 'undefined' ? this.handleClick(info.name) : ''}>
                    <ListItemIcon>
                        {typeof this.state[info.name] !== 'undefined' ?
                            this.state[info.name] ? <ExpandLess /> : <ExpandMore />
                            : <Remove />}
                    </ListItemIcon>
                    <ListItemText primary={info.name} secondary="Hay 0 productos en esta categoria" />
                    <ListItemSecondaryAction>
                        <Checkbox
                            checked={ parseInt(this.props.categoriaPadre) === parseInt(info.id) }
                            name={info.id}
                            onChange={this.handleChange()}
                            color="primary"
                        />
                    </ListItemSecondaryAction>
                </ListItem>
                {typeof info.subcategorias !== 'undefined' ?
                    <Collapse in={this.state[info.name]} timeout="auto" unmountOnExit>
                        {this.imprimeSubLista(info.subcategorias)}
                    </Collapse>
                    : ''}
            </React.Fragment>
        );
    }
}

export default withStyles(styles)(ListaCategorias);