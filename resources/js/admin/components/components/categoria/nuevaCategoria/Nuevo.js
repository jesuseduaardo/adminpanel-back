import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';

import { Grid } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Remove from '@material-ui/icons/Remove';
import ListaCategoria from './ListaCategorias.js';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Checkbox from '@material-ui/core/Checkbox';
import Divider from '@material-ui/core/Divider';

import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
//Redux
import { connect } from 'react-redux';
import { mostrarCategorias, agregarCategoria, mostrarCategoria, editarCategoria } from '../../actions/categoriasActions';

const styles = theme => ({
    root: {
        width: '100%',
        padding: theme.spacing(3, 2),
    },
    button: {
        margin: theme.spacing(1),
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: '100%',
    },
    formControl: {
        margin: theme.spacing(2, 1),
        minWidth: '100%',
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
});

class Nuevo extends Component {

    state = { 
        'categoria':'',
        'categoriaPadre':0,
        'enable':true,
        'error':undefined,
        'categorias':[]
     }

    componentDidMount(props) {

        if (this.props.match.params.id !== "" && !isNaN(this.props.match.params.id)) {
            this.props.mostrarCategoria(this.props.match.params.id);
        }
        this.props.mostrarCategorias();
    }

    componentDidUpdate(prevProps, prevState) {
        if ( this.props !== prevProps) {
            if (this.props.match.params.id !== "" && !isNaN(this.props.match.params.id)) {
                const categoria = this.props.categoria;
                this.setState({
                    ...this.state,
                    'categorias': this.props.categorias,
                    'category_id': categoria.id,
                    'enable': categoria.enable,
                    'categoria': categoria.name,
                    'categoriaPadre': categoria.category_id,
                });
            }else{
                this.setState({
                    ...this.state,
                    'categorias': this.props.categorias
                });
            }
        }
    }

    handleChange = name => event => {
        let value = event.target.value;
        if (name === 'enable') {
            value = !this.state[name];
        }
        this.setState({ ...this.state, [name]: value });
    };

    listItemClick = id =>{
        this.setState({
            ...this.state, 'categoriaPadre': id
        })
    }

    handleSubmit = () =>{
        if(this.state.categoria === '') {
            return this.setState({
                ...this.state,
                'error': {
                    'mensaje': 'El campo no puede estar vacio'
                }
            })
        }
        if (this.state.categoria.length < 2){
            return this.setState({
                ...this.state,
                'error': {
                    'mensaje': 'El campo debe contener al menos 2 caracteres'
                }
            })
        }
        if(typeof this.state.category_id !== 'undefined'){
            this.props.editarCategoria(this.state);
            this.setState({ state: this.state });
        }else{
            this.props.agregarCategoria(this.state);
            this.setState({
                'categoria': '',
                'categoriaPadre': 0,
                'enable': true,
                'error': undefined,
            }, () => this.props.mostrarCategorias());
        }
        
    }

    render() { 
        const { classes } = this.props;
        const { categorias } = this.state;
        return (
            <Paper className={classes.root}>
                    <FormControlLabel
                        control={
                            <Switch
                                checked={this.state.enable === 1 || this.state.enable}
                                onChange={this.handleChange('enable')}
                                color="primary"
                            />
                        }
                        label="Habilitado"
                        style={{ margin: "auto" }}
                    />
                    <Grid container>
                        <Grid item xs={12}>
                            <TextField
                                id="standard-name"
                                label="Nombre Categoria"
                                className={classes.textField}
                                value={this.state.categoria}
                                onChange={this.handleChange('categoria')}
                                margin="normal" 
                                error={typeof this.state.error !== 'undefined'}
                                helperText={ typeof this.state.error !== 'undefined' ? this.state.error.mensaje : ''}  
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Typography variant="body1" className="paddingTop">
                                Seleccione si pertenece a otra categoria
                            </Typography>
                            <Divider variant="middle" />
                            <List component="nav">
                            <ListItem key={0}>
                                <ListItemIcon>
                                    <Remove />
                                </ListItemIcon>
                                <ListItemText primary="Principal" />
                                <ListItemSecondaryAction>
                                    <Checkbox
                                        checked={ parseInt(this.state.categoriaPadre) === 0 } 
                                        onClick={()=>this.listItemClick(0)}
                                        value={0}
                                        color="primary"
                                        
                                    />
                                </ListItemSecondaryAction>
                            </ListItem>
                            {
                                Object.keys(categorias).length > 0 ?
                                    Object.keys(categorias).map((categoria, index) => (
                                        <ListaCategoria 
                                            key={index} 
                                            info={categorias[index]} 
                                            categoriaPadre = {this.state.categoriaPadre}
                                            listItemClick={this.listItemClick } />
                                    ))
                                    : "No hay categorias para mostrar"
                            }
                            </List>
                        </Grid>
                        <Button 
                            variant="contained" 
                            color="primary" 
                            className={classes.button} 
                            onClick={()=>this.handleSubmit()}>
                            Guardar
                        </Button>
                    </Grid>
                    <ToastContainer autoClose={6000} />
            </Paper>
        );
    }
}
 

//state
const mapStateToProps = state => ({
    categorias: state.categorias.categorias,
    categoria: state.categorias.categoria
});

export default connect(mapStateToProps, { mostrarCategorias, agregarCategoria, mostrarCategoria, editarCategoria })(withStyles(styles)(Nuevo));