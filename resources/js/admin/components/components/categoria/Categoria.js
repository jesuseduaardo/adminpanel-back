import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom'; 
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Remove from '@material-ui/icons/Remove';
import BotonBorrar from '../botonBorrar/BotonBorrar';
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import './categoria.css';
import { object } from 'prop-types';

const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: '#fff',
    },
    nested: {
        paddingLeft:8,
        marginLeft: 8,
    },
    paper: {
        padding: 5,
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    fixedHeight: {
        height: 125,
    },
});

class Categoria extends Component {
    
    state = {}

    componentDidMount(){
        const categorias = this.props.info;
        this.makeStateList(categorias)
    }

    edit(id){
        this.props.history.push("/"+id);
    }

    makeStateList(categorias){
        let listas = {...this.state.listas}
        if(typeof categorias !== 'undefined'){
            if (Array.isArray(categorias) && categorias.length > 0){
                Object.keys(categorias).map((val, i) => {
                    this.makeStateList(categorias[val])
                })
            } else {
                if (typeof categorias.subcategorias !== 'undefined') {
                    listas = { ...listas, [categorias.name]: false};
                    this.makeStateList(categorias.subcategorias)
                }
            }
        }
        this.setState({
            ...this.state,
            ...listas
        })
    }

    response=(val)=>{
        this.props.borrar(val)
    }

    handleClick = name => event => {
        if(typeof this.state[name] !== 'undefined'){
            let val = this.state[name];
            this.setState({
                ...this.state,
                [name]: !val
            });
        }
    }
    imprimeSubLista(lista){
        if (Object.keys(lista).length > 0){
            return(
                <List component="div" className="anidado">
                    {
                        Object.keys(lista).map((categoria, index) => (
                        <React.Fragment>
                                <ListItem key={lista[index].id} 
                                    button={typeof this.state[lista[index].name] !== 'undefined'}
                                    onClick={typeof this.state[lista[index].name] !== 'undefined' ? this.handleClick(lista[index].name) : ''}>
                                <ListItemIcon>
                                {
                                typeof this.state[lista[index].name] !== 'undefined' ?
                                        this.state[lista[index].name] ? <ExpandLess /> : <ExpandMore />
                                        : <Remove/>
                                }
                                </ListItemIcon>
                            <ListItemText
                                primary={lista[index].name}
                                secondary="Hay 0 productos en esta categoria" />
                            <ListItemSecondaryAction>
                                <Link to={`./categorias/${lista[index].id}`}>

                                    <IconButton aria-label={`Editar categoria ${lista[index].name}` }>
                                        <EditIcon />
                                    </IconButton>
                                </Link>
                                <BotonBorrar
                                    title="Confirme Borrar..."
                                    message={`Se borrara la categoria ${lista[index].name}`}
                                    value={lista[index].id}
                                    respuesta={this.response} 
                                />
                            </ListItemSecondaryAction>
                            
                        </ListItem>
                        {typeof lista[index].subcategorias !== 'undefined' ?
                                <Collapse in={this.state[lista[index].name]} timeout="auto" unmountOnExit>
                                {this.imprimeSubLista(lista[index].subcategorias)}
                            </Collapse>
                            : ''}
                    </React.Fragment>
                    ))
                    }
                </List>
            );
        }
    }

    render() {
        const {info, classes} = this.props;
        let lista = [info];
        return (
            <React.Fragment>
                <ToastContainer autoClose={6000} />
                { this.imprimeSubLista(lista) }
            </React.Fragment>
        );
    }
}
 
export default withStyles(styles)(Categoria);