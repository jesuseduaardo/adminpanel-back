import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import AjaxRequest from '../components/ajaxRequest/AjaxRequest';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withStyles } from '@material-ui/core/styles';
import { Redirect } from "react-router-dom";
import './login.css';
//Redux
import {connect} from 'react-redux';
import { loginAction, fraseDiaria } from './actions/loginActions';


const styles = theme => ({
  root: {
    height: '100vh',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
});

class Login extends React.Component {

    state = {
        email: "",
        password: "",
        loggin:false,
        fraseDiaria:{
            imagen:'',
            frase:'',
            autor:'',
            titulo:''
        }
    }

    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit = event => {
        event.preventDefault()
        this.props.loginAction(this.state)
    }

    componentDidMount(){
        if (sessionStorage.getItem('loggedIn')==='true') {
            this.setState({
                ...this.state, loggedIn:true
            });
        }else{
           this.props.fraseDiaria();
        }
    }    

    componentDidUpdate(prevProps, prevState) {
        if (sessionStorage.getItem('loggedIn') === 'true') {
            this.setState({
                ...this.state, loggedIn: true
            });
        }
        if (this.props !== prevProps) {
            this.setState({
                fraseDiaria: this.props.frasediaria[0]
            });
        }
    }   

    render(){
        const { classes } = this.props;
        const  fraseDiaria  = { ...this.state.fraseDiaria };
        return (
            this.state.loggedIn === true ? <Redirect to="/"/> : (
            <React.Fragment> 
                <AjaxRequest request={this.props.loading} />
                <Grid container component="main" className={classes.root}>
                <CssBaseline />
                {
                    fraseDiaria.imagen!=='' ? 
                    <Grid item xs={false} sm={4} md={7} className="phrase-image" style={{ backgroundImage: `url(./../img/dailyPhrase/${fraseDiaria.imagen})` }}>
                        <div className="phrase">
                            <h1>{typeof fraseDiaria.frase !=='undefined' ? `"${fraseDiaria.frase}"` : ''}</h1>
                            <h3>{typeof fraseDiaria.autor !== 'undefined' ? `- ${fraseDiaria.autor} -`:''}</h3>
                            <h5>{fraseDiaria.titulo}</h5>
                        </div>
                    </Grid>
                    : <Grid item xs={false} sm={4} md={7} className="phrase-image">
                        <CircularProgress className="cargando" />
                      </Grid>
                    }
                <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                    <div className={classes.paper}>
                        <Avatar className={classes.avatar}>
                            <LockOutlinedIcon />
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            Sign in
                        </Typography>
                        <form className={classes.form} noValidate onSubmit={this.handleSubmit}>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="email"
                                label="Email"
                                name="email"
                                autoComplete="email"
                                autoFocus
                                value={this.state.email}
                                onChange={this.handleChange}      
                            />
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                name="password"
                                label="Contraseña"
                                type="password"
                                id="password"
                                autoComplete="current-password" 
                                value={this.state.password}
                                onChange={this.handleChange}
                            />
                            <FormControlLabel
                                control={<Checkbox value="remember" color="primary" />}
                                label="Recuerdame"
                            />
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit} 
                                disabled={this.state.email ==="" || this.state.password ===""}
                            >
                            Iniciar sesi&oacute;n
                            </Button>
                            <Grid container>
                                <Grid item xs>
                                    <Link href="#" variant="body2">
                                        Olvido su contrase&ntilde;a?
                                    </Link>
                                </Grid>
                            </Grid>
                            <Box mt={5}></Box>
                        </form>
                    </div>
                </Grid>
                </Grid>
            </React.Fragment>
            )
        );
    }
}

//state
const mapStateToProps = state =>({
    login:state.login.login,
    frasediaria:state.login.frasediaria,
    loading:state.loading
});

export default connect(mapStateToProps, { loginAction, fraseDiaria })(withStyles(styles)(Login));