import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Preview from '../producto/preview/Preview';
import Producto from '../producto/Producto';
//Redux
import { connect } from 'react-redux';
import { mostrarProductos, borrarProducto, activeProduct } from '../actions/productosActions';
import SearchInput from '../searchInput/SearchInput';

const styles = theme => ({
    paper: {
        marginTop: 90,
        width: 345,
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        overflowY: 'scroll'
    },
});

class Inventario extends Component {

    state={
        open:false,
        productoId:""
    }

    componentDidMount() {
        this.props.mostrarProductos();
    }

    componentDidUpdate(prevProps, prevState) {}

    borrar = (val) => {
        this.props.borrarProducto(val);
    }

    handleClose=()=>{
        this.setState({
            open:false
        })
    }
    handleOpen=(id)=>{
        this.setState({
            open:true,
            productoId:id
        })
    }

    render() {
        const { classes,productos } = this.props;
        return ( 

            <React.Fragment>
                <Modal
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description"
                    open={this.state.open} 
                    
                    onClose={() => { this.handleClose() }}
                    className={classes.modal}
                >
                    <div className={classes.paper}>
                        <Preview productoId = {this.state.productoId} />
                    </div>
                </Modal>
                <SearchInput/>
                {
                    Object.keys(productos).length > 0 ? 
                        Object.keys(productos).map((producto, index) => ( 
                            <Producto key={producto} 
                                info={this.props.productos[producto]} 
                                borrar={this.borrar} 
                                enable={this.props.activeProduct} 
                                preview={this.handleOpen}/>
                        ))
                    : "No hay productos para mostrar"
                }
                
            </React.Fragment>
         );
    }
}

//state
const mapStateToProps = state => ({
    productos: state.productos.productos
});
 
export default connect(mapStateToProps, { mostrarProductos, borrarProducto, activeProduct })(withStyles(styles)(Inventario));