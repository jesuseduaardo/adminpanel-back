import React, { Component } from 'react';
import { Link  } from 'react-router-dom' 
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from '../Title';
import AjaxRequest from '../ajaxRequest/AjaxRequest';
import { decimalFormat, pad } from '../../utils/NumberHandle';


import { connect } from 'react-redux';
import { mostrarOrdenes, borrarOrden, editarOrden, agregarOrden } from '../actions/ordenesActions';


const styles = theme => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
  link:{
    textDecorationLine:'none'
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
});

class Orders  extends Component {
  state = { 
    'ordenes':[]
   }

  componentDidMount() {
    this.props.mostrarOrdenes();
  }

  getTotalOrder(totalProducts, shipCharge, paymentCharge, paymentDiscount, totalDiscount, totalTax){

    let tProducts = (typeof totalProducts !== 'undefined' && totalProducts != null) 
                    ? parseFloat(totalProducts) : 0;
    let sCharge = (typeof shipCharge !== 'undefined' && shipCharge != null)
                  ? parseFloat(shipCharge) : 0;
    let pCharge = (typeof paymentCharge !== 'undefined' && paymentCharge != null)
                  ? parseFloat(paymentCharge) : 0;
    let pDiscount = (typeof paymentDiscount !== 'undefined' && paymentDiscount != null)
                  ? parseFloat(paymentDiscount) : 0;
    let tDiscount = (typeof totalDiscount !== 'undefined' && totalDiscount != null)
                  ? parseFloat(totalDiscount) : 0;
    let tTax = (typeof totalTax !== 'undefined' && totalTax != null)
                  ? parseFloat(totalTax) : 0;
    let total = decimalFormat((tProducts + sCharge + pCharge + tTax) - (pDiscount + tDiscount));
    
    return total;
  }

  render() { 
    const pattern = /ordenes/g;
    const { ordenes, location, classes } = this.props;
    const isPage = ((typeof location !== 'undefined') && pattern.test(location.pathname));
    return (
      <React.Fragment>
        <Paper className={classes.paper}>
          {this.props.loading.position !== 'inner' ? <AjaxRequest request={this.props.loading} /> : ''}
          {isPage ? <Title>Ordenes</Title> : <Title>Ordenes Recientes</Title>}
          <Table size="small">
            <TableHead>
              <TableRow>
                <TableCell>#Orden</TableCell>
                <TableCell>Fecha</TableCell>
                <TableCell>Cliente</TableCell>
                <TableCell>Status</TableCell>
                <TableCell align="right">Total</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {ordenes.map((row, i)=>
                <TableRow key={row.order_number} className={classes.link} component={Link} to={`/ordenes/orden/${row.order_number}`}>
                  <TableCell >{pad(row.order_number)}</TableCell>
                  <TableCell >{row.order_date}</TableCell>
                  <TableCell>{row.user_name}</TableCell>
                  <TableCell>{row.status_name}</TableCell>
                  <TableCell align="right">
                    {
                      this.getTotalOrder(row.total_products, row.ship_charge, row.payment_charge, row.payment_discount, row.total_discount, row.total_tax)
                    }
                  </TableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>
          {isPage ? "" : ordenes.length > 5 ? <div className={classes.seeMore}><Link to={`./ordenes`}>Ver todo</Link></div>: "" }
        </Paper>

      </React.Fragment>
    );
  }
}

//state
const mapStateToProps = state => ({
  ordenes: state.ordenes.ordenes,
  loading: state.loading,
});
 
export default connect(mapStateToProps, { mostrarOrdenes, editarOrden, borrarOrden, agregarOrden })(withStyles(styles)(Orders));