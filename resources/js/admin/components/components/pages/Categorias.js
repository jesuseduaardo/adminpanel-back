import React, { Component } from 'react';
import { fade, withStyles } from '@material-ui/core/styles';
import Categoria from '../categoria/Categoria';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
//Redux
import { connect } from 'react-redux';
import { mostrarCategorias, borrarCategoria } from '../actions/categoriasActions';

const styles = theme => ({
    root: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
    button: {
        margin: theme.spacing(1),
    },
});

class Categorias extends Component{

    state={
        'categorias':[]
    }

    componentDidMount(props) {
        this.props.mostrarCategorias()
    }
    componentDidUpdate(prevProps, prevState){
        if(this.props !== prevProps){
            this.setState({
                'categorias': this.props.categorias
            });
        }
        
    }

    borrar= (val) => {
        this.props.borrarCategoria(val);
    }

    handleClick = () => {
        this.props.history.push("./categorias/nuevo");
    }

    render(){
        const { classes } = this.props;
        const { categorias } = this.state;
        return(
            <React.Fragment>
                <Button 
                    variant="contained" 
                    color="primary" 
                    className={classes.button} 
                    onClick={this.handleClick}>
                    Nueva Categoria
                </Button>
                <List
                    component="nav"
                    className={classes.root}
                    >
                    {
                        Object.keys(categorias).length > 0 ?
                        Object.keys(categorias).map((categoria, index) => (
                            <Categoria key={index} info={categorias[index]} borrar={this.borrar} />
                            ))
                            : "No hay categorias para mostrar"
                        }
                </List>
           </React.Fragment>
        )
    }
}
//state
const mapStateToProps = state => ({
    categorias: state.categorias.categorias
});

export default connect(mapStateToProps, { mostrarCategorias, borrarCategoria })(withStyles(styles)(Categorias));