import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import PeopleIcon from '@material-ui/icons/People';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Group from '@material-ui/icons/Group';
import Contacts from '@material-ui/icons/Contacts';
import { Link } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    nested: {
        paddingLeft: theme.spacing(4),
    },
}));

export default function NestedList() {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    function handleClick() {
        setOpen(!open);
    }

    return (
        <React.Fragment>
            <ListItem button onClick={handleClick}>
                <ListItemIcon>
                    <PeopleIcon />
                </ListItemIcon>
                <ListItemText primary="Clientes" />
                {open ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            
            <Collapse in={open} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                    <Link to="/clientes/todos" className="clear-text">
                        <ListItem button className={classes.nested}>
                                <ListItemIcon>
                                    <Group />
                                </ListItemIcon>
                            <ListItemText primary="Todos los clientes" />
                        </ListItem>
                    </Link>
                    <ListItem button className={classes.nested}>
                        <ListItemIcon>
                            <Contacts />
                        </ListItemIcon>
                        <ListItemText primary="Clientes en linea" />
                    </ListItem>
                </List>
            </Collapse>
        </React.Fragment>
    );
}