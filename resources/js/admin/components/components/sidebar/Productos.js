import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Assignment from '@material-ui/icons/Assignment';
import IconClass from '@material-ui/icons/Class';
import NoteAdd from '@material-ui/icons/NoteAdd';
import { Link } from 'react-router-dom'; 

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    nested: {
        paddingLeft: theme.spacing(4),
    },
}));

export default function NestedList() {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    function handleClick() {
        setOpen(!open);
    }

    return (
        <React.Fragment>
            <ListItem button onClick={handleClick}>
                <ListItemIcon>
                    <LocalOfferIcon />
                </ListItemIcon>
                <ListItemText primary="Productos" />
                {open ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={open} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                    <Link to={`/productos/inventario/nuevo`} className="clear-text">
                        <ListItem button className={classes.nested}>
                            <ListItemIcon>
                                <NoteAdd />
                            </ListItemIcon>
                            <ListItemText primary="Nuevo Producto" />
                        </ListItem>
                    </Link> 
                    <Link to={`/productos/inventario`} className="clear-text">
                        <ListItem button className={classes.nested}>
                            <ListItemIcon>
                                <Assignment />
                            </ListItemIcon>
                            <ListItemText primary="Inventario" />
                        </ListItem>
                    </Link> 
                    <Link to={`/productos/categorias`} className="clear-text">
                        <ListItem button className={classes.nested}>
                            <ListItemIcon>
                                <IconClass />
                            </ListItemIcon>
                            <ListItemText primary="Categorias" />
                        </ListItem>
                    </Link>
                </List>
            </Collapse>
        </React.Fragment>
    );
}