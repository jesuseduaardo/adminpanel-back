import React from 'react';
import { withStyles } from '@material-ui/core/styles';
//Router
import { Route, Switch, Redirect } from 'react-router-dom';
import Breadcrumbs from './topbar/breadcrumbs/Breadcrumbs';
//EndRouter
//Pages
import Index from './pages/Index';
import Inventario from './pages/Inventario';
import Categorias from './pages/Categorias';
import NuevaCategoria from '../components/categoria/nuevaCategoria/Nuevo';
import Orders from './pages/Orders';
import Orden from '../components/ordenes/Orden';
import Clientes from './pages/Clientes';
import NuevoProducto from '../components/producto/nuevoProducto/Nuevo';
//EndPages
import TopBar from './topbar/TopBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';
import AjaxRequest from '../components/ajaxRequest/AjaxRequest';
//Redux
import { connect } from 'react-redux';
import { loginAction } from './actions/loginActions';

function MadeWithLove() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Built with love by the '}
      <Link color="inherit" href="https://material-ui.com/">
        Material-UI
      </Link>
      {' team.'}
    </Typography>
  );
}

const drawerWidth = 240;

const styles = theme => ({
  root: {
    display: 'flex',
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
    position:'relative'
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
  progressContainer:{
    width:"100%",
    height:"100%",
    position:'absolute',
    backgroundColor:'rgb(255, 255, 255, 0.5)',
    zIndex:9999,
  },
  progress: {
    position:'relative',
    top: '50%',
    left: '50%',
    margin: '-20px -20px',
  },
});


class Dashboard extends React.Component {

  state={
    login:null,
  }

  componentDidMount(){
    if (sessionStorage.getItem('loggedIn')!== 'undefined' && sessionStorage.getItem('loggedIn') === 'true') {
      this.setState({
        ...this.state,
        loggedIn: sessionStorage.getItem('loggedIn')
      })
    }
  }

  componentDidUpdate() {
    if (sessionStorage.getItem('loggedIn') === null) {
      this.setState({
        ...this.state,
        loggedIn: false
      })
    }
  }
  
render() {
  const { classes } = this.props;
  return (
        this.state.loggedIn === false ? <Redirect to="/" /> : 
        <React.Fragment>
          {this.props.loading.position !== 'inner' ? <AjaxRequest request={this.props.loading} /> : ''}
          <div className={classes.root}>
            <CssBaseline />
            <TopBar/>
            <main className={classes.content}>
              <div className={classes.appBarSpacer} />
              <Container maxWidth="lg" className={classes.container}>
                  {this.props.loading.position === 'inner' ? <AjaxRequest request={this.props.loading} /> : ''}
                  <Breadcrumbs />
                  <Switch>
                    <Route exact path="/" component={Index} />
                    <Route exact path="/productos" render={() => (<Redirect to="/productos/inventario" />)} />
                    <Route exact path="/productos/inventario" component={Inventario} />
                    <Route path="/productos/inventario/nuevo" component={NuevoProducto} />
                    <Route path="/productos/producto/:id" component={NuevoProducto} />
                    <Route exact path="/productos/categorias" component={Categorias} />
                    <Route path="/productos/categorias/nuevo" component={NuevaCategoria} />
                    <Route path="/productos/categorias/:id" component={NuevaCategoria} />
                    <Route exact path="/ordenes" component={Orders} />
                    <Route exact path="/ordenes/orden" render={() => (<Redirect to="/ordenes" />)}/>
                    <Route path="/ordenes/orden/:id" component={Orden} />
                    <Route exact path="/clientes" render={() => (<Redirect to="/clientes/todos" />)} />
                    <Route path="/clientes/todos" component={Clientes} />
                    <Route exact path="/logout" render={() => (<Redirect to="/login" />)} />
                  </Switch>
              </Container>
              <MadeWithLove />
            </main>
          </div>
        </React.Fragment>
        );
  }
}

//state
const mapStateToProps = state => ({
  login: state.login.login,
  loading: state.loading,
});

export default connect(mapStateToProps, { loginAction})(withStyles(styles)(Dashboard));