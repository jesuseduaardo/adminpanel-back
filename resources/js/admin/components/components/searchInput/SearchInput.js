import React from 'react';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import InputBase from '@material-ui/core/InputBase';
import InputAdornment from '@material-ui/core/InputAdornment';
import Search from '@material-ui/icons/Search';
import Clear from '@material-ui/icons/Clear';
import IconButton from '@material-ui/core/IconButton';
import { withStyles } from '@material-ui/core/styles';
const styles = theme => ({
    root: {
        padding: '5px',
        display: 'flex',
        alignItems: 'center',
        width: '100%',
    },
    input: {
        padding:'2px',
        marginLeft: 8,
        flex: 1,
        width:'100%'
    },
    iconButton: {
        color:'gray'
    },
    
});

class SearchInput extends React.Component {
    state = { 
        clear:false,
        search:''
     }

    searchRef = React.createRef();

    borrarBusqueda=()=>{
        this.setState({
            clear: false,
            search: ''
        })
    }
    handleChange=event=>{
        console.log(this.searchRef.value)
        if (this.searchRef.value !=='' && !this.state.clear) {
            this.setState({
                ...this.state,
                clear: true,
                search: event.target.value
            })
        }else{
            this.setState({ ...this.state, search: event.target.value });
        }
    }
    render() {
        const { classes } = this.props;
        return ( 
            <Box component="span" m={1}>
                <Paper className={classes.root}>
                <InputBase 
                    inputRef={this.searchRef}
                    className={classes.input} 
                    value={this.state.search}
                    onChange={this.handleChange}
                    placeholder="Buscar"
                    endAdornment={
                        <InputAdornment position="end" className={classes.iconButton}>
                            {this.state.clear ? 
                            <IconButton onClick={this.borrarBusqueda}>
                                <Clear />
                            </IconButton> : <Search />
                            
                        }
                        </InputAdornment>
                    }
                    aria-describedby="Escriba su busqueda"
                    inputProps={{
                        'aria-label': 'buscar',
                    }}
                />

                </Paper>
            </Box>
         );
    }
}
export default (withStyles(styles)(SearchInput));