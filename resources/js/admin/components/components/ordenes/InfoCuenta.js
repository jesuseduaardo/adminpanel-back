import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        margin: theme.spacing(0),
        padding: theme.spacing(0),
        overflowX: 'auto',
    },
    grid: {
        padding: theme.spacing(0.5),
    },
}));

const InfoCuenta = (props) => {
    const classes = useStyles();
    const rows = props.data;    
    return ( 
        
        <Paper className={classes.root}>
            <Table className={classes.table}>
                <TableHead>
                    <TableRow>
                        <TableCell colSpan={2}>Info. Cuenta</TableCell>
                    </TableRow>
                </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell className="table-header" component="th" scope="row">
                                Cliente
                            </TableCell>
                            <TableCell align="right">
                                { rows.first_name + " " +rows.last_name }
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell className="table-header" component="th" scope="row">
                                Email
                            </TableCell>
                            <TableCell align="right">
                                {rows.email}
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell className="table-header" component="th" scope="row">
                                Usuario
                            </TableCell>
                            <TableCell align="right">
                                {rows.username}
                            </TableCell>
                        </TableRow>
                    </TableBody>
            </Table>
        </Paper>
     );
}
 
export default InfoCuenta;