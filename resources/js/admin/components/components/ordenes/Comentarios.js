import React, { useState } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import Table from '@material-ui/core/Table';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        margin: theme.spacing(0),
        padding: theme.spacing(0),
        overflowX: 'auto',
    },
    grid: {
        padding: theme.spacing(0.5),
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
    button: {
        margin: theme.spacing(1),
    },
}));

const saveStatus = (props, estado, comentarios)=>{
    // console.log(estado+" "+comentarios)
    props.setOrderStatus({ 'orden': props.orderNum,'estado':estado, 'comentarios':comentarios })
}

const Comentarios = (props) => {

    const classes = useStyles();
    const [estado, setEstado] = useState("");
    const [comentarios, setComentarios] = useState("");


    const rows = props.data;
    const status = props.status;

    const inputLabel = React.useRef(null);
    const [labelWidth, setLabelWidth] = React.useState(0);
    React.useEffect(() => {
        setLabelWidth(inputLabel.current.offsetWidth);
    }, []);

    return (
        <React.Fragment>
            <Paper className={classes.root}>
                <form className={classes.root} autoComplete="off">
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell colSpan={2}>Status & Comments</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableRow>
                        <FormControl className={classes.formControl} variant="outlined">
                            <InputLabel ref={inputLabel} htmlFor="status">Status</InputLabel>
                            <Select
                                value={estado}
                                onChange={e => setEstado(e.target.value)}
                                input={<OutlinedInput labelWidth={labelWidth} 
                                name="status" 
                                id="status" />}
                            >
                                {
                                    typeof status !== 'undefined' ? 
                                        Object.keys(status).length > 0 ?
                                            Object.keys(status).map((stats, index) => (
                                                <MenuItem key={index} value={status[index].id}>{status[index].status_description}</MenuItem>
                                        ))
                                        : ""
                                    : ''
                                }
                            </Select>
                        </FormControl>
                    </TableRow>
                    <TableRow>
                        <FormControl className={classes.formControl}>
                            <TextField
                                id="comentarios"
                                name="comentarios" 
                                label="Comentarios"
                                margin="dense"
                                variant="outlined"
                                multiline
                                rows="4"
                                rowsMax="10"
                                value={comentarios}
                                onChange={e => setComentarios(e.target.value)}
                                helperText="Agregar un comentario por cada status ayudara a un mejor control de la orden"
                            />
                        </FormControl>
                    </TableRow>
                    <TableRow>
                            <Button 
                                variant="outlined" 
                                color="primary" 
                                className={classes.button}
                                onClick={e => saveStatus(props, estado, comentarios) } >
                                Guardar
                            </Button>
                    </TableRow>
                </Table>
                </form>
                <Divider variant="middle" />
                <List className={classes.root}>
                    
                    {rows.map((row, i) => (
                    <ListItem alignItems="flex-start" key={i} className="state-list">
                        <ListItemText
                            primary={
                                    <Typography variant="subtitle2" gutterBottom>
                                        {row.date + " | " + row.status_description}
                                    </Typography>    
                                    }
                            secondary={
                                <React.Fragment>
                                    <Typography
                                        component="span"
                                        variant="body2"
                                        className={classes.inline}
                                        color="textPrimary"
                                    >
                                        {row.name} - 
                                    </Typography>
                                    <Typography component="span" variant="body2" gutterBottom>
                                        { row.comentarios}
                                    </Typography>
                                </React.Fragment>
                            }
                        />
                    </ListItem>
                    ))}
                    <Divider variant="inset" component="li" />
                </List>
            </Paper>                
        </React.Fragment>
    );
}

export default Comentarios;