import React from 'react';

import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Assignment from '@material-ui/icons/Assignment'
import LocationOn from '@material-ui/icons/LocationOn'
import Payment from '@material-ui/icons/Payment'
import ShoppingCart from '@material-ui/icons/ShoppingCart'
import LocalAtm from '@material-ui/icons/LocalAtm'
import AjaxRequest from '../ajaxRequest/AjaxRequest';
import DireccionFactura from './DireccionFactura';
import DireccionCompra from './DireccionCompra';
import Comentarios from './Comentarios';
import MetodoEnvio from './MetodoEnvio';
import MetodoPago from './MetodoPago';
import ItemsOrder from './ItemsOrder';
import InfoCuenta from './InfoCuenta';
import TotalOrden from './TotalOrden';
import InfoOrden from './InfoOrden';

import { HashLink as Link } from 'react-router-hash-link';

import { pad } from '../../utils/NumberHandle';

import './orden.css';

import { connect } from 'react-redux';
import { mostrarOrden, borrarOrden, editarOrden } from '../actions/ordenesActions';
import { mostrarOrdenStatus, agregarOrdenStatus } from '../actions/ordenestatusActions';

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing(0),
        overflowX: 'auto',
    },
    panel:{
        width: '100%',
        backgroundColor: 'rgba(0, 0, 0, 0)',
        boxShadow:'none',
        padding: theme.spacing(0),
        margin: theme.spacing(0),
    },
    noPadding:{
        padding: theme.spacing(0),
        margin: theme.spacing(0),
    },
    titulo:{
        margin:theme.spacing(4.5, 0, -0.5, 0)
    },
    grid:{
        padding: theme.spacing(0, 0.5),
    },
    load:{
        position:"relative"
    }
    
});

class Orden extends React.Component{

    state = {
        'orden': []
    }

    componentDidMount(){
        if(this.props.match.params.id !== "" && !isNaN(this.props.match.params.id)){
            this.props.mostrarOrden(this.props.match.params.id);
            this.props.mostrarOrdenStatus(this.props.match.params.id);
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps !== this.props) {
            this.setState({
                'orden':{...this.props.orden},
                'ordenStatus': { ...this.props.ordenStatus},
            })
        }
    }

    setOrderStatus=(data)=>{
        this.props.agregarOrdenStatus(data);
    }

    ordersTotals(){
        let orderTotal = {};
        if(this.state.orden !== null){
            if (typeof this.state.orden.products !== 'undefined'){
                orderTotal['products'] = 0;
                orderTotal['tax'] = 0;
                if (this.state.orden.products.length > 0){
                    this.state.orden.products.map((val) => {
                        orderTotal['products'] += parseInt(val.product_qty) * parseFloat(val.product_price)
                        orderTotal['tax'] +=parseFloat(val.product_tax)
                    });
                }
            }
            if(typeof this.state.orden.order !== 'undefined'){
                orderTotal['ship_charge']= this.state.orden.order[0].ship_charge;
                orderTotal['payment_charge'] = this.state.orden.order[0].payment_charge;
            }
        }
        return orderTotal;
    }

    render(){

        const rows = this.state.orden;
        const orderTotals = this.ordersTotals();
        const {  classes } = this.props;
        return (
            <Grid container>
                <div className="navMenu">
                    <Link to="#OrderAccountInformation"
                        scroll={el => el.scrollIntoView({ behavior: "smooth", block: "end" })}>
                        <Assignment />
                    </Link>
                    <Link to="#AddressInformation"
                        scroll={el => el.scrollIntoView({ behavior: "smooth", block: "start" })}>
                        <LocationOn />
                    </Link>
                    <Link to="#PaymentShippingMethod"
                        scroll={el => el.scrollIntoView({ behavior: "smooth", block: "start" })}>
                        <Payment />
                    </Link>
                    <Link to="#ItemsOrdered"
                        scroll={el => el.scrollIntoView({ behavior: "smooth", block: "start" })}>
                        <ShoppingCart />
                    </Link>
                    <Link to="#OrderTotal"
                        scroll={el => el.scrollIntoView({ behavior: "smooth", block: "start" })}>
                        <LocalAtm />
                    </Link>
                </div>
                <div id="OrderAccountInformation" className="scrollEle"></div>
                <Grid container>
                    <Grid className={classes.grid} item xs={12}>
                        <Typography variant="h5" gutterBottom className={classes.titulo}>
                            <Assignment /> Order #
                            { (typeof rows.order !== "undefined") ? pad(rows.order[0].order_id) : "" }
                        </Typography>
                    </Grid>
                    <Grid className={classes.grid} item xs={12} md={6}>
                        <InfoOrden data={(typeof rows.order !== "undefined") ? rows.order[0] :""} />
                    </Grid>
                    <Grid className={classes.grid} item xs={12} md={6}>
                        <InfoCuenta data={(typeof rows.user !== "undefined") ? rows.user[0] : ""} />
                    </Grid>
                </Grid>
                <Typography variant="h5" gutterBottom className={classes.titulo}>
                    <LocationOn /> Address Information
                </Typography>
                <div id="AddressInformation" className="scrollEle"></div>
                <Grid container>
                    <Grid className={classes.grid} item xs={12} md={6}>
                        <DireccionFactura data={(typeof rows.user !== "undefined") ? rows.user[0] : ""} />
                    </Grid>
                    <Grid className={classes.grid} item xs={12} md={6}>
                        <DireccionCompra data={(typeof rows.user !== "undefined") ? rows.user[0] : ""} />
                    </Grid>
                </Grid>
                <Typography variant="h5" gutterBottom className={classes.titulo}>
                    <Payment /> Payment & Shipping
                </Typography>
                <div id="PaymentShippingMethod" className="scrollEle"></div>
                <Grid container>
                    <Grid className={classes.grid} item xs={12} md={6}>
                        <MetodoPago data={(typeof rows.order !== "undefined") ? rows.order[0] : ""} />
                    </Grid>
                    <Grid className={classes.grid} item xs={12} md={6}>
                        <MetodoEnvio data={(typeof rows.order !== "undefined") ? rows.order[0] : ""} />
                    </Grid>
                </Grid>

                <Grid item xs={12}>
                    <Typography variant="h5" gutterBottom className={classes.titulo}>
                        <ShoppingCart /> Items Ordered
	                </Typography>
                    <div id="ItemsOrdered" className="scrollEle"></div>
                    <ItemsOrder data={(typeof rows.products !== "undefined") ? rows.products : []} />
                </Grid>

                <Grid container>
                    <Grid className={classes.grid} item style={{position:"relative"}} xs={12} md={8}>
                        {this.props.statusloading.position === 'inner' ? <AjaxRequest request={this.props.statusloading} className={classes.load} /> : 
                            <div>
                                <Typography variant="h5" gutterBottom className={classes.titulo}>
                                    <LocalAtm /> Status & Comments 
                                </Typography>
                                <Comentarios 
                                    data={this.props.ordenStatus} 
                                    status={(typeof rows.status !== "undefined") ? rows.status : '' } 
                                    orderNum={(typeof rows.order !== "undefined") ? rows.order[0].order_id : ""} 
                                    setOrderStatus={this.setOrderStatus}
                                    />
                            </div>
                        }
                    </Grid>
                    <Grid className={classes.grid} item xs={12} md={4}>
                        <Typography variant="h5" gutterBottom className={classes.titulo}>
                            <LocalAtm /> Order Total
                        </Typography>
                        <div id="OrderTotal" className="scrollEle"></div>
                        <TotalOrden data={orderTotals} />
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}

//state
const mapStateToProps = state => ({
    orden: state.ordenes.orden,
    ordenStatus: state.ordenes.ordenStatus,
    statusloading: state.loading,
});

export default connect(mapStateToProps, { mostrarOrden, editarOrden, borrarOrden, mostrarOrdenStatus, agregarOrdenStatus })(withStyles(styles)(Orden));