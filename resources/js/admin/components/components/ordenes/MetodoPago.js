import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { decimalFormat } from '../../utils/NumberHandle';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        margin: theme.spacing(0),
        padding: theme.spacing(0),
        overflowX: 'auto',
    },
    grid: {
        padding: theme.spacing(0.5),
    }
}));

const MetodoPago = (props) => {
    const classes = useStyles();
    const rows = props.data;
    return (
        <Paper className={classes.root}>
            <Table className={classes.table}>
                <TableHead>
                    <TableRow>
                        <TableCell colSpan={2}>Pago</TableCell>
                    </TableRow>
                </TableHead>    
                    <TableBody>
                        <TableRow>
                            <TableCell component="th" className="table-header" scope="row">
                                <img src={rows.payment_image} alt={rows.payment_method} title={rows.payment_method} width="50" />
                            </TableCell>
                            <TableCell align="right">
                                {rows.payment_description}
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell component="th" className="table-header" scope="row">
                                Monto
                            </TableCell>
                            <TableCell align="right">
                                ${ decimalFormat(rows.payment_amount) }
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell component="th" className="table-header" scope="row">
                                Cargo adicional
                            </TableCell>
                            <TableCell align="right">
                                ${decimalFormat(rows.payment_charge)}
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell component="th" className="table-header" scope="row">
                                Descuento
                            </TableCell>
                            <TableCell align="right">
                                ${decimalFormat(rows.payment_discount)}
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell component="th" className="table-header" scope="row">
                                TOTAL
                            </TableCell>
                            <TableCell align="right">
                                ${decimalFormat((rows.payment_amount + rows.payment_charge) - rows.payment_discount)}
                            </TableCell>
                        </TableRow>
                    </TableBody>
            </Table>
        </Paper>
    );
}

export default MetodoPago;