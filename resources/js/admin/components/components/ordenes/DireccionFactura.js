import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        margin: theme.spacing(0),
        padding: theme.spacing(0),
        overflowX: 'auto',
    },
}));

const DireccionFactura = (props) => {
    const classes = useStyles();
    const rows = props.data;
    return (
        <Paper className={classes.root}>
            <Table className={classes.table}>
                <TableHead>
                    <TableRow>
                        <TableCell colSpan={2}>Facturacion</TableCell>
                    </TableRow>
                </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell component="th" className="table-header" scope="row">
                                Direccion
                            </TableCell>
                            <TableCell align="right">
                                {rows.billing_address}
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell component="th" className="table-header" scope="row">
                                Ciudad
                            </TableCell>
                            <TableCell align="right">
                                {rows.city}
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell component="th" className="table-header" scope="row">
                                Codigo Postal
                            </TableCell>
                            <TableCell align="right">
                                {rows.zip_code}
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell component="th" className="table-header" scope="row">
                                Pais
                            </TableCell>
                            <TableCell align="right">
                                {rows.country}
                            </TableCell>
                        </TableRow>
                    </TableBody>
            </Table>
        </Paper>
    );
}

export default DireccionFactura;