import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { decimalFormat } from '../../utils/NumberHandle';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        margin: theme.spacing(0),
        padding: theme.spacing(0),
        overflowX: 'auto',
    },
    grid: {
        padding: theme.spacing(0.5),
    },
    table:{
        borderTopStyle:"none",

    }
}));



const TotalOrden = (props) => {
    const classes = useStyles();
    const rows = props.data;
    const subtotal = parseFloat(rows.products) + parseFloat(rows.ship_charge) + parseFloat(rows.payment_charge);
    return (
    <React.Fragment>
        <Paper className={classes.root}>
                <Table>
                <TableHead>
                    <TableRow>
                        <TableCell colSpan={2}>Order Totals</TableCell>
                    </TableRow>
                </TableHead>
                    <TableBody>
                        <TableRow style={{ backgroundColor:'#dedede'}}>
                            <TableCell component="th" scope="row">
                                Productos
                        </TableCell>
                            <TableCell align="right">
                                ${decimalFormat(rows.products) }
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell component="th" scope="row">
                                Manejo & Envio
                        </TableCell>
                            <TableCell align="right">
                                ${decimalFormat(rows.ship_charge) }
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell component="th" scope="row">
                                Recargo pago
                            </TableCell>
                            <TableCell align="right">
                                ${decimalFormat(rows.payment_charge) }
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell component="th" scope="row">
                                Subtotal
                            </TableCell>
                            <TableCell align="right">
                                ${decimalFormat(subtotal) }
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell component="th" scope="row">
                                Impuestos
                            </TableCell>
                            <TableCell align="right">
                                ${decimalFormat(parseFloat(rows.tax))}
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell component="th" scope="row">
                                Descuentos
                            </TableCell>
                            <TableCell align="right">
                                ${typeof rows.discount !== "undefined" ? rows.discount : 0 }
                            </TableCell>
                        </TableRow>
                        <TableRow style={{ backgroundColor: '#dedede' }}>
                            <TableCell component="th" scope="row">
                                Total a Pagar
                            </TableCell>
                            <TableCell align="right">
                                {decimalFormat(subtotal + parseFloat(rows.tax))}
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </Paper>
        </React.Fragment>
    );
}

export default TotalOrden;