import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { decimalFormat } from '../../utils/NumberHandle';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        margin: theme.spacing(0),
        padding: theme.spacing(0),
        overflowX: 'auto',
    },
    grid: {
        padding: theme.spacing(0.5),
    }
}));

const ItemsOrder = (props) => {
    const classes = useStyles();
    const rows = props.data;
    return (
        <Grid className={classes.grid} item xs={12}>
        <Paper className={classes.root}>
            <Table className={classes.table}>
                <TableHead>
                    <TableRow>
                        <TableCell>Cant.</TableCell>
                        <TableCell>SKU</TableCell>
                        <TableCell>Producto</TableCell>
                        <TableCell>Subtotal</TableCell>
                        <TableCell>Impuestos</TableCell>
                        <TableCell>Descuentos</TableCell>
                        <TableCell>Total</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                {rows.map((row, i)=> (
                    <TableRow key={i}>
                        <TableCell component="th" scope="row">{row.product_qty}</TableCell>
                        <TableCell component="th" scope="row">{row.sku}</TableCell>
                        <TableCell component="th" scope="row">{row.name}</TableCell>
                        <TableCell component="th" scope="row">${ decimalFormat(parseFloat(row.product_price) * parseInt(row.product_qty))}</TableCell>
                        <TableCell component="th" scope="row">${decimalFormat(parseFloat(row.product_tax) * parseInt(row.product_qty))}</TableCell>
                        <TableCell component="th" scope="row">$</TableCell>
                        <TableCell component="th" scope="row">${decimalFormat((parseFloat(row.product_price) + parseFloat(row.product_tax)) * parseInt(row.product_qty))}</TableCell>
                    </TableRow>
                ))}
                </TableBody>
            </Table>
        </Paper>
        </Grid>
    );
}

export default ItemsOrder;