import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { decimalFormat } from '../../utils/NumberHandle';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        margin: theme.spacing(0),
        padding: theme.spacing(0),
        overflowX: 'auto',
    },
    grid: {
        padding: theme.spacing(0.5),
    }
}));

const MetodoEnvio = (props) => {
    const classes = useStyles();
    const rows = props.data;
    return (
        <Paper className={classes.root}>
            <Table className={classes.table}>
                <TableHead>
                    <TableRow>
                        <TableCell colSpan={2}>Envio</TableCell>
                    </TableRow>
                </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell component="th" className="table-header" scope="row">
                                <img src={rows.ship_image} alt={rows.ship_method} title={rows.ship_method} width="50" />
                            </TableCell>
                            <TableCell align="right">
                                {rows.ship_description}
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell component="th" className="table-header" scope="row">
                                Tracking number
                            </TableCell>
                            <TableCell align="right">
                                {rows.ship_tracking_number}
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell component="th" className="table-header" scope="row">
                                Peso (Kg)
                            </TableCell>
                            <TableCell align="right">
                                {rows.ship_weight }
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell component="th" className="table-header" scope="row">
                                Costo
                            </TableCell>
                            <TableCell align="right">
                                ${ decimalFormat(rows.ship_charge)}
                            </TableCell>
                        </TableRow>
                    </TableBody>
            </Table>
        </Paper>
    );
}

export default MetodoEnvio;