import React from 'react';
import { withStyles  } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import { IoIosFemale, IoIosMale, IoIosRadioButtonOff, IoIosCheckmark, IoIosClose } from "react-icons/io";
import AccountCircle from '@material-ui/icons/AccountCircle';
import MailOutline from '@material-ui/icons/MailOutline';
import PhonelinkRing from '@material-ui/icons/PhonelinkRing';
import LocationOn from '@material-ui/icons/LocationOn';
import Cake from '@material-ui/icons/Cake';
import './cliente.css';
const styles = theme => ({
    root: {
        padding: theme.spacing(1),
        margin: theme.spacing(1, 0),
    },
    grid: {
        padding: theme.spacing(0.5),
    },
    right:{
        textAlign:"right",
        paddingRight: theme.spacing(3),
    },
    textField:{
        padding: theme.spacing(0),
        marginTop: theme.spacing(2),
    }
});

class Cliente extends React.Component{

    state={
        clientes:[
            {
                'Nombre': 'Jesus Eduardo',
                'Apellido': 'Castillo Aguilar',
                'Email': 'jesuseduaardo@gmail.com',
                'Telefono': '1127834634',
                'CodigoPostal': '1093',
                'Direccion': 'Av Belgrano 1265, Monserrat',
                'EstadoProvincia':'Caba',
                'Pais': 'Argentina',
                'Registrado':'01-01-2019', 
                'Conectado': '01-02-2019',
                'Confirmado': true,
                'Cumpleanios': '23-07-1986',
                'Genero': 'm',
                'CaptadoDesde':'facebook' 
            },
            {
                'Nombre': 'Yulia Vanessa',
                'Apellido': 'Da Silva Marin',
                'Email': 'yds88@gmail.com',
                'Telefono': '1127834604',
                'CodigoPostal': '1093',
                'Direccion': 'Av Belgrano 1265',
                'EstadoProvincia': 'Caba',
                'Pais': 'Argentina',
                'Registrado': '01-01-2019',
                'Conectado': '01-02-2019',
                'Confirmado': true,
                'Cumpleanios': '02-06-1987',
                'Genero': 'f',
                'CaptadoDesde': 'web'
            },
    ]
    }
    
    
    render(){
        const { classes } = this.props;

        const customerBasicData =(listaClientes)=>{
            const clientes = listaClientes.map((valor, clave)=>
                <Paper className={classes.root}>
                    <Grid container>
                        <Grid xs={12} md={6}>
                            <ul className="customerList">
                                <li>
                                    <AccountCircle />
                                    {listaClientes[clave].Nombre + ' ' + listaClientes[clave].Apellido + ' '}
                                    <div className="gender">
                                        {(listaClientes[clave].Genero === 'm') ? <IoIosMale /> : (listaClientes[clave].Genero === 'f') ? <IoIosFemale /> : <IoIosRadioButtonOff />}
                                    </div>
                                </li>
                                <li><Cake /> {listaClientes[clave].Cumpleanios}</li>
                                <li><MailOutline /> {listaClientes[clave].Email}</li>
                                <li><PhonelinkRing /> {listaClientes[clave].Telefono}</li>
                                <li><LocationOn /> <div>{
                                    listaClientes[clave].Direccion + '. ' +
                                    listaClientes[clave].EstadoProvincia + '. ' +
                                    listaClientes[clave].Pais + '. ' +
                                    'CP:'+listaClientes[clave].CodigoPostal 
                                    }
                                    </div>
                                </li>
                            </ul>
                        </Grid>
                        <Grid xs={12} md={6}>
                            <ul className="customerList">
                                <li>Registrado: {listaClientes[clave].Registrado}</li>
                                <li>Ultima Conexion: {listaClientes[clave].Conectado}</li>
                                <li>Email Confirmado: <div className="emailConfirm">{listaClientes[clave].Confirmado ? <IoIosCheckmark /> : <IoIosClose />}</div> </li>
                                <li>Captado desde: {listaClientes[clave].CaptadoDesde}</li>
                            </ul>
                        </Grid>
                        <Grid xs={12}>
                            <ExpansionPanel className="noShadow">
                                <ExpansionPanelSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1a-content"
                                    id="panel1a-header"
                                >
                                    <Typography variant="caption">Ultimos productos visitados</Typography>
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails>
                                    <ul>
                                        <li>articulo1</li>
                                        <li>articulo2</li>
                                        <li>articulo3</li>
                                    </ul>
                                </ExpansionPanelDetails>
                            </ExpansionPanel>
                        </Grid>
                    </Grid>
                </Paper>
            )
            return clientes;
        }
        return (
            <div>
                {customerBasicData(this.state.clientes)}
            </div>
        )
    }
}

export default withStyles(styles)(Cliente)