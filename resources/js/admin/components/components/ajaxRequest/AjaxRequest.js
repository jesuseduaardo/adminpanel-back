import React, { Component } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withStyles } from '@material-ui/core/styles';
import Notificacion from '../modals/notificacion/Notificacion';


const styles = theme => ({
   progressContainer: {
        width:"100%",
        height: "100vh",
        position: 'absolute',
        top:0,
        left:0,
        zIndex: 9999,
    },
    progress: {
        position: 'relative',
        top: '50%',
        left: '50%',
        margin: '-20px -20px',
    },
});
class AjaxRequest extends Component {


    render() { 
        const { classes } = this.props;
        const request = this.props.request;
        const backgroundColor = request.position === 'inner' ? {backgroundColor:'#fff'} : {backgroundColor: 'rgb(255, 255, 255, 0.5)'};
        return ( 
            request.loading === true ? 
                <div className={classes.progressContainer} style={backgroundColor}>
                    <CircularProgress className={classes.progress} />
                </div>
            : 
                typeof request.error !== 'undefined' ?
                    <Notificacion open={request.error !== ''} title="" message={request.error}></Notificacion>    
                : ''
         );
    }
}
 
export default (withStyles(styles)(AjaxRequest));