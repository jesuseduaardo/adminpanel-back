import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles(theme => ({
    formControl: {
        margin: theme.spacing(0),
        minWidth: "100%",
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));

export default function SimpleSelect(props) {
    const classes = useStyles();
    const [cantidad, setCantidad] = React.useState(1);

    const inputLabel = React.useRef(null);
    const [labelWidth, setLabelWidth] = React.useState(0);
    React.useEffect(() => {
        setLabelWidth(inputLabel.current.offsetWidth);
    }, []);

    const handleChange = event => {
        setCantidad(event.target.value);
    };

    return (
        <FormControl variant="outlined" className={classes.formControl}>
            <InputLabel ref={inputLabel} id="demo-simple-select-outlined-label">
                Cantidad
            </InputLabel>
            <Select
                labelId="cantidad"
                id="cantidad-select"
                value={cantidad}
                onChange={handleChange}
                labelWidth={labelWidth}
                variant="outlined"
                className={classes.formControl}
            >
                {
                   
                    _.range(1, props.cantidad + 1).map((index) => (
                        <MenuItem key={index} value={index}>{index}</MenuItem >
                    ))
                       
                }
            </Select>
        </FormControl>
    );
}