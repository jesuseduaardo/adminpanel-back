import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Carousel } from 'react-responsive-carousel';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import ImageOutlined from '@material-ui/icons/ImageOutlined';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import "./preview.css"

import { connect } from 'react-redux';
import { mostrarProducto } from '../../actions/productosActions';
import SimpleSelect from './components/select/SimpleSelect';

const styles = theme => ({
    root: {
        position: 'relative',
        top:'30px'
    },
    button: {
        padding: theme.spacing(2),
        margin: theme.spacing(1),
        width: "100%"
    },
    card: {
        minWidth: 275,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: "100%",
    },
});

class Preview extends Component {
    state = { 
        'producto':'',
        'cantidad':1,
        'labelWidth':0
     }

     
    componentDidMount() {
        this.props.mostrarProducto(this.props.productoId);
        if (this.inputLabelRef.current !== null){
            this.setState({ ...this.state, 'labelWidth': this.inputLabel.current.offsetWidth })
        }
    }
    
    
    inputLabelRef = React.createRef(null);

    
    handleChange = event => {
        let value = event.target.value;
        this.setState({...this.state, 'cantidad': value});
    };

    productsImages = (pics, route) => {
        const images = pics.split(",");
        let image = <ImageOutlined className="image-icon" />;
        if (typeof images !== 'undefined') {
            image = <Carousel showArrows={true}
                showThumbs={false}
                emulateTouch={true}
                infiniteLoop={true}
                showIndicators={false}
                width="95%"
                >

                {images.map((img, index) => (
                    <div key={index}>
                        <img src={`${sessionStorage.getItem('baseurl')}${route}${img}`} alt="" />
                    </div>
                ))}
            </Carousel>;
        }
        return image;
    }

    render() { 
        const { classes, producto } = this.props;
        return (  
            <React.Fragment>
                <Grid container spacing={3} className={classes.root}>
                    <Grid item xs={12}>
                        { typeof producto[0] !== "undefined" ? 
                            <React.Fragment>
                                <Paper className="paper">
                                    {typeof producto[0].imagenes !== "undefined" ? this.productsImages(producto[0].imagenes, producto[0].thumb_route) : "" }
                                </Paper>
                                <Paper className="paper paper2">
                                    <Typography variant="body2" className="product-condition">
                                        Nuevo - 0 Vendidos
                                    </Typography>
                                    <Typography variant="h6" className="product-name" gutterBottom>
                                        {typeof producto[0].name !== "undefined" ? producto[0].name : ''}
                                    </Typography>
                                    <Typography variant="body1" className="product-price" gutterBottom>
                                        {typeof producto[0].totalPrice !== "undefined" ? "$" + producto[0].totalPrice : "" }
                                    </Typography>
                                    <Typography variant="body1" style={{ margin:"-17px 0px 25px"}}>Stock disponible</Typography>
                                    <SimpleSelect cantidad={typeof producto[0].stock !== "undefined" ? producto[0].stock : 1 } />
                                    <div className="actions">
                                        <Button variant="contained" color="primary" className={classes.button}>
                                            Comprar ahora
                                        </Button>
                                        <Button variant="outlined" color="primary" className={classes.button}>
                                            Agregar al carrito
                                        </Button>
                                    </div>
                                </Paper> 
                            </React.Fragment>
                            : "" }
                    </Grid>
                </Grid>
                
            </React.Fragment>
        );
    }
}

//state
const mapStateToProps = state => ({
    producto: state.productos.producto
});

export default connect(mapStateToProps, { mostrarProducto })(withStyles(styles)(Preview));