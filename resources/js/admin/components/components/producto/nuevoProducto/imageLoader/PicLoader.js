import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { FilePond, registerPlugin } from 'react-filepond';
import FilePondPluginImageValidateSize from 'filepond-plugin-image-validate-size';
import FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type';
import FilePondPluginFileValidateSize from 'filepond-plugin-file-validate-size';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css';
import 'filepond/dist/filepond.min.css';
import './PicLoader.css';
import Button from '@material-ui/core/Button';
//import { ToastContainer, toast } from 'react-toastify';
//import 'react-toastify/dist/ReactToastify.css';

// Register the plugins
registerPlugin(FilePondPluginFileValidateSize);
registerPlugin(FilePondPluginFileValidateType);

const styles = theme => ({
    root: {
        width: '100%',
        minHeight:'250px',
        backgroundColor: 'white',
        padding: theme.spacing(3, 2),
        margin: theme.spacing(3, 0),
        overflow:'hidden'
    },
    button: {
        margin: theme.spacing(1),
    },
    stepperButtons: {
        marginTop:'30px',
        display: "flex",
        justifyContent: "space-between",
    },
});

class PicLoader extends Component {

    state = {
    // Set initial files, type 'local' means this is a file
    // that has already been uploaded to the server (see docs)
        files: [],
    };

 
    filePondServe = {
            process: `${window.Laravel.baseUrl}/filepond/api/process`,
            load: `${window.Laravel.baseUrl}/filepond/api/load/`,
            fetch: null,
            revert: null
        };
    
    componentDidMount(props, state) {
        this.setState({
            ...this.state,
            files: this.props.images,
            nextEnable:true
        })

    }

    handleInit() {
        console.log('FilePond instance has initialised', this.pondRef);
        this.desactiveNext();
    }

    activeNext(){
        this.setState({
            ...this.state,
            nextEnable:false
        })
    }
    desactiveNext() {
        this.setState({
            ...this.state,
            nextEnable: true
        })
    }

    handleNext = () => {
        this.filesToUpload();
        /* if(data){
             this.props.handleNext();
         }*/
    }

    filesToUpload=()=>{
        const data=[]
        const files = this.pond.getFiles();
        Object.keys(files).map(function(key, file){
            files[key].file['serverId'] = files[key].serverId;
            files[key].file['extension']= files[key].fileExtension;
            files[key].file['fileName'] = files[key].filenameWithoutExtension;
            files[key].file['fileSize'] = files[key].fileSize;
            data.push(files[key].file);
        });
        this.props.filesToUpload(data);
    }
  
    render() {
        const { classes } = this.props;
        const enable =  this.state.disabled;
        registerPlugin(FilePondPluginImagePreview);
        registerPlugin(FilePondPluginImageValidateSize);
        registerPlugin(FilePondPluginFileValidateType);
        return(
            <React.Fragment>
                <div className={classes.root}>
                    <div className="picArea">
                        <FilePond 
                            labelIdle="Arrastra aquí las imagenes o haz clic para seleccionarlas desde el equipo"
                            ref={ref => this.pond = ref}
                            //files={this.props.images}
                            files={this.state.files}
                            server={this.filePondServe}
                            onupdatefiles={(fileItems) => {
                                // Set current file objects to this.state
                                this.setState({
                                    files: fileItems.map(fileItem => fileItem.file)
                                });
                            }}
                            onprocessfile = {()=>{ this.activeNext()}}
                            oninit={() => this.handleInit()}
                            //onupdatefiles={(fileItems) => { this.filesToUpload(fileItems.map(fileItem => fileItem.file)) }}
                            allowMultiple={true} 
                            maxFiles={5} 
                            maxFileSize={"100KB"} 
                            labelMaxFileSizeExceeded={"La imagen es demasiado pesada"} 
                            allowImageValidateSize={true} 
                            imageValidateSizeMinWidth={1}
                            imageValidateSizeMaxWidth={1024} 
                            imageValidateSizeMinHeight={1} 
                            imageValidateSizeMaxHeight={1024} 
                            imageValidateSizeLabelFormatError={"Tipo de imagen no soportada"} 
                            imageValidateSizeLabelImageSizeTooSmall={"La imagen es muy pequeña"} 
                            imageValidateSizeLabelImageSizeTooBig={"La imagen es muy grande"} 
                            allowFileTypeValidation={true} 
                            acceptedFileTypes={['image/*' ]} 
                            itemInsertLocation ={'after'} 
                            onprocessfilestart={() =>{ this.desactiveNext() }}
                            name="file"
                        />
                    </div>
                </div>
                <div className={classes.stepperButtons}>
                    <Button
                        disabled={this.props.step === 0 || this.state.nextEnable}
                        onClick={this.props.handleBack}
                        className={classes.backButton}
                    >
                        Atr&aacute;s
                </Button>
                    <Button 
                        variant="contained" 
                        color="primary" 
                        onClick={this.handleNext} 
                        disabled={this.state.nextEnable} 
                    >
                        {this.props.step === this.props.stepLength - 1 ? 'Guardar' : 'Siguiente'}
                    </Button>
                </div>
            </React.Fragment>
      );
  }
}
export default withStyles(styles)(PicLoader);