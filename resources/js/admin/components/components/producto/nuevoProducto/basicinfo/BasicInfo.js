import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';

import Switch from '@material-ui/core/Switch';
import NativeSelect from '@material-ui/core/NativeSelect';
import { Grid } from '@material-ui/core';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';
import FormHelperText from '@material-ui/core/FormHelperText';

//validaciones
import { valida } from '../../../../validations/validate';

//Redux
import { connect } from 'react-redux';
import { mostrarCategorias, agregarCategoria, editarCategoria } from '../../../actions/categoriasActions';

const styles = theme => ({
    root: {
        width: '100%',
        backgroundColor:'white', 
        padding: theme.spacing(3, 2),
        margin: theme.spacing(3, 0),
    },
    display: {
        display: 'block',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: '98%',
    },
    formControl: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: '98%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(18),
        fontWeight: theme.typography.fontWeightRegular,
    },
    button: {
        margin: theme.spacing(1),
    },
    iconSmall: {
        fontSize: 20,
        marginRight: theme.spacing(1),
    },
    stepperButtons: {
        display: "flex",
        justifyContent: "space-between",
    },
    chips: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip: {
        margin: 2,
    },
});

class BasicInfo extends Component {

    state = { 
        'enable': true,
        'name': '',
        'sku': '',
        'price': '',
        'tax': '',
        'qty': '',
        'stock': '',
        'peso': '',
        'categorias': '',
        'visibilidad': '',
        'nuevoDesde': '',
        'nuevoHasta': '',
        'description': '',
        'shortDescription': '',
        'errors':{},
     }

    data = {
        nombreRef : React.createRef(),
        skuRef : React.createRef(),
        precioRef : React.createRef(),
        impuestoRef : React.createRef(),
        stockRef : React.createRef(),
        categoriasRef : React.createRef(),
        visibilidadRef : React.createRef(),
        ndesdeRef : React.createRef(),
        nhastaRef : React.createRef(),
        descripcionRef : React.createRef(),
        descripcionLargaRef : React.createRef(),
    }

    validaciones = {
        name : ['notEmpty'],
        sku:['notEmpty'],
        price:['notEmpty', 'onlyNumbers'],
        tax: ['notEmpty', 'onlyNumbers'],
        qty: ['notEmpty', 'onlyNumbers'],
        stock: ['notEmpty', 'onlyNumbers'],
        categorias: ['notEmpty'],
        visibilidad: ['notEmpty'],
        shortDescription: ['notEmpty']
    }

    componentDidMount(props){
        this.props.mostrarCategorias();
         this.setState({
            ...this.props.basicInfo
         })
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props !== prevProps) {
            this.setState({
                ...this.props.basicInfo
            })
        }
    }

    setBasicInfo=()=>{
        const errors = {};
        const state = {...this.state}
        state['enable'] = this.state.enable;
        Object.keys(this.data).map(input=>{
            Object.keys(this.validaciones).map(val=>{
                if (this.data[input].current.name === val){
                    let valor = this.data[input].current.value;
                    this.validaciones[val].forEach(e => {
                        let result = valida[e](valor);
                        if(result!==null){
                            errors[this.data[input].current.name] = result;
                        }
                    });
                }
            })
            state[this.data[input].current.name] = this.data[input].current.value;
        });
        if (Object.keys(errors).length > 0){
            this.setState({
                errors
            });
            return false;
        }
        this.props.setBasicInfo(state);
        return true;
    }
    
    handleNext = () => {
        this.setBasicInfo();
       /* if(data){
            this.props.handleNext();
        }*/
    }

    handleChange = name => event => {
        let value = event.target.value;
        if (name === 'enable') {
            value = !this.state[name];
        }
        this.setState({ ...this.state, [name]: value});
    };

    getToday = () =>{
        const today = new Date();
        const dd = String(today.getDate()).padStart(2, '0');
        const mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        const yyyy = today.getFullYear();
        return dd + '/' + mm + '/' + yyyy;
    }

    getStyles(name, personName, theme) {
        return {
            fontWeight:
                personName.indexOf(name) === -1
                    ? theme.typography.fontWeightRegular
                    : theme.typography.fontWeightMedium,
        };
    }

    listaCategor=[];

    containsObject(obj, list) {
        var x;
        for (x in list) {
            if (list[x].key == obj.id) {
                return true;
            }
        }
        return false;
    }

    getMenuCategory(category){
        return(
            <option key={category.id} value={category.id}>{category.name}</option>
        );
    }

    listCategories(categorias){
        for (let index = 0; index < categorias.length; index++) {
            let element = categorias[index];
            if(!this.containsObject(element, this.listaCategor)){
                this.listaCategor.push(this.getMenuCategory(element));
            }
            if(typeof element.subcategorias !== 'undefined'){
                this.listCategories(element.subcategorias)
            }
        }
    }

    formatPrice(price){
        if(typeof price !== "undefined" && price !== null){
            const splitValWithDecimals = price.split(",");
            const entirePart = splitValWithDecimals[0].replace(".", "");
            const decimalPart = splitValWithDecimals[1];
            return entirePart +"."+decimalPart;
        }
        return price;
    }

    render() { 
        const { price, tax } = this.state;
        const { categorias, classes } = this.props;
        this.listCategories(categorias);
        return ( 
            <div className={classes.root}>
            <Grid container>
                <Grid item xs={12}>
                    <FormControlLabel
                        control={
                            <Switch
                                checked={this.state.enable === 1 || this.state.enable}
                                onChange={this.handleChange('enable')}
                                color="primary"
                            />
                        }
                        label="Habilitado"
                        style={{ margin: "auto" }}
                    />
                </Grid>
            </Grid>
            <Grid container>
                <Grid item xs={12} md={4}>
                    <TextField
                        name="sku"
                        label="Sku"
                        className={classes.textField}
                        value={this.state.sku}
                        onChange={this.handleChange('sku')}
                        inputRef={this.data.skuRef}
                        margin="normal"
                        error={ typeof this.state.errors.sku !== 'undefined' && this.state.errors.sku.error } 
                        helperText={ typeof this.state.errors.sku !== 'undefined' && this.state.errors.sku.error ? this.state.errors.sku.text : ''} 
                        required={true}
                        fullWidth
                    />
                </Grid>
                <Grid item xs={12} md={8}>
                    <TextField
                        label="Nombre"
                        name="name"
                        className={classes.textField}
                        value={this.state.name}
                        onChange={this.handleChange('name')}
                        inputRef={this.data.nombreRef}
                        margin="normal"
                        error={typeof this.state.errors.name !== 'undefined' && this.state.errors.name.error}
                        helperText={typeof this.state.errors.name !== 'undefined' && this.state.errors.name.error ? this.state.errors.name.text : ''}
                        fullWidth
                        required={true}
                    />
                </Grid>
                <Grid item xs={12} md={4}>
                    <TextField
                        name="price"
                        label="Precio"
                        className={classes.textField}
                        value={price}
                        onChange={this.handleChange('price')}
                        InputProps={{
                            startAdornment: <InputAdornment position="start">$</InputAdornment>,
                        }}
                        inputRef={this.data.precioRef}
                        margin="normal"
                        fullWidth
                        required={true}
                        error={ typeof this.state.errors.price !== 'undefined' && this.state.errors.price.error }
                        helperText={ typeof this.state.errors.price !== 'undefined' && this.state.errors.price.error ? this.state.errors.price.text : ''} 
                    />
                </Grid>
                <Grid item xs={12} md={4}>
                    <TextField
                        name="tax"
                        label="% Impuesto"
                        className={classes.textField}
                        value={tax}
                        onChange={this.handleChange('tax')}
                        InputProps={{
                            startAdornment: <InputAdornment position="start">%</InputAdornment>,
                        }}
                        inputRef={this.data.impuestoRef}
                        margin="normal"
                        fullWidth
                        required={true}
                        error={typeof this.state.errors.tax !== 'undefined' && this.state.errors.tax.error }
                        helperText={ typeof this.state.errors.tax !== 'undefined' && this.state.errors.tax.error ? this.state.errors.tax.text : ''}
                    />
                </Grid>
                <Grid item xs={12} md={4}>
                    <TextField
                        name="stock"
                        label="Stock"
                        className={classes.textField}
                        value={this.state.stock}
                        onChange={this.handleChange('stock')}
                        inputRef={this.data.stockRef}
                        margin="normal"
                        fullWidth
                        required={true}
                        error={typeof this.state.errors.stock !== 'undefined' && this.state.errors.stock.error}
                        helperText={typeof this.state.errors.stock !== 'undefined' && this.state.errors.stock.error ? this.state.errors.stock.text : ''}

                    />
                </Grid>
            </Grid>
            <Grid container>
                <Grid item xs={6}>
                    <FormControl 
                        className={classes.formControl}
                        error={typeof this.state.errors.categorias !== 'undefined' && this.state.errors.categorias.error}
                        required
                    >
                        <InputLabel htmlFor="categorias">Categorias</InputLabel>
                        <NativeSelect
                            name="categorias"
                            input={<Input id="categorias" />}
                            inputRef={this.data.categoriasRef}
                            required={true}
                            value={this.state.categorias}
                            onChange={this.handleChange('categorias')} 
                        >
                            <option value="" />
                            {
                                typeof this.listaCategor !== 'undefined' ? 
                                this.listaCategor.map((val, index)=>(
                                    this.listaCategor[index]
                                ))
                                :""
                            }
                        </NativeSelect>
                        <FormHelperText>
                            {typeof this.state.errors.categorias !== 'undefined' && this.state.errors.categorias.error
                                ? this.state.errors.categorias.text : ''}
                        </FormHelperText>
                    </FormControl>
                </Grid>
                <Grid item xs={6}>
                    <FormControl className={classes.formControl} 
                            error={typeof this.state.errors.visibilidad !== 'undefined' && this.state.errors.visibilidad.error} 
                            required
                    >
                        <InputLabel htmlFor="visibilidad">Visibilidad</InputLabel>
                        <NativeSelect
                            name="visibilidad"
                            input={<Input id="visibilidad" />} 
                            className={classes.textField} 
                            inputRef={this.data.visibilidadRef}
                            required={true} 
                            value={this.state.visibilidad } 
                            onChange={this.handleChange('visibilidad')}
                        >
                        <option value="" />
                        <option value="m">Principal</option>
                        <option value="c">Catalogo</option>
                        <option value="s">Busqueda</option>
                        <option value="mc">Principal y Catalogo</option>
                        <option value="cs">Catalogo y Busqueda</option>
                        <option value="mcs">Principal, Catalogo y Busqueda</option>
                        </NativeSelect>
                        <FormHelperText>
                            {typeof this.state.errors.visibilidad !== 'undefined' && this.state.errors.visibilidad.error ? 
                            this.state.errors.visibilidad.text : ''}
                        </FormHelperText>
                    </FormControl>
                </Grid>
            </Grid>
            <Grid container>
                <Grid item container xs={12}>
                    <Grid item xs={12} md={6}>
                        <FormControl className={classes.formControl}>
                            <TextField
                                id="nuevoDesde"
                                name="nuevoDesde"
                                label="Indicar Nuevo Desde"
                                onChange={this.handleChange('nuevoDesde')}
                                type="datetime-local"
                                value={this.state.nuevoDesde}
                                className={classes.textField}
                                InputLabelProps={{
                                    shrink: true,
                                }} 
                                inputRef={this.data.ndesdeRef} 
                            />
                        </FormControl>
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <FormControl className={classes.formControl}>
                            <TextField
                                id="nuevoHasta"
                                name="nuevoHasta"
                                label="Indicar Nuevo Hasta"
                                onChange={this.handleChange('nuevoHasta')}
                                inputRef={this.data.nhastaRef}
                                type="datetime-local"
                                value={this.state.nuevoHasta}
                                type="datetime-local"
                                className={classes.textField}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </FormControl>
                    </Grid>
                </Grid>
            </Grid>
            <Grid container>
                <Grid item xs={12}>
                    <FormControl className={classes.formControl}>
                    <TextField
                        name="shortDescription" 
                        id="shortDescription"
                        label="Descripcion Corta"
                        multiline
                        rows="2"
                        value={this.state.shortDescription}
                        onChange={this.handleChange('shortDescription')}
                        inputRef={this.data.descripcionRef}
                        className={classes.textField}
                        margin="normal"
                        fullWidth
                        required={true}
                        error={ typeof this.state.errors.shortDescription !== 'undefined' && this.state.errors.shortDescription.error}
                        helperText={ typeof this.state.errors.shortDescription !== 'undefined' && this.state.errors.shortDescription.error ? this.state.errors.shortDescription.text : ''}

                    />
                    </FormControl>
                </Grid>
                <Grid item xs={12}>
                        <FormControl className={classes.formControl}>
                    <TextField
                        name="description" 
                        id="description"
                        label="Descripcion Larga"
                        multiline
                        rows="4"
                        value={this.state.description}
                        onChange={this.handleChange('description')}
                        inputRef={this.data.descripcionLargaRef}
                        margin="normal"
                        fullWidth
                    />
                    </FormControl>
                </Grid>
            </Grid>
            <div className={classes.stepperButtons}>
                <Button
                    disabled={this.props.step === 0}
                    onClick={this.props.handleBack}
                    className={classes.backButton}
                >
                    Atr&aacute;s
                </Button>
                <Button variant="contained" color="primary" onClick={this.handleNext}>
                    {this.props.step === this.props.stepLength - 1 ? 'Guardar' : 'Siguiente'}
                </Button>
            </div>
            </div>
         );
    }
}
 
//state
const mapStateToProps = state => ({
    categorias: state.categorias.categorias
});

export default connect(mapStateToProps, { mostrarCategorias })(withStyles(styles)(BasicInfo));