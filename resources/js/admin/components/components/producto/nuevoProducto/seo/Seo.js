import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import { Grid } from '@material-ui/core';
import Button from '@material-ui/core/Button';

const styles = theme => ({
    root: {
        width: '100%',
        backgroundColor: 'white',
        padding: theme.spacing(3, 2),
        margin: theme.spacing(3, 0),
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: '98%',
    },
    formControl: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: '98%',
    },
    stepperButtons: {
        display: "flex",
        justifyContent: "space-between",
    },
});

class Seo extends Component {

    state = {
        urlKey:'',
        metaTitle:'',
        metaKeywords:'',
        metaDescription:''
    }

    data = {
        urlKeyRef: React.createRef(),
        metaTitleRef: React.createRef(),
        metaKeywordsRef: React.createRef(),
        metaDescriptionRef: React.createRef()
    }

    handleChange = name => event => {
        let value = event.target.value;
        this.setState({ ...this.state, [name]: value });
    };

    componentDidMount(props, state) {
        this.setState({
            ...this.props.seoInfo
        })
    }

    setSeoInfo = () => {
        const state = {}
        Object.keys(this.data).forEach(input => {
            let val = this.data[input].current.value;
            state[this.data[input].current.name] = val;
        });
        this.props.setSeoInfo(state);
        return true;
    }

    handleNext = () => {
        this.setSeoInfo();
    }

    render() { 
        const { classes } = this.props;
        return (
            <div className={classes.root}>
                <Grid container>
                    <Grid item xs={12}>
                        <FormControl className={classes.formControl}>
                            <TextField
                                label="Clave Url"
                                name="urlKey" 
                                className={classes.textField}
                                inputRef={this.data.urlKeyRef}
                                value={this.state.urlKey } 
                                helperText="Url con la que se mostrara el producto."
                                margin="normal" 
                                onChange={this.handleChange('urlKey')}
                                fullWidth 
                            />
                        </FormControl>
                    </Grid>
                    <Grid item xs={12}>
                        <FormControl className={classes.formControl}>
                            <TextField
                                label="Meta titulo" 
                                name="metaTitle" 
                                className={classes.textField}
                                inputRef={this.data.metaTitleRef}
                                value={this.state.metaTitle } 
                                onChange={this.handleChange('metaTitle')}
                                margin="normal"
                                helperText="Titulo que mostrara el producto en los buscadores"
                                fullWidth
                            />
                        </FormControl>
                    </Grid>
                    <Grid item xs={12}>
                        <FormControl className={classes.formControl}>
                            <TextField
                                label="Palabras Clave (Keywords)" 
                                name="metaKeywords"
                                className={classes.textField}
                                inputRef={this.data.metaKeywordsRef}
                                value={this.state.metaKeywords}
                                margin="normal"
                                onChange={this.handleChange('metaKeywords')}
                                fullWidth
                                helperText="Asociacion de palabras para el producto en buscadores"

                            />
                        </FormControl>
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            id="standard-multiline-static"
                            label="Descripcion Corta"
                            name="metaDescription"
                            multiline
                            rows="2"
                            className={classes.textField}
                            inputRef={this.data.metaDescriptionRef}
                            value={this.state.metaDescription} 
                            onChange={this.handleChange('metaDescription')}
                            margin="normal"
                            fullWidth
                            helperText="Descripcion breve que se mostrara en buscadores"
                        />
                    </Grid>
                </Grid>
                <div className={classes.stepperButtons}>
                    <Button
                        disabled={this.props.step === 0}
                        onClick={this.props.handleBack}
                        className={classes.backButton}
                    >
                        Atr&aacute;s
                    </Button>
                    <Button variant="contained" color="primary" onClick={this.handleNext}>
                        {this.props.step === this.props.stepLength - 1 ? 'Guardar' : 'Siguiente'}
                    </Button>
                </div>
            </div>
         );
    }
}
 
export default withStyles(styles)(Seo);