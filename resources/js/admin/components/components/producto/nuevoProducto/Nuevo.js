import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';

import BasicInfo from './basicinfo/BasicInfo';
import PicLoader from './imageLoader/PicLoader';
import Details from './details/Details';
import Seo from './seo/Seo';

import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

//Redux
import {connect} from 'react-redux';
import { agregarProducto, editarProducto, mostrarProducto } from '../../actions/productosActions';


const styles = theme => ({
    stepperButtons: {
        display: "flex",
        justifyContent: "space-between",
    },
});


class NuevoProducto extends Component {
    state = {
        activeStep:0,
        edition:0,
        basic: {
            'enable': true,
            'name': '',
            'sku': '',
            'price': '',
            'tax': '',
            'stock': '',
            'peso': '',
            'categorias': '',
            'visibilidad': '',
            'nuevoDesde': '',
            'nuevoHasta': '',
            'description': '',
            'shortDescription': '',
        },
        details:{
            'peso': '',
            'alto': '',
            'ancho': '',
            'profundidad': '',
            'color0': '',
            'color1': '',
            'color2': '',
        },
        images:'',
        seo: {
            'urlKey': '',
            'metaTitle': '',
            'metaKeywords': '',
            'metaDescription': '',
        },
        relatedProducts:{},
        allowCoupons: {}
     }

     baseState = this.state;
    
    activeNext(){

    }

    componentDidMount() {
        if (this.props.match.params.id !== "" && !isNaN(this.props.match.params.id)) {
            this.props.mostrarProducto(this.props.match.params.id);
        }
    }

    componentDidUpdate(prevProps, prevState){
        if (prevProps !== this.props) {
            if (this.props.match.params.id !== "" && !isNaN(this.props.match.params.id)) {
                const {producto} = this.props;
                if(producto.length > 0){
                    let basic = this.state.basic;
                    let details = this.state.details;
                    let images = [];
                    let seo = this.state.seo;
                    for (let [key, value] of Object.entries(producto[0])) {
                        if(key==='imagenes'){
                            if (typeof value !== "undefined" && value!==null){
                                images = value.split(",");
                                for (let i = 0; i < images.length; i++) {
                                    images[i] = sessionStorage.getItem('baseurl') + producto[0].thumb_route + images[i]
                                }
                            }
                        }
                        if (Object.keys(basic).indexOf(key) > -1) {
                            
                            basic = {...basic, [key]:value};
                        }
                        if (Object.keys(details).indexOf(key) > -1) {
                            
                            details = {...details, [key]:value};
                        }
                        if (Object.keys(seo).indexOf(key) > -1) {
                            seo = {...seo, [key]:value};
                        }
                }
                this.setState({
                    modified:true,
                    activeStep:0,
                    edition: producto[0].id,
                    basic,
                    details,
                    images,
                    seo
                }, () => {})
                }
            }else{
                this.setState({ ...this.baseState}, () => { })
            }
        }
    }

    getSteps=()=>{
        return ['Info Basica', 'Detalles', 'Imagenes', 'SEO'];
    }

    getStepContent=stepIndex=>{
        switch (stepIndex) {
            case 0:
                return <BasicInfo basicInfo={this.state.basic}
                            setBasicInfo={this.setBasicInfo}
                            step={this.state.activeStep} 
                            stepLength={this.getSteps().length} 
                            handleBack = {this.handleBack}
                            handleNext={this.handleNext}
                            />;
            case 1:
                return <Details detailsInfo={this.state.details} 
                            setDetailsInfo={this.setDetailsInfo} 
                            step={this.state.activeStep} 
                            stepLength={this.getSteps().length} 
                            handleBack = {this.handleBack}
                            handleNext={this.handleNext}/>
            case 2:
                return <PicLoader images={this.state.images} 
                            filesToUpload={this.setPics}
                            step={this.state.activeStep}
                            stepLength={this.getSteps().length}     
                            handleBack={this.handleBack}
                            handleNext={this.handleNext} 
                            />;
            case 3:
                return <Seo seoInfo={this.state.seo} 
                            setSeoInfo={this.setSeoInfo} 
                            step={this.state.activeStep}
                            stepLength={this.getSteps().length} 
                            handleBack={this.handleBack}
                            handleNext={this.handleNext}
                            />;
            default:
                return 'Uknown stepIndex';
        }
    }

    handleNext=()=>{
        const steps = this.getSteps();
        if (this.state.activeStep === (steps.length - 1)){
            let result = this.state.edition === 0 ? this.props.agregarProducto(this.state) : this.props.editarProducto(this.state);
            this.setState({
                ...this.state,
                activeStep: 0
            });
            //this.forceUpdate();
        }else{
            this.setState({
                ...this.state,
                activeStep: this.state.activeStep+1
            })
        }
    }

    handleBack=()=>{
        this.setState({
            ...this.state,
            activeStep: this.state.activeStep-1
        })
    }

    handleReset=()=>{
        this.setState({
            ...this.state,
            activeStep: 0
        })
    }
    

    setBasicInfo = values =>{
        this.setState({...this.state, basic : values }, ()=>{
            this.handleNext();
        });
    }

    setDetailsInfo = values => {
        this.setState({...this.state, details:values}, ()=>{
            this.handleNext();
        })
    }

    setPics = values => {
        this.setState({ ...this.state, images: values }, ()=> {
            this.handleNext();
        });
    }

    setSeoInfo = values => {
        this.setState({ ...this.state, seo: values }, () => {
            this.handleNext();
        });
    }
    
    render() {
        //const { classes } = this.props;
        const steps = this.getSteps();
        return ( 
            <React.Fragment>
                <Stepper activeStep={this.state.activeStep} alternativeLabel>
                    {steps.map(label => (
                    <Step key={label}>
                        <StepLabel>{label}</StepLabel>
                    </Step>
                    ))}
                </Stepper>
                <div>
                    {this.getStepContent(this.state.activeStep)}
                    <ToastContainer autoClose={6000} />
                </div>
                
            </React.Fragment>
        );
    }
}

//state
const mapStateToProps = state => ({
    producto: state.productos.producto
});
 
export default connect(mapStateToProps, { agregarProducto, editarProducto, mostrarProducto })(withStyles(styles)(NuevoProducto));