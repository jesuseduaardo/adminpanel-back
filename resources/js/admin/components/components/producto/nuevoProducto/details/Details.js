import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';

import { Grid } from '@material-ui/core';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import ColorPicker from './colorPicker/ColorPicker';
import "./details.css";


const styles = theme => ({
    root: {
        width: '100%',
        backgroundColor: 'white',
        padding: theme.spacing(3, 2),
        margin: theme.spacing(3, 0),
    },
    display: {
        display: 'block',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: '98%',
    },
    formControl: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: '98%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(18),
        fontWeight: theme.typography.fontWeightRegular,
    },
    button: {
        margin: theme.spacing(1),
    },
    iconSmall: {
        fontSize: 20,
        marginRight: theme.spacing(1),
    },
    stepperButtons: {
        display: "flex",
        justifyContent: "space-between",
    },
    chips: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip: {
        margin: 2,
    },
});

class Details extends Component {

    state = {
        'ancho': '',
        'alto': '',
        'profundidad': '',
        'peso': '',
        'color0': '',
        'color1': '',
        'color2': '',
    }

    componentDidMount(props) {
        this.setState({
            'ancho': this.props.detailsInfo.ancho,
            'alto': this.props.detailsInfo.alto,
            'profundidad': this.props.detailsInfo.profundidad,
            'peso': this.props.detailsInfo.peso,
            'color0': this.props.detailsInfo.color0 !== "" ? { 'hex': this.props.detailsInfo.color0 } : "",
            'color1': this.props.detailsInfo.color1 !== "" ? { 'hex': this.props.detailsInfo.color1 } : "",
            'color2': this.props.detailsInfo.color2 !== "" ? { 'hex': this.props.detailsInfo.color2 } : "",
        })
    }

    componentDidUpdate(prevProps) {
        if (this.props !== prevProps) {
            this.setState({
                ...this.props.detailsInfo,
                'color0': this.props.detailsInfo.color0 !== "" ? { 'hex': this.props.detailsInfo.color0 } : "",
                'color1': this.props.detailsInfo.color1 !== "" ? { 'hex': this.props.detailsInfo.color1 } : "",
                'color2': this.props.detailsInfo.color2 !== "" ? { 'hex': this.props.detailsInfo.color2 } : "",
            })
        }
    }

    handleChange = name => event => {
        let value = event.target.value;
        this.setState({ ...this.state, [name]: value});
    };

    setColor = data => {
        switch (data.name) {
            case "color0":
                if(this.state.color0 !== data['color']){
                    this.setState({ ...this.state, "color0": data['color'] });
                }
                //this.data.color0Ref = { name:"color0", value: data['color'] };
                break;
            case "color1":
                if (this.state.color1 !== data['color']) {
                    this.setState({ ...this.state, "color1": data['color'] });
                }
                break;
            case "color2":
                if (this.state.color2 !== data['color']) {
                    this.setState({ ...this.state, "color2": data['color'] });
                }
                break;
            default:

        }
    }

    setDetailsInfo = () => {
        const {color0, color1, color2 } = this.state;
        const pseudostate = this.state;
        pseudostate['color0'] = color0.hex;
        pseudostate['color1'] = color1.hex;
        pseudostate['color2'] = color2.hex;
        this.props.setDetailsInfo(pseudostate)
    }

    handleNext = () => {
        this.setDetailsInfo();
    }

    render() { 
        const {classes } = this.props;

        return (
            <div className={classes.root}>
                <Grid container>
                    <Grid item xs={12} md={6}>
                        <TextField
                            name="ancho"
                            label="Ancho"
                            type="number" 
                            className={classes.textField}
                            value={this.state.ancho}
                            onChange={this.handleChange('ancho')}
                            
                            InputProps={{
                                endAdornment: <InputAdornment position="end">cm</InputAdornment>,
                            }}
                            margin="normal"
                            helperText="Valores expresados en centimetros"
                        />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <TextField
                            name="alto"
                            label="Alto"
                            type="number" 
                            className={classes.textField}
                            value={this.state.alto}
                            onChange={this.handleChange('alto')}
                            
                            InputProps={{
                                endAdornment: <InputAdornment position="end">cm</InputAdornment>,
                            }}
                            margin="normal"
                            helperText="Valores expresados en centimetros"
                        />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <TextField
                            name="profundidad"
                            label="Profundidad"
                            type="number" 
                            className={classes.textField}
                            value={this.state.profundidad}
                            onChange={this.handleChange('profundidad')}
                            
                            InputProps={{
                                endAdornment: <InputAdornment position="end">cm</InputAdornment>,
                            }}
                            margin="normal"
                            helperText="Valores expresados en centimetros"
                        />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <TextField
                            name="peso"
                            label="Peso"
                            type="number" 
                            className={classes.textField}
                            value={this.state.peso}
                            onChange={this.handleChange('peso')}
                            
                            InputProps={{
                                endAdornment: <InputAdornment position="end">g</InputAdornment>,
                            }}
                            margin="normal"
                            helperText="Valores expresados en gramos"
                        />
                    </Grid>
                </Grid>
                <Grid container className="paddingTop">
                    <Grid item xs={12} md={4}>
                        <FormControlLabel
                            control={<ColorPicker hex={ this.state.color0 !== "" ? this.state.color0 : "" } name="color0" saveColor={this.setColor} />}
                            label="Color 1"
                            labelPlacement="top"
                        />
                    </Grid>
                    <Grid item xs={12} md={4}>
                        <FormControlLabel
                            control={<ColorPicker hex={this.state.color1 !== "" ? this.state.color1 : "" } name="color1" saveColor={this.setColor} />}
                            label="Color 2"
                            labelPlacement="top"
                        />
                    </Grid>
                    <Grid item xs={12} md={4}>
                        <FormControlLabel
                            control={<ColorPicker hex={this.state.color2 !== "" ? this.state.color2 : "" } name="color2" saveColor={this.setColor} />}
                            label="Color 3"
                            labelPlacement="top"
                        />
                    </Grid>
                </Grid>
                <div className={classes.stepperButtons}>
                    <Button
                        disabled={this.props.step === 0}
                        onClick={this.props.handleBack}
                        className={classes.backButton}
                    >
                        Atr&aacute;s
                </Button>
                    <Button variant="contained" color="primary" onClick={this.handleNext}>
                        {this.props.step === this.props.stepLength - 1 ? 'Guardar' : 'Siguiente'}
                    </Button>
                </div>
            </div>
        );
    }
}
 
export default withStyles(styles)(Details);