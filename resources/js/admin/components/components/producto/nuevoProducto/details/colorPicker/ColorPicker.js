import React from 'react'
import { SketchPicker, ChromePicker } from 'react-color';
import './colorpicker.css';

class ColorPicker extends React.Component {

    
    state = {
        color:{hex: ""},
        name:""
    }

    componentDidMount(props) {
        this.setState({
            color: {hex:this.props.hex},
            name: this.props.name
        })
    }

    componentDidUpdate(prevProps) {
        if (this.props !== prevProps) {
            this.setState({
                color: this.props.hex,
                name: this.props.name
            })
        }
    }

    handleChangeComplete = (color) => {
        this.setState({ color: color });
        this.saveColor();
    };

    saveColor(){
        this.props.saveColor(this.state)
    }
   
    render(){
        return (
                <div className="colorContainer">
                    <SketchPicker color={this.state.color.hex} onChangeComplete={this.handleChangeComplete} />
                    <div className="code-color">
                        {
                        typeof this.state.color.hex !== "undefined" && this.state.color.hex !=="" ? 
                                <div>
                                <span style={{ backgroundColor: this.state.color.hex }}></span>
                                {this.state.color.hex}
                                <button title="Eliminar color" onClick={() => { this.setState({ ...this.state, color: { hex: "" }}) }}>x</button> 
                                </div> :
                            "No hay color seleccionado"
                        }
                    </div>
                </div>
            );
    }
}
export default ColorPicker;