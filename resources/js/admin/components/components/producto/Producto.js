import React, { Component } from 'react';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom'; 

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ImageOutlined from '@material-ui/icons/ImageOutlined';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import BotonBorrar from '../botonBorrar/BotonBorrar';
import Button from '@material-ui/core/Button';
import './producto.css';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";

const styles = theme => ({
    root: {
        width: '100%',
        flexGrow: 1,
    },
    
    paper: {
        padding: 8,
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
        margin:2
    },
    fixedHeight: {
        minHeight: 120,
    },
    button:{
        color:'white',
        margin:'20px 5px 5px 5px',
        width: 35,
        height: 35,
    },
    colorColumnName:{
        color:"#bfbfbf"
    },
    editButton:{
        background: 'linear-gradient(to right, rgba(73,155,234,1) 0%, rgba(32,124,229,1) 100%)',
    },
    deleteButton:{
        background: ' linear-gradient(to right, rgba(234,72,75,1) 0%, rgba(229,38,31,1) 100%)',
    }
});
class Producto extends Component {
    
    
    state={
        'enable':false
    }

    producto = this.props.info;
    
    componentDidMount(){
        this.setState({
            ...this.state, 
            ...this.producto,
            'enable': (this.producto.enable === 1) ? true:false,
        })
    }

    handleChange = name => event => {
        let value = event.target.value;
        if (name === 'enable') {
            value = !this.state[name];
            this.props.enable(event.target.id, !this.state[name]);
        }
        this.setState({ ...this.state, [name]: value });
    }

    response = (val) => {
        this.props.borrar(val)
    }

    productsImages=(pics, route)=>{
        let image = <ImageOutlined className="image-icon"/>;
        if(pics){
            let images;
            const patt = /,/g;
            if(patt.test(pics)){
                images  = pics.split(",");
            }else{
                images=[pics]
            }
            if (typeof images !== 'undefined') {
                image = <Carousel   showArrows={true} 
                                    showThumbs={false} 
                                    emulateTouch={true} 
                                    infiniteLoop={true} 
                                    showIndicators={false} 
                                    width="75%">
                                       
                    {images.map((img, index) => (
                        <div key={index}>
                            <img src={`${sessionStorage.getItem('baseurl')}${route}${img}`} alt="" />
                        </div>
                    ))}
                </Carousel>;
            }
            return image;
        }
        
    }
    
    render() {
        const producto = this.producto;
        const { classes } = this.props
        const fixedHeightPaper = clsx(this.props.classes.paper, this.props.classes.fixedHeight);
        return ( 
            <Paper className={fixedHeightPaper}>
                <Grid container>
                    <Grid item xs={12} md={3} style={{ textAlign: 'center', paddingRight:'20px' }}>
                        {this.productsImages(this.producto.imagenes, this.producto.thumb_route)}
                    </Grid>
                    <Grid item xs={12} md={7}>
                        <Typography variant="button">{producto.name}</Typography>
             
                        <List style={{ padding: 0 }}>
                            <ListItem style={{ padding: 0 }}>
                                <ListItemText
                                    primary={<Typography className={classes.colorColumnName} variant="caption">SKU</Typography> }
                                    secondary={ <Typography variant="body1">{producto.sku}</Typography> }
                                />
                                <ListItemText
                                    primary={<Typography className={classes.colorColumnName} variant="caption">CATEGORIA</Typography>}
                                    secondary={<Typography variant="body1">{producto.categoria}</Typography>}
                                />
                                <ListItemText
                                    primary={<Typography className={classes.colorColumnName} variant="caption">PRECIO</Typography>}
                                    secondary={<Typography variant="body1">{"$" + producto.price}</Typography>}
                                />
                                <ListItemText
                                    primary={<Typography className={classes.colorColumnName} variant="caption">CANTIDAD</Typography>}
                                    secondary={<Typography variant="body1">{producto.stock}</Typography>}
                                />
                            </ListItem>
                        </List>
                    </Grid>
                    <Grid item xs={12} md={2} style={{textAlign:'center'}}>
                        <FormControlLabel
                            control={
                                <Switch
                                    checked={this.state.enable} 
                                    id={toString(producto.id)}
                                    onChange={this.handleChange('enable')}
                                    color="primary"
                                />
                            }
                            label={this.state.enable ? "Habilitado" : "Inhabilitado"} 
                            labelPlacement="top"
                        />
                        <div style={{
                                display: 'flex',
                                justifyContent:'center' 
                            }}>

                            <Link to={`./producto/${producto.id}`}>
                                <IconButton aria-label={`Editar producto ${producto.name}` }>
                                    <EditIcon />
                                </IconButton>
                            </Link>
                            <BotonBorrar
                                title="Confirme Borrar..."
                                message={ "Se borrara el producto \n"+producto.sku+"-"+producto.name }
                                value={producto.id}
                                respuesta={this.response} 
                            />
                        </div>
                    </Grid>
                    <Grid item xs={12} md={3} style={{ textAlign: 'center'}}>
                        <Button color="primary" onClick={()=>{this.props.preview(producto.id)}}>Vista Previa</Button>
                    </Grid>
                    
                </Grid>
            </Paper >
         );
    }
}


 
export default withStyles(styles)(Producto);