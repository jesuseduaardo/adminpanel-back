import { MOSTRAR_ORDENES, MOSTRAR_ORDEN, ELIMINAR_ORDEN, AGREGAR_ORDEN, 
         EDITAR_ORDEN, MOSTRAR_STATUS, ELIMINAR_STATUS, AGREGAR_STATUS, 
         EDITAR_STATUS } from '../actions/types';

//cada reducer tiene su propio state
const initialState = {
    ordenes: [],
    orden: [],
    ordenStatus:[]
}

export default function (state = initialState, action) {
    switch (action.type) {
        case MOSTRAR_ORDENES:
            return {
                ...state,
                ordenes: action.payload
            }
        case ELIMINAR_ORDEN:
            return {
                ...state,
                ordenes: action.payload
            }
        case AGREGAR_ORDEN:
            return {
                ...state,
                ordenes: action.payload
            }
        case MOSTRAR_ORDEN:
            return {
                ...state,
                orden: action.payload
            }
        case EDITAR_ORDEN:
            return {
                ...state,
                orden: action.payload[0]
            }
        case MOSTRAR_STATUS:
            return {
                ...state,
                ordenStatus: action.payload
            }
        case ELIMINAR_STATUS:
            return {
                ...state,
                ordenStatus: action.payload[0]
            }
        case AGREGAR_STATUS:
            return {
                ...state,
                ordenStatus: action.payload
            }
        case EDITAR_STATUS:
            return {
                ...state,
                ordenStatus: action.payload[0]
            }
        default:
            return state;
    }
}