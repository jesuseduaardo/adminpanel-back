import { combineReducers } from 'redux';
import productosReducer from './productosReducers';
import categoriasReducer from './categoriasReducers';
import loginReducer from './loginReducer';
import ajaxReducer from './ajaxReducer';
import orderReducer from './orderReducer';
import modalReducer from './modalReducer';

export default combineReducers({
    productos: productosReducer,
    categorias: categoriasReducer,
    ordenes: orderReducer,
    login: loginReducer,
    loading:ajaxReducer,
    modal:modalReducer
});