import { REQUEST, SUCCESS, FAILURE } from '../actions/types';

const initialState = {
  loading: false,
  position:''
}
export default function(state = initialState, action){
    switch (action.type) {
      case REQUEST:
        return {
          loading: true,
          position: action.payload,
          }
      case SUCCESS:
        return{
          ...state,
          loading: false,
          position: '',
          mensaje: action.payload
        }
      case FAILURE:
        return {
          ...state,
          loading: false,
          position: '',
          error: action.payload
        }
      default:
        return state;
    }
}