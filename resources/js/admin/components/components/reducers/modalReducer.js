import { CONFIRM } from '../actions/types';


const initialState = {
    'modal': [],
    'confirm':{ 
        'open':false,
        'titulo':'',
        'mensaje':'',
        'callback':'',
        'params':''
     }
}

export default function (state = initialState, action) {
    switch (action.type) {
        case CONFIRM:
            return {
                ...state,
                confirm: action.payload
            }
        default:
            return state;
    }
}