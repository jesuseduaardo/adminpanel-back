import React from 'react';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import IconButton from '@material-ui/core/IconButton';
//import Confirm from '../../modal/Confirm';

//redux
import {connect} from 'react-redux';
import { logoutAction } from '../../actions/loginActions';

class Profile extends React.Component {
    state = { 
        open:false,
     }

    anchorRef = React.createRef();


    logout = (props) => {
        /*this.setState({
            ...this.state,
            modalOpen:true,
            modalTitulo:"Cerrar Sesion",
            modalMensaje:"Desea cerrar la sesion?"
        });*/

        this.props.logoutAction();
    }

    handleToggle = () => {
        this.setState({
            ...this.state, open:!this.state.open
        })
    }
    handleClose = event => {
        if (this.anchorRef.current && this.anchorRef.current.contains(event.target)) {
            return;
        }
    }

    render(){
    return (
        <React.Fragment>
            <IconButton color="inherit" ref={this.anchorRef}
                aria-controls="menu-list-grow"
                aria-haspopup="true"
                onClick={this.handleToggle}>
                <AccountCircleIcon />
            </IconButton>
            <Popper open={this.state.open} anchorEl={this.anchorRef.current} keepMounted transition disablePortal>
                {({ TransitionProps, placement }) => (
                    <Grow
                        {...TransitionProps}
                        style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom', display: this.state.open ? 'block' : 'none' }}
                    >
                        <Paper id="menu-list-grow">
                            <ClickAwayListener onClickAway={this.handleClose}>
                                <MenuList>
                                    <MenuItem onClick={this.handleClose}>Profile</MenuItem>
                                    <MenuItem onClick={this.handleClose}>My account</MenuItem>
                                    <MenuItem onClick={ this.logout }>Logout</MenuItem>
                                </MenuList>
                            </ClickAwayListener>
                        </Paper>
                    </Grow>
                )}
            </Popper>
            {/*<Confirm open={this.modalOpen}/>*/}
        </React.Fragment>
        );
    }
}

export default connect(null, { logoutAction })(Profile);