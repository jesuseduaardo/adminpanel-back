import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import { Link } from "react-router-dom";
import withBreadcrumbs from "react-router-breadcrumbs-hoc";
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Home from '@material-ui/icons/Home';

const useStyles = makeStyles(theme => ({
    root: {
        justifyContent: 'center',
        flexWrap: 'wrap',
    },
    paper: {
        padding: theme.spacing(1, 2),
        margin:theme.spacing(-2,0,1,0),
    },
    link:{
        textDecorationLine:'none',
    },
    text:{
        color: '#6f6d6d'
    },
    home:{
        color: '#6f6d6d',
        float: 'left',
        fontSize: '20px',
        marginRight: theme.spacing(0.5),
    }
}));

function UiBreadcrumbs({breadcrumbs}){
    
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Paper className={classes.paper}>
                <Home className={classes.home} />
                <Breadcrumbs aria-label="Breadcrumb">
                    {breadcrumbs.map(({ breadcrumb, match }, index) => (
                    <Link to={match.url || ""} key={match.url} className={classes.link}>
                        <Typography className={classes.text}>    
                            {breadcrumb}
                        </Typography>
                        
                    </Link>
                ))}
                </Breadcrumbs>
            </Paper>
        </div>
    );
}

export default withBreadcrumbs()(UiBreadcrumbs);