import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';

class BotonBorrar extends Component {
    state = {
        'nombre':'',
        'id':'',
        'dialog':{
            'open': false,
            'title':'',
            'message':''
        }
    }

    response(value){
        this.setState({
            'dialog': { ...this.state.dialog, 'open': false }
        }, ()=>{
            this.props.respuesta(value);
        })
    }

    handleClose() {
        this.setState({
            'dialog': { ...this.state.dialog, 'open': false }
        })
    }
    render() { 
        const { title, message, value } = this.props;
        return ( 
            <React.Fragment>
                <IconButton
                    aria-label="delete"
                    onClick={() => { 
                        this.setState({ 
                            'dialog':{...this.state.dialog, 'open':true}
                        }) }
                    }>
                    <DeleteIcon />
                </IconButton>
                <Dialog
                    open={this.state.dialog.open}
                    onClose={this.handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">
                        {title}
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            {message}
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={
                            () => {
                                this.setState({
                                    'dialog': { ...this.state.dialog, 'open': false }
                                })}
                            }
                        color="primary" autoFocus>
                            Cancelar
                        </Button>
                        <Button onClick={()=>this.response(value)}>
                            Confirmar
                        </Button>
                    </DialogActions>
                </Dialog>
            </React.Fragment>
         );
    }
}
 
export default BotonBorrar;