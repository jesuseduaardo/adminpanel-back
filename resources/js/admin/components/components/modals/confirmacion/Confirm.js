import React, { Component } from 'react';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


class Confirm extends Component {
    state = { 
        open:false
     }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps !== this.props) {
            this.setState({
                ...this.state, open: this.props.open
            })
        }
     }
    
    handleClose() {
        this.setState({
            ...this.state, open:false
        })
    }

    confirmar=()=>{
        this.props.respuesta(true)
        this.handleClose()
    }
    
    render() { 
        const { titulo, mensaje } = this.props;
        return ( 
            <Dialog
                open={this.state.open}
                onClose={this.handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{ titulo }</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        { mensaje }
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={()=>this.handleClose()} color="primary" autoFocus>
                        Cancelar
                    </Button>
                    <Button onClick={ this.confirmar }>
                        Confirmar
                    </Button>
                </DialogActions>
            </Dialog>
         );
    }
}
 
export default Confirm;