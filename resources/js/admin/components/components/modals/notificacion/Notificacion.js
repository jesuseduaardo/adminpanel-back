import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

class Notificacion extends React.Component {

    state = { 
        open:false
     }

    componentDidMount(){
        this.setState({
            ...this.state, open: this.props.open
        })
    }

    handleClickOpen = () => {
        this.setState({
            ...this.state, open:true
        });
    }

    handleClose = () => {
        this.setState({
            ...this.state, open:false
        })
    }

    render() { 
        return ( 
            <div>
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">{this.props.title }</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            { this.props.message}   
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary" autoFocus>
                            Cerrar
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
         );
    }
}
 
export default Notificacion;