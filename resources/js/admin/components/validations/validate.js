export const valida = {
    "notEmpty": (value) => {
        if(typeof value==="undefined" || value==="" || value===null){
            return { error:true, text:"El campo no puede estar vacio" };
        } 
        return null;
    },
    "onlyNumbers":(value)=>{
        if (typeof value === "undefined" || value === "" || value === null) {
            return { error: true, text: "El campo no puede estar vacio" };
        } 
        const pattern = /^\d+\.?\d*(\,\d{2})?$/;
        if(!pattern.test(value)){
            return { error: true, text: "El campo debe tener solo numeros" };
        }
        return null;
    },
    "onlyLetters":(value)=>{
        if (typeof value === "undefined" || value === "" || value === null) {
            return { error: true, text: "El campo no puede estar vacio" };
        } 
        const pattern = /^[A-Za-z]+$/;
        if(!pattern.test(value)){
            return { error: true, text: "El campo debe tener solo letras" };
        }
        return null;
    },
    "currency": (value)=>{
        if (typeof value === "undefined" || value === "" || value === null) {
            return { error: true, text: "El campo no puede estar vacio" };
        } 
        const pattern = /^(?!0\.00)\d{1,3}(.\d{3})*(\d\d)?$/;
        if (!pattern.test(value)) {
            return { error: true, text: "El campo no tiene un formato valido" };
        }
        return null;
    },
    "mail": (value) => {
        if (typeof value === "undefined" || value === "" || value === null) {
            return { error: true, text: "El campo no puede estar vacio" };
        } 
        const pattern = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
        if (!pattern.test(value)) {
            return { error: true, text: "El campo no tiene un formato valido" };
        }
        return null;
    },
    }