import React, { Component } from 'react';
import Dashboard from './components/Dashboard';
import Login from './components/Login';
import PrivateRoute from './components/PrivateRoute';

//Router
import {BrowserRouter as Router, Route, Switch } from 'react-router-dom';

//Redux
import { Provider } from 'react-redux';
import store from './components/store';

class App  extends Component {

  render() {
    return (
      <div className="App">
        <Provider store={store}>
          <Router >
            <Switch>
              <Route exact path="/login" component={Login}/>
              <PrivateRoute exact path="/*" component={Dashboard} />
            </Switch>
          </Router >
        </Provider>
      </div>
    );
  }
}

export default App;