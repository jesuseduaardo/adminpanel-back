import { AGREGAR_ITEM, MOSTRAR_ITEMS, BORRAR_ITEM } from './types';
import { toast } from "react-toastify";

export const agregarProducto = producto => async dispatch =>{
    if(localStorage.getItem("items") !== null){
        const allproducts = JSON.parse(localStorage.getItem("items"));
        let coincidence = false;
        allproducts.forEach((product)=>{
            if(product.sku === producto.sku){
                product.cantidad+= typeof producto.cantidad!=="undefined" ? producto.cantidad : 1;
                coincidence=true;
            }
        });
        if(!coincidence){
            allproducts.push(producto);
        }
        localStorage.setItem('items', JSON.stringify(allproducts));
    }else{
        localStorage.setItem('items', JSON.stringify([producto]));
    }
    if(typeof producto.cantidad!=="undefined"){ 
        toast.success("Se ha agregado el producto al carrito");
    }
    dispatch({ 
        type: AGREGAR_ITEM,
        payload: JSON.parse(localStorage.getItem("items"))
    })
}

export const mostrarProductos =() => async dispatch =>{
    dispatch({ 
        type: MOSTRAR_ITEMS,
        payload: JSON.parse(localStorage.getItem("items")) || []
    })
}

export const borrarProducto = producto => async dispatch =>{
    if(localStorage.getItem("items") !== null){
        const allproducts = JSON.parse(localStorage.getItem("items"));
        let coincidence = false;
        allproducts.forEach((product, index)=>{
            if(product.sku === producto.sku){
                if(product.cantidad > 0){
                    product.cantidad--;
                }else{
                    allproducts.splice( index, 1 );
                }
                coincidence=true;
            }
        });
        localStorage.setItem('items', JSON.stringify(allproducts));
    }
    dispatch({ 
        type: BORRAR_ITEM,
        payload: JSON.parse(localStorage.getItem("items"))
    })
}

export const borrarProductos = producto => async dispatch =>{
    if(localStorage.getItem("items") !== null){
        const allproducts = JSON.parse(localStorage.getItem("items"));
        let coincidence = false;
        allproducts.forEach((product, index)=>{
            if(product.sku === producto.sku){
                allproducts.splice( index, 1 );
            }
        });
        localStorage.setItem('items', JSON.stringify(allproducts));
    }
    toast.info("Se elimino el producto del carrito");
    dispatch({ 
        type: BORRAR_ITEM,
        payload: JSON.parse(localStorage.getItem("items"))
    })
}

// export const editarProducto = producto => async dispatch=>{
//     const token = sessionStorage.getItem('token') !== 'undefined' ? sessionStorage.getItem('token') : null
//     const config = {
//         headers: {
//             "authorization": token, //the token is a variable which holds the token
//             'Content-Type': "application/json"
//         },
//     };
//     dispatch({
//         type: REQUEST,
//         payload: 'inner',
//     })
//     try {
//         const respuesta = await axios.put(`${server}/api/admin/productos/${producto['edition'] }`, producto, config);
//         console.log(respuesta);
//         dispatch({ type: SUCCESS })
//         dispatch({
//             type: EDITAR_PRODUCTO,
//             payload: respuesta.data
//         })
//         toast.info("Producto editado exitosamente");
//         return true;
//     } catch (error) {
//         dispatch({ type: FAILURE, payload: error.response.data.message })
//         return false;
//     }
// }