import { CHECKOUT,REQUEST, SUCCESS, FAILURE } from './types';
import { toast } from "react-toastify";
import axios from 'axios';
import {server} from '../utils/utils';

sessionStorage.getItem('checkout-data') !== 'undefined' ? sessionStorage.getItem('checkout-data') : sessionStorage.setItem('checkout-data', {});

export const setCheckout = (checkoutData) => async dispatch => {
    dispatch({
        type:CHECKOUT,
        payload:""        
    })
    const token = sessionStorage.getItem('token') !== 'undefined' ? sessionStorage.getItem('token') : null
    const config = {
        headers: {
            "authorization": token, //the token is a variable which holds the token
            "Content-Type": "application/json"
        },
    };
    dispatch({ 
        type: REQUEST,
        payload:'',
     })

    try {
        const respuesta = await axios.post(server+"/checkout", checkoutData, config);
        dispatch({ type: SUCCESS })
        dispatch({
            type:CHECKOUT,
            payload: respuesta.data        
        })
    } catch (error) {
        dispatch({ type: FAILURE, payload: error.response.data.message })
        return false;
    }
}

export const setPersonalData =(data) => async dispatch =>{
    dispatch({ 
        type: "setPersonalData",
        payload: {...data}
    })
    
}

export const setAddressData =(data) => async dispatch =>{
    dispatch({ 
        type: "setAddressData",
        payload: {...data}
    })
}

export const setFiscalData =(data) => async dispatch =>{
    dispatch({ 
        type: "setFiscalData",
        payload: {...data}
    })
}

export const setSumary =(data) => async dispatch =>{
    dispatch({ 
        type: "setSumary",
        payload: {...data}
    })
}

export const setPickupInStore =(data) => async dispatch =>{
    dispatch({ 
        type: "setPickupInStore",
        payload: data
    })
}

export const setAddressAsFiscal =(data) => async dispatch =>{
    dispatch({ 
        type: "setAddressAsFiscal",
        payload: data
    })
}

export const setActiveStep =(data) => async dispatch =>{
    dispatch({ 
        type: "setActiveStep",
        payload: data
    })
}

export const setPaymethod =(data) => async dispatch =>{
    dispatch({ 
        type: "setPaymethod",
        payload: data
    })
}

export const setEnableNext =(data) => async dispatch =>{
    dispatch({ 
        type: "setEnableNext",
        payload: {...data}
    })
}

export const cleanCheckout=()=>async dispatch => {
    dispatch({type: "cleanState"})
}
