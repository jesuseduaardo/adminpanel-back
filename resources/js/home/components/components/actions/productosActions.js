import { MOSTRAR_PRODUCTOS, MOSTRAR_PRODUCTO, ELIMINAR_PRODUCTO, AGREGAR_PRODUCTO, EDITAR_PRODUCTO, BUSCAR_PRODUCTO, REQUEST, SUCCESS, FAILURE } from './types';
import { toast } from "react-toastify";
import axios from 'axios';
import {server} from '../utils/utils';

export const mostrarProductos = (categoria, pag) => async dispatch => {
    const token = sessionStorage.getItem('token') !== 'undefined' ? sessionStorage.getItem('token') : null
    const config = {
        headers: {
            "authorization": token, //the token is a variable which holds the token
            "Content-Type": "application/json"
        },
    };
    dispatch({ 
        type: REQUEST,
        payload:'',
     })
    let pagina
    if(typeof pag !== "undefined" && parseInt(pag)>1){
        pagina = pag;
    }else{
        pagina = 1;
    }
    try {
        const respuesta = await axios.get(server+"/productos/"+categoria+"?pag="+pagina, config);
        dispatch({ type: SUCCESS })
        dispatch({
            type:MOSTRAR_PRODUCTOS,
            payload: respuesta.data        
        })
    } catch (error) {
        dispatch({ type: FAILURE, payload: error.response.data.message })
        return false;
    }
}

export const mostrarProducto = id => async dispatch => {
    const token = sessionStorage.getItem('token') !== 'undefined' ? sessionStorage.getItem('token') : null
    const config = {
        headers: {
            "authorization": token, //the token is a variable which holds the token
            'Content-Type': "application/json"
        },
    };
    dispatch({
        type: REQUEST,
        payload: '',
    })
    try {
        const respuesta = await axios.get(`${server}/producto/${id}`, config);
        dispatch({ type: SUCCESS })
        dispatch({
            type: MOSTRAR_PRODUCTO,
            payload: respuesta.data
        })
    } catch (error) {
        dispatch({ type: FAILURE, payload: error.response.data.message })
    }
}

export const buscarProducto = palabraClave => async dispatch => {
    const token = sessionStorage.getItem('token') !== 'undefined' ? sessionStorage.getItem('token') : null
    const config = {
        headers: {
            "authorization": token, //the token is a variable which holds the token
            'Content-Type': "application/json"
        },
    };
    dispatch({
        type: REQUEST,
        payload: '',
    })
    try {
        const respuesta = await axios.get(`${server}/productos/search/${palabraClave}`, config);
        dispatch({ type: SUCCESS })
        dispatch({
            type: BUSCAR_PRODUCTO,
            payload: respuesta.data
        })
    } catch (error) {
        dispatch({ type: FAILURE, payload: error.response.data.message })
    }
}

export const activeProduct = (id, value) => async dispatch => {
    const token = sessionStorage.getItem('token') !== 'undefined' ? sessionStorage.getItem('token') : null
    const config = {
        headers: {
            "authorization": token, //the token is a variable which holds the token
            'Content-Type': "application/json"
        },
    };
    dispatch({
        type: REQUEST,
        payload: '',
    })
    try {
        const producto = { 'activeProduct': value };
        const respuesta = await axios.put(`${server}/api/admin/productos/${id}`, producto, config);
        dispatch({ type: SUCCESS })
        dispatch({
            type: MOSTRAR_PRODUCTOS,
            payload: respuesta.data
        })
    } catch (error) {
        dispatch({ type: FAILURE, payload: error.response.data.message })
    }
}

export const borrarProducto = id => async dispatch =>{
    const token = sessionStorage.getItem('token') !== 'undefined' ? sessionStorage.getItem('token') : null
    const config = {
        headers: {
            "authorization": token, //the token is a variable which holds the token
            'Content-Type': "application/json"
        },
    };
    dispatch({
        type: REQUEST,
        payload: '',
    })
    try {
        const respuesta = await axios.delete(`${server}/api/admin/productos/${id}`, config);
        dispatch({ type: SUCCESS })
        dispatch({
            type: ELIMINAR_PRODUCTO,
            payload: respuesta.data
        });
        toast.info("Se ha borrado el producto exitosamente!");
        return true;
    } catch (error) {
        dispatch({ type: FAILURE, payload: error.response.data.error });
        return false;
    }
}

export const agregarProducto = producto => async dispatch=>{
    const token = sessionStorage.getItem('token') !== 'undefined' ? sessionStorage.getItem('token') : null
    const config = {
        headers: {
            "authorization": token, //the token is a variable which holds the token
            'Content-Type': "application/json"
        },
    };
    dispatch({
        type: REQUEST,
        payload: '',
    })
    try {
        const respuesta = await axios.post(`${server}/api/admin/productos/`, producto, config);
        dispatch({ type: SUCCESS })
        dispatch({
            type: AGREGAR_PRODUCTO,
            payload: respuesta.data
        })
        toast.info("Producto guardado exitosamente");
        return true;
    } catch (error) {
        dispatch({ type: FAILURE, payload: error.response.data.message })
        return false;
    }
}

export const editarProducto = producto => async dispatch=>{
    const token = sessionStorage.getItem('token') !== 'undefined' ? sessionStorage.getItem('token') : null
    const config = {
        headers: {
            "authorization": token, //the token is a variable which holds the token
            'Content-Type': "application/json"
        },
    };
    dispatch({
        type: REQUEST,
        payload: '',
    })
    try {
        const respuesta = await axios.put(`${server}/api/admin/productos/${producto['edition'] }`, producto, config);
        console.log(respuesta);
        dispatch({ type: SUCCESS })
        dispatch({
            type: EDITAR_PRODUCTO,
            payload: respuesta.data
        })
        toast.info("Producto editado exitosamente");
        return true;
    } catch (error) {
        dispatch({ type: FAILURE, payload: error.response.data.message })
        return false;
    }
}