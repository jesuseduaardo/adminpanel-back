import { LOGIN, LOGOUT, FRASEDIARIA, MOSTRAR_ERROR, REQUEST, SUCCESS, FAILURE } from './types';
import axios from 'axios';
import {server} from '../utils/utils';

export const loginAction = (login) => async dispatch => {

    dispatch({type: REQUEST})

    const config = {
      method: "POST",
      headers: {
        'Content-Type':'application/json',
        'Accept':'application/json',
        'X-CSRF-TOKEN': window.Laravel.csrfToken
      },
    }
    
    try {
        const respuesta = await axios.post(server + "/api/login", login, config);
        if (respuesta.status === 200 && typeof respuesta.data.success.token !== 'undefined'){
            //console.log(respuesta.data.success.token);
            sessionStorage.setItem("token", `Bearer ${respuesta.data.success.token}`);
            sessionStorage.setItem('loggedIn', true);
            sessionStorage.setItem('baseurl', 'http://localhost:8000/');
            dispatch({type: SUCCESS})
            dispatch({
                type:LOGIN,
                payload: respuesta.data        
            })
            dispatch({ type: FAILURE })
        }
    } catch (error) {
        dispatch({ type: FAILURE, payload: error.response.data.error })
    }
}

export const logoutAction = () => async dispatch => {

    dispatch({ type: REQUEST })
    const token = sessionStorage.getItem('token') !== 'undefined' ? sessionStorage.getItem('token') : null
    const config = {
        headers: {
            "authorization": token, //the token is a variable which holds the token
            'Content-Type': "application/json"
        },
    };
    try {
        const respuesta = await axios.get(server + "/api/logout", config);
        if (respuesta.data.success !== 'undefined') {
            sessionStorage.clear();
            dispatch({ type: SUCCESS })
            dispatch({
                type: LOGOUT,
                payload: respuesta.data
            })
        }
    } catch (error) {
        if(error.response.data.message==="Unauthenticated."){
            sessionStorage.clear();
            dispatch({ type: SUCCESS })
            dispatch({
                type: LOGOUT,
                payload: {success:true}
            })
            return;
        }
        dispatch({ type: FAILURE, payload: error.response.data.message })
    }
}

export const fraseDiaria = () => async dispatch => {

    try {
        const respuesta = await axios.get(server + "/api/frasediaria");
        dispatch({ type: FRASEDIARIA, payload: respuesta.data })
    } catch (error) {
    }
}

export const mostrarError = (estado) => {
    return {
        type: MOSTRAR_ERROR,
        payload: estado
    }
}