import React, { useState, useEffect, useRef } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Card, 
    CardActionArea,
    CardActions,
    CardContent,
    CardMedia,
    Button,
    Typography,
    Grid,
    Collapse,
    Popover
} from '@material-ui/core';
import LocalShipping from '@material-ui/icons/LocalShipping';
import {isMobile} from 'react-device-detect';
import {montoTotal, getProductCategoryPath, setThumbImagePath} from '../utils/utils';
import NumberFormat from '../utils/numberformat/NumberFormat';
import history from '../history';
import "./product-thumb.css";
import ShowRoom from '../showRoom/ShowRoom';

const useStyles = makeStyles(theme => ({
    card: {
        maxWidth: "100%",
      },
      media: {
        minHeight: 250,
        backgroundSize:"contain"
      },
      actionButtons:{
          width:"100%"
      },
      envio:{
        position: "absolute",
        bottom: "3px",
        right: "11px",
        border: "1px solid",
        borderRadius: "30px",
        padding: "5px",
        zIndex:"9",
        color:"#00a650",
        backgroundColor:"#fff"
      },
      popover: {
        pointerEvents: 'none',
      },
      paper: {
        padding: theme.spacing(1),
      },
  }));

function ProductThumb(props){

    const classes = useStyles();

    const [show, setShow] = useState(true);
    const [popoverOpen, setPopoverOpen] = useState('');
    const [anchor, setAnchor] = useState(null);
    const [cantidad, setCantidad] = useState(1);
    const [product, setProduct] =  useState({});

    const firstRender = useRef(true);

    useEffect(()=>{
        if(firstRender.current){
            firstRender.current=false;
        }
        setShow(isMobile);
        setProduct({...props.data});
    }, [])

    const showDetails=(category, productId)=>{
        const categoryList = JSON.parse(sessionStorage.getItem("categorias"));
        const path = getProductCategoryPath(category, categoryList);
        history.push("/productos/"+path.toLowerCase()+"/"+productId);
    }

    const showDescription=(show)=>{
        setShow(show);
    }

    const handlePopoverOpen = event => {
        setAnchor(event.currentTarget);
        setPopoverOpen(true);
    }
    
    const handlePopoverClose = () => {
        setAnchor(null);
        setPopoverOpen(false);
    }

    const showDeliveryFee=()=>{
        return(
             props.data.deliveryFee === 0 ?
            <React.Fragment>
                <LocalShipping 
                    className={classes.envio} 
                    aria-owns={popoverOpen ? 'mouse-over-popover' : undefined} 
                    onMouseEnter={(e)=>handlePopoverOpen(e)}
                    onMouseLeave={()=>handlePopoverClose()} />
                <Popover
                    id="mouse-over-popover"
                    className={classes.popover}
                    classes={{ paper: classes.paper, }}
                    open={state.popoverOpen}
                    anchorEl={state.anchorEl}
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'center',
                    }}
                    transformOrigin={{
                        vertical: 'bottom',
                        horizontal: 'center',
                    }}
                    onClose={()=>handlePopoverClose}
                    disableRestoreFocus
                >
                    <Typography variant="caption">Envio gratis</Typography>
                </Popover>
            </React.Fragment>
            : ""
        )
    }

    const add2cart=()=>{
        const data = {...product, cantidad}
        props.addItem(data);
    }
    
    const buynow=()=>{
        const data = {...product, cantidad}
        props.buynow(data);
    }

    const values = typeof props.data !== "undefined" ?  montoTotal(props.data) : "";
    let imgs = setThumbImagePath(props.data.productImg);
    
    return(
            <Grid item xs={12} md={3} className="thumbox">
                <Card className={classes.card} 
                    onMouseEnter={ isMobile ? ()=>{} : ()=>showDescription(true) } 
                    onMouseLeave={ isMobile ? ()=>{} : ()=>showDescription(false) }
                    >
                    <CardActionArea>
                        <CardMedia
                            className={classes.media}
                            image={ imgs }
                            title={props.data.productName} 
                            onClick={()=>showDetails(props.data.cat_id, props.data.sku)}
                        />
                         {props.data.stock == 1 ? 
                                <Typography variant="body2" 
                                    style={{
                                            color:"red", 
                                            paddingLeft:14, 
                                            position:"absolute",
                                            bottom: 0
                                            }} 
                                    component="p">
                                   Ultimo
                                </Typography>
                                :""
                            }
                            {showDeliveryFee()}
                    </CardActionArea>
                   
                    {props.data.discountPercent > 0 ? 
                        <Collapse in={show}>
                            <CardContent style={{padding:"8px 16px 0px"}}>
                            <Typography variant="body1" className="old-price" >
                                <NumberFormat>{values.priceWithTax}</NumberFormat>
                            </Typography>
                            </CardContent>
                        </Collapse>
                        : "" }
                        <CardContent style={{padding:"6px 16px 4px"}}>
                            <Grid container>
                                <Grid item xs={6}>
                                    <Typography gutterBottom 
                                        variant="h5" 
                                        component="h2" 
                                        display="inline" 
                                        align="left">
                                        <NumberFormat>{values.totalWithDiscount}</NumberFormat>
                                    </Typography>
                                </Grid>
                                <Grid item xs={6} style={{display: "flex", alignItems: "center"}}>
                                {props.data.discountPercent > 0 ?
                                    <Typography 
                                        variant="body1" 
                                        display="inline" 
                                        className="promo-color discount-percent" 
                                        style={{paddingLeft:16}} 
                                        gutterBottom>
                                        {values.discountPercent}
                                    </Typography>
                                :""}
                                </Grid>
                            </Grid>
                        </CardContent>

                    <Collapse in={show}>
                        <CardContent style={{paddingTop:0, paddingBottom:3}}>
                            <Typography variant="body2" color="textSecondary" component="p">
                                {props.data.productName}
                            </Typography>
                        </CardContent>
                        <CardActions>
                            <Grid container spacing={0}>
                                <Grid item xs={6} md={12}>
                                    <Button 
                                        className={classes.actionButtons} 
                                        variant="contained" 
                                        color="primary" 
                                        onClick={()=>{buynow()}}>
                                        Comprar ahora
                                    </Button>
                                </Grid>
                                <Grid item xs={6} md={12}>
                                    <Button 
                                        size="small" 
                                        className={classes.actionButtons} 
                                        color="primary" 
                                        onClick={()=>{add2cart()}}>
                                        Agregar al carrito
                                    </Button>
                                </Grid>
                            </Grid>
                        </CardActions>
                    </Collapse>
                </Card>
            </Grid>
    );
}

export default ProductThumb;

ProductThumb.propTypes = {
    data: PropTypes.object
};