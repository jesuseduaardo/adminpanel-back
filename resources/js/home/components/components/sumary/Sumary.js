import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { List, ListItem, Typography, Divider } from '@material-ui/core';
import ItemDetails from './itemDetails/ItemDetails';
import NumberFormat from '../utils/numberformat/NumberFormat';
import {montoTotal} from '../utils/utils';
import "./sumary.css";

//Redux
import { connect } from 'react-redux';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    backgroundColor:"#fff",
  },
  inline: {
    display: 'inline',
  },
}));

function Sumary(props){

  const classes = useStyles();

  const [productList, setProductList] = useState([]);
  
  useEffect(()=>{
    setProductList(props.cart);
  }, [props.cart]);

  const handleRemoveItem = sku => {
    const data = {"sku":sku}
    props.updateSumary(data);
  }

  const updateQty = (sku, count) => {
    const data = {"sku":sku, "type":count}
    props.updateSumary(data);
  }

  const getTotal=(productList)=>{
    let discountPercent=0;
    let discountAmount=0;
    let taxAmount = 0;
    let subtotal = 0;
    let total = 0;

    productList.forEach(data => {
      let values = montoTotal(data);
      discountPercent+= typeof data.discountPercent !== "undefined" ? data.cantidad * values.discountPercent : 0;
      discountAmount+= data.cantidad * values.discountAmount; 
      taxAmount += data.cantidad * data.tax;
      subtotal += data.cantidad * values.priceWithTax;
      total +=  data.cantidad * values.totalWithDiscount
    });
    return total;
  }

  const itemList=()=>{
    const info = productList;
    const list = Object.keys(productList).map((item, i) => (
                  <ListItem key={i}>
                      <ItemDetails 
                        data={productList[i]} 
                        key={i} 
                        deleteItem={handleRemoveItem} 
                        updateQty={updateQty} />
                  </ListItem>
              ))
    
    return (
      <React.Fragment>
        <List className={classes.root}>
            { typeof info !== "undefined" ? list : "" }
        </List>
        <Divider />
        <Typography variant="h6" className="total" gutterBottom>
          Total <NumberFormat>{getTotal(productList)}</NumberFormat>
        </Typography>
      </React.Fragment>
    )
  }
    
  return ( 
          typeof productList !== "undefined" ? 
            itemList() : 
            <Typography variant="h6" gutterBottom>No hay elementos en el carrito</Typography>
  );
}

//state
const mapStateToProps = state => ({
  cart: state.cart.items
});

export default connect(mapStateToProps, {})(Sumary);