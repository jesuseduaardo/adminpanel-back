import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Card, CardContent, Typography, Avatar, Grid, IconButton, Hidden } from '@material-ui/core';
import {Delete} from '@material-ui/icons';
import Counter from '../../controls/counter/Counter'
import NumberFormat from '../../utils/numberformat/NumberFormat';
import {montoTotal} from '../../utils/utils';
import "./itemDetails.css";

const useStyles = makeStyles(theme => ({
  card: {
    
  },
  actions: {
    verticalAlign: 'middle',
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
    verticalAlign: 'middle',
    margin: 'auto',
    textAlign: 'center'
  },
  pos: {
    marginBottom: 12,
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 75,
  },
}));

function ItemDetails(props){

  const classes = useStyles();
  
  const [product, setProduct] = useState({});

  useEffect(()=>{
    setProduct({...props.data})
  }, [props.data])

  const updateQty =(count)=> {
    props.updateQty(product.sku, count);
  }

  const values = montoTotal(product);
  const imgThumb =  typeof product.productImg !== "undefined" && product.productImg.length > 0 ? product.productImg[0].thumbnail : "";
  
  return ( 
    <Card className="card">
     <CardContent>
        <Grid container spacing={0}>
            <Grid container item xs={2} style={{display:"flex", alignItems:"center",justifyContent:"center"}}>
              <div>
                <IconButton aria-label="delete" onClick={ ()=> props.deleteItem(product.sku) }>
                    <Delete/>
                </IconButton>
              </div>
            </Grid>
            <Grid container item xs={8} md={2}>
              <div style={{textAlign:"center"}}>
                <Typography>
                  {product.productName} 
                </Typography>
                <Avatar 
                    style={{width:'40%', margin:'auto'}}
                    variant="square" 
                    src={imgThumb} />
                
                {product.discountPercent > 0 ? 
                  <Typography variant="body1" className="old-price" >
                    <NumberFormat>{product.cantidad * values.priceWithTax}</NumberFormat>
                  </Typography>
                : "" }
                <Typography gutterBottom 
                    variant="h5" 
                    component="h2" 
                    display="inline" 
                    align="left">
                    <NumberFormat>{ product.cantidad * values.totalWithDiscount}</NumberFormat>
                </Typography>
                {product.discountPercent > 0 ?
                    <Typography 
                        variant="body2" 
                        className="promo-color discount-percent" 
                        component="p"
                        >
                         {values.discountPercent}
                    </Typography>
                :""}
              </div>
            </Grid>
            <Hidden xsDown>
                <Grid container item md={6} className={classes.actions} direction="row" justify="flex-start" alignItems="center">
                    {product.productDescription}
                </Grid>
            </Hidden>
            <Grid container item xs={2} className={classes.actions} direction="row" justify="center" alignItems="center">
                <Counter data={product} updateQty={updateQty} />
            </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
}
 
export default ItemDetails;