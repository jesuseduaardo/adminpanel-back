import React, { Component } from 'react';
import {withRouter} from 'react-router';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Typography from '@material-ui/core/Typography';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import Link from '@material-ui/core/Link';
import history from "../history";

class BreadCrumb extends Component {
    state = {
        routes:this.createRoutes(),
    }
    
    componentDidUpdate(prevProps){
        if(prevProps !== this.props){
            this.setState({
                routes: this.createRoutes()
            })
        }
    }

    handleListItemClick = (event, page) => {
        event.preventDefault();
        history.push(page);
    };

    createRoutes(){
        const pathname = this.props.location.pathname; 
        if(pathname && pathname !== ""){
            const routes = pathname.split("/");
            const newRoutes = [];
            for (let index = 0; index < routes.length; index++) {
                if(routes[index] !== ""){
                    let route = "";
                    for (let i = 1; i <= index; i++) {
                        if(i === index){
                            route+="/"+routes[i];
                            // if(routes[i] === "productos"){
                            //     route+="/";
                            // }
                            continue;
                        }
                        route+="/"+routes[i];
                        
                    }
                    newRoutes.push({"link": routes[index], "path":route+"/"});
                }
            }
            return newRoutes;
        }
    }

    render() { 
        const routes =  this.state.routes;
        const route = typeof routes !== "undefined" ?
        routes.map((item, key) =>
            item !== "" ? key === (routes.length -1) ? 
                <Typography color="textPrimary" key={key}>{item.link}</Typography>
            : <Link key={key} color="inherit" href="#" 
                    onClick={(e)=>this.handleListItemClick(e, item.path)}
                >
                    {item.link}
                </Link>
            :""
        ) : "";
        return ( 
            <Breadcrumbs 
                style={{
                    display:"flex", flexDirection:"row", justifyContent:"flexStart", 
                    alignItems:"center", padding:"20px 10px"
                }} 
                separator={<NavigateNextIcon 
                fontSize="small" />}  
                aria-label="breadcrumb"
                >
                    <Link color="inherit" href="#" onClick={(e)=>this.handleListItemClick(e, "/")}>Inicio</Link>
                    {route}
            </Breadcrumbs>
         );
    }
}
 
export default withRouter(BreadCrumb);