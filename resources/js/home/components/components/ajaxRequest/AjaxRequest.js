import React, { Component } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withStyles } from '@material-ui/core/styles';
import Notificacion from '../modals/notificacion/Notificacion';
import "./ajax-request.css";

const styles = theme => ({
});

class AjaxRequest extends Component {


    render() { 
        const { classes } = this.props;
        const request = this.props.request;
        
        return ( 
            request.loading === true ? 
                <div className="progressContainer">
                    <CircularProgress className="progress" />
                </div>
            : 
                typeof request.error !== 'undefined' ?
                    <Notificacion open={request.error !== ''} title="" message={request.error}></Notificacion>    
                : ''
         );
    }
}

export default (withStyles(styles)(AjaxRequest));