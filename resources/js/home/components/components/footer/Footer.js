import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Paper, Button, Grid, Typography, TextField, IconButton, Link } from '@material-ui/core';
import {Facebook, Twitter, Instagram, WhatsApp, LinkedIn, YouTube} from '@material-ui/icons';
import "./footer.css";
import CookieConsent from "react-cookie-consent";

const useStyles = makeStyles(theme => ({
      paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
      },
  }));

function Footer(){

    const classes = useStyles();

    const y2k=number=>{ 
        return (number < 1000) ? number + 1900 : number; 
    }

    const today = new Date();
    const year = y2k(today.getYear());

    return (
    <div className="footer">
        <Grid container>
            <Grid item xs={12} md={5} style={{ padding:"0 20px  30px"}}>
                <Typography variant="h6">Conectate con nosotros!</Typography>
                <Typography variant="body2">
                    Siguenos en nuestras redes, subscribete y mantente
                    <br/>informado de nuestras promociones y ofertas.
                </Typography>
                <div className="subscribe-box">
                    <form className={classes.root} noValidate autoComplete="off">
                        <TextField 
                            id="suscribe"
                            label="Email"
                            type="text"
                            autoComplete="email"
                            variant="outlined" 
                            className="subscribe-txt"
                            />
                        <Button variant="contained" size="large" color="primary" className="subscribe-button">
                                Suscribirme
                        </Button>
                    </form>
                </div>
                <div className="subscribe-social">
                    <IconButton aria-label="facebook">
                        <Facebook/>
                    </IconButton>
                    <IconButton aria-label="twitter">
                        <Twitter/>
                    </IconButton>
                    <IconButton aria-label="twitter">
                        <Instagram/>
                    </IconButton>
                    <IconButton aria-label="twitter">
                        <WhatsApp/>
                    </IconButton>
                    <IconButton aria-label="twitter">
                        <LinkedIn/>
                    </IconButton>
                    <IconButton aria-label="twitter">
                        <YouTube/>
                    </IconButton>
                </div>
            </Grid>
            <Grid item xs={12} md={4} style={{ padding:"0 20px 30px"}}>
                <Typography variant="h6">Enlaces de interes</Typography>
                <ul className="enlaces-interes">
                    <li>
                        <Link
                            component="button"
                            variant="body2"
                            onClick={() => {
                                console.info("I'm a button.");
                            }}
                            >
                            Nosotros
                        </Link>
                    </li>
                    <li>
                        <Link
                            component="button"
                            variant="body2"
                            onClick={() => {
                                console.info("I'm a button.");
                            }}
                            >
                            Contactanos
                        </Link>
                    </li>
                    <li>
                        <Link
                            component="button"
                            variant="body2"
                            onClick={() => {
                                console.info("I'm a button.");
                            }}
                            >
                            Condiciones de servicio
                        </Link>
                    </li>
                </ul>
            </Grid>
            <Grid item xs={12} md={3} style={{ padding:"0 20px 30px"}}>

            </Grid>
            <Grid item xs={12} style={{ padding:"0 20px 30px"}}>

            </Grid>
            <Grid container item xs={12} 
                    style={{ 
                        padding:"0 20px 30px", 
                        display:"flex", 
                        justifyContent:"space-around",
                        alignItems:"center"
                    }}>
                <Grid container item xs={4} style={{textAlign:"center"}}>
                    <Link
                        component="button"
                        variant="body2"
                        onClick={() => {
                            console.info("I'm a button.");
                        }}>
                        Politica de privacidad
                    </Link>
                </Grid>
                <Grid container item xs={4} style={{textAlign:"center"}}>
                    <Link
                        component="button"
                        variant="body2"
                        onClick={() => {
                            console.info("I'm a button.");
                        }}>
                        Sitemap
                    </Link>
                </Grid>
                <Grid container item xs={4} style={{textAlign:"center"}}>
                    <Typography variant="body2">
                        {'© Copyright Saenca '+year}
                    </Typography>
                </Grid>
            </Grid>
        </Grid>
        <CookieConsent 
            disableStyles={true}
            location="bottom"
            buttonText="Acepto"
            expires={120} 
            buttonClasses="cookie-consent-btn"
            containerClasses="cookie-consent-container"
            contentClasses="cookie-consent-content"
        >
            Este Web utiliza cookies propias y de terceros para ofrecerle una mejor experiencia y servicio.
            <br/> 
            Al navegar o utilizar nuestros servicios el usuario acepta el uso que hacemos de las cookies.
        </CookieConsent>
       
    </div>
    );
}
 
export default Footer;