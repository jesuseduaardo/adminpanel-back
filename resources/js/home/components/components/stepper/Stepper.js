import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';

const container = { display: "flex", justifyContent: "space-between", padding:"30px 5px", width:"100%" }; 

class Stepper extends Component {
    state = {  }

    componentDidMount(){
        
    }

    handleBack(){
        console.log("Atras");
    }
    
    handleNext(){
        console.log("Siguiente");
    }

    render() { 
        return ( 
            <div style={container}>
                <Button size="small" onClick={()=>this.handleBack()} disabled={false}>
                    <KeyboardArrowLeft />
                    Anterior
                </Button>
                <Button size="small" onClick={()=>this.handleNext()} disabled={false}>
                    Siguiente
                    <KeyboardArrowRight />
                </Button>
            </div>

         );
    }
}
 
export default Stepper;