import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import FilledInput from '@material-ui/core/FilledInput';
import FormControl from '@material-ui/core/FormControl';
import InputAdornment from '@material-ui/core/InputAdornment';
import SearchIcon from '@material-ui/icons/Search';
import "./search-box.css"
import history from '../history';

class SearchBox extends Component {
    state = {  
        value:""
    }

    handleClick = (event) => {
        event.persist();
        const { value } = this.state;
        if(value!==""){
            history.push("/productos?"+value);
            this.props.toggleDrawer(false);
        }
    };

    handleMouseDown = event => {
        event.preventDefault();
      };
    handleChange(e){
        this.setState({
            ...this.state,
            value: e.target.value,
        })
    }

    render() { 
        return ( 
            <FormControl className="SearchBox">
                <FilledInput
                    id="search"
                    type="text" 
                    placeholder="Buscar"
                    value={this.state.value}
                    onChange={(e)=>this.handleChange(e)}
                    endAdornment={
                    <InputAdornment position="end">
                        <IconButton
                            aria-label="Search input"
                            onClick={(e)=>this.handleClick(e)} 
                            onMouseDown={(e)=>this.handleMouseDown(e)}
                            edge="end"
                            color="inherit"
                        >
                            <SearchIcon/>
                        </IconButton>
                    </InputAdornment>
                    }
                />
            </FormControl>
         );
    }
}

export default SearchBox;