import React from 'react';
import Products from './Products';

function Home(props){
    return(
        <Products location={{"pathname": props.location.pathname}}/>
    );
}

export default Home;