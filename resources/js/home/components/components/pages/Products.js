import React, { useEffect, useRef } from 'react';
import ShowRoom from '../showRoom/ShowRoom';

//Redux
import { connect } from 'react-redux';
import { mostrarProductos,buscarProducto } from '../actions/productosActions';

function Products(props){
    
    const myRef = useRef();
    const isFirstRender = useRef(true);

    useEffect(()=>{
        if (isFirstRender.current) {
            isFirstRender.current = false;
            getUrlData()
        } 
        scrollToMyRef();
    }, [props.loading, props.location])

    const getUrlData=()=>{
        const { location } = props;
        if(typeof location.search !== "undefined" && location.search!==""){
            const keyword = location.search.slice(1);
            return getSearch(keyword);
        }
        if(typeof location.pathname !== "undefined"){
            const path = location.pathname;
            const pieceOfPath = path.split("/");
            let lastPath = pieceOfPath[pieceOfPath.length -1];
            if(lastPath.charAt(lastPath.length-1) !== "/"){
                lastPath = pieceOfPath[pieceOfPath.length -2];
            }
            const category = lastPath!== "productos"  && lastPath!== "" ?  lastPath : 0;
            return getCategory(category);
        }
    }

    const getSearch=(keyword)=>{
        props.buscarProducto(keyword);
    }
    
    const getCategory=(category)=>{
        props.mostrarProductos(category);
    }
    
    const scrollToMyRef = () => window.scrollTo(0, myRef.current.offsetTop);
    
    return ( 
        <div ref={myRef}>
            <ShowRoom 
                productList = {props.productos} />
        </div>
    );

}

//state
const mapStateToProps = state => ({
    loading: state.loading,
    productos: state.productos.productos,
});

export default connect(mapStateToProps, {mostrarProductos, buscarProducto})(Products);