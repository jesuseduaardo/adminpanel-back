import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import ProductThumb from '../productThumb/ProductThumb';
import AjaxRequest from '../ajaxRequest/AjaxRequest';
import Stepper from '../stepper/Stepper';
import Filter from '../filter/Filter';
import "./showroom.css";

//Redux
import { connect } from 'react-redux';
import { agregarProducto } from '../actions/cartActions';

import history from "../history";

const styles = theme => ({
    root: {
        flexGrow: 1,
      },
  });

class ShowRoom extends Component {
    state = { 
        productList:[],
        pagination:"",
        loading:false,
     }

    componentDidMount(){
      this.setState({
        ...this.state,
        productList:Object.values({...this.props.productList.products}),
        pagination: this.props.productList.pagination,
      })
    }
  
    componentDidUpdate(prevProps){
      if(this.props !== prevProps ){
        this.setState({
          ...this.state,
          loading:this.props.loading.loading,
          productList:Object.values({...this.props.productList.products}),
          pagination: this.props.productList.pagination,
        })
      }
    }

    filtrar=(filters)=>{
      const { categoria, evento, price } = filters;
      const productList = Object.values({...this.props.productList.products});
      if(categoria ==="" && evento === "" && price.min === 0 && price.max === 0){
          this.setState({
            ...this.state,
            "productList":productList,
          },()=>{});
          return
      }
     if(productList.length > 0){
          productList.map((products, index) => {
            if(!isNaN(index)){
              if(price.min > 0 && price.max > 0){ 
                let prodPrice = productList[index]["price"] || 0;
                let prodtax = productList[index]["tax"] || 0;
                let costoProducto = parseInt(prodPrice + ((prodPrice * prodtax) /100 ));
                if(Object.entries(price).length > 0 && (costoProducto < price.min || costoProducto > price.max)){
                  productList.splice(index, 1);
                  
                }
              }
              if((evento !== "" && typeof productList[index] !== "undefined") && (productList[index]['evento'].toLowerCase() !== evento.toLowerCase())){
                productList.splice(index, 1);
              }
              if((categoria !== "" && typeof productList[index] !== "undefined") && (productList[index]['categoria'].toLowerCase() !== categoria.toLowerCase())){
                productList.splice(index, 1);
              }
            }else{
              productList.splice(index, 1);
            }
          });
      }
      this.setState({
        ...this.state,
        "productList":productList,
      })
    }

    addItem=(producto)=>{
      this.props.agregarProducto(producto);
    }

    buyNow=(producto)=>{
      this.props.agregarProducto(producto);
      history.push("/checkout");
    }

    render() { 
        const { classes, loading } = this.props;
        const productos = this.state.productList;
        const pagination = this.state.pagination;
        const handle = this.props.location.pathname;
        const match = handle.match(/^.([a-z]+)/i);
        return (
          <React.Fragment>
            <AjaxRequest request={loading} className={ this.state.loading ? "visible":"hidden"}/>
            <div className={ this.state.loading ? "hidden" : "visible"}>
                <Grid container ref={this.myRef}>
                 { match !== null && (this.props.location.search === "undefined" || this.props.location.search === "") ? <Filter productos={productos} filtrar={this.filtrar}/> : "" }
                { Object.keys(productos).length > 0 ?
                      Object.keys(productos).map((producto, index) => (
                        typeof productos[index] !== "undefined" ?  
                          <ProductThumb key={index} data={productos[index]} addItem={this.addItem} buynow={this.buyNow}/>
                        : ""
                        ))
                        :
                        <Grid item xs={12} style={{padding:"60px 20px"}}>
                          <Typography align="center" variant="h6">No se encontraron resultados</Typography>
                        </Grid>
                    }
                    <Grid container ref={this.myRef}>
                      { match !== null ?  
                        typeof pagination !== "undefined" && pagination["next_page"] !== "" ? <Stepper/> : ""
                      : "" }
                    </Grid>
                </Grid>
            </div>
            
          </React.Fragment>
         );
    }
}
 
//state
const mapStateToProps = state => ({
  loading: state.loading,
  cart: state.cart.items,
});

export default connect(mapStateToProps, { agregarProducto })(withRouter(withStyles(styles)(ShowRoom)));