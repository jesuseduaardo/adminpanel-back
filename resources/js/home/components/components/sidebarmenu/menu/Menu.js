import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import HomeIcon from '@material-ui/icons/Home';
import LocalMallIcon from '@material-ui/icons/LocalMall';
import history from '../../history';
import { mainCategories } from '../../utils/utils';

const styles = theme => ({
      nested: {
        paddingLeft: theme.spacing(4),
      },
})

class Menu extends Component {
    state = {
        categorias:[]
     }
    
    componentDidUpdate(prevProps){
        if(prevProps !== this.props){
            this.setState({
                ...this.state,
                categorias:this.props.categorias
            })
        }
    }

    handleListItemClick(event, page){
        event.persist();
        history.push(page);
        this.props.toggleDrawer(false);
    };

    categoriesBuilder(categories){
        let main_categories;
        if(categories.length > 0){
            main_categories =  mainCategories(categories);
        }else{
            main_categories =  categories;
        }        
        return(
            <React.Fragment>
                { main_categories.map((category, i) => (
                <ListItem 
                    key={i} 
                    button 
                    onClick={(e) => this.handleListItemClick(e, "/productos/"+category.nombre.toLowerCase()+"/")}
                    >
                    <ListItemIcon>
                        <LocalMallIcon/>
                    </ListItemIcon>
                    <ListItemText primary={category.nombre} />
                </ListItem>
            ))}
            </React.Fragment>
        )
    }

    render() { 
        const categories = this.categoriesBuilder(this.state.categorias);
        const {classes} = this.props;
        return ( 
            <div>
                <List component="nav">
                    <ListItem 
                        button
                        onClick={(e)=>this.handleListItemClick(e, "/")} 
                        >
                        <ListItemIcon>
                            <HomeIcon/>
                        </ListItemIcon>
                        <ListItemText primary="Inicio"/>
                    </ListItem>
                    {categories}
                </List>
                <Divider />
                <List>
                    {['All mail', 'Trash', 'Spam'].map((text, index) => (
                    <ListItem button key={text} key={index}>
                        <ListItemIcon><HomeIcon/></ListItemIcon>
                        <ListItemText primary={text} />
                    </ListItem>
                    ))}
                </List>
            </div>
         );
    }
}
 
export default withStyles(styles)(Menu);

