import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import ClearIcon from '@material-ui/icons/Clear';
import Divider from '@material-ui/core/Divider';
import Menu from './menu/Menu';
import {isMobile} from 'react-device-detect';
import SearchBox from '../search-box/SearchBox';

const drawerWidth = isMobile ? "100%" : 350;

const styles = theme => ({
    root: {
        display: 'flex',
      },
      menuButton: {
        marginRight: theme.spacing(2),
      },
      hide: {
        display: 'none',
      },
      drawer: {
        width: drawerWidth,
        flexShrink: 0,
      },
      drawerPaper: {
        width: drawerWidth,
      },
      drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
      },
})

class SideBarMenu extends Component {

    state = { 
        open:false,
        categorias:[],
     }

    componentDidMount(){
        if(this.props.categorias.length > 0){
            this.setState({
                ...this.state,
                categorias : this.props.categorias,
            })
        }
    }

    componentDidUpdate(prevProps){
        if(prevProps !== this.props){
            if(this.state.categorias.length > 0){
                this.setState({
                    ...this.state,
                    open:this.props.open,
                })
            }else{
                this.setState({
                    ...this.state,
                    open:this.props.open,
                    categorias:this.props.categorias
                })
            }
            
        }
    }

    toggleDrawer = open => event => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }
        this.setState({ 
            ...this.state, 
            open : open 
        });
        this.props.sidebarOpen(open)
    }
      
    render() {
        const { classes } =  this.props;
        return ( 
            <Drawer
                className={classes.drawer}
                variant="persistent"
                anchor="left"
                open={this.state.open} 
                onClose={this.toggleDrawer(false)}
                classes={{
                    paper: classes.drawerPaper,
                }}
            >
                <div
                className={classes.list}
                role="presentation"
                >
                    <div className={classes.drawerHeader}>
                        <IconButton onClick={this.toggleDrawer(false)}>
                            <ClearIcon />
                        </IconButton>
                    </div>
                    <Divider />
                    { isMobile ? <SearchBox toggleDrawer={this.toggleDrawer(false)}/> : "" } 
                    <Menu categorias={this.state.categorias} toggleDrawer={this.toggleDrawer(false)}/>
                </div>
            </Drawer>
         )
    }
}
 
export default withStyles(styles)(SideBarMenu);