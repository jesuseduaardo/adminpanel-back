import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import Typography from '@material-ui/core/Typography';
import Clear from '@material-ui/icons/Clear';
import Badge from '@material-ui/core/Badge';
import Sumary from '../sumary/Sumary';
import "./shoppingCart.css";
import history from "../history";

//Redux
import { connect } from 'react-redux';
import { mostrarProductos, agregarProducto, borrarProducto, borrarProductos } from '../actions/cartActions';

const styles = theme => ({
    root: {
    },
    paper: {
        position: 'absolute',
        width: "85%",
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(1),
      },
    button: {
    margin: theme.spacing(1),
    width: '90%',
    color: "#fff",
    },
})

class ShoppingCart extends Component {

    state = { 
        sumary: [],
        open:false,
        totalItems:0
     }

    componentDidMount(){
        this.props.mostrarProductos();
        this.setState({
           ...this.state,
           sumary: this.props.cart
        })
      }
     
     componentDidUpdate(prevProps){
        if(prevProps !== this.props){
            this.setState({
                ...this.state,
                sumary: this.props.cart,
                totalItems: this.getTotalItems(this.props.cart)
             })
        }
     } 

     getTotalItems(data){
        let cantidad = 0;
        data.forEach(item => {
            cantidad+=item.cantidad;
        });
        return cantidad;
     }

     updateSumary = (data) => {
        if(typeof data.type === "undefined"){
            this.props.borrarProductos(data);
        }
        if(data.type ==="down"){
            this.props.borrarProducto(data);
        }
        if(data.type==="up"){
            this.props.agregarProducto(data);
        }
     }

    handleOpen = () => {
        this.setState({
            ...this.state,
            open:true
        })
    };
    
    handleClose = () => {
        this.setState({
            ...this.state,
            open:false
        })
    };

    getModalStyle() {
        const top = 50;
        const left = 50;
      
        return {
          top: `${top}%`,
          left: `${left}%`,
          transform: `translate(-${top}%, -${left}%)`,
        };
      }

    handleCheckout(){
      history.push("/checkout");
      this.handleClose()
    }
    render() {
        const {classes} = this.props;
        return ( 
            <div>
                <IconButton color="default" className="totalItems" aria-label="add to shopping cart" onClick={this.handleOpen}> 
                    <Badge badgeContent={this.state.totalItems} color="primary">
                        <ShoppingCartIcon style={{color:"#fff"}} />
                    </Badge>
                </IconButton>
                <Dialog 
                    fullScreen 
                    open={this.state.open}
                    onClose={this.handleClose}
                    scroll="body"
                    aria-labelledby="scroll-dialog-title"
                    aria-describedby="scroll-dialog-description" 
                >
                    <div className="titulo">
                        <DialogTitle>Carrito de Compras</DialogTitle>
                        <div>
                            <IconButton aria-label="delete" onClick={()=>this.handleClose()}>
                                <Clear />
                            </IconButton>
                        </div>
                    </div>
                    { this.state.sumary && this.state.sumary.length ? 
                        <div>
                            <DialogContent dividers={false} className="modal">
                                
                                <Sumary 
                                    dataItems={typeof this.state.sumary !== "undefined" ? this.state.sumary : "" } 
                                    updateSumary={this.updateSumary} 
                                />
                            </DialogContent>
                            <DialogActions>
                                <Button
                                    variant="contained"
                                    size="large"
                                    className={classes.button} 
                                    color="primary" 
                                    onClick={()=>this.handleCheckout()}
                                > 
                                    Confirmar Orden
                                </Button>
                            </DialogActions>
                        </div>
                        : <Typography variant="body1" align="center">
                            No hay productos en el carrito
                          </Typography> 
                        }
                </Dialog>
            </div>
         );
    }
}

//state
const mapStateToProps = state => ({
    cart: state.cart.items
  });
 
export default connect(mapStateToProps, { mostrarProductos, agregarProducto, borrarProducto, borrarProductos })(withStyles(styles)(ShoppingCart));