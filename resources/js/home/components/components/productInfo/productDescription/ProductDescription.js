import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
   description:{
       fontSize:"1.2em"
   }
 });

class ProductDescription extends Component {
    state = {  }
    render() { 
        const {classes, data} = this.props;
        return ( 
            <div className="product-description">
                <Typography variant="h6" gutterBottom>
                    Descripci&oacute;n
                </Typography>
                <Typography variant="body1" paragraph className={classes.description}>
                    {data.productDescription}
                </Typography>
            </div>
         );
    }
}
 
export default withStyles(styles)(ProductDescription);