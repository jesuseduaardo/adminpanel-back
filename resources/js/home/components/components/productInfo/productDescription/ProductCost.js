import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import LocalShipping from '@material-ui/icons/LocalShipping';
import Button from '@material-ui/core/Button';
import {montoTotal} from '../../utils/utils';
import NumberFormat from '../../utils/numberformat/NumberFormat';

const styles = theme => ({
   cantidad:{
       display:"flex",
       alignItems:"end",
       paddingTop:10
   },
});

class ProductCost extends Component {
    state = {
        cantidad:1
    }

    componentDidMount(){
        if(typeof this.props.data !== "undefined"){
            this.setState({
                ...this.state,
                ...this.props.data
            })
        } 
    }

    componentDidUpdate(prevProps){
        if(prevProps !== this.props){
            this.setState({
                ...this.state,
                ...this.props.data
            })
        } 
    }

    productStock=(stock)=>{
        const numbers = []
        if(typeof stock !== "undefined"){
            for (let i = 1; i <= stock; i++) {
                numbers.push(<MenuItem key={i} value={i}>{i}</MenuItem>)
            } 
        }else{
            numbers.push(<MenuItem key="0" value=""></MenuItem>)
        }
        return numbers;
    }

    handleChange = event => {
        this.setState({
            cantidad: event.target.value
        });
      };

    add2cart(){
        this.props.addItem({...this.state});
    }
    
    showDeliveryFee(){
        return(
            this.state.deliveryFee === 0 ?
            <div className="costo-envio">
                <LocalShipping className="promo-color" style={{paddingRight:6}} />
                <Typography 
                    className="promo-color" 
                    variant="body2" 
                    display="inline"
                    gutterBottom>
                        Envio gratis
                </Typography>
            </div>
            :
            <div className="costo-envio">
                <LocalShipping style={{paddingRight:6}} />
                <Typography 
                    variant="body2" 
                    display="inline"
                    gutterBottom>
                        Costo de envio <NumberFormat>{this.state.deliveryFee}</NumberFormat>
                </Typography>
            </div>
        )
    }

    render() { 
        const {classes } = this.props;
        const data = {...this.state};
        const values = montoTotal(data);
        const cantidad = this.productStock(data.stock);
        return ( 
            <div className="product-description">
                <Typography className="product-name" variant="h1" style={{paddingBottom:12}} gutterBottom>
                    {data.productName}
                </Typography>
                {data.discountPercent > 0 ? 
                <Typography variant="body1" className="old-price" >
                    <NumberFormat>
                        {values.priceWithTax}
                    </NumberFormat>
                </Typography>
                :""}
                <div className="product-total">
                    <Typography variant="h4" display="inline">
                        <NumberFormat>{values.totalWithDiscount}</NumberFormat>
                    </Typography>
                    {data.discountPercent > 0 ? 
                    <Typography 
                        variant="body1" 
                        display="inline" 
                        className="promo-color discount-percent" 
                        style={{paddingLeft:16}} 
                        gutterBottom>
                        {values.discountPercent}
                    </Typography>
                    :""}
                </div>
                {
                    data.stock > 0 ? 
                    <Typography 
                        variant="body2" 
                        className="stock-disponible"  
                        gutterBottom>
                        Stock disponible
                    </Typography>
                    :""
                }
                {this.showDeliveryFee()}
                <div className={classes.cantidad}>
                    <Typography 
                        variant="body1" 
                        display="inline" 
                        style={{padding:"5px 10px 5px 0"}} 
                        gutterBottom>
                        Cantidad:
                    </Typography>
                    <TextField
                        id="outlined-select-currency"
                        select
                        value={this.state.cantidad}
                        onChange={(e)=>this.handleChange(e)}
                        >
                        {cantidad}
                    </TextField>
                </div>
                <div style={{paddingTop:25}}>
                    <Button className="action-buttons" variant="contained" color="primary">
                        Comprar ahora
                    </Button>

                    <Button 
                        onClick={()=>{this.add2cart()}}
                        className="action-buttons" 
                        variant="outlined" 
                        color="primary">
                        Agregar al carrito
                    </Button>
                </div>
            </div>
         );
    }
}
 
export default withStyles(styles)(ProductCost);