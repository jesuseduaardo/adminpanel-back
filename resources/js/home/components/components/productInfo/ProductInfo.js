import React, { useState, useRef, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import ImageGallery from 'react-image-gallery';
import ProductCost from './productDescription/ProductCost';
import ProductDescription from './productDescription/ProductDescription';
import AjaxRequest from '../ajaxRequest/AjaxRequest';
import "react-image-gallery/styles/css/image-gallery.css";
import "./product-info.css";
import { getDataImages } from '../utils/utils';

//Redux
import { connect } from 'react-redux';
import { mostrarProductos, mostrarProducto } from '../actions/productosActions';
import { agregarProducto } from '../actions/cartActions';

const useStyles = makeStyles(theme => ({
   
}));

  const dataItems = { 
      id: 0,
      productImg:" , ",
      productName:"",
      productDescription:"",
      price:0,
      tax:0,
      qty:0,
      stock:0,
      discountPercent:0,
      deliveryFee: 0
    }

function ProductInfo(props){

    const classes = useStyles();

    const [productos, setProductos] = useState([]);
    const [producto, setProducto] = useState({});
    const [loading, setLoading] = useState(false);

    const myRef = useRef();
    const firstRender = useRef(true);

    useEffect(()=>{
        if(firstRender.current){
            firstRender.current=false;
            showData();
        }
        scrollToMyRef();
        setProducto(typeof props.producto[0] !== "undefined" ? props.producto[0] : "");
        setProductos(props.productos);
        setLoading(props.loading.loading)
    }, [props.loading.loading, props.productos, props.producto])


    const showData=()=>{
        const path = props.location.pathname;
        const lastPath = path.split("/");
        const sku = lastPath[lastPath.length -1];
        const category = lastPath[lastPath.length -2];
        const productos =  props.productos;
        let encontrado = false;
        Object.keys(productos).map((producto, index) => {
           if(typeof productos[index] !== "undefined"){
                if(productos[index].sku === sku){
                    setProducto(props.productos[index]);
                    encontrado = true;
                }
           }
        });
        if(!encontrado){
            props.mostrarProducto(sku);
            props.mostrarProductos(category);
        }
    }
    
    const addItem=(producto)=>{
        props.agregarProducto(producto);
    }

    const scrollToMyRef = () => window.scrollTo(0, myRef.current.offsetTop)

    let imagenes = producto !== "" ? getDataImages(producto) : getDataImages(dataItems);
    
    return ( 
            <React.Fragment>
                <AjaxRequest request={props.loading} className={ loading ? "visible":"hidden"}/>
                <Grid container ref={myRef} className={ loading ? "hidden" : "visible"}>
                    <Grid item xs={12} md={7}>
                        <ImageGallery items={ imagenes } />
                    </Grid>
                    <Grid item xs={12} md={5}>
                        <ProductCost 
                            data={ producto !== "" ? producto : dataItems } 
                            addItem={addItem}
                            />
                    </Grid>
                    <Grid item xs={12} md={7}>
                        <ProductDescription data={producto !== "" ? producto : dataItems }/>
                    </Grid>
                </Grid>
            </React.Fragment>
         );
}

//state
const mapStateToProps = state => ({
    productos: state.productos.productos,
    producto: state.productos.producto,
    cart: state.cart.items,
    loading: state.loading,
  });
 
export default connect(mapStateToProps, { mostrarProductos, mostrarProducto, agregarProducto})(ProductInfo);