export const montoTotal = price => {
    const discountPercent = typeof price.discountPercent !=="undefined" && price.discountPercent!=="" 
                            ? price.discountPercent : 0;
    const priceWithTax = price.price + (price.price * price.tax / 100);
    const discountAmount =  (priceWithTax * discountPercent) / 100;
    const totalWithDiscount = priceWithTax - discountAmount;
    const values = {
        'discountPercent':discountPercent,
        'priceWithTax':priceWithTax,
        'discountAmount':discountAmount,
        'totalWithDiscount':totalWithDiscount
    }
    return values;
}  

export const with2Decimals = (num) => {
    // return num.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]
    return num.toFixed(2);
}

export const summaryTotal=dataItems=>{
    if((typeof dataItems !== "undefined" && dataItems!==null)  && dataItems.length > 0){
        let descuentoMonto=0;
        let impuestos = 0;
        let subtotal = 0;
        let total = 0;
        let envio=0;

        dataItems.forEach((data, key) => {
            let values = montoTotal(data);
            descuentoMonto+= data.cantidad * values.discountAmount; 
            impuestos += data.cantidad * (values.priceWithTax - data.price);
            subtotal += data.cantidad * data.price;
            envio += typeof data.deliveryFee !== "undefined" && data.deliveryFee !== "" ? data.deliveryFee : 0;
        });
        total = (subtotal + impuestos) - descuentoMonto;
        return { total, subtotal, impuestos, descuentoMonto, envio }
    }
    return 0;
}

export const getDataImages=data=>{
    const patt = /,/g;
    let images;
    if(patt.test(data.productImg)){
        images = data.productImg.split(",");
    }else{
        images=[data.productImg]
    }
    let imagenCollection = [];
    const imagePattern =  RegExp('\.(png|jpg|gif|bmp)$')
    images.forEach((img, key) => {
        if(typeof images[key] !=="undefined" && imagePattern.test(images[key])){
            const originalImg = setImagePath(img);
            const thumbImg =  setThumbImagePath(img);
            if(originalImg !=="" && thumbImg !== ""){
                imagenCollection.push({
                    'original':originalImg, 
                    'thumbnail':thumbImg
                });
            }
        }
    });
    return imagenCollection;
}


export const mainCategories = (categories) => {
    let categoryList = [];
    let categoryId = "";
    categories.forEach((category) => {
        if(category["category_id"] === 0){
            categoryId = category["id"];
            Object.assign(categoryList, {[category.id]:{"nombre":category.name}});
            
        }
    });
    return categoryList;
}

export const getProductCategoryPath = (category, categories) => {
    let category_id;
    let categoryList = [];
    categories.forEach((cat) => {
        if(cat.id === category){
            categoryList.push(cat.name);
            category_id = cat.category_id;
            while (category_id !== 0) {
                categories.forEach((cate) => {
                    if(cate.id === category_id){
                        category_id = cate.category_id;
                        categoryList.push(cate.name);
                    }
                });
            }
        } 
    });
    const categorias = categoryList.reverse().join("/");
    return categorias;
}

export const getCategoriesList = (categoryBase, categories) => {
    let categoryList = [];
    let category_id;
    categories.forEach((cat) => {
        if(cat.category_id === categoryBase){
            categoryList.push(cat.name);
        } 
    });
    return categoryList;
}

export const setImagePath = (image)=>{
    if(typeof image!=="undefined" && image!==null && image!==""){
        const patt = /^https?:\/\//g;
        if(patt.test(image)){
            return image;
        }
        const base_url = window.location.origin;
        return base_url+"/img/products/"+image;
    }
    return "";
}

export const setThumbImagePath = (image)=>{
    if(typeof image!=="undefined" && image!==null && image!==""){
        const patt = /^https?:\/\//g;
        if(patt.test(image)){
            return image;
        }
        const base_url = window.location.origin;
        return base_url+"/img/thumbs/products/"+image;
    }
    return "";
}

export const saveFile=(url, method, fileNameAndExtension)=>{
    axios({
        url: url,
        method: method,
        responseType: 'blob', // important
      }).then((response) => {
         const url = window.URL.createObjectURL(new Blob([response.data]));
         const link = document.createElement('a');
         link.href = url;
         link.setAttribute('download', fileNameAndExtension); //or any other extension
         document.body.appendChild(link);
         link.click();
      }).catch(function (error) {
         console.log(error);
      });
}

export const onlyWordsReg = /[A-Za-z0-9áéíóú ]+/g; //Regex for only words
export const onlynumbersReg = /^\d*$/g; //Regex for only numbers
export const onlylettersReg = /[A-Za-záéíóú ]+/g; //Regex for only Chars
export const codeCountry = /^\+[0-9]+$/g; //Regex for international CodePhone
export const onlyEmail = /^[a-z0-9.-]+@[a-z0-9.-]+/g; //Regex for Email

export const server = "http://api.localhost:8000";