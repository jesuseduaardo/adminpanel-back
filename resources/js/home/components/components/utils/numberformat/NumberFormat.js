import React, { Component } from 'react';
import "./number-format.css";
import { with2Decimals } from "../utils";

class NumberFormat extends Component {

    zeroPad = (num, places) => String(num).padEnd(places, '0');

    render() { 
        let entire = 0;
        let decimal = 0;
        let number = this.props.children;
        if(isNaN(number)){
            number = 0;
        }
        const twoDecimals = with2Decimals(number);
        const numbers = twoDecimals.toString().split(".");
        if(numbers.length > 0){
            entire = parseInt(numbers[0]);
        }
        if(numbers.length > 1){
            decimal = numbers[1];
        }
        return ( 
            <span className="number-format">
                <span className="entero">{entire.toLocaleString('de-DE', { style: 'decimal', decimal: '2' })}</span> 
                <span className="decimal">{ this.zeroPad(decimal, 2) }</span>
            </span>
         );
    }
}
 
export default NumberFormat;