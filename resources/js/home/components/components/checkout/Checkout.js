import React, { useState, useEffect, useRef } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { 
  Box, 
  Stepper, 
  Step, 
  StepLabel, 
  Button, 
  Typography, 
  Card, 
  CardActions, 
  CardContent 
}  from '@material-ui/core';
import Paymethod from './paymethod/Paymethod';
import Subtotal from './subtotal/Subtotal';
import Sumary from '../sumary/Sumary';
import AddressForm from './addressform/AddressForm';
import PaymethodSelect from './paymethodSelect/PaymethodSelect';
import AjaxRequest from '../ajaxRequest/AjaxRequest';
import Result from '../detalleCompra/result/Result';
import PersonalForm from './personalform/PersonalForm';
import history from '../history';
import "./checkout.css";

//Redux
import { connect } from 'react-redux';
import { mostrarProductos, agregarProducto, borrarProducto, borrarProductos } from '../actions/cartActions';
import {
  setCheckout,
  setSumary,
  setActiveStep,
  setEnableNext
} from '../actions/checkoutActions';

import {onlyWordsReg, onlynumbersReg, onlylettersReg, codeCountry, onlyEmail} from '../utils/utils';

const useStyles = makeStyles(theme => ({
  card: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
}));

function Checkout(props){

  const checkout = props.checkout;

  const classes = useStyles();

  const firstRender = useRef(true);

  const [loading, setLoading] = useState(false);

  useEffect(()=>{
    setLoading(props.loading.loading);
    if(typeof props.checkout.checkout.status !== "undefined" && props.checkout.checkout.status == "200"){
      localStorage.removeItem("items");
      history.push("/compras/"+props.checkout.checkout.order+"/detalle");
    }
}, [props.checkout])

  const getSteps=()=>{
    return ['Datos Comprador', 'Direccion de envio', 'Metodo de pago', 'Confirma orden'];
  }

  const handleNext = () => {
    props.setActiveStep(checkout.activeStep + 1);
    window.scrollTo(0,0);
  };

  const handleBack = () => {
    props.setActiveStep(checkout.activeStep -1);
    window.scrollTo(0,0);
  };

  const handleReset = () => {
    props.setActiveStep(0);
    window.scrollTo(0,0);
  };

  const setMethodSelected = (method) =>{
    props.setPaymethod(method);
  }

  const validaPersonalData=()=>{
    const {nombre, apellido, tipoDocumento, documento, codigo, telefono, email} = props.checkout.personalData;
    if(email !== "" && email.length >= 3 && email.match(onlyEmail)!==null){
      if(telefono !== "" && telefono.length >= 3 && telefono.match(onlynumbersReg)!==null){
        if(codigo !== "" && codigo.length >= 3 && codigo.match(codeCountry)!==null){
        if(documento !== "" && documento.length >= 6 && documento.match(onlynumbersReg)!==null){
          if(tipoDocumento !== ""){
            if(apellido !=="" && apellido.length >= 3 && apellido.match(onlylettersReg)!==null){
                if(nombre!=="" && nombre.length >= 3 && nombre.match(onlylettersReg)!==null){
                  return false;
                }
              }
            }
          }
        }
      }
    }
    return true;
  }

  const validAddress=(data)=>{
    const { direccion, ciudad, estado, zip, pais } = data;
    if(pais!=="" && pais.length >=3 && pais.match(onlyWordsReg)!==null){
      if(zip!=="" && zip.length >=4 && zip.match(onlynumbersReg)!==null){
        if(estado!=="" && estado.length >=3 && estado.match(onlyWordsReg)!==null){
          if(ciudad!=="" && ciudad.length >=3 && ciudad.match(onlyWordsReg)!==null){
            if(direccion!=="" && direccion.length >=3 && direccion.match(onlyWordsReg)!==null){
              return true;
            }
          }
        }
      }
    }
    return false;
  }

  const validaDireccionData=()=>{
    const {addressData, fiscalData, pickupinstore, addressAsFiscal} = checkout;
    let validAdress = validAddress(addressData);
    let validFiscal = validAddress(fiscalData);

    if(pickupinstore){
      return true;
    }
    if(addressAsFiscal  && validAdress){
      return true;
    }
    if(validFiscal && validAdress){
      return true;
    }
    return false;
  }

  const useAddressAsFiscal=(bool)=>{
    const address = addressData;
    setFiscalData(bool ? {...address} : {direccion:"",ciudad:"",estado:"",zip:"",pais:""});
  }

  const getStepContent=(step)=>{
    switch (step) {
      case 0:
        return (<PersonalForm/>);
      case 1:
        return (<AddressForm/>);
      case 2:
        return (<PaymethodSelect/>);
      case 3:
        return checkoutTotal();
      default:
        return 'Unknown step';
    }
  }

  const activeNext=()=>{
    switch (checkout.activeStep) {
      case 0:
        return (
          validaPersonalData()
          );
      case 1:        
        return (
          !validaDireccionData()
          );
      case 2:
        return checkout.paymethod==="";
      case 3:
        return false;
      default:
        return false;
    }
  }

  const updateSumary = (data) => {
    if(typeof data.type === "undefined"){
        props.borrarProductos(data);
    }
    if(data.type ==="down"){
        props.borrarProducto(data);
    }
    if(data.type==="up"){
        props.agregarProducto(data);
    }
 }

const getTotalItems=(data)=>{
  let cantidad = 0;
  data.forEach(item => {
      cantidad+=item.cantidad;
  });
  return cantidad;
}

const checkoutTotal=()=>{
  return(
    <Card className={classes.card}>
      <CardContent>
        <Sumary updateSumary={updateSumary} />
        <Subtotal />
      </CardContent>
    </Card>
  )
}

const confirmOrder=()=>{
  const checkout = {...props.checkout, sumary:{...props.cart}};
  props.setCheckout(checkout);
  window.scrollTo(0,0);
}

const steps = getSteps();

return ( 
  <React.Fragment>
    <Stepper activeStep={checkout.activeStep} alternativeLabel style={{padding:"30px 0"}}>
      {steps.map((label, index) => {
        const stepProps = {};
        const labelProps = {};
        return (
          <Step key={label} {...stepProps}>
            <StepLabel {...labelProps}>{label}</StepLabel>
          </Step>
        );
      })}
    </Stepper>
      
      {props.activeStep === steps.length ? (
        <div className="stepperContent">
          
        </div>
      ) : (
        <div className="stepperContent">
          <AjaxRequest request={props.loading} className={ loading ? "visible":"hidden"}/>
            <Result data={checkout.checkout} />
            <Box component="span" visibility={ props.loading.loading ? "hidden":"visible" }>
              {getStepContent(checkout.activeStep)}
              <div className="stepperActions">
                <Button disabled={checkout.activeStep === 0} onClick={handleBack} className={classes.button}>
                  Volver
                </Button>
                {checkout.activeStep === steps.length - 1 ? <Paymethod confirmOrder={confirmOrder}/> : 
                <Button
                  variant="contained"
                  color="primary" 
                  disabled={activeNext()}
                  onClick={handleNext}
                  className={classes.button}
                >Siguiente</Button>}
            </div>
          </Box>
        </div>
    )}
</React.Fragment>
  );
}

//state
const mapStateToProps = state => ({
  loading: state.loading,
  checkout: state.checkout,
  cart: state.cart.items,
});
export default connect(mapStateToProps, { mostrarProductos, agregarProducto, 
                                          borrarProducto, borrarProductos, setCheckout,
                                          setSumary, setActiveStep, setEnableNext
                                        })(Checkout);