import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Typography, FormControlLabel, Checkbox, Collapse } from '@material-ui/core';
import Form from './form/form';
import "./addressForm.css";

//Redux
import { connect } from 'react-redux';
import {
  setAddressData,
  setFiscalData,
  setPickupInStore,
  setAddressAsFiscal,
  setActiveStep,
  setEnableNext
} from '../../actions/checkoutActions';

const useStyles = makeStyles(theme => ({
      mainContainer:{
          marginBottom: theme.spacing(3),
          padding: theme.spacing(0, 2),
      }
  }));

function AddressForm(props){

    const classes = useStyles(); 

    const [pickupinstore, setPickupInStore] = useState(false);
    const [noBussinessAddress, setNoBussinessAddress] = useState(false);
    

    useEffect(()=>{
        setPickupInStore(props.checkout.pickupinstore);
        setNoBussinessAddress(props.checkout.addressAsFiscal);
    }, [])

    const usePickupInStore=()=>{
        props.setPickupInStore(!pickupinstore);
        setPickupInStore(!pickupinstore);
    }

    const useAddressAsFiscal=()=>{
        props.setAddressAsFiscal(!noBussinessAddress);
        setNoBussinessAddress(!noBussinessAddress);
    }

    const getAddress=(data)=>{
        props.setAddressData(data);
    }
    const getFiscalAddress=(data)=>{
        props.setFiscalData(data);
    }

    return ( 
        <React.Fragment>
            <div className={classes.mainContainer}>
                <Grid item xs={12}>
                    <FormControlLabel
                        control={
                            <Checkbox color="primary" 
                                name="pickupinstore" 
                                value="yes" 
                                checked={pickupinstore}
                                onChange={usePickupInStore}
                            />}
                        label="Lo retiro en Tienda"
                    />
                </Grid>
                <Collapse in={!pickupinstore}>
                <Grid item xs={12} >
                    <Form setData={getAddress} data={props.checkout.addressData}/>
                </Grid>
                <Grid item xs={12} >
                    <Typography variant="h6" style={{padding:"32px 0 0px", textAlign:"center"}}>
                        Direccion de Facturacion
                    </Typography>
                    <FormControlLabel
                        control={
                            <Checkbox color="primary" 
                                name="noBussinessAddress" 
                                value={noBussinessAddress}
                                checked={noBussinessAddress}
                                onChange={useAddressAsFiscal} 
                                //disabled={this.enableSameAddress()}
                            />}
                        label="Usar la misma direccion de envio"
                    />
                </Grid>
                <Collapse in={!noBussinessAddress}>
                    <Grid item xs={12} >
                        <Form setData={getFiscalAddress} data={props.checkout.fiscalData}/>
                    </Grid>
                </Collapse>
            </Collapse>
            </div>
        </React.Fragment>
    );
}

//state
const mapStateToProps = state => ({
    checkout: state.checkout,
});
 
export default connect(mapStateToProps, { setAddressData, setFiscalData, setPickupInStore, 
                                            setAddressAsFiscal, setActiveStep, setEnableNext
                                        })(AddressForm);