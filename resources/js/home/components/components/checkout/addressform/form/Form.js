import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, TextField, FormControl, InputLabel, Select, Input, MenuItem } from '@material-ui/core';
import { countryList } from '../../../utils/countryList';

const useStyles = makeStyles(theme => ({
    formControl: {
      margin: theme.spacing(0),
      minWidth: 150,
      width: "100%"
    },
    mainContainer:{
        marginBottom: theme.spacing(3),
    }
  }));

function Form(props){

    const { countryCodes } = countryList;
    
    const classes = useStyles();

    const [values, setValues] = useState({
            direccion:"",
            ciudad:"",
            estado:"",
            zip:"",
            pais:"",
        });

    useEffect(()=>{
        setValues({...props.data})
    }, [])

    const handleChange=(e)=>{
            setValues({ ...values, [e.target.name] : e.target.value });
        }
    const handleBlur=(e)=>{
        props.setData({...values, [e.target.name]:e.target.value})
    }

    const getCountries = (countries) => {
        return(
            countries.map((country, index)=>(
                <MenuItem key={index} value={country.country_name}>{country.country_name}</MenuItem>
            ))
        )
    }

    return (
        <Grid container spacing={3}>
            <Grid item xs={12}>
                <TextField
                    required
                    name="direccion"
                    label="Direccion" 
                    value={values.direccion}
                    fullWidth
                    autoComplete="billing address-line1" 
                    onChange={handleChange}
                    onBlur={handleBlur}
                />
            </Grid>
            <Grid item xs={12} sm={6}>
                <TextField
                    required
                    name="ciudad"
                    label="Ciudad" 
                    value={values.ciudad}
                    fullWidth
                    autoComplete="billing address-level2" 
                    onChange={handleChange} 
                    onBlur={handleBlur}
                />
            </Grid>
            <Grid item xs={12} sm={6}>
                <TextField 
                    required
                    name="estado" 
                    label="Estado/Provincia/Region" 
                    value={values.estado}
                    fullWidth 
                    onChange={handleChange} 
                    onBlur={handleBlur}
                />
            </Grid>
            <Grid item xs={12} sm={6}>
                <TextField
                    required
                    name="zip"
                    label="Codigo Postal"
                    value={values.zip}
                    fullWidth
                    autoComplete="billing postal-code" 
                    onChange={handleChange} 
                    onBlur={handleBlur}
                />
            </Grid>
            <Grid item xs={12} md={6}>
                <FormControl required className={classes.formControl}>
                    <InputLabel htmlFor="pais">Pais</InputLabel>
                    <Select
                        name="pais" 
                        value={values.pais}
                        onChange={handleChange} 
                        onBlur={handleBlur}
                        input={<Input id="pais" />} 
                    >
                        {getCountries(countryCodes)}
                    </Select>
                </FormControl>
            </Grid>
        </Grid>
    );
}
export default Form;