import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, List, ListItem, ListItemIcon, ListItemText, Divider, Collapse, Typography } from '@material-ui/core';
import { ExpandLess, ExpandMore, Delete, Remove } from '@material-ui/icons';
import {summaryTotal} from '../../utils/utils';
import NumberFormat from '../../utils/numberformat/NumberFormat';

//Redux
import { connect } from 'react-redux';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));

function Subtotal(props){

  const classes = useStyles();
  
  const [totalDetails, setTotalDetails] = useState(false);
  const [dataItems, setDataItems] = useState([]);
  const [cost, setCost] = useState({
      impuestos: 0,
      subtotal: 0,
      total: 0,
      descuentoPorc: 0,
      descuentoMonto: 0
  });

  useEffect(()=>{
    const totales = summaryTotal(props.cart);
    if(totales !==0){
      setCost({
        ...cost,
        subtotal:totales.subtotal,
        impuestos:totales.impuestos,
        total:totales.total,
        descuentoMonto:totales.descuentoMonto,
        envio:totales.envio
      })
    }
  },[props.cart]);

  const showTotalDetails=()=>{
    setTotalDetails(!totalDetails);
  }

  return ( 
    <div className={classes.root}>
    <Grid container spacing={3}>
      <Grid item xs={12}>
          <List className={classes.root}>
              <Collapse in={totalDetails} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                  <ListItem>
                      <ListItemText primary="Subtotal" />
                      <NumberFormat>
                        {typeof cost.subtotal !== "undefined" ? cost.subtotal : 0}
                      </NumberFormat>
                  </ListItem>
                  <ListItem>
                      <ListItemIcon>
                        <Remove/>
                      </ListItemIcon>
                      <ListItemText primary="Impuestos" />
                        <NumberFormat>
                          {typeof cost.impuestos !== "undefined" ? cost.impuestos : 0}
                        </NumberFormat>
                  </ListItem>
                  <ListItem>
                      <ListItemIcon>
                        <Remove/>
                      </ListItemIcon>
                      <ListItemText primary="Descuentos" />
                      <NumberFormat>
                        {typeof cost.descuentoMonto !== "undefined" ? cost.descuentoMonto : 0}
                      </NumberFormat>
                  </ListItem>
                  <Divider  component="li" />
                  
                </List>
              </Collapse>
              <ListItem button onClick={showTotalDetails}>
                  <ListItemIcon>
                    {totalDetails ? <ExpandLess /> : <ExpandMore />}
                  </ListItemIcon>
                  <ListItemText primary="Total" />
                  <NumberFormat>{typeof cost.total !== "undefined" ? cost.total : 0 }</NumberFormat>
                  
              </ListItem>
              { typeof cost.total !== "undefined" && typeof cost.envio !== "undefined" ?
                <React.Fragment>
                  <Divider  component="li" />
                  <ListItem>
                      <ListItemText primary="Envio" />
                      <NumberFormat>
                        {cost.envio}
                      </NumberFormat>
                  </ListItem>
                  <Divider  component="li" />
                  <ListItem>
                      <ListItemText primary="Total + Envio" />
                      <NumberFormat>
                        {cost.total + cost.envio}
                      </NumberFormat>
                  </ListItem>
                </React.Fragment>
                :""}
          </List>
      </Grid>
    </Grid>
  </div>

    );
}

//state
const mapStateToProps = state => ({
  cart: state.cart.items
});

export default connect(mapStateToProps, {})(Subtotal);