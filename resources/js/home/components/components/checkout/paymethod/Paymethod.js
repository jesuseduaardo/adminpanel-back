import React, { useState, useEffect } from 'react';
import { createMuiTheme, makeStyles, ThemeProvider } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import ThumbUp from '@material-ui/icons/ThumbUp';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1),
    width: '100%',
    color: "#fff"
  },
}));

export default function Paymethod(props) {
  const classes = useStyles();
  
  const [paymentMethod, setPaymentMethod] = React.useState(props);

  useEffect(() => {
    setPaymentMethod(props);
  }, [props])

  return (
    <div>
      <Button
            variant="contained"
            size="large"
            className={classes.button} 
            color="primary" 
            onClick={paymentMethod.confirmOrder}
          > 
            Confirmar Orden
      </Button>
    </div>
  );
}
