import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, TextField, Select, MenuItem, InputLabel, Input, FormControl } from '@material-ui/core';
import {valida} from '../../utils/validations/validate'; 
import { countryList } from '../../utils/countryList';

//Redux
import { connect } from 'react-redux';
import {
  setPersonalData,
  setActiveStep,
  setEnableNext
} from '../../actions/checkoutActions';

const useStyles = makeStyles(theme => ({
    formControl: {
        margin: theme.spacing(0),
        width: "100%"
      },
      mainContainer:{
          marginBottom: theme.spacing(3),
          padding: theme.spacing(0, 2),
      }
  }));

function PersonalForm(props){

    const classes = useStyles(); 
    const { countryCodes } = countryList;

    const [value, setValue] = useState({
        "nombre":"",
        "apellido":"",
        "tipoDocumento":"",
        "documento":"",
        "codigo":"",
        "telefono":"",
        "email":""
    });
    const [error, setError] = useState({});

    useEffect(()=>{
        setValue({...props.checkout.personalData})
    }, [])

    const handleChange=(e)=>{
        const name = e.target.name;
        const val = e.target.value;
        const alphabetic = /^[A-Za-z ]+$/;
        const numeric = /^[0-9]+$/;
        switch (name) {
            case "nombre":
            case "apellido":
            case "tipoDocumento":
                if(!alphabetic.test(val)){
                    setError({...error, [name]:true });
                }else{
                    setError({...error, [name]:undefined });
                }
                break;
            case "documento":
                if(!numeric.test(val)){
                    setError({...error, [name]:true });
                }else{
                    setError({...error, [name]:undefined });
                }
                break;
            case "telefono":
                if(!numeric.test(val)){
                    setError({...error, [name]:true });
                }else{
                    setError({...error, [name]:undefined });
                }
                break;
            default:
                break;
        }
        setValue({...value, [name]:val});
        props.setPersonalData({...value, [name]:val})
    }

    const getCountryIcon = country =>{
        const countryCode =  country.toLowerCase();
        return <span className={`flag-icon flag-icon-${countryCode}`}></span>;
    }

    return ( 
        <React.Fragment>
            <Grid container spacing={3} className={classes.mainContainer}>
                <Grid item xs={12} md={6}>
                    <TextField
                        required 
                        value= {value.nombre}
                        name="nombre"
                        label="Nombre"
                        fullWidth
                        error={ typeof error.nombre !== "undefined"}
                        helperText={ 
                            typeof error.nombre !== "undefined" ? "Nombre no puede tener numeros o caracteres especiales" : "" 
                        }
                        onChange={handleChange}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField
                        required
                        value= {value.apellido}
                        name="apellido"
                        label="Apellido"
                        fullWidth
                        onChange={handleChange} 
                        error={ typeof error.apellido !== "undefined" }
                        helperText={ 
                            typeof error.apellido !== "undefined" ? "Apellido no puede tener numeros o caracteres especiales" : "" 
                        }
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <FormControl required className={classes.formControl}>
                        <InputLabel htmlFor="tipoDocumento">Tipo Documento</InputLabel>
                        <Select
                            name="tipoDocumento"
                            value={value.tipoDocumento}
                            onChange={handleChange}
                            input={<Input id="tipoDocumento" />} 
                            required
                        >
                            <MenuItem value='undefined'></MenuItem>
                            <MenuItem value="dni">Documento Nacional de Identidad</MenuItem>
                            <MenuItem value="passport">Pasaporte</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={12} md={6}>
                        <TextField
                            required
                            value= {value.documento}
                            name="documento"
                            label="Nro Documento"
                            fullWidth
                            onChange={handleChange} 
                            error={ typeof error.documento !== "undefined" }
                            helperText={ 
                                typeof error.documento !== "undefined" ? "Nro Documento no puede tener letras o caracteres especiales" : "" 
                            }
                        />
                </Grid>
                <Grid container item xs={12} md={6}>
                    <Grid item xs={4}>
                        <FormControl required className={classes.formControl}>
                            <InputLabel htmlFor="codigo">Pais</InputLabel>
                            <Select
                                name="codigo" 
                                value={typeof value.codigo !== "undefined" ? value.codigo : ""}
                                onChange={handleChange} 
                                // onBlur={handleBlur}
                                input={<Input id="codigo" />} 
                            >
                               
                                {
                                    countryCodes.map((country, index)=>(
                                        typeof country !== "undefined" || country !== ""? 
                                        <MenuItem 
                                            key={index} 
                                            title = {country.country_name}
                                            value={country.dialling_code}>
                                                {getCountryIcon(country.country_code)}
                                                {" "} 
                                                {country.dialling_code} 
                                        </MenuItem>
                                    :""
                                    ))
                                }
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={8}>
                        <TextField
                            required
                            value= {value.telefono}
                            name="telefono"
                            label="Teléfono"
                            fullWidth
                            onChange={handleChange} 
                            error={ typeof error.telefono !== "undefined" }
                            helperText={ 
                                typeof error.telefono !== "undefined" ? "Nro de Telefono debe contener solo numeros" : "" 
                            }
                        />
                    </Grid>
                </Grid>
                <Grid item xs={12} md={6}>
                        <TextField
                            required
                            value= {value.email}
                            name="email"
                            label="Email"
                            fullWidth
                            onChange={handleChange} 
                            error={ typeof error.email !== "undefined" }
                            helperText={ 
                                typeof error.email !== "undefined" ? "Correo electronico no puede tener caracteres invalidos" : "" 
                            }
                        />
                </Grid>
            </Grid>
        </React.Fragment>
        );
}

//state
const mapStateToProps = state => ({
    checkout: state.checkout,
    location: state.location,
  });
 
export default connect(mapStateToProps, {setPersonalData, setActiveStep, setEnableNext 
                                        })(PersonalForm);