import React, { useState, useEffect, useRef } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { List, ListItem, ListItemIcon, ListItemText, Divider } from '@material-ui/core';
import AjaxRequest from '../../ajaxRequest/AjaxRequest';
import { server } from '../../utils/utils';
import "./paymentfont.css";

//Redux
import { connect } from 'react-redux';
import { setPaymethod } from '../../actions/checkoutActions';
import axios from 'axios';


const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
        margin:"auto",
        padding: theme.spacing(0, 2),
      },
      visible: {
          display:'block'
      },
      hidden: {
          display: 'none'
      }
  }));

function PaymethodSelect(props){

    const classes = useStyles();
    const firstRender = useRef(true);
    
    const [selectedIndex, setSelectedIndex] = useState("");
    const [paymentMethods, setPaymentMethods] = useState([]);
    const [loading, setLoading] = useState({loading:true});

    useEffect(()=>{
        if(firstRender.current){
            axios.get(`${server}/paymentmethod`)
                .then(res => {
                const paymentMethods = res.data;
                setPaymentMethods(paymentMethods);
                setLoading({loading:false});
            });
            firstRender.current=false;
        }
        setSelectedIndex(props.checkout.paymethod);
    }, [props.checkout.paymethod]);
    
    const handleListItemClick = (event, index) => {
        props.setPaymethod(index);
    };
    return ( 

        <div className={classes.root}>
            <AjaxRequest request={loading} className={ loading.loading ? classes.visible : classes.hidden }/>
            <List component="nav" aria-label="main mailbox folders" className={ loading.loading ? classes.hidden : classes.visible }>
                {
                    Object.entries(paymentMethods).length !== 0 && paymentMethods.constructor !== Object ?
                    paymentMethods.map((paymethod, key)=>(
                        <ListItem 
                            key={key}
                            button
                            selected={ selectedIndex === paymethod.id }
                            onClick={(e)=>handleListItemClick(event, paymethod.id)}
                        >
                            <ListItemIcon>
                                <i className={`pf ${ paymethod.payment_image }`}></i>
                            </ListItemIcon>
                            <ListItemText primary={ paymethod.payment_description } />
                        </ListItem>
                    )) : ""
                }
                <Divider />
                <ListItem
                    button
                    selected={selectedIndex === 0}
                    onClick={(e)=>handleListItemClick(event, 0)}
                    >
                    <ListItemText primary="Acordar con el vendedor" />
                </ListItem>
            </List>
        </div>
    );
}

//state
const mapStateToProps = state => ({
    checkout: state.checkout,
});
 
export default connect(mapStateToProps, { setPaymethod })(PaymethodSelect)