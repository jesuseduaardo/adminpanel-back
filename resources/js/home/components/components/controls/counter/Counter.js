import React, { useState, useEffect } from 'react';
import "./counter.css";
import { IconButton } from '@material-ui/core';
import { KeyboardArrowLeft, KeyboardArrowRight }  from '@material-ui/icons';

function Counter(props){

    const [count, setCount] = useState(1);
    const [stock, setStock] = useState(0);
 
    useEffect(()=>{
        setCount(props.data.cantidad);
        setStock(props.data.stock);
    }, [props.data]);

    const handleChange=(e)=>{
        const val = parseInt(e.target.value);
        setCount(isNaN(val) ? 0 : parseInt(e.target.value))
    }

    const addValue = () =>{
         if(count < stock){
            props.updateQty("up");
         }
     }

    const lessValue = () =>{
         if(count > 1){
            props.updateQty("down");
         }
    }

    return ( 
        <React.Fragment>
            <div className="contenedor">
                <div className="boton-arriba">
                    <IconButton onClick={addValue}>
                        <KeyboardArrowRight />
                        </IconButton>
                </div>
                <input  type="text" className="inputContador" 
                    value={count || ""} 
                    onChange={handleChange} />
                    <div className="boton-abajo">
                        <IconButton onClick={lessValue}>
                                <KeyboardArrowLeft />
                        </IconButton>
                    </div>
                </div>
            </React.Fragment>
        );
}
 
export default Counter;