import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Chip from '@material-ui/core/Chip';

const styles = theme => ({
    formControl: {
        margin: theme.spacing(1),
        width: "100%",
      },
})

class FilterCategories extends Component {
    state = { 
        categoria:''
     }

    handleChange=(e)=>{
        this.setState({
            ...this.state,
            categoria: e.target.value,
        }, ()=>{
            this.props.filtrocategoria(e.target.value)
        });
    }

    handleDelete = () => {
        this.setState({
            categoria:''
        }, ()=>{
            this.props.filtrocategoria('')
        })
    }
    
    // handleClick = () => {
    //     console.info('You clicked the Chip.');
    // }
    
    render() { 
        const { classes, categories }  = this.props;
        return ( 
            <FormControl className={classes.formControl}>
                {this.state.categoria==='' ? 
                    <React.Fragment>
                    <InputLabel id="categoria-label">Categoria</InputLabel>
                    <Select
                        labelId="categoria-label"
                        id="categoria-simple-select"
                        value={this.state.categoria}
                        onChange={(e)=>this.handleChange(e)}
                    >
                        <MenuItem value=""><em>Ninguno</em></MenuItem>
                        {categories.map((category, key) => (
                            <MenuItem key={key} value={category}>{category}</MenuItem>
                        ))}
                    </Select>
                    </React.Fragment>
                :   <Chip
                        label={this.state.categoria}
                        onClick={()=>this.handleClick()}
                        onDelete={this.handleDelete}
                    /> 
               }
            </FormControl>
         );
    }
}
 
export default withStyles(styles)(FilterCategories);