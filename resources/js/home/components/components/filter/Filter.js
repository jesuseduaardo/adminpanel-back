import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router';
import MuiExpansionPanel from '@material-ui/core/ExpansionPanel';
import MuiExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import MuiExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import FilterCategories from './categories/FilterCategories';
import FilterPromo from './promo/FilterPromo';
import { getCategoriesList } from '../utils/utils';
import FilterPrice from './price/FilterPrice';
import "./filter.css";

const ExpansionPanel = withStyles({
    root: {
      width:'100%',  
      border: '1px solid rgba(0, 0, 0, .125)',
      boxShadow: 'none',
      '&:before': {
        display: 'none',
      },
      '&$expanded': {
        margin: 'auto',
      },
    },
    expanded: {},
  })(MuiExpansionPanel);
  
  const ExpansionPanelSummary = withStyles({
    root: {
      backgroundColor: 'rgba(0, 0, 0, .03)',
      borderBottom: '1px solid rgba(0, 0, 0, .125)',
      marginBottom: -1,
      minHeight: 56,
      '&$expanded': {
        minHeight: 56,
      },
    },
    content: {
      '&$expanded': {
        margin: '12px 0',
      },
    },
    expanded: {},
  })(MuiExpansionPanelSummary);
  
  const ExpansionPanelDetails = withStyles(theme => ({
    root: {
      display: 'flex',
      flexFlow: 'column',
      padding: theme.spacing(2),
    },
  }))(MuiExpansionPanelDetails);

class Filter extends Component {
    state = { 
        expanded:false,
        categories: [],
        destacadoList:[],
        prices:[],
        categoria:"",
        promo:"",
        precio:{min:0, max:0},
     }

    componentDidMount(){
        this.setState({
            ...this.state,
            categories:this.getCategories(),
        })
    }

    componentDidUpdate(prevProps){
        if(prevProps !== this.props){
            this.setState({
                ...this.state,
                destacadoList: this.filterPromo(this.props.productos),
                prices:this.filterPrice(this.props.productos),
                categories:this.getCategories(),
            });
        }
    }

    filtrar(){
        this.props.filtrar({
            "categoria":this.state.categoria,
            "evento":this.state.promo,
            "price":this.state.precio,
        })
    }

    handleChange =()=> {
        this.setState({
            expanded:!this.state.expanded
        });
    };

    filterPromo(destaques){
        let eventos = [];
        if(Object.keys(destaques).length > 0){
            Object.keys(destaques).map((value)=>{
                let evento = destaques[value].evento;
                if(typeof evento !== "undefined" && evento!==""){
                    if(!eventos.includes(evento)){
                        eventos.push(evento);
                    }
                }
            })
        }
        return eventos;
    }

    filterPrice(productos){
        let prices = [];
        if(Object.keys(productos).length > 0){
            Object.keys(productos).map((value)=>{
                let price = productos[value].price;
                let tax = productos[value].tax;
                if(typeof price !== "undefined" && price!==""){
                    if(!prices.includes(price)){
                        prices.push(price + ((price * tax) /100 ));
                    }
                }
            })
        }
        prices.sort(function(a, b){return a-b});
        return prices;
    }

    getCategories(){
        const handle = this.props.location.pathname;
        const match = handle.match(/([a-z]+)\/$/i);
        let categoryName;
        if(typeof match !== "undefined" && match!=null && Array.isArray(match)){
            categoryName = match[1].toLowerCase();
        }
        const categoryList = JSON.parse(sessionStorage.getItem("categorias")) || [];
        let categoryBase;
        categoryList.map((cat)=>{
            if(cat.name.toLowerCase() === categoryName){
                categoryBase = cat.id;
            }
        });
        if(categoryList.length > 0 ){
            return getCategoriesList(categoryBase, categoryList);
        }
        return [];
    }

    filtroCategoria=(category)=>{
        this.setState({
            ...this.state,
            categoria:category,
        }, ()=>{
            this.filtrar();
        })
    }
    filtroPromo=(promo)=>{
        this.setState({
            ...this.state,
            "promo":promo,
        }, ()=>{
            this.filtrar()
        });
    }

    filtroPrecio=(precio)=>{
        this.setState({
            ...this.state,
            "precio":precio,
        }, ()=>{
            this.filtrar()
        });
    }

    render() { 
        const {categories, destacadoList, prices} = this.state;

        return ( 
            <ExpansionPanel className="expansion-panel" square expanded={this.state.expanded} onChange={()=>this.handleChange()}>
                <ExpansionPanelSummary 
                    expandIcon={<ExpandMoreIcon />}
                    className="expansion-panel-summary" 
                    aria-controls="panel1d-content" 
                    id="panel1d-header">
                    <Typography>Filtros</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <FilterCategories categories={categories} filtrocategoria={this.filtroCategoria}/>
                    <FilterPromo destaques={destacadoList} filtropromo={this.filtroPromo}/>
                    <FilterPrice prices={prices} filtroprecio={this.filtroPrecio}/>
                </ExpansionPanelDetails>
            </ExpansionPanel>
         );
    }
}
 
export default withRouter(Filter);