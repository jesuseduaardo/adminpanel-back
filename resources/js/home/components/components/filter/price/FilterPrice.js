import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import InputRange from 'react-input-range';
import "./filter-price.css";

const styles = theme => ({
    root: {
      width: "100%",
      margin:"8px",
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
      color: "rgb(175, 175, 175)",
      touchAction: 'none'
    },
  });

class FilterPrice extends Component {
    state = {  
        value: { min: 0, max: 0 },
        min:0,
        max:100,
    }

    componentDidMount(){
        const min = this.props.prices[0];
        const max = this.props.prices[this.props.prices.length - 1];
        this.setState({
            ...this.state,
            "min": parseInt(min), 
            "max": parseInt(max),
            value: { "min": parseInt(min), "max": parseInt(max) }
        });
    }
    componentDidUpdate(prevProps){
        if(prevProps !== this.props){
            if(isNaN(this.state.min)){
                const minimo = parseInt(this.props.prices[0]);
                const maximo = parseInt(this.props.prices[this.props.prices.length - 1]);
                this.setState({
                    ...this.state,
                    "min": minimo, 
                    "max": maximo,
                    value: { "min": minimo, "max": maximo },
                });
            }
        }
    }

    handleChange = (value) => {
        this.setState({
            ...this.state,
            'value': value
        }, ()=>{});
    };

    updatePrecio(value){
        const {min, max} = this.state;
        if(value.min === min && value.max === max){
            this.props.filtroprecio({'min':0, 'max':0});
        }else{
            this.props.filtroprecio(this.state.value)
        }
    }
        
    render() { 

        const { classes } = this.props;
        const startValue = this.state.value;

        return ( 
            <div className={classes.root}>
                <Typography id="range-slider" gutterBottom>
                    Rango Precio
                </Typography>
                <div className="input-range-container">
                    <InputRange 
                        formatLabel={value => `$${value}`}
                        maxValue={ this.state.max }
                        minValue={ this.state.min }
                        value={startValue}
                        onChange={(value) => this.handleChange(value)}
                        onChangeComplete={(value)=> this.updatePrecio(value) } />
                </div>
            </div>
         );
    }
}
 
export default withStyles(styles)(FilterPrice);