import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Chip from '@material-ui/core/Chip';

const styles = theme => ({
    formControl: {
        margin: theme.spacing(1),
        width: "100%",
      },
})

class FilterPromo extends Component {
    state = { 
        destacado:'',
        destacadoList:[]
     }
    
    componentDidUpdate(prevProps){
        if(prevProps !== this.props){
            this.setState({
                ...this.state,
                destacadoList: this.props.destaques
            });
        }
    }

    handleChange(e){
        this.setState({
            ...this.state,
        destacado: e.target.value
        }, ()=> this.props.filtropromo(e.target.value))
    }

    handleDelete = () => {
        this.setState({
            destacado:''
        }, ()=>{
            this.props.filtropromo('')
        })
    }
    
    render() { 
        const { classes }  = this.props;
        const destacados = this.state.destacadoList;
        return ( 
            <FormControl className={classes.formControl}>
                {this.state.destacado==='' ? 
                <React.Fragment>
                    <InputLabel id="categoria-label">Tipo promocion</InputLabel>
                    <Select
                        labelId="categoria-label"
                        id="categoria-simple-select"
                        value={this.state.destacado}
                        onChange={(e)=>this.handleChange(e)}
                    >
                        <MenuItem value=""><em>Ninguno</em></MenuItem>
                        {destacados.map((destaque, key) => (
                            <MenuItem key={key} value={destaque}>{destaque}</MenuItem>
                        ))}
                    </Select>
                </React.Fragment> : 
                <Chip
                    label={this.state.destacado}
                    onClick={()=>this.handleClick()}
                    onDelete={this.handleDelete}
                /> 
                }
            </FormControl>
         );
    }
}
 
export default withStyles(styles)(FilterPromo);
