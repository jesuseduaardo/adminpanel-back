import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {Check, Clear} from '@material-ui/icons';
import {IconButton, Button, Dialog, DialogTitle, DialogContentText, DialogActions, DialogContent, Typography} from '@material-ui/core';
import "./result.css";

const useStyles = makeStyles(theme => ({
    root: {
    },
    paper: {
        position: 'absolute',
        width: "85%",
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(1),
      },
    button: {
        margin: theme.spacing(1),
        width: '90%',
        color: "#fff",
    },
}));

const Result = (props) =>{

    const classes = useStyles();

    const [status, setStatus] = useState("");
    const [message, setMessage] = useState("");
    const [open, setOpen] =  useState(false);
    const [buttonText, setButtonText] = useState("");


    useEffect(()=>{
        setStatus(props.data.status);
        setMessage(props.data.message);
        setOpen(props.data.open || false);
        setButtonText(props.data.status === "200" ? "Ver detalle de compra" : "Cerrar");
    }, [props.data.status, props.data.message, props.data.open]);

    const getModalStyle=()=>{
        const top = 50;
        const left = 50;
      
        return {
          top: `${top}%`,
          left: `${left}%`,
          transform: `translate(-${top}%, -${left}%)`,
        };
      }


    const checkStatus=(status)=>{
        return status == "200" ? success() : error();
    }

    const success=()=>{
        return(
            <DialogContentText>
                <div className="icon_container result_success">
                    <Check style={{ fontSize: 50 }}/>
                </div>
                <Typography variant="h4" className="message_text">
                    Se realizo la compra exitosamente!
                </Typography>
            </DialogContentText>
        );
    }

    const error=()=>{
        return(
            <DialogContentText>
                <div className="icon_container result_error">
                    <Clear style={{ fontSize: 50 }}/>
                </div>
                <Typography variant="h4" className="message_text">
                    No se pudo concretar la compra
                </Typography>
            </DialogContentText>
            );
    }

    return (
        <Dialog 
            fullScreen 
            open={open}
            onClose={()=>setOpen(false)}
            scroll="body"
            aria-labelledby="scroll-dialog-title"
            aria-describedby="scroll-dialog-description" 
        >
            <div className="titulo">
                <div>
                    <IconButton aria-label="delete" onClick={()=>setOpen(false)}>
                        <Clear />
                    </IconButton>
                </div>
            </div> 
            <div>
                <DialogContent dividers={false} className="modal">   
                    {checkStatus(status)}
                </DialogContent>
                <DialogActions>
                    <Button
                        variant="contained"
                        size="large"
                        className={ status==="200" ? "result_success" : "result_error" } 
                        onClick={()=>setOpen(false)}
                    > 
                        {buttonText}
                    </Button>
                </DialogActions>
            </div>
        </Dialog>
        );
}
 
export default Result;