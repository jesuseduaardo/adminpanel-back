import React, { useState, useEffect } from 'react';
import { TableContainer, Table, TableBody, TableRow, TableCell, Paper } from '@material-ui/core';
import NumberFormat from '../../utils/numberformat/NumberFormat';

const TotalDetail = (props) => {

    const [envio, setEnvio] = useState(0);
    const [impuesto, setImpuesto] = useState(0);
    const [descuento, setDescuento] = useState(0);
    const [subtotal, setSubtotal] = useState(0);

    useEffect(()=>{
        if(typeof props.data !== "undefined" && props.data.length > 0){
            let impuestoI = 0;
            let descuentoD = 0;
            let subtotalS = 0;
            props.data.map((val)=>{
                impuestoI += val.product_tax * val.product_qty;
                descuentoD += val.product_discount * val.product_qty;
                subtotalS += val.product_price * val.product_qty;
            });
            setEnvio(props.data.envio);
            setImpuesto(impuestoI);
            setDescuento(descuentoD);
            setSubtotal(subtotalS);
        }
    }, [props.data]);

    const getTotal=()=>{
        const total = (envio+impuesto+subtotal)-descuento;
        return total || 0;
    }
    const total = getTotal();
    return ( 
    <TableContainer>
      <Table aria-label="simple table">
        <TableBody>
            <TableRow>
                <TableCell component="th" scope="row">Subtotal</TableCell>
                <TableCell component="th" scope="row">
                    <NumberFormat>
                        {subtotal}
                    </NumberFormat>
                </TableCell>
            </TableRow>
            <TableRow>
                <TableCell component="th" scope="row">Envio</TableCell>
                <TableCell component="th" scope="row">
                    <NumberFormat>
                        {envio}
                    </NumberFormat>
                </TableCell>
            </TableRow>
            <TableRow>
                <TableCell component="th" scope="row">Impuesto</TableCell>
                <TableCell component="th" scope="row">
                    <NumberFormat>
                        {impuesto}
                    </NumberFormat>
                </TableCell>
            </TableRow>
            <TableRow>
                <TableCell component="th" scope="row">Descuento</TableCell>
                <TableCell component="th" scope="row">
                    <NumberFormat>
                        {descuento}
                    </NumberFormat>
                </TableCell>
            </TableRow>
            <TableRow>
                <TableCell component="th" scope="row">Total</TableCell>
                <TableCell component="th" scope="row">
                    <NumberFormat>
                        {(subtotal + impuesto)-descuento}
                    </NumberFormat>
                </TableCell>
            </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
     );
}
 
export default TotalDetail;