import React, { useState, useEffect, useRef } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Paper, Typography, Hidden, Button, CircularProgress } from '@material-ui/core';
import { GetApp } from '@material-ui/icons';
import ItemsCompra from './itemsCompra/ItemsCompra';
import TotalDetail from './totalDetail/TotalDetail';
import Result from './result/Result';
import AjaxRequest from '../ajaxRequest/AjaxRequest';
import NumberFormat from '../utils/numberformat/NumberFormat';
import { server } from '../utils/utils';
import axios from 'axios';

//Redux
import { connect } from 'react-redux';
import { cleanCheckout } from '../actions/checkoutActions';

const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
      color: theme.palette.text.primary,
      padding: theme.spacing(8, 2),
      maxWidth:"900px",
      margin:'auto'
    },
    content: {
        color: theme.palette.text.secondary,
        padding: theme.spacing(0,0,2,0),
    },
    detailValue: {
        color: theme.palette.text.primary,
        padding: theme.spacing(0,0,0,1),
        '&::before': {
            content: '"$"',
            display: 'inline-block',
        }
    },
    total:{
        padding: "25px 30px !important",
        backgroundColor: "#f7f7f7",
        borderRadius: 15
    },
    visible: {
        display:'block'
    },
    hidden: {
        display: 'none'
    },
    wrapper: {
        margin: "35px auto 10px",
        position: 'relative',
        width: 'fit-content',
    },
    buttonProgress: {
        color: '#4CAF50',
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
      },
  }));

const DetalleCompra = (props) => {
    const classes = useStyles();
    const firstRender = useRef(true);
    const [order, setOrder] = useState({
        order:[],
        products:[]
    });
    const [loading, setLoading] = useState({loading:true});
    const [downloadLoading, setDownloadLoading] = useState(false);

    useEffect(()=>{
        if(firstRender.current){
            const { match } = props;
            const { params } = match;
            axios.get(`${server}/detalle/${params.order_number}`)
                .then(res => {
                const order = res.data;
                setOrder(order);
                setLoading({loading:false});
                props.cleanCheckout();
            });
            firstRender.current=false;
        }
    }, []);

    const dirreccionEnvio = (direccion) => {
        if(direccion.length > 0){
            return(
                <div>
                    <Typography display="block" variant="h6">Detalle de envio</Typography>
                    <Typography variant="body1" className={classes.content}>
                            {direccion[0].direccion}
                    </Typography>
                </div>
            );
        }
    }

    const descuentoPago=(payment_discount)=>{
        if(payment_discount>0){
            return(
                <Typography variant="body1">
                    Descuento pago: 
                    <span className={classes.detailValue}>
                        <NumberFormat>{payment_discount}</NumberFormat>
                    </span>
                </Typography>
            );
        }
        return "";
    }

    const recargoPago=(payment_charge)=>{
        if(payment_charge > 0){
            return(
                <Typography variant="body1">
                    Recargo pago: 
                    <span className={classes.detailValue}>
                        <NumberFormat>{payment_charge}</NumberFormat>
                    </span>
                </Typography>
            );
        }
        return "";
    }

    const detallePago =(data)=>{
        if(data.length > 0){
            return(
                <div>
                    <Typography display="block" variant="h6">Detalle de pago</Typography>
                    <div className={ classes.content }>
                        <Typography variant="body1">
                            Metodo de Pago: <span style={{color:'black'}}>{data[0].payment_method}</span>
                        </Typography>
                        {recargoPago(data[0].payment_charge)}
                        {descuentoPago(data[0].payment_discount)}
                        <Typography variant="body1">
                            Monto: 
                            <span className={classes.detailValue}>
                                <NumberFormat>{ data[0].payment_amount }</NumberFormat>
                            </span>
                        </Typography>
                    </div>
                </div>
            );
        }
    }

    const descargaOrden=()=>{
        if(order.order.length > 0){
            let result = true;
            const orderNumber = order.order[0].order_number;
            const orderNumberNoZero = orderNumber.match(/[1-9][0-9]*/g);
            const downloadUri = `${server}/orden/descarga/${orderNumberNoZero.toString()}`;
            const fileName = `order_${orderNumberNoZero.toString().padStart(6, '0')}.pdf`;
            result = saveFile(downloadUri, 'get', fileName);
        }
    }

    const saveFile=(url, method, fileNameAndExtension)=>{
        setDownloadLoading(true);
        axios({
            url: url,
            method: method,
            responseType: 'blob', // important
          }).then((response) => {
             const url = window.URL.createObjectURL(new Blob([response.data]));
             const link = document.createElement('a');
             link.href = url;
             link.setAttribute('download', fileNameAndExtension); //or any other extension
             document.body.appendChild(link);
             link.click();
             setDownloadLoading(false);
          }).catch(function (error) {
             console.log(error);
             setDownloadLoading(false);
          });
          
    }

    return ( 
        <div className={classes.root}>
            <AjaxRequest request={loading} className={ loading.loading ? classes.visible : classes.hidden }/>
            <div className={ loading.loading ? classes.hidden : classes.visible }>
                <Result data={props.checkout.checkout} />
                <Grid container spacing={2}>
                    <Grid item xs={12} md={6} style={{padding:"8px 30px"}}>
                        <Grid item xs={12}>
                            <Typography variant="h6" className={classes.content}>Resumen de compra</Typography>
                            <Typography variant="h5">
                                Orden Nro {
                                    order.order.length > 0 ?
                                    order.order[0].order_number : 
                                    0
                                }
                            </Typography>
                            <Typography variant="body1" className={classes.content}>Se ha enviado una copia de el detalle de la compra a su correo electronico</Typography>
                        </Grid>
                        <Grid item xs={12}>
                            { dirreccionEnvio(order.order) }
                        </Grid>
                        <Grid item xs={12}>
                            { detallePago(order.order) }
                        </Grid>
                        <Hidden smDown>
                            <div className={classes.wrapper}>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    size="small"
                                    className={classes.button}
                                    startIcon={<GetApp/>} 
                                    disabled={downloadLoading} 
                                    onClick={descargaOrden}
                                >
                                    Guardar Orden
                                </Button>
                                { downloadLoading && <CircularProgress size={24} className={classes.buttonProgress } />}
                            </div>
                        </Hidden>
                    </Grid>
                    <Grid item xs={12} md={6} className={classes.total}>
                        <Typography display="block" variant="h6">Detalle de compra</Typography>
                        {
                            order.products.length > 0 ?
                            Object.keys(order.products).map((product, i) => (
                                <ItemsCompra key={i} data={order.products[i]}/>
                            ))
                            :""
                        }
                        <TotalDetail data={order.products}/>
                    </Grid>
                </Grid>
                <Hidden mdUp>
                    <div className={classes.wrapper}>
                        <Button
                            variant="contained"
                            color="primary"
                            size="small"
                            className={classes.button}
                            startIcon={<GetApp/>} 
                            disabled={downloadLoading} 
                            onClick={descargaOrden}
                        >
                            Guardar Orden
                        </Button>
                        { downloadLoading && <CircularProgress size={24} className={classes.buttonProgress } />}
                    </div>
                </Hidden>
            </div>
        </div>
     );
}

//state
const mapStateToProps = state => ({
    checkout: state.checkout,
  });

export default connect(mapStateToProps, { cleanCheckout })(DetalleCompra);