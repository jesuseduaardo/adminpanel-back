import React from 'react';
import { Grid, Typography, Avatar } from '@material-ui/core';
import {setThumbImagePath} from '../../utils/utils';
import NumberFormat from '../../utils/numberformat/NumberFormat';

const ItemsCompra = (props) => {
    const image = setThumbImagePath(props.data.image);
    
    return ( 
        <Grid container spacing={2}>
            <Grid item xs={2}>
                <Avatar alt="" src={image} />
            </Grid>
            <Grid item xs={10}>
                <Typography variant="body1">{props.data.name}</Typography>
                <Typography variant="body2">{props.data.short_description}</Typography>
                <Typography variant="body2">
                    <NumberFormat>
                        { (props.data.product_price + props.data.product_tax) -  props.data.product_discount }
                    </NumberFormat> x { props.data.product_qty }</Typography>
            </Grid>
        </Grid>
     );
}
 
export default ItemsCompra;