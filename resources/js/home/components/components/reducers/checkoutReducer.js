import { CHECKOUT } from '../actions/types';

const initialState = {
    checkout:"",
    personalData:{
        nombre:"",
        apellido:"",
        tipoDocumento:"",
        documento:"",
        codigo:"",
        telefono:"",
        email:""
    },
    addressData:{
        direccion:"",
        ciudad:"",
        estado:"",
        zip:"",
        pais:""
    },
    fiscalData:{
        direccion:"",
        ciudad:"",
        estado:"",
        zip:"",
        pais:""
    },
    pickupinstore: false,
    addressAsFiscal: false, 
    activeStep: 0, 
    paymethod: "", 
    loading: false, 
    enableNext: false,
}

export default function (state = initialState, action) {
    switch (action.type) {
        case CHECKOUT:
            return {
                ...state,
                checkout: action.payload
            }
        case "setPersonalData":
            return {...state, personalData:{...state.personalData, ...action.payload}};
        
        case "setAddressData": 
            return {...state, addressData: {...state.addressData, ...action.payload} }
        
        case "setFiscalData":
            return {...state, fiscalData: {...state.fiscalData, ...action.payload} } 
        
        case "setSumary":
            return { ...state, sumary: action.payload }
        
        case "setPickupInStore":
            return { ...state, pickupinstore: action.payload }
        
        case "setAddressAsFiscal":
            return { ...state, fiscalData: {...state.addressData} }
        
        case "setActiveStep":
            return { ...state, activeStep: action.payload }
        
        case "setPaymethod":
            return { ...state, paymethod: action.payload }
        
        case "setEnableNext":
            return { ...state, enableNext: action.payload }
        case "cleanState":
                return {
                checkout:"",
                personalData:{
                    nombre:"",
                    apellido:"",
                    tipoDocumento:"",
                    documento:"",
                    codigo:"",
                    telefono:"",
                    email:""
                },
                addressData:{
                    direccion:"",
                    ciudad:"",
                    estado:"",
                    zip:"",
                    pais:""
                },
                fiscalData:{
                    direccion:"",
                    ciudad:"",
                    estado:"",
                    zip:"",
                    pais:""
                },
                pickupinstore: false,
                addressAsFiscal: false, 
                activeStep: 0, 
                paymethod: "", 
                loading: false, 
                enableNext: false,
            };
        default:
            return state;
    }
}

