import { LOGIN, LOGOUT, FRASEDIARIA } from '../actions/types';


const initialState = {
  login:false,
  logout:true,
  token:'',
  frasediaria:{}
}

export default function(state = initialState, action){
    switch (action.type) {
      case LOGIN:
        return {
          ...state, 
          token: 'Bearer '+action.payload.success.token, 
          login:true,
          }
      case LOGOUT:
        return {
          ...state,
          logout: typeof action.payload.success !== 'undefined' ? true : false,
          token: '',
          login: false,
        }
      case FRASEDIARIA:
        return {
          ...state,
          frasediaria: action.payload
        }
      default:
        return state;
    }
}