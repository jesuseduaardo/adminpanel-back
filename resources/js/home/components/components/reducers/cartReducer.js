import { AGREGAR_ITEM, MOSTRAR_ITEMS, BORRAR_ITEM } from '../actions/types';

//cada reducer tiene su propio state
const initialState = {
    items:[],
}

export default function(state = initialState, action){
    switch (action.type) {
        case AGREGAR_ITEM:
            return {
                ...state,
                items:action.payload
            }
        case MOSTRAR_ITEMS:
            return{
                ...state,
                items:action.payload
            }
        case BORRAR_ITEM:
            return{
                ...state,
                items:action.payload
            }
        default:
            return state;
    }
}