import { MOSTRAR_CATEGORIAS, MOSTRAR_CATEGORIA, ELIMINAR_CATEGORIA, AGREGAR_CATEGORIA, EDITAR_CATEGORIA } from '../actions/types';

//cada reducer tiene su propio state
const initialState = {
    categorias:[],
    categoria:[]
}

export default function(state = initialState, action){
    switch (action.type) {
        case MOSTRAR_CATEGORIAS:
            return {
                ...state,
                categorias:action.payload
            }
        case ELIMINAR_CATEGORIA:
            return{
                ...state,
                categorias: action.payload
            }
        case AGREGAR_CATEGORIA:
            return{
                ...state,
                categorias: action.payload
            }
        case MOSTRAR_CATEGORIA:
            return{
                ...state,
                categoria: action.payload[0]
            }
        case EDITAR_CATEGORIA:
            return{
                ...state,
                categoria: action.payload[0]
            }
        default:
            return state;
    }
}