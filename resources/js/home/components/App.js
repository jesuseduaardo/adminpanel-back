import React, { Component } from 'react';
import { createMuiTheme, makeStyles, ThemeProvider } from '@material-ui/core/styles';
import Checkout from "./components/checkout/Checkout";
import NavBar from "./components/appbar/NavBar";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ProductInfo from './components/productInfo/ProductInfo';
import Footer from './components/footer/Footer';
import Carousel from './components/header/Carousel';
import Home from './components/pages/Home';
import Products from './components/pages/Products';
import DetalleCompra from './components/detalleCompra/DetalleCompra';
import BreadCrumb from './components/breadcrumb/BreadCrumb';
import InitialComponent from './components/initialComponent/InitialComponent';
import { Router, Switch, Route, Link } from "react-router-dom";
import history from "./components/history";
// import Construction from './components/pages/Construction';

//Redux
import { Provider } from 'react-redux';
import store from './components/store';

import "./App.css";


const theme = createMuiTheme({
  palette:{
    primary: { main: '#4CAF50', contrastText: '#ffffff' },
    secondary: { main: '#ffc400', contrastText: '#ffffff' },

  }
});

class App extends Component {
  state = {  }

  render() {
    
    return (
      // <Construction />
      <Provider store={store}>
          <Router history={history}>
            <ThemeProvider theme={theme}>
                <InitialComponent/>
                <NavBar/>
                <Carousel/>
                <BreadCrumb/>
                <Switch>
                  <Route exact path="/" component={Home} />
                  <Route exact strict path="/productos(.*/*)/:categoria(.*/*)/:sku" component={ProductInfo} />
                  <Route exact strict path="/productos(.*/*)/:categoria/" component={Products} />
                  <Route exact path="/productos?(.*)" component={Products} />
                  <Route exact path="/productos/" component={Products} />
                  <Route exact path="/checkout" component={Checkout}/>
                  {/* <Route exact path="/compras/" component={Compras}/> */}
                  <Route exact strict path="/compras/:order_number/detalle" component={DetalleCompra}/>
                </Switch>
                <Footer/>
                <ToastContainer position="bottom-right" autoClose={5000} />
            </ThemeProvider>
          </Router>
        </Provider >
     );
  }
}

export default App;