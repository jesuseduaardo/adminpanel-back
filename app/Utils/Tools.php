<?php
namespace App\Utils;
use Illuminate\Support\Str;

class Tools{

    public static function createImagePath($sku, $filename, $extension){
        if(stripos($filename, $sku)){
            return array("name"=>$filename, "extension"=>$extension);
        }
        return array('name' => $sku."_".Str::random(5), "extension" => $extension);
    }

}