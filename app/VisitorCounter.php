<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
class VisitorCounter extends Model
{
    protected $guarded = array('id');

    public function countIncrementWithUpdate($ip = "0.0.0.0"){
    	static::where('ip',$ip)->update(['count' => DB::raw('count+1')]);
    }
}
