<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DB;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //DB::enableQueryLog();
        $ordenes = DB::table('orders_number')
        ->join('orders', 'orders.order_id', '=', 'orders_number.id')
        ->leftJoin('orders_status', 'orders_status.id', '=', 'orders.orders_status_code')
        ->leftJoin('orders_shipping', 'orders_shipping.order_id', '=', 'orders.order_id')
        ->leftJoin('orders_payment', 'orders_payment.order_id', '=', 'orders.order_id')
        ->leftJoin('users', 'users.id', '=', 'orders.users_id')
        ->leftJoin('user_info', 'user_info.user_id', '=', 'orders.users_id')
        ->select('orders_number.id as order_number', 'users.email', 
             DB::raw('concat(user_info.first_name, " ", user_info.last_name) as user_name'), 
            'orders.order_date', 'orders_status.status_name', 'orders_payment.payment_charge',
            'orders_payment.payment_discount', 'orders_shipping.ship_charge', 
        DB::raw("(SELECT SUM(orders_items.product_price) FROM orders_items
        WHERE orders_items.order_id = orders.order_id) as total_products"),
        DB::raw("(SELECT SUM(orders_items.product_tax) FROM orders_items
        WHERE orders_items.order_id = orders.order_id) as total_tax"),
        DB::raw("(SELECT SUM(orders_items.product_discount) FROM orders_items
        WHERE orders_items.order_id = orders.order_id) as total_discount")
        )
        ->get();

            if(count($ordenes)>0){
                return $ordenes;
            }
        return response($content = json_encode(array("error"=>"No hay ordenes para mostrar")), $status = 400);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $orderId = DB::table('orders_number')->insertGetId([
                "created_at"=> now(), "updated_at"=> now()
                ]);
            
            if(is_null($orderId)){
                throw new \Exception("Hubo un error al crear la orden", 1);
            }

            $order = DB::table('orders')->insertGetId([
                'order_id' => $orderId,
                'users_id' => 1,
                'orders_status_code' => 1,
                'purchasedFrom'=>'site',
                'IP'=>$request->ip(),
                'order_date' => now(),
                "created_at"=> now(), "updated_at"=> now()
                ]);

            DB::table('orders_items')->insert([
                'product_id' => 2,
                'order_id' => $order,
                'product_qty' => 1,
                'product_price' => 100,
                "created_at"=> now(), "updated_at"=> now()
                ]);
        } catch (\Exception $e) {
            return response($content = json_encode(
                                        array(
                                            "error"=>mb_convert_encoding(
                                                $e->getMessage(), 'UTF-8', 'UTF-8'))), $status = 401);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //DB::enableQueryLog();
        $orden = DB::select(
            DB::raw(
                "select `o`.*, `e`.`status_name`, `e`.`status_description`, `s`.`ship_weight`, \n"
                ."`s`.`ship_charge`, `s`.`ship_tracking_number`, `s`.`delivery_time`, `p`.* \n"
                ."from `orders` as `o`  \n"
                ."inner join `orders_shipping` as `s` on `o`.`order_id` = `s`.`order_id` \n"
                ."inner join `orders_status` as `e` on `o`.`orders_status_code` = `e`.`id` \n"
                ."inner join ( \n"
                    ."select `orderpay`.`payment_amount`, `orderpay`.`payment_charge`, `orderpay`.`payment_discount`, \n"
                    ."`orderpay`.`verification_time`, `pay`.`payment_method`, `pay`.`payment_description`, \n"
                    ."`pay`.`payment_image` \n"
                    ."from `orders_payment` as `orderpay` \n"
                    ."inner join `payment` as `pay` on `orderpay`.`payment_method_id` = `pay`.`id` \n"
                    ."where `pay`.`enable`=1 and `pay`.`deleted`=0 \n"
                .") as `p` \n"
                ."where `o`.`order_id` =".$id
                )
        );        

        if(isset($orden) && !is_null($orden)){
            $productList = array();
            $productos = DB::table('orders_items')->where('order_id',  $orden[0]->order_id)->get();
            foreach ($productos as $key => $value) {
               $producto = DB::select(
                DB::raw(
                    "select `p`.`id`, `p`.`sku`, `p`.`name`, FORMAT(`p`.`tax`, 2, 'es_AR') as `tax`, FORMAT(`p`.`price`, 2, 'es_AR') as `price`, `p`.`stock`, `p`.`category_id` as `categorias`, \n"
                    . "FORMAT((`p`.`price`+`p`.`tax`) , 2, 'es_AR') as `totalPrice`, `p`.`short_description` as `shortDescription`, `p`.`visibility` as `visibilidad`, `p`.`enable`, `p`.`created_at`, \n"
                    . "`c`.`name` as `categoria`, `a`.`weight` as `peso`, `a`.`height` as `alto`, `a`.`width` as `ancho`, `a`.`depth` as `profundidad`, `a`.`color0`, `a`.`color1`, `a`.`color2`, \n"
                    . "`d`.`long_description` as `description`, GROUP_CONCAT(CONCAT(`i`.`name`, \".\",`i`.`extension`)) AS `imagenes`, \n"
                    . "DATE_FORMAT(`e`.`date_from`, '%Y-%m-%dT%H:%i') as `nuevoDesde`, DATE_FORMAT(`e`.`date_to`, '%Y-%m-%dT%H:%i') as `nuevoHasta`, \n"
                    . "`s`.`url_key` as `urlKey`, `s`.`meta_keywords` as `metaKeywords`, `s`.`meta_title` as `metaTitle`, `s`.`meta_description` as `metaDescription`, \n"
                    . "(SELECT `route` FROM `routes` WHERE `name` = 'products') as `thumb_route` \n"
                    . "from `products` as `p` \n"
                    . "inner join `products_attributes` as `a` on `p`.`id` = `a`.`product_id` \n"
                    . "left join `products_description` as `d` on `p`.`id` = `d`.`product_id` \n"
                    . "left join `category` as `c` on `c`.`id` = `p`.`category_id` \n"
                    . "left join `products_images` as `i` on `p`.`id` = `i`.`product_id` \n"
                    . "left join `products_seo` as `s` on `p`.`id` = `s`.`product_id` \n"
                    . "left join (select * from `products_events` where `description`='Nuevo') as `e` on `p`.`id` = `e`.`product_id` \n"
                    . "where `p`.`deleted` = 0 and `c`.`enable`=1 and `p`.`id`= ".$productos[$key]->product_id
                    )
                );
                $productList[] = $producto[0];
            }
            
            $user = DB::table('users')->where('users.id',  $orden[0]->users_id)
                        ->join('user_info', 'user_info.user_id', '=', 'users.id')
                        ->join('user_info_address', 'user_info_address.user_id', '=', 'users.id')
                        ->select('users.name as username', 'users.email', 'users.active', 'users.created_at',
                              'user_info.first_name', 'user_info.last_name', 'user_info.birthday',
                              'user_info.gender', 'user_info_address.telephone1', 'user_info_address.telephone2',
                              'user_info_address.country', 'user_info_address.state', 'user_info_address.city', 'user_info_address.zip_code',
                              'user_info_address.user_address', 'user_info_address.billing_address')
                        ->get();
            
            $status = DB::table('orders_status')->get();
            

            if(!is_null($user) && (!is_null($productList) && count($productList) > 0)){
                return array(
                    'order'=> $orden, 
                    'products'=>$productList, 
                    'user'=>$user, 
                    'status' => !is_null($status) ? $status : ""
                );
            }
            return response($content = json_encode(array("error"=>"No hay ordenes para mostrar")), $status = 400);
        }
        
        //dd(DB::getQueryLog());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
