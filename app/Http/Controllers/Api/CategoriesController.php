<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DB;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cat = array();
        $categorias = DB::table('category')->where('category_id', 0)->get();
        foreach ($categorias as $categoria) {
            $sub = "";
            $sub = $this->getSubCategoria($categoria->id);
            if($sub!=""){
                $categoria->subcategorias = $this->getSubCategoria($categoria->id);
            }
        }
        return $categorias;
    }
    
    public function getSubCategoria($id){
        if(DB::table('category')->where('category_id', $id)->exists()){
            $$id = DB::table('category')->where('category_id', $id)->get();
            foreach ($$id as $categoria) {
                $sub = $this->getSubCategoria($categoria->id);
                if(isset($sub)){
                    $categoria->subcategorias = $this->getSubCategoria($categoria->id);
                }
            }
        return $$id;
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $categoria = DB::table('category')->where('name',  $request->categoria)->get();
        if(count($categoria)>0){
            return response($content = json_encode(
                                            array(
                                                "error"=>"La categoria ".$request->categoria." ya existe"
                                                )
                                            ), $status = 401);
        }
        try {
             $category = DB::table('category')->insertGetId([
                'name' => $request->categoria, 'enable' => $request->enable,
                'category_id' =>$request->categoriaPadre,
                "created_at"=> now(), "updated_at"=> now()
            ]);
            if(is_null($category)){
                throw new \Exception("Hubo un error al crear la categoria", 1);
            }
            $categorias =  $this->index();
            return $categorias;
        } catch (\Exception $e) {
            return response($content = json_encode(
                                        array(
                                            "error"=>mb_convert_encoding(
                                                $e->getMessage(), 'UTF-8', 'UTF-8'))), $status = 401);
        }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categoria = DB::table('category')->where('id',  $id)->get();
        if(count($categoria)>0){
            return $categoria;
        }
        return response($content = json_encode(array("error"=>"La categoria seleccionada no existe")), $status = 400);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            DB::table('category')->where('id', $id)->update([
                'name' => $request->categoria,
                'enable' => $request->enable,
                'category_id' =>$request->categoriaPadre,
                "updated_at"=> now()]);

            return $this->show($id);
        } catch (\Exception $e ) {
            return response($content = json_encode(
                                        array(
                                            "error"=>mb_convert_encoding(
                                                $e->getMessage(), 'UTF-8', 'UTF-8'))), $status = 401);
        }
        return response($content = json_encode(array("error"=>"Hubo un problema actualizando la categoria")), $status = 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $categorias = array();
            $categoria = DB::table('category')->where('category_id',  $id)->get();
            if(count($categoria)>0){
                foreach ($categoria as $cat) {
                    $categorias[] = $cat->name;
                }
                throw new \Exception("No se puede borrar por que contiene las siguientes subcategorias: ".implode(", ", $categorias), 1);
            }
            $success =  DB::table('category')->where('id', $id)->delete();
            if($success){
                return $this->index();
            }
        } catch (\Exception $e) {
           return response($content = json_encode(
                                        array(
                                            "error"=>mb_convert_encoding(
                                                $e->getMessage(), 'UTF-8', 'UTF-8'))), $status = 401);
        }
        return response($content = json_encode(array("error"=>"Hubo un problema borrando")), $status = 400);
    }
}
