<?php
namespace App\Http\Controllers\API;
use DB;
use App\User;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Sopamo\LaravelFilepond\Filepond;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use App\Utils\Tools;

class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productos = null;
        $productos = DB::select(
            DB::raw(
                "select `p`.`id`, `p`.`sku`, `p`.`name`, `p`.`tax`, FORMAT(`p`.`price`, 2, 'es_AR') as `price`, `p`.`stock`, `p`.`category_id`, 
                `p`.`short_description`, `p`.`visibility`, `p`.`enable`, `p`.`created_at`, `c`.`name` as `categoria`, `a`.`weight`, 
                `a`.`height`, `a`.`width`,
                `a`.`depth`, `a`.`color0`, `a`.`color1`, `a`.`color2`, `d`.`long_description`, 
                `i`.`imagenes`, `e`.`description` as `evento`, 
                `e`.`discount` as `descuento`, `e`.`date_from` as `evento_desde`, 
                `e`.`date_to` as `evento_hasta`, (SELECT `route` FROM `routes` WHERE `name` = 'products_thumbs') as `thumb_route`
                from `products` as `p` 
                inner join `products_attributes` as `a` on `p`.`id` = `a`.`product_id` 
                left join `products_description` as `d` on `p`.`id` = `d`.`product_id` 
                left join `category` as `c` on `c`.`id` = `p`.`category_id` 
                left join (select `product_id`, GROUP_CONCAT(CONCAT(`name`, '.',`extension`)) as `imagenes` from `products_images` group by `product_id`) as `i` on `p`.`id` = `i`.`product_id`
                left join (select * from `products_events` order by `id` desc limit 1) as `e` on `p`.`id` = `e`.`product_id` 
                where `p`.`deleted` = 0 and `c`.`enable`=1;"
            )
        );
        if(!is_null($productos)){
            return $productos;
        }
        return response($content = json_encode(array("error"=>"No hay productos para mostrar")), $status = 400);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $data = $request->json()->all();

        try {
            $entity = DB::table('products')->insertGetId([
                'enable' => $data['basic']['enable'],
                'name'=> $data['basic']['name'], 
                'sku' => $data['basic']['sku'], 
                'price'=> $data['basic']['price'],
                'stock'=> $data['basic']['stock'],
                'tax'=> $data['basic']['tax'], 
                'category_id'=> $data['basic']['categorias'], 
                'visibility' => $data['basic']['visibilidad'],
                'short_description'=> $data['basic']['shortDescription'], 
                'created_at'=> now(), 
                'updated_at'=> now()
            ]);
            if(!is_null($entity)){
                DB::table('products_attributes')->insert([
                    'product_id'=>  $entity, 
                    'width'     =>  $data['details']['ancho'],
                    'height'    =>  $data['details']['alto'],
                    'depth'     =>  $data['details']['profundidad'],
                    'weight'    =>  $data['details']['peso'],
                    'color0'    =>  $data['details']['color0']!=null ? $data['details']['color0'] :"",
                    'color1'    =>  $data['details']['color1']!=null ? $data['details']['color1'] :"",
                    'color2'    =>  $data['details']['color2']!=null ? $data['details']['color2'] :"",
                ]);
                DB::table('products_description')->insert([
                    'product_id'=>  $entity, 
                    'long_description'=>  $data['basic']['description'],
                ]);
                DB::table('products_seo')->insert([
                    'product_id'=>  $entity, 
                    'url_key'=>  $data['seo']['urlKey'],
                    'meta_title'=>  $data['seo']['metaTitle'],
                    'meta_keywords'=>  $data['seo']['metaKeywords'],
                    'meta_description'=>  $data['seo']['metaDescription'],
                ]);
                DB::table('products_events')->insert([
                    'product_id'=>  $entity, 
                    'description'=>  "Nuevo",
                    'discount'=>  0,
                    'date_from'=>  $data['basic']['nuevoDesde'],
                    'date_to'=>  $data['basic']['nuevoHasta'],
                    'created_at'=> now(), 
                    'updated_at'=> now()
                ]);
                $imagenes = $data['images'];
                $this->uploadImg($imagenes, $data['basic']['sku'], $entity);
            }
        } catch(\Exception $e) {
            return response($content = json_encode(
                                            array("error"=>mb_convert_encoding(
                                                "Hubo un problema guardando: ".$e->getMessage(), 'UTF-8', 'UTF-8'))), $status = 401);
        }
    }

    public function resize_crop_image($max_width, $max_height, $source_file, $dst_dir, $quality = 80){
        $imgsize = getimagesize($source_file);
        $width = $imgsize[0];
        $height = $imgsize[1];
        $mime = $imgsize['mime'];

        switch($mime){
            case 'image/gif':
                $image_create = "imagecreatefromgif";
                $image = "imagegif";
                break;

            case 'image/png':
                $image_create = "imagecreatefrompng";
                $image = "imagepng";
                $quality = 7;
                break;

            case 'image/jpeg':
                $image_create = "imagecreatefromjpeg";
                $image = "imagejpeg";
                $quality = 80;
                break;

            default:
                return false;
                break;
        }

        $dst_img = imagecreatetruecolor($max_width, $max_height);
        $src_img = $image_create($source_file);

        $width_new = $height * $max_width / $max_height;
        $height_new = $width * $max_height / $max_width;
        //if the new width is greater than the actual width of the image, then the height is too large and the rest cut off, or vice versa
        if($width_new > $width){
            //cut point by height
            $h_point = (($height - $height_new) / 2);
            //copy image
            imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
        }else{
            //cut point by width
            $w_point = (($width - $width_new) / 2);
            imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
        }

        $image($dst_img, $dst_dir, $quality);

        //if($dst_img)imagedestroy($dst_img);
        //if($src_img)imagedestroy($src_img);
}
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $producto = null;
        $producto = DB::select(
            DB::raw(
                "select `p`.`id`, `p`.`sku`, `p`.`name`, FORMAT(`p`.`tax`, 2, 'es_AR') as `tax`, FORMAT(`p`.`price`, 2, 'es_AR') as `price`, `p`.`stock`, `p`.`category_id` as `categorias`, \n"
                . "FORMAT((`p`.`price`+`p`.`tax`) , 2, 'es_AR') as `totalPrice`, `p`.`short_description` as `shortDescription`, `p`.`visibility` as `visibilidad`, `p`.`enable`, `p`.`created_at`, \n"
                . "`c`.`name` as `categoria`, `a`.`weight` as `peso`, `a`.`height` as `alto`, `a`.`width` as `ancho`, `a`.`depth` as `profundidad`, `a`.`color0`, `a`.`color1`, `a`.`color2`, \n"
                . "`d`.`long_description` as `description`, GROUP_CONCAT(CONCAT(`i`.`name`, \".\",`i`.`extension`)) AS `imagenes`, \n"
                . "DATE_FORMAT(`e`.`date_from`, '%Y-%m-%dT%H:%i') as `nuevoDesde`, DATE_FORMAT(`e`.`date_to`, '%Y-%m-%dT%H:%i') as `nuevoHasta`, \n"
                . "`s`.`url_key` as `urlKey`, `s`.`meta_keywords` as `metaKeywords`, `s`.`meta_title` as `metaTitle`, `s`.`meta_description` as `metaDescription`, \n"
                . "(SELECT `route` FROM `routes` WHERE `name` = 'products') as `thumb_route` \n"
                . "from `products` as `p` \n"
                . "inner join `products_attributes` as `a` on `p`.`id` = `a`.`product_id` \n"
                . "left join `products_description` as `d` on `p`.`id` = `d`.`product_id` \n"
                . "left join `category` as `c` on `c`.`id` = `p`.`category_id` \n"
                . "left join `products_images` as `i` on `p`.`id` = `i`.`product_id` \n"
                . "left join `products_seo` as `s` on `p`.`id` = `s`.`product_id` \n"
                . "left join (select * from `products_events` where `description`='Nuevo') as `e` on `p`.`id` = `e`.`product_id` \n"
                . "where `p`.`deleted` = 0 and `c`.`enable`=1 and `p`.`id`= ".$id
            )
        );
        if(!is_null($producto)){
            return $producto;
        }
        return response($content = json_encode(array("error"=>"El producto seleccionado no existe")), $status = 400);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        
        $data = $request->json()->all();
        if(isset($data['activeProduct'])){
            try {
                DB::table('products')->where('id', $id)->update([
                    'enable' => $data['activeProduct'], 
                    "updated_at"=> now()
                    ]);
                return $this->index();
            } catch (\Exception $e) {
                return response($content = json_encode(array("error"=>mb_convert_encoding("Hubo un problema actualizando ".$e->getMessage(), 'UTF-8', 'UTF-8'))), $status = 401);
            }
        }
        $price = str_replace(",", ".", str_replace(".", "", $data['basic']['price']));
        $tax = str_replace(",", ".", str_replace(".", "", $data['basic']['tax']));
        try {
            DB::table('products')->where('id', $id)->update([
                'enable' => $data['basic']['enable'],
                'name'=> $data['basic']['name'], 
                'sku' => $data['basic']['sku'], 
                'stock'=> $data['basic']['stock'],
                'price'=> $price,
                'tax'=> $tax, 
                'category_id'=> $data['basic']['categorias'], 
                'visibility' => $data['basic']['visibilidad'],
                'short_description'=> $data['basic']['shortDescription'], 
                'updated_at'=> now()
            ]);
            DB::table('products_attributes')->where('product_id', $id)->update([
                    'width'     =>  $data['details']['ancho'],
                    'height'    =>  $data['details']['alto'],
                    'depth'     =>  $data['details']['profundidad'],
                    'weight'    =>  $data['details']['peso'],
                    'color0'    =>  isset($data['details']['color0']) && $data['details']['color0']!=null ? $data['details']['color0'] :"",
                    'color1'    =>  isset($data['details']['color1']) && $data['details']['color1']!=null ? $data['details']['color1'] :"",
                    'color2'    =>  isset($data['details']['color2']) && $data['details']['color2']!=null ? $data['details']['color2'] :"",
            ]);
            DB::table('products_description')->where('product_id', $id)->update([
                    'long_description'=>  $data['basic']['description'],
            ]);
            DB::table('products_seo')->where('product_id', $id)->update([
                    'url_key'=>  $data['seo']['urlKey'],
                    'meta_title'=>  $data['seo']['metaTitle'],
                    'meta_keywords'=>  $data['seo']['metaKeywords'],
                    'meta_description'=>  $data['seo']['metaDescription'],
            ]);
            DB::table('products_events')->where('product_id', $id)->update([
                    'description'=>  "Nuevo",
                    'discount'=>  0,
                    'date_from'=>  $data['basic']['nuevoDesde'],
                    'date_to'=>  $data['basic']['nuevoHasta'],
                    'updated_at'=> now()
            ]);
            if($this->uploadImg($data['images'], $data['basic']['sku'], $id)){
                return $this->show($id);
            }
        } catch (\Exception $e) {
            return response($content = json_encode(array("error"=>mb_convert_encoding("Hubo un problema actualizando ".$e->getMessage(), 'UTF-8', 'UTF-8'))), $status = 401);
        }catch (\Throwable $th) {
            //throw $th;
            return response($content = json_encode(array("error"=>"Hubo un problema actualizando")), $status = 401);
        }
        return response($content = json_encode(array("error"=>"Hubo un problema actualizando")), $status = 401);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        try {
            if($this->deleteAttr($id)){
                $success = DB::table('products')->where('id', $id)->delete();
                if($success){
                    return $this->index();
                }
            }
        } catch (\Exception $e) {
            return response($content = json_encode(
                                        array(
                                            "error"=>mb_convert_encoding(
                                                $e->getMessage(), 'UTF-8', 'UTF-8'))), $status = 401);
        }
    }

    public function uploadImg($imagenes, $sku, $product_id){
        
        $savePath = "img/products/";
        $thumbsSavePath = "img/thumbs/products/";
        
        $imagenesServidor = DB::table('products_images')
        ->where('product_id', $product_id)
        ->get();
        $filepond = new Filepond;
        if(count($imagenesServidor)>0){
            foreach ($imagenesServidor as $imagen) {
                foreach ($imagenes as $key => $value) {
                    if($imagen->name == $value['fileName'] && $imagen->size == $value['fileSize']){
                        unset($imagenes[$key]);
                        continue 2;
                    }
                }
                DB::table('products_images')->where('id', $imagen->id
                )->delete();
                try {
                    unlink(public_path('\img\products\\'.$imagen->name.'.'.$imagen->extension));
                    unlink(public_path('\img\thumbs\products\\'.$imagen->name.'.'.$imagen->extension));
                } catch (\Throwable $th) {
                    continue;
                }
            }
            if(isset($imagenes) && count($imagenes)>0){
                foreach ($imagenes as $key => $value) {
                    $path = $filepond->getPathFromServerId($imagenes[$key]["serverId"]);
                    $fileImage = Tools::createImagePath($sku, $imagenes[$key]['fileName'], $imagenes[$key]['extension']);
                    $finalLocation = public_path($savePath.$fileImage["name"].".".$fileImage["extension"]);
                    // Move the file from the temporary path to the final location
                    $this->resize_crop_image(100, 100, $path, "img/thumbs/products/".$fileImage["name"].".".$fileImage["extension"]);
                    rename($path, $finalLocation);
                    $success= DB::table('products_images')->insertGetId([
                        'product_id' => $product_id, 
                        'name' => $fileImage["name"], 
                        'extension' => $fileImage["extension"],
                        "size"=> $imagenes[$key]['fileSize'],
                        "created_at"=> now()]
                    );
                }
            }
            return true;
            
        }
        try {
            foreach ($imagenes as $key => $value) {
                $path = $filepond->getPathFromServerId($imagenes[$key]["serverId"]);
                $fileImage = Tools::createImagePath($sku, $imagenes[$key]['fileName'], $imagenes[$key]['extension']);
                // $fileImage = $sku."_".$imagenes[$key]['fileName'].".".$imagenes[$key]['extension'];
                $finalLocation = public_path($savePath.$fileImage["name"].".".$fileImage["extension"]);
                // Move the file from the temporary path to the final location
                $this->resize_crop_image(100, 100, $path, $thumbsSavePath.$fileImage["name"].".".$fileImage["extension"]);
                rename($path, $finalLocation);
                $success= DB::table('products_images')->insertGetId([
                    'product_id' => $product_id, 
                    'name' => $fileImage["name"], 
                    'extension' => $fileImage["extension"],
                    "size"=> $imagenes[$key]['fileSize']
                ]);
            }
            return true;
        } catch (\Exception $e) {
            return response(
                    $content = json_encode(
                        array(
                            "error"=>"Hubo un problema actualizando las imagenes del articulo ".$e->getMessage(),
                            'UTF-8', 'UTF-8'
                        )
                    ), 
                    $status = 400);
        }
    }

    public function deleteImg($id){
        try {
           $imagenes = DB::table('products_images')->where('product_id',  $id)->get();
            foreach ($imagenes as $imagen) {
                unlink(public_path('\img\products\\'.$imagen->name.'.'.$imagen->extension));
                unlink(public_path('\img\thumbs\products\\'.$imagen->name.'.'.$imagen->extension));
            }
            DB::table('images')->where('entity_id', $id)->delete();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
  
    public function deleteAttr($id){
        try {
            DB::table('products_attributes')->where('product_id', $id)->delete();
            DB::table('products_description')->where('product_id', $id)->delete();
            DB::table('products_images')->where('product_id', $id)->delete();
            DB::table('products_seo')->where('product_id', $id)->delete();
            DB::table('products_stats')->where('product_id', $id)->delete();
            $this->deleteImg($id);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    
    }

}
