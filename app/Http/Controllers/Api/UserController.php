<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use App\Notifications\SignupActivate;

class UserController extends Controller 
{
    public $successStatus = 200;
    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(){
        $credentials = request(['email', 'password']);
        if($credentials["email"]=="" || $credentials["password"]==""){
            return response($content = json_encode(array("error"=>"Email y Contraseña no pueden estar vacios")), $status = 401);
        }
        $credentials['active'] = 1;
        $credentials['deleted_at'] = null;
        try {
            if(Auth::attempt($credentials)){
                $user = Auth::user();
                $success['token'] =  $user->createToken('MyApp')->accessToken; 
                return response()->json(['success' => $success], $this->successStatus); 
            }
            throw new \Exception("Usuario o clave incorrectos", 1);
        } catch (\Exception $e) {
            $regex = "/SQLSTATE/";
            if (preg_match($regex, $e->getMessage())) {
                return response($content = json_encode(array("error"=>"Hubo un error al conectar con la base de datos")), $status = 401);
            }
            return response($content = json_encode(array("error"=>mb_convert_encoding($e->getMessage(), 'UTF-8', 'UTF-8'))), $status = 401);
        } catch (\Throwable $e) {
            return response($content = json_encode(array("error"=>mb_convert_encoding($e->getMessage(), 'UTF-8', 'UTF-8'))), $status = 401);
        }           
    }
/** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    // public function register(Request $request) 
    // { 
    //     $validator = Validator::make($request->all(), [ 
    //         'name' => 'required', 
    //         'email' => 'required|email', 
    //         'password' => 'required', 
    //         'c_password' => 'required|same:password', 
    //     ]);
    //     if ($validator->fails()) { 
    //         return response()->json(['error'=>$validator->errors()], 401);            
    //     }
    //     $input = $request->all(); 
    //     $input['password'] = bcrypt($input['password']); 
    //     $input['activation_token'] = str_random(60);    
    //     $user = User::create($input); 
    //     $success['token'] =  $user->createToken('MyApp')->accessToken; 
    //     $success['name'] =  $user->name;
    //     $user->notify(new SignupActivate($user));
    //     return response()->json(['success'=>$success], $this->successStatus); 
    // }

    /** 
     * details api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function details() 
    { 
        $user = Auth::user(); 
        return response()->json(['success' => $user], $this->successStatus); 
    } 

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json(['success' => 
            'Successfully logged out']);
    }

    public function signupActivate($token)
    {
        $user = User::where('activation_token', $token)->first();
        if (!$user) {
            return response()->json(['message' => 'El token de activación es inválido'], 404);
        }
        $user->active = true;
        $user->activation_token = '';
        $user->email_verified_at = date('Y-m-d H:i:s');
        $user->save();
        return $user;
    }
}