<?php

namespace App\Http\Controllers\Api;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FraseController extends Controller
{
    public function index(){
        $frase = DB::table('frase_diaria')
                    ->select('imagen','frase', 'autor', 'titulo')
                    // ->where('mes', date('m'))
                    ->where('mes', 1)
                    ->get()->random(1);   
        return $frase;

    }
}
