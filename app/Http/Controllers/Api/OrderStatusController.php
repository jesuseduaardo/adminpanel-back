<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DB;

class OrderStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->json()->all();
        $userId = Auth::id();
        
        try {
             $status = DB::table('order_status_history')->insert([
                'order_id' => $data['orden'],
                'user_id'  => $userId,
                'orders_status_code' => $data['estado'],
                'comentarios' => $data['comentarios'],
                "created_at"=> now(), "updated_at"=> now()
                ]);

            if(is_null($status)){
                throw new \Exception("Hubo un error al cambiar el historico de la orden", 1);
            }
            $update = DB::table('orders')->where('order_id', $data['orden'])->update([
                'orders_status_code' => $data['estado'],
                'updated_at'=> now()
            ]);
            if(is_null($update)){
                throw new \Exception("Hubo un error al cambiar el status de la orden", 1);
            }
            $status =  $this->show($data['orden']);
            return $status;
        } catch (\Exception $e) {
            return response($content = json_encode(
                                        array(
                                            "error"=>mb_convert_encoding(
                                                $e->getMessage(), 'UTF-8', 'UTF-8'))), $status = 401);
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $status = DB::table('order_status_history')
                  ->leftJoin('users', 'users.id', '=', 'order_status_history.user_id')
                  ->leftJoin('user_info', 'user_info.user_id', '=', 'order_status_history.user_id')
                  ->leftJoin('orders_status', 'orders_status.id', '=', 'order_status_history.orders_status_code')
                  ->select('orders_status.status_description',
                            'order_status_history.comentarios', DB::raw('DATE_FORMAT(order_status_history.updated_at, "%d/%m/%Y - %H:%i") date'),
                            'users.name','user_info.first_name', 'user_info.last_name'
                            )
                  ->where('order_id', $id)
                  ->orderBy('order_status_history.updated_at', 'desc')
                  ->get();
        return $status;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
