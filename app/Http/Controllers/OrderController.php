<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function neworder(Request $request){
        $data = $request->json()->all();
        return null;
        try {
            $orderId = DB::table('orders_number')->insertGetId([
                "created_at"=> now(), "updated_at"=> now()
                ]);
            
            if(is_null($orderId)){
                throw new \Exception("Hubo un error al crear la orden", 1);
            }

            $order = DB::table('orders')->insertGetId([
                'order_id' => $orderId,
                'users_id' => 1,
                'orders_status_code' => 1,
                'purchasedFrom'=>'site',
                'IP'=>$request->ip(),
                'order_date' => now(),
                "created_at"=> now(), "updated_at"=> now()
                ]);

            DB::table('orders_items')->insert([
                'product_id' => 2,
                'order_id' => $order,
                'product_qty' => 1,
                'product_price' => 100,
                "created_at"=> now(), "updated_at"=> now()
                ]);
        } catch (\Exception $e) {
            return response($content = json_encode(
                                        array(
                                            "error"=>mb_convert_encoding(
                                                $e->getMessage(), 'UTF-8', 'UTF-8'))), $status = 401);
        }
    }
}
