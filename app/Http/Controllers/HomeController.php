<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Stevebauman\Location\Position;
use Jenssegers\Agent\Agent;
use App\VisitorCounter;

class HomeController extends Controller
{
    public function index(Request $request){
        $this->visitCounter($request);
        return view( 'home' );
    }

    public function visitCounter($request){
        $ip= $request->ip();
        $data = \Location::get($ip);
        if($data && null == session('VisitCounted')){
            $agent = new Agent();
            if(!$agent->isRobot()){
                $platform = $agent->platform();
                $platformWithVersion = $platform."-".$agent->version($platform);
                $browser = $agent->browser();
                $browserWithVersion = $browser."-".$agent->version($browser);
                if($agent->isDesktop()){
                    $device = "Desktop";
                }else{
                    $device = $agent->device();
                }
                #$ip= "190.210.107.97";
                $visitor = VisitorCounter::updateOrCreate(
                    ['ip'           => $ip], 
                    ['os'           => $platformWithVersion,
                    'browser'       => $browserWithVersion, 
                    'device'        => $device,
                    'countryCode'   => $data->countryCode,
                    'countryName'   => $data->countryName,
                    'cityName'      => $data->cityName,
                    'zipCode'       => $data->zipCode, 
                    'latitude'      => $data->latitude,
                    'longitude'     => $data->longitude]
                )->countIncrementWithUpdate($ip);
                session(['VisitCounted' => true]);
            }
        }
    }
    
}
