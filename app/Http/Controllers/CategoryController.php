<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias = DB::table('category')
            ->where('enable', 1)
            ->orderBy('category_id', 'asc')
            ->get();
        return $categorias;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categoria = DB::table('categories_entity')
            ->join('categories_attributes', 'categories_entity.id', '=', 'categories_attributes.categories_entity_id')
            ->leftJoin('categories_values', 'categories_attributes.id', '=', 'categories_values.categories_attributes_id')
            ->select('categories_entity.name', 'categories_attributes.attribute', 'categories_values.value')
            ->where([['categories_entity.id',  $id], ['categories_entity.enable', 1]])
            ->get();
        if(count($categoria)>0){
            return $categoria;
        }
        return response($content = json_encode(array("error"=>"La categoria seleccionada no existe")), $status = 400);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
       //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
