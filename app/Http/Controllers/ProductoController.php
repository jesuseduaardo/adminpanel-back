<?php
namespace App\Http\Controllers;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $category)
    {    
        // if(!Cache::has('productos')){
            if(!is_null($category) && !empty($category)){
                //1. Primero obtenemos la categoria segun el nombre
                $categoria = DB::select(
                    DB::raw("SELECT * FROM react_laravel.category WHERE `enable` > 0 AND `name` LIKE '".$category."'")
                );
                //Declaramos un array donde se almacenaran las categorias padre
                $catNum = array();
                //2. Iteramos sobre el resultado para obtener los valores: id, name y category_id
                foreach($categoria as $cat){
                    //Asignamos el id a la variable $category_id
                    $category_id = array($cat->id);
                    array_push($catNum, $cat->id);
                    //Mientras haya una category_id consultaremos a la base de datos por las otras categorias
                    while(count($category_id) > 0){
                        $subCategoria = DB::select(
                            DB::raw("SELECT * FROM react_laravel.category 
                                    WHERE `enable` > 0 AND `category_id`=".$category_id[0])
                        );
                        if(count($subCategoria) > 0){
                            //Iteramos sobre el resultado para obtener los valores: id, name y category_id
                            foreach($subCategoria as $subCat){

                                if($subCat->id > 0){
                                    array_push($catNum, $subCat->id);
                                }
                                $arr = array("id"=>$subCat->id, "name"=>$subCat->name);

                                array_push($category_id, $subCat->id);
                            }
                            array_shift($category_id);
                        
                        }else{
                            array_shift($category_id);
                        }
                    }
                }
            }
            if(isset($category) && $category=="0"){
                $visibility = " and `p`.`visibility` REGEXP '(m|c)'";
            }else{
                $visibility = " and `p`.`visibility` REGEXP '(m)'";
            }
            if(!is_null($category) && (isset($catNum) && count($catNum) > 0)){
                $categoriaQuery=" and `c`.`id` in (".implode(",", $catNum).")";
            }else{
                $categoriaQuery="";
            }
            
            $input = $request->all();
            $pagination = array();
            $paginationQuery = "";

            if(!empty($input) && isset($input["pag"])){
                $totalProducts = DB::table('products')->count();
                $current_page = isset($input["pag"]) && is_numeric($input["pag"]) ? intval($input["pag"]) : 1;
                $num_results_on_page = 20;
                $calc_page = ($current_page - 1) * $num_results_on_page;
                if(ceil($totalProducts / $num_results_on_page) > 0){
                    $pagination["current_page"]    =  $current_page;
                    $pagination["last_page"]       =  intval(ceil($totalProducts / $num_results_on_page));
                    $pagination["next_page"]   =  ($current_page < ceil($totalProducts / $num_results_on_page)) ? intval($current_page)+1 :"";
                    $pagination["prev_page"]   =  (ceil($totalProducts / $num_results_on_page) > 1) ? intval($current_page)-1 :"";
                }
                $paginationQuery=" limit ".$calc_page.", ".$num_results_on_page;
            }
            #rutaImagenesMiniatura
            // $prod['imageThumbPath'] = DB::select(
            //     DB::raw("SELECT `route` FROM `routes` WHERE `name` = 'products_thumbs'")
            // );
            DB::enableQueryLog();
            $prod['products'] = DB::select(
                    DB::raw(
                        "select 
                        #productos
                        `p`.`id`, `p`.`sku`, `p`.`name` as `productName`, `p`.`tax`, `p`.`price`, 
                        `p`.`stock`, `p`.`short_description`, `p`.`visibility`, `p`.`enable`, `p`.`created_at`,
                        #atributos
                        `a`.`weight`, `a`.`height`, `a`.`height`, `a`.`depth`, `a`.`color0`, `a`.`color1`, `a`.`color2`,
                        #categorias
                        `c`.`id` as `cat_id`, `c`.`name` as `categoria`, 
                        #descripcion
                        `d`.`long_description` as `productDescription`,
                        #promciones
                        `e`.`description` as `evento`, `e`.`discount` as `discountPercent`, `e`.`date_from` as `evento_desde`, 
                        `e`.`date_to` as `evento_hasta`,
                        #imagenes
                        `i`.`imagenes` as `productImg`
                        from `products` as `p` 
                        inner join `products_attributes` as `a` on `p`.`id` = `a`.`product_id` 
                        inner join `category` as `c` on `c`.`id` = `p`.`category_id`
                        left join `products_description` as `d` on `p`.`id` = `d`.`product_id` 
                        left join (
                            select `product_id`, GROUP_CONCAT(CONCAT(`name`, '.',`extension`)) as `imagenes` 
                            from `products_images` group by `product_id`
                            ) as `i` on `p`.`id` = `i`.`product_id` 
                        left join `products_events` as `e` on `p`.`id` = `e`.`product_id` 
                        where `p`.`deleted` = 0 and `c`.`enable`=1".$visibility.$categoriaQuery." 
                        order by `p`.`updated_at` desc".$paginationQuery
                    )
                );
            // dd(DB::getQueryLog());
            $prod["pagination"]=$pagination;
            // $expiresAt = Carbon::now()->addMinutes(60);
            // Cache::put('productos', $productos, $expiresAt);
            return $prod;
        // }
        //return Cache::get('productos');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //  if (!\Cache::has('productos')){
        //      $productos = DB::select(
        //     DB::raw(
        //         "select `p`.`id`, `p`.`sku`, `p`.`name` as `productName`, `p`.`tax`, FORMAT(`p`.`price`, 2, 'es_AR') as `price`, `p`.`stock`, `p`.`category_id`, 
        //         `p`.`short_description`, `p`.`visibility`, `p`.`enable`, `p`.`created_at`, `c`.`name` as `categoria`, `a`.`weight`, 
        //         `a`.`height`, `a`.`width`,
        //         `a`.`depth`, `a`.`color0`, `a`.`color1`, `a`.`color2`, `d`.`long_description` as `productDescription`, 
        //         `i`.`imagenes`, `e`.`description` as `evento`, 
        //         `e`.`discount` as `discountPercent`, `e`.`date_from` as `evento_desde`, 
        //         `e`.`date_to` as `evento_hasta`, (SELECT `route` FROM `routes` WHERE `name` = 'products_thumbs') as `thumb_route`
        //         from `products` as `p` 
        //         inner join `products_attributes` as `a` on `p`.`id` = `a`.`product_id` 
        //         left join `products_description` as `d` on `p`.`id` = `d`.`product_id` 
        //         left join `category` as `c` on `c`.`id` = `p`.`category_id` 
        //         left join (select `product_id`, GROUP_CONCAT(CONCAT(`name`, '.',`extension`)) as `imagenes` from `products_images` group by `product_id`) as `i` on `p`.`id` = `i`.`product_id`
        //         left join (select * from `products_events` order by `id` desc limit 1) as `e` on `p`.`id` = `e`.`product_id` 
        //         where `p`.`deleted` = 0 and `c`.`enable`=1;"
        //     )
        // );
        //     $expiresAt = Carbon::now()->addMinutes(60);
        //     \Cache::put('productos', $productos, $expiresAt);
        // }
        // return \Cache::get('productos');

        $productos = DB::select(
            DB::raw(
                "select `p`.`id`, `p`.`sku`, `p`.`name` as `productName`, `p`.`tax`, `p`.`price`, `p`.`stock`, `p`.`category_id`, 
                `p`.`short_description`, `p`.`visibility`, `p`.`enable`, `p`.`created_at`, `c`.`name` as `categoria`, `a`.`weight`, 
                `a`.`height`, `a`.`width`,
                `a`.`depth`, `a`.`color0`, `a`.`color1`, `a`.`color2`, `d`.`long_description` as `productDescription`, 
                `i`.`imagenes` as `productImg`, `e`.`description` as `evento`, 
                `e`.`discount` as `discountPercent`, `e`.`date_from` as `evento_desde`, 
                `e`.`date_to` as `evento_hasta`, (SELECT `route` FROM `routes` WHERE `name` = 'products_thumbs') as `thumb_route`
                from `products` as `p` 
                inner join `products_attributes` as `a` on `p`.`id` = `a`.`product_id` 
                left join `products_description` as `d` on `p`.`id` = `d`.`product_id` 
                left join `category` as `c` on `c`.`id` = `p`.`category_id` 
                left join (select `product_id`, GROUP_CONCAT(CONCAT(`name`, '.',`extension`)) as `imagenes` from `products_images` group by `product_id`) as `i` on `p`.`id` = `i`.`product_id`
                left join (select * from `products_events` order by `updated_at` desc) as `e` on `p`.`id` = `e`.`product_id` 
                where `p`.`deleted` = 0 and `c`.`enable`=1 and `p`.`sku` ='".$id."'"
            )
        );

    // foreach($productos as $producto){
    //     $imagesString = $producto->productImg;
    //     $thumbImagePath = "/".$producto->thumb_route;

    //     $images = explode(",", $imagesString);

    //     $productImg = array();

    //     foreach ($images as $img) {
    //         array_push($productImg, array( "original" => $img, "thumbnail" => $thumbImagePath.$img));
    //     }

    //     $producto->productImg =  $productImg;
    // }
    return $productos;

    }

    public function search($keyword){
        $result = DB::select(
            DB::raw(
            "select `p`.`id`, `p`.`sku`, `p`.`name` as `productName`, `p`.`tax`, `p`.`price`, 
            `p`.`stock`, `p`.`category_id`, `p`.`short_description`, `p`.`visibility`, 
            `p`.`enable`, `p`.`created_at`, `c`.`id` as `cat_id`, `c`.`name` as `categoria`, `a`.*, 
            `d`.`long_description` as `productDescription`, `i`.`imagenes` as `productImg`, 
            `e`.`description` as `evento`, `e`.`discount` as `discountPercent`, `e`.`date_from` as `evento_desde`, 
            `e`.`date_to` as `evento_hasta`, 
            (SELECT `route` FROM `routes` WHERE `name` = 'products_thumbs') as `thumb_route`
            from `products` as `p` 
            inner join `products_attributes` as `a` on `p`.`id` = `a`.`product_id` 
            left join `products_description` as `d` on `p`.`id` = `d`.`product_id` 
            left join `category` as `c` on `c`.`id` = `p`.`category_id` 
            left join (select `product_id`, GROUP_CONCAT(CONCAT(`name`, '.',`extension`)) as `imagenes` 
            from `products_images` group by `product_id`) as `i` on `p`.`id` = `i`.`product_id`
            left join (select * from `products_events` order by `updated_at` desc) as `e` on `p`.`id` = `e`.`product_id` 
            where `p`.`deleted` = 0 and `c`.`enable`=1 and `p`.`name` LIKE '%".$keyword."%' order by `p`.`updated_at` desc"
            )
        );
        $prod['products'] = $result;
        return $prod;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
       //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
