<?php

namespace App\Http\Controllers;

use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\User;

class CheckoutController extends Controller
{
    public function index(Request $request){
        $userID = $this->checkUserExist($request->personalData);
        if(null == $userID){
            $userID = $this->preRegister($request);
        }
        $nroOrder = $this->neworder($request, $userID);
        $pay = $this->getPayMethod($request->paymethod);
        if($pay->id == 0 || $pay->payment_method == "cash"){
            return $this->success($nroOrder);
        }
        return $this->failure($request);

    }

    public function success($nroOrder){
        $data["order"] = $nroOrder;
        $data["status"]="200";
        $data["message"]="Compra realizada con exito!";
        return $data;
    }

    public function failure(Request $request){
        $data["open"]=true;
        $data["status"]="400";
        $data["message"]="No se pudo realizar la compra";
        return $data;
    }

    public function neworder(Request $request, $userID){
        $data = $request->json()->all();
        try {
            $orderId = DB::table('orders_number')->insertGetId([
                "created_at"=> now(), "updated_at"=> now()
                ]);
            if(is_null($orderId)){
                throw new \Exception("Hubo un error al crear la orden", 1);
            }
            $order = DB::table('orders')->insertGetId([
                'order_id' => $orderId,
                'users_id' => $userID,
                'orders_status_code' => 8,
                'purchasedFrom'=>'site',
                'IP'=>$request->ip(),
                'order_date' => now(),
                "created_at"=> now(), "updated_at"=> now()
                ]);
            if($order){
                $subtotal = 0;
                $impuesto = 0;
                $descuento = 0;
                foreach ($request->sumary as $key => $value) {
                    $qty = $value['cantidad'];
                    $price = $value['price'];
                    $tax = ($price * $value['tax'])/100;
                    $discount = (($price+$tax) * $value['discountPercent'])/100;
                   
                    $subtotal += $qty * $price;
                    $impuesto += $qty * $tax;
                    $descuento+= $qty * $discount;

                    DB::table('orders_items')->insert([
                        'product_id' => $value['id'],
                        'order_id' => $orderId,
                        'product_qty' => $qty,   
                        'product_price' => $price,
                        'product_tax' => $tax,
                        'product_discount' => $discount,
                        "created_at"=> now(), "updated_at"=> now()
                        ]);
                }
                if(isset($request->shippingData) && count($request->shippingData)>0){
                    $shipId = DB::table('orders_shipping')->insertGetId([
                        'order_id' => $orderId,
                        'shipping_id' => $request->shippingData['id'],
                        'ship_weight' => $request->shippingData['pesoTotal'],
                        'ship_charge' => $request->shippingData['cargoEnvio'],
                        'ship_tracking_number' => $request->shippingData['trackingNumber'],
                        'delivery_time' => $request->shippingData['tiempoEntrega'],
                        "created_at"=> now(), "updated_at"=> now()
                    ]);
                }
                if(isset($request->paymethod)){
                    $pay = $this->getPayMethod($request->paymethod);
                    if(is_null($pay)){
                        $payment_charge = 0;
                        $payment_discount = 0;
                    }else{
                        $payment_charge = $pay->payment_charge;
                        $payment_discount = $pay->payment_discount;
                    }
                    DB::table('orders_payment')->insert([
                        'order_id' => $orderId,
                        'payment_method_id' => $request->paymethod,
                        'payment_amount' => ($subtotal+$impuesto)-$descuento,
                        'payment_charge' => $payment_charge,
                        'payment_discount' => $payment_discount,
                        'verification_time' => null,
                        "created_at"=> now(), "updated_at"=> now()
                    ]);
                    // DB::table('orders_status_history')->insert([
                    //     'order_id' => $orderId,
                    //     'user_id' => $userID,
                    //     'orders_status_code' => 8,
                    //     'comentarios' => "",
                    //     "created_at"=> now(), "updated_at"=> now()
                    // ]);
                }
                return $orderId;
            }
        } catch (\Exception $e) {
            return response($content = json_encode(
                                        array(
                                            "error"=>mb_convert_encoding(
                                                $e->getMessage(), 'UTF-8', 'UTF-8'))), $status = 401);
        }
    }

    public function checkUserExist($request){
        $user = User::where('email', '=', $request["email"])->first();
        if($user == null){
            return null;
        }
        return $user->id;
    }

    public function getPayMethod($payMethod){
        $pay = DB::table('payment')->where('id', $payMethod)->first();
        return $pay;
    }

    public function preRegister($request){
        $input['name'] = $request->personalData["nombre"];
        $input['email'] = $request->personalData["email"];
        $input['telephone'] = $request->personalData["codigo"]."-".$request->personalData["telefono"]; 
        $user = User::create($input);
        DB::table('user_info')->insert([
            'user_id' => $user->id,
            'first_name' => $request->personalData["nombre"],
            'last_name' => $request->personalData["apellido"],
            'document_type' => $request->personalData["tipoDocumento"],
            'document_number' => $request->personalData["documento"],
            "created_at"=> now(), "updated_at"=> now()
            ]);
        if(!$request->pickupinstore){
            DB::table('user_info_address')->insert([
                'user_id' => $user->id,
                'address' => $request->addressData["direccion"],
                'country' => $request->addressData["pais"],
                'state' => $request->addressData["estado"],
                'city' => $request->addressData["ciudad"],
                'zip_code' => $request->addressData["zip"],
                'is_billing_address'=> $request->addressAsFiscal,
                "created_at"=> now(), "updated_at"=> now()
            ]);
            if(!$request->addressAsFiscal){
                DB::table('user_info_address')->insert([
                    'user_id' => $user->id,
                    'address' => $request->fiscalData["direccion"],
                    'country' => $request->fiscalData["pais"],
                    'state' => $request->fiscalData["estado"],
                    'city' => $request->fiscalData["ciudad"],
                    'zip_code' => $request->fiscalData["zip"],
                    "created_at"=> now(), "updated_at"=> now()
                ]);
            }
        }
        return $user->id; 
    }
}