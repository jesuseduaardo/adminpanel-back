<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\User;

class DetalleCompraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data["order"] = DB::select(
            DB::raw("select concat('#', lpad(`o`.`order_id`, 6, 0)) as `order_number`, `o`.`users_id`, 
            DATE_FORMAT(`o`.`order_date`, '%d-%m-%Y %H:%i') as `order_date`,
            concat(`uia`.`address`,' ', `uia`.`city`,' (', `uia`.`zip_code`,'), ', `uia`.`state`,'. ', `uia`.`country`) as `direccion`,  
            `ui`.`first_name`, `ui`.`last_name`, `ui`.`document_type`, `ui`.`document_number`,
            IF(`op`.`payment_method_id`= 0, 'acordar_con_vendedor',  
            (SELECT `payment_description`  FROM `payment` WHERE `id` = `op`.`payment_method_id`)) as `payment_method`, 
            `op`.`payment_amount`, `op`.`payment_charge`, `op`.`payment_discount`, 
            (SELECT `ship_method` FROM `shipping` WHERE `id` = `os`.`shipping_id`) as `shippment_method`, 
            `os`.`ship_charge` 
            from `orders` as `o` 
            left join `orders_payment` as `op` on `op`.`order_id` = `o`.`order_id` 
            left join `orders_shipping` as `os` on `os`.`order_id` = `o`.`order_id` 
            left join `user_info_address` as `uia` on `o`.`users_id` = `uia`.`user_id` 
            left join `user_info` as `ui` on `o`.`users_id` = `ui`.`user_id` 
            where `o`.`order_id` = $id and `uia`.`is_billing_address` = 0" )
                );
        $data["products"] = DB::select(
                DB::raw("select `oi`.`product_qty`, `oi`.`product_price`, `oi`.`product_tax`,
                `oi`.`product_discount`, `p`.`sku`, `p`.`name`, `p`.`short_description`, `pi`.`image` 
                from `orders_items` as `oi` 
                left join (select `product_id`, concat(`name`,'.', `extension`) as `image` from `products_images` GROUP BY(`product_id`)) as `pi` on `pi`.`product_id` = `oi`.`product_id` 
                left join `products` as `p` on `p`.`id` = `oi`.`product_id` 
                where `oi`.`order_id` = $id")
                );
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function downloadOrder($id){
        $data = $this->show($id);
        $subtotal=0;
        $impuesto=0;
        $envio=0;
        $descuento=0;
        $total=0;
        foreach ($data['products'] as $key => $product) {
            $subtotal += $product->product_price * $product->product_qty;
            $impuesto += $product->product_tax * $product->product_qty;
            $descuento += $product->product_discount * $product->product_qty;
        }
        $data['amounts']['subtotal']    = number_format($subtotal, 2, ',', '.');
        $data['amounts']['impuesto']    = number_format($impuesto, 2, ',', '.');
        $data['amounts']['descuento']   = number_format($descuento, 2, ',', '.');
        $data['amounts']['total']       = number_format(($subtotal + $impuesto) - $descuento, 2, ',', '.');
        $pdf = \PDF::loadView('orders/order', $data);
     
        return $pdf->download('archivo.pdf');
    }
}
