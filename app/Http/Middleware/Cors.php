<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        header('Access-Control-Allow-Origin: *');
        $headers = [
            "Access-Control-Allow-Methods"=>"POST, GET, OPTIONS, PUT, DELETE",
            "Access-Control-Allow-Headers"=>"Origin, X-Requested-With, X-Auth-Token, Content-Type, Content-Disposition, Accept, Key, Authorization"
        ];
        if($request->getMethod()=="OPTIONS"){
            return response()->json("Ok", 200, $headers);
        }
        $response = $next($request);
        foreach ($headers as $key => $value) {
            $response->header($key, $value);
        } 
        return $response;
    }
}
