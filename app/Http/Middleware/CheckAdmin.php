<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;


class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd(Auth::guard('api')->user());
        if (\Auth::guard('api')->user() && \Auth::guard('api')->user()->admin == 1) {
            return $next($request);
        }
        return response($content = json_encode(array("error"=>"Debe estar logueado")), $status = 400);

        //return redirect('/home'); // If user is not an admin.
    }
}
