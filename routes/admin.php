<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get( '/{path?}', function(){
//     return view( 'welcome' );
// } )->where('path', '.*');



Route::get('validate-token', function () {
	return ['success' => 'valid'];
})->middleware('auth:api');
Route::post('login', 'Api\UserController@login');
Route::post('register', 'Api\UserController@register');
Route::get('signup/activate/{token}', 'Api\p@signupActivate');
Route::get('frasediaria', 'Api\FraseController@index');
Route::group(['middleware' => 'auth:api'], function(){
			Route::post('details', 'Api\UserController@details');
			Route::get('logout', 'Api\UserController@logout');
});

Route::group([    
    'namespace' => 'Auth',    
    'middleware' => 'api',    
    'prefix' => 'password'
	], function () {    
	    Route::post('create', 'PasswordResetController@create');
	    Route::get('find/{token}', 'PasswordResetController@find');
		Route::post('reset', 'PasswordResetController@reset');
});

Route::group([  
    'middleware' => ['auth:api','api','check.admin', 'cors'],    
    'prefix' => 'admin'
	], function () {
	Route::post('details', 'Api\UserController@details');
	Route::post('password/create', 'PasswordResetController@create');
	Route::get('password/find/{token}', 'PasswordResetController@find');
	Route::post('password/reset', 'PasswordResetController@reset');
	Route::get('logout', 'Api\UserController@logout');
	Route::resource('productos', 'Api\ProductosController');
	Route::resource('ordenes', 'Api\OrdersController');
	Route::resource('ordenStatus', 'Api\OrderStatusController');
	Route::resource('categorias', 'Api\CategoriesController');
});


Route::get('{reactRoutes}', function () {
    return view('welcome'); // your start view
})->where('reactRoutes', '^((?!api).)*$'); // except 'api' word*/