<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::group([  
    'middleware' => ['api']
	], function () {
    Route::get('productos/{categoria}', 'ProductosController@index');
    Route::get('productos/search/{key}', 'ProductosController@search');
    Route::get('producto/{id}', 'ProductosController@show');
    Route::post('checkout', 'CheckoutController@index');
    Route::get('checkout/success/{data}', 'CheckoutController@success');
    Route::get('checkout/error/{data}', 'CheckoutController@failure');
    Route::get('paymentmethod', 'Paymentcontroller@index');
	// Route::resource('productos', 'ProductosController', ['only' => ['index','show']]);
	Route::get('orden/descarga/{id}', 'DetalleCompraController@downloadOrder');
	Route::resource('detalle', 'DetalleCompraController', ['only' => ['index','show']]);
	Route::resource('categorias', 'CategoryController', ['only' => ['index','show']]);
});

Route::get('{reactRoutes}', function () {
    return view('home'); // your start view
})->where('reactRoutes', '^((?!api).)*$'); // except 'api' word
